//
//  ViewController.swift
//  MSDSkeletons
//
//  Created by Tomas Hofmann on 2019-03-08.
//  Copyright © 2019 Tomas Hofmann. All rights reserved.
//

import UIKit
import MSDTouchLibraries

class SkeletonOverviewViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    private var tableController = MSDTableController()
    
    private let samples: [SkeletonSample] = [
        SkeletonSample(title: "Error indicating input field", description: "Shows an input field with an error message underneath that can animates in and out.", sample: { return ErrorIndicatingInputFieldSampleViewController() }),
        SkeletonSample(title: "Information Bubble", description: "Pop up a little bubble with info", sample: { return InfoBubbleViewController() })
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableController.tableView = self.tableView
        let cellController = TemplatedTableCellController<SkeletonOverviewCell, SkeletonSample>(for: self.tableView, configureBlock: { (cell, content, _) in
            cell.configure(content: content)
        }, actionBlock: { (cell, content) in
            self.navigationController?.pushViewController(content.sample(), animated: true)
        }, willDisplayBlock: nil)
        self.tableController.addSection(with: ArrayFeed(content: samples), cellController: cellController)
    }
}

