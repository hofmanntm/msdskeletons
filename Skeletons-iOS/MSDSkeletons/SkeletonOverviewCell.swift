//
//  SkeletonOverviewCell.swift
//  MSDSkeletons
//
//  Created by Tomas Hofmann on 2019-03-08.
//  Copyright © 2019 Tomas Hofmann. All rights reserved.
//

import UIKit

class SkeletonOverviewCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(content: SkeletonSample) {
        self.titleLabel.text = content.title
        self.descriptionLabel.text = content.description
    }
}
