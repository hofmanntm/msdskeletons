//
//  SkeletonSamples.swift
//  MSDSkeletons
//
//  Created by Tomas Hofmann on 2019-03-11.
//  Copyright © 2019 Tomas Hofmann. All rights reserved.
//

import Foundation
import  UIKit

struct SkeletonSample {
    let title: String
    let description: String
    let sample: () -> UIViewController
}
