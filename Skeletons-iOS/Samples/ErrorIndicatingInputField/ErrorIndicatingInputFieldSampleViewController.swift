//
//  ErrorIndicatingInputFieldSampleViewController.swift
//  MSDSkeletons
//
//  Created by Tomas Hofmann on 2019-03-11.
//  Copyright © 2019 Tomas Hofmann. All rights reserved.
//

import UIKit

class ErrorIndicatingInputFieldSampleViewController: UIViewController {

    @IBOutlet weak var errorIndicatingInputField: ErrorIndicatingInputField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.errorIndicatingInputField.delegate = self
    }
}

extension ErrorIndicatingInputFieldSampleViewController:  ErrorIndicatingInputFieldDelegate {
    func textFieldDidChange(_ inputField: ErrorIndicatingInputField) {
        var showError = false
        if let last = inputField.text?.last, last == "x" {
            showError = true
        }
        inputField.showErrorMessage = showError
    }
}
