//
//  PaddedTextField.swift
//  Grandslam
//
//  Created by Tomas Hofmann on 2019-02-19.
//

import UIKit

@IBDesignable
class PaddedTextField: UITextField {
    private let leadingPadding: CGFloat = 5
    private let topPadding: CGFloat = 5
    private let trailingPadding: CGFloat = 5
    private let bottomPadding: CGFloat = 5
    private lazy var padding: UIEdgeInsets = {
        return UIEdgeInsets(top: topPadding, left: leadingPadding, bottom: bottomPadding, right: trailingPadding)
    }()
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
