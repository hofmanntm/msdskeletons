import UIKit

@IBDesignable
class ErrorIndicatingInputField: NibViewBase {
    
    // Mark: Properties
    let aniamtionFadeInDuration: TimeInterval = 0.35
    let animationFadeOutDuration: TimeInterval = 0.2
    
    override var nibName: String {
        return "ErrorIndicatingInputField"
    }
    
    public weak var delegate: ErrorIndicatingInputFieldDelegate?
    
    @IBInspectable
    public var text: String? {
        get { return self.textField.text }
        set { self.textField.text = text }
    }
    
    @IBInspectable
    public var errorMessage: String? {
        get { return self.errorLabel.text ?? "" }
        set { self.errorLabel.text = newValue }
    }

    @IBInspectable
    public var placeholder: String? { // This lets you set your own placeholder color
        get { return self.textField.placeholder }
        set { self.textField.placeholder = newValue }
    }
    
//    public var placeholder: String? { // This lets you set your own placeholder color
//        get { return self.textField.attributedPlaceholder?.string }
//        set { self.setAttributedPlaceholder(placeholder: newValue) }
//    }
    
    public var showErrorMessage: Bool = false {
        didSet {
            if showErrorMessage != oldValue {
                self.setErrorMessageIsVisible(to: self.showErrorMessage, animated: true)
                self.setNeedsLayout()
            }
        }
    }
    
    public var textContentType: UITextContentType {
        get { return self.textField.textContentType }
        set { self.textField.textContentType = newValue }
    }
    
    public var returnKeyType: UIReturnKeyType {
        get { return self.textField.returnKeyType }
        set { self.textField.returnKeyType = newValue }
    }
    
    public var isSecureTextEntry: Bool {
        get { return self.textField.isSecureTextEntry }
        set { self.textField.isSecureTextEntry = newValue }
    }
    
    open override var canBecomeFirstResponder: Bool {
        get {
            return self.textField.canBecomeFirstResponder
        }
    }
    
    @discardableResult
    open override func becomeFirstResponder() -> Bool {
        return self.textField.becomeFirstResponder()
    }
    
    @discardableResult
    open override func resignFirstResponder() -> Bool {
        return self.textField.resignFirstResponder()
    }
        
    @IBOutlet private(set) weak var textField: UITextField!
    @IBOutlet weak var horizontalRule: UIView!
    @IBOutlet private weak var errorLabel: UILabel!
    @IBOutlet private weak var errorContainer: UIStackView!
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setErrorMessageIsVisible(to: true, animated: false)
        commonSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setErrorMessageIsVisible(to: false, animated: false)
        commonSetup()
    }
    
    private func commonSetup() {
        self.textField.delegate = self
        self.textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.delegate?.textFieldDidChange(self)
    }
    
    // Leaving this here since it shows how to set your own placeholder color
//    private func setAttributedPlaceholder(placeholder: String?) {
//        if let ph = placeholder {
//            self.textField.attributedPlaceholder =
//                NSAttributedString(string: ph, attributes: [NSAttributedString.Key.font : [Your font],
//                                                              NSAttributedString.Key.foregroundColor : [Your color])
//        } else {
//            self.textField.attributedPlaceholder = nil
//        }
//    }

    private func setErrorMessageIsVisible(to show: Bool, animated: Bool) {
        if animated {
            if show {
                UIView.animate(withDuration:self.aniamtionFadeInDuration) {
                    self.errorContainer.alpha = 1.0
                    self.horizontalRule.backgroundColor = self.errorLabel.textColor
                }
            } else {
                UIView.animate(withDuration: self.animationFadeOutDuration) {
                    self.errorContainer.alpha = 0.0
                    self.horizontalRule.backgroundColor = .lightGray
                }
            }
        } else {
            if show {
                self.errorContainer.alpha = 1.0
                self.horizontalRule.backgroundColor = self.errorLabel.textColor
            } else {
                self.errorContainer.alpha = 0.0
                self.horizontalRule.backgroundColor = .lightGray
            }
        }
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self.textField
        } else {
            return super.hitTest(point, with: event)
        }
    }
}

// Mark: - UITextFieldDelegate
extension ErrorIndicatingInputField: UITextFieldDelegate {
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            return delegate.textFieldShouldBeginEditing(self)
        } else {
            return true
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        delegate?.textFieldDidBeginEditing(self)
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            return delegate.textFieldShouldEndEditing(self)
        } else {
            return true
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.resignFirstResponder()
        self.layoutIfNeeded()
        self.delegate?.textFieldDidEndEditing(self)
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let delegate = self.delegate {
            return delegate.textField(self, shouldChangeCharactersIn: range, replacementString: string)
        } else {
            return true
        }
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            return delegate.textFieldShouldClear(self)
        } else {
            return true
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let delegate = self.delegate {
            return delegate.textFieldShouldReturn(self)
        } else {
            return true
        }
    }
}

protocol ErrorIndicatingInputFieldDelegate: class {
    func textFieldShouldBeginEditing(_ inputField: ErrorIndicatingInputField) -> Bool
    func textFieldDidBeginEditing(_ inputField: ErrorIndicatingInputField)
    
    func textFieldShouldEndEditing(_ inputField: ErrorIndicatingInputField) -> Bool
    func textFieldDidEndEditing(_ inputField: ErrorIndicatingInputField)
    
    func textField(_ inputField: ErrorIndicatingInputField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    func textFieldDidChange(_ inputField: ErrorIndicatingInputField)
    
    func textFieldShouldClear(_ inputField: ErrorIndicatingInputField) -> Bool
    func textFieldShouldReturn(_ inputField: ErrorIndicatingInputField) -> Bool
}

extension ErrorIndicatingInputFieldDelegate {
    func textFieldShouldBeginEditing(_ inputField: ErrorIndicatingInputField) -> Bool { return true }
    func textFieldDidBeginEditing(_ inputField: ErrorIndicatingInputField) {}
    
    func textFieldShouldEndEditing(_ inputField: ErrorIndicatingInputField) -> Bool { return true }
    func textFieldDidEndEditing(_ inputField: ErrorIndicatingInputField) {}
    
    func textField(_ inputField: ErrorIndicatingInputField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { return true }
    func textFieldDidChange(_ inputField: ErrorIndicatingInputField) {}
    
    func textFieldShouldClear(_ inputField: ErrorIndicatingInputField) -> Bool { return true }
    func textFieldShouldReturn(_ inputField: ErrorIndicatingInputField) -> Bool { return true }
}
