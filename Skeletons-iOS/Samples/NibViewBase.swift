
import UIKit

// This is a good baseclass for when you're creating nibs you want to be able to use inside another nib/storyboard
class NibViewBase: UIView {

    var nibName: String {
        return "overrideThis"
    }
    private var contentView: UIView?
    
    // Mark: Setup
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.nibInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.nibInit()
    }
    
    private func nibInit() {
        guard let view = self.loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        self.contentView = view
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
}
