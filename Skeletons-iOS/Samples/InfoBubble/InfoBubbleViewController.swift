//
//  InfoBubbleViewController.swift
//  MSDSkeletons
//
//  Created by Tomas Hofmann on 2019-03-11.
//  Copyright © 2019 Tomas Hofmann. All rights reserved.
//

import UIKit

class InfoBubbleViewController: UIViewController {

    @IBOutlet weak var showButton: UIButton!
    @IBOutlet weak var infoBubble: InfoBubble!
    
    @IBAction func showBubbleTapped(_ sender: Any) {
        guard let button = sender as? UIButton else {
            return
        }
        self.showBubble(at: button)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.infoBubble.delegate = self
    }

    private func showBubble(at button: UIButton) {
        let buttonPosition = self.infoBubble.convert(button.bounds, from: button)
        let buttonCenter = (buttonPosition.minX + buttonPosition.maxX) / 2
        self.infoBubble.arrowOffset = .left(offset: buttonCenter)
        self.infoBubble.setNeedsDisplay()
        self.infoBubble.show()
    }
}

extension InfoBubbleViewController: InfoBubbleDelegate {
    func didTapOnInfoBubble(_ bubble: InfoBubble) {
        self.infoBubble.hide()
    }
}
