//
//  InfoBubble.swift
//  
//
//  Created by Tomas Hofmann on 2019-02-28.
//

import UIKit

@IBDesignable
class InfoBubble: UIView {

    // properties
    let arrowHeight: CGFloat = 12
    let arrowWidth: CGFloat = 18
    let cornerRadius: CGFloat = 8
    let fadeInTime: TimeInterval = 0.35
    let fadeOutTime: TimeInterval = 0.25
    
    private let nibName = "InfoBubble"
    private var contentView: UIView?
    
    public weak var delegate: InfoBubbleDelegate?
    
    @IBOutlet private weak var topConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bubbleView: UIView!
    @IBOutlet private weak var contentLabel: UILabel!
    
    @IBAction func didTapOnBubble(_ sender: Any) {
        delegate?.didTapOnInfoBubble(self)
    }
    
    enum ArrowLocation {
        case top
        case bottom
    }
    
    enum ArrowOffset {
        case left(offset: CGFloat)
        case right(offset: CGFloat)
    }
    
    var arrowLocation: ArrowLocation = .top {
        didSet {
            updateBubblePosition()
        }
    }
    var arrowOffset: ArrowOffset = .right(offset: 40)
    private(set) var isVisible = false
    
    @IBInspectable
    var message: String? {
        get { return self.contentLabel.text }
        set { self.contentLabel.text = newValue }
    }
    
    // Mark: Setup
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        self.backgroundColor = .clear
        contentView = view
    }
    
    private func loadViewFromNib() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.commonSetup()
        self.isVisible = true
        self.show()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonSetup()
        self.hide(animated: false)
    }
    
    private func commonSetup() {
        self.updateBubblePosition()
        self.bubbleView.layer.cornerRadius = self.cornerRadius
        self.bubbleView.layer.masksToBounds = true
    }
    
    private func updateBubblePosition() {
        self.topConstraint.constant = self.arrowLocation == .top ? self.arrowHeight : 0
        self.bottomConstraint.constant = self.arrowHeight - self.topConstraint.constant
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        guard let context = UIGraphicsGetCurrentContext() else { return }
        
        var center: CGFloat
        switch self.arrowOffset {
        case let .left(offset):
            center = offset
        case let .right(offset):
            center = rect.maxX - offset
        }
        
        let point: CGFloat = self.arrowLocation == .top ? 0 : rect.maxY
        let base: CGFloat = self.arrowLocation == .top ? point + self.arrowHeight : point - self.arrowHeight
        
        context.beginPath()
        context.move(to: CGPoint(x: center, y: point))
        context.addLine(to: CGPoint(x: center + (self.arrowWidth / 2), y: base))
        context.addLine(to: CGPoint(x: center - (self.arrowWidth / 2), y: base))
        context.closePath()
        
        context.setFillColor(self.bubbleView.backgroundColor!.cgColor)
        context.fillPath()
    }
    
    func show() {
        guard !self.isVisible else {
            return
        }
        self.isVisible = true
        UIView.animate(withDuration: self.fadeOutTime) {
            self.alpha = 1.0
        }
    }
    
    func hide() {
        self.hide(animated: true)
    }
    
    private func hide(animated: Bool) {
        if !animated {
            self.alpha = 0.0
            self.isVisible = false
            return
        }
        guard self.isVisible else {
            return
        }
        self.isVisible = false
        UIView.animate(withDuration: self.fadeOutTime) {
            self.alpha = 0.0
        }
    }
}

protocol InfoBubbleDelegate: class {
    func didTapOnInfoBubble(_ bubble: InfoBubble)
}
