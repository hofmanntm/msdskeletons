# MSDTouchLibraries

Shared iOS code for MindSea projects.

When adding C or Objective-C code, please use the `MSD` prefix for classes/functions and `msd_` for category methods added to types in system frameworks.

When adding Swift code, please don't use the `MSD` or `msd_` prefixes. Swift actually has namespaces!

Some info on what's in MSDTL can be found [in All the dev things](https://docs.google.com/document/d/1mdHg0OidmPjq6q3gdefCL-0YkX1pzSn2FQbOu8_mELo/edit#bookmark=id.j92lmaysd4ez).

## Build utilities

These now live in [MSDBuildTools](https://bitbucket.org/mindsea/msdbuildtools), including build scripts and theme helpers.

## Swift versions

* Swift 4.2 support is on the `master` branch.
* Swift 3 support begins with MSDTL 9.0.
* Swift 2.2 support is on the `swift-2` branch and ends with MSDTL 8.x.

## Updating MSDTouchLibraries

This is a somewhat tedious process with several steps, but it ensures that all of our projects get to benefit from your additions and fixes.

The first step is to clone this repository. You'll make your changes here. It's easy to just start editing the MSDTL files that CocoaPods installed into your app project, but those changes aren't shared with everyone and will disappear the next time you run `pod install`.

Now that you've cloned the MSDTL repo it's time to make your changes. There's a `MSDTouchLibraries.xcworkspace` here if that's useful to you. The more common approach is to install MSDTL as a "development pod" in the project you're actually working on. You do this by editing the project's `Podfile` to point to your cloned copy of MSDTL. For example, you might find the line

```
pod 'MSDTouchLibraries'
```

and change it temporarily to

```
pod 'MSDTouchLibraries', :path => "~/code/msdtouchlibraries"
```

then run `pod install`. You'll see in the `Pods` project in your workspace there's a new "Development Pods" folder. Basically what's happened here is CocoaPods has installed MSDTouchLibraries like always, but instead of checking out its own copy of the files it's pointing at your cloned repo. *Now* you can make changes directly in your project and they'll be reflected in the MSDTL repo.

Go ahead and make your changes. Test them out in your app. Once you're satisfied that they work, you should update the change log in the MDSTL repo (`CHANGELOG.md`). The idea behind the change log is so you can glance down a list of changes and hopefully answer the question "what's going to break when I update this two-year-old project to the latest MSDTL?". Hot tip: you can add a new section titled `## Unreleased` to the change log, and it'll get replaced with the version number when you get to a future step.

All set? Commit your changes to MSDTL. Your commit message might be pretty similar to your change log entry, that's ok.

Once you've committed some changes to MSDTL, you'll want to use those changes in your app without having to use the development pod thing. First, push your changes to MSDTL. Then, run the `tag-and-push` script in the MSDTL repo root. This script will increment the version number, update the podspec, update the change log, and push those changes. Please try to stick to semantic versioning: if there's no changes to public API or changes are purely additive, bump the minor version; otherwise bump the major version.

Nearly there, I promise! Now you need to update our CocoaPods specs repository. This is how CocoaPods figures out how to install libraries. We have our own private specs repo called `mindseaspecs`. You'll need to clone that repo. In that repo, look for the folder `Specs/MSDTouchLibraries`. Add a new folder for your new version. Copy `MSDTouchLibraries.podspec` from the MSDTL repo into the new folder. Then commit and push your change to `mindseaspecs`.

Finally it's time to get the updated MSDTL version into your app project. If you did the development pod thing, now is when you undo it back to just

```
pod 'MSDTouchLibraries'
```

Now, in your app project, run `pod update MSDTouchLibraries`. You should see CocoaPods update MSDTL to the version you just pushed.

Congrats, you're done! It gets easier once you're familiar with all the steps.
