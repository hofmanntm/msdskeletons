//
//  MSDSimplePurchaseProcessor.h
//
//  Created by John Arnold on 2013-04-04.
//
//

#import "MSDPurchaseProduct.h"

NS_ASSUME_NONNULL_BEGIN

// Notification for debug free purchase unlock
extern NSNotificationName const MSDPurchaseCompletedDebugFreeUnlockNotification;


NS_SWIFT_NAME(SimplePurchaseProcessor)
@interface MSDSimplePurchaseProcessor : NSObject <MSDPurchaseProcessor>

@property (class, readonly, nonatomic) MSDSimplePurchaseProcessor *sharedPurchaseProcessor;

@property (nonatomic, strong, readonly) MSDPurchaseManager *purchaseManager;

// Known products are ones loaded from a local resource or plist.
@property (nonatomic, copy, readonly) NSDictionary *knownProducts; // Store Product ID => MSDPurchaseProduct

// Valid products are entries from knownProducts that have been confirmed as present from StoreKit.
@property (nonatomic, copy, nullable, readonly) NSDictionary *validProducts; // Store Product ID => MSDPurchaseProduct

@property (nonatomic, strong, nullable, readonly) NSError *lastProductInfoRequestError;
@property (nonatomic, strong, nullable, readonly) NSError *lastTransactionRequestError;

- (void)loadProducts;

// Convenience methods
- (BOOL)isProductIdentifierPurchased:(NSString*)storeProductID;

// Debug methods
#if defined(DEBUG) || defined(MSD_DEBUG_FREE_IN_APP_PURCHASE)
- (void)debugPurchaseProductForFree:(MSDPurchaseProduct *)product;
- (void)debugResetPurchases;
#endif

@end

NS_ASSUME_NONNULL_END
