//
//  MSDPurchaseManager.m
//
//  Created by Mike Burke on 11-04-04.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDPurchaseManager.h"

#import "MSDLogger.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

NSString *const MSDPurchaseProductsRequestDidBeginNotification = @"MSDPurchaseProductsRequestDidBeginNotification";
NSString *const MSDPurchaseProductsRequestDidCompleteNotification = @"MSDPurchaseProductsRequestDidCompleteNotification";
NSString *const MSDPurchaseProductsRequestDidFailNotification = @"MSDPurchaseProductsRequestDidFailNotification";
NSString *const MSDPurchaseNotificationProductsResponseKey = @"MSDPurchaseNotificationProductsResponseKey";
NSString *const MSDPurchaseNotificationLoadedProductsKey = @"MSDPurchaseNotificationLoadedProductsKey";
NSString *const MSDPurchaseNotificationRequestErrorKey = @"MSDPurchaseNotificationRequestErrorKey";

NSString *const MSDPurchasePaymentTransactionDidBeginNotification = @"MSDPurchasePaymentTransactionDidBeginNotification";
NSString *const MSDPurchasePaymentTransactionDidCompleteNotification = @"MSDPurchasePaymentTransactionDidCompleteNotification";
NSString *const MSDPurchasePaymentTransactionDidFailNotification = @"MSDPurchasePaymentTransactionDidFailNotification";
NSString *const MSDPurchasePaymentTransactionDidRestoreNotification = @"MSDPurchasePaymentTransactionDidRestoreNotification";
NSString *const MSDPurchaseNotificationPaymentTransactionKey = @"MSDPurchaseNotificationPaymentTransactionKey";
NSString *const MSDPurchaseNotificationPaymentKey = @"MSDPurchaseNotificationPaymentKey";

NSString *const MSDPurchaseCompletedTransactionsRestoreDidBeginNotification = @"MSDPurchaseCompletedTransactionsRestoreDidBeginNotification";
NSString *const MSDPurchaseCompletedTransactionsRestoreDidCompleteNotification = @"MSDPurchaseCompletedTransactionsRestoreDidCompleteNotification";
NSString *const MSDPurchaseCompletedTransactionsRestoreDidFailNotification = @"MSDPurchaseCompletedTransactionsRestoreDidFailNotification";

@interface MSDPurchaseManager () <SKPaymentTransactionObserver, SKProductsRequestDelegate, SKRequestDelegate>

@property (nonatomic, copy, nullable) NSDictionary *productsInfo;
@property (nonatomic, strong) NSNotificationCenter *notificationCenter;
@property (nonatomic, strong) SKPaymentQueue *paymentQueue;
@property (nonatomic, strong, nullable) SKProductsRequest *productsRequest;
@property (nonatomic, assign, getter=isRestoringCompletedTransactions) BOOL restoringCompletedTransactions;

@end


@implementation MSDPurchaseManager {
    id<MSDPurchaseProcessor> _processor;
}

- (instancetype)initWithProcessor:(id<MSDPurchaseProcessor>)aProcessor {
    if ((self = [super init])) {
        _processor = aProcessor;
        self.paymentQueue = [SKPaymentQueue defaultQueue];
        [self.paymentQueue addTransactionObserver:self];
        self.notificationCenter = [NSNotificationCenter defaultCenter];
    }
    return self;
}

- (nullable SKProduct*)productForIdentifier:(NSString*)productID {
    SKProduct *product = self.productsInfo[productID];
    return product;
}

#pragma mark - Actions

- (void)loadProductInfoForIdentifiers:(NSSet *)productIdentifiers {
    MSDCheck(!self.productsRequest, @"Product request already in progress");
    //MSDDebugLog(@"Requesting product info");

    self.productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
    self.productsRequest.delegate = self;
    [self.productsRequest start];

    [self.notificationCenter postNotificationName:MSDPurchaseProductsRequestDidBeginNotification
                                           object:self
                                         userInfo:[NSDictionary dictionary]];
}

- (void)restoreCompletedTransactions {
    if (self.restoringCompletedTransactions) {
        return;
    }
    self.restoringCompletedTransactions = YES;
    [self.paymentQueue restoreCompletedTransactions];
    
    [self.notificationCenter postNotificationName:MSDPurchaseCompletedTransactionsRestoreDidBeginNotification
                                           object:self
                                         userInfo:nil];
}

- (SKPayment *)purchaseProduct:(SKProduct *)product {
    SKPayment *payment = [SKPayment paymentWithProduct:product];
    [self.paymentQueue addPayment:payment];
    
    [self.notificationCenter postNotificationName:MSDPurchasePaymentTransactionDidBeginNotification
                                           object:self
                                         userInfo:[NSDictionary dictionaryWithObject:payment
                                                                              forKey:MSDPurchaseNotificationPaymentKey]];
    return payment;
}

#pragma mark - StoreKit protocol implementation

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    MSDDebugLog(@"Begin update for %@ transactions; identifiers = %@", @([transactions count]), [transactions valueForKey:@"transactionIdentifier"]);
    if ([_processor respondsToSelector:@selector(purchaseManagerWillBeginTransactionUpdates:)]) {
        [_processor purchaseManagerWillBeginTransactionUpdates:self];
    }
    
    for (SKPaymentTransaction *transaction in transactions) {
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchasing: {
                MSDDebugLog(@"Transaction entered 'purchasing' state; identifier = %@", transaction.transactionIdentifier);
            } break;

            case SKPaymentTransactionStateDeferred: {
                // nop; transaction will show up again with new state once it goes through (or gets rejected)
            } break;

            case SKPaymentTransactionStatePurchased: {
                MSDDebugLog(@"Transaction %@ for product %@ purchased", transaction.transactionIdentifier, transaction.payment.productIdentifier);
                
                MSDPurchaseManager __weak *weak_self = self;
                MSDPurchaseCompletionBlock completionBlock = ^(void){
                    MSDPurchaseManager __strong *self = weak_self;
                    MSDCheck(self, @"MSDPurchaseManager instance was nil for completion callback.  This indicates that the purchase manager was deallocated before finishing the transaction.");
                    MSDDebugLog(@"Finish purchased transaction %@ and fire notification callbacks", transaction.transactionIdentifier);
                    [queue finishTransaction:transaction];
                    [self firePurchaseCompleteCallbacksForPayment:transaction];
                };
                
                if ([_processor respondsToSelector:@selector(purchaseManager:transactionDidComplete:completionBlock:)]) {
                    [_processor purchaseManager:self transactionDidComplete:transaction completionBlock:completionBlock];
                } else {
                    [_processor purchaseManager:self transactionDidComplete:transaction];
                    completionBlock();
                }
            } break;

            case SKPaymentTransactionStateFailed: {
                MSDDebugLog(@"Transaction %@ for product %@ failed", transaction.transactionIdentifier, transaction.payment.productIdentifier);
                
                MSDPurchaseManager __weak *weak_self = self;
                MSDPurchaseCompletionBlock completionBlock = ^(void){
                    MSDPurchaseManager __strong *self = weak_self;
                    MSDCheck(self, @"MSDPurchaseManager instance was nil for completion callback.  This indicates that the purchase manager was deallocated before finishing the transaction.");
                    MSDDebugLog(@"Finish failed transaction %@ and fire notification callbacks", transaction.transactionIdentifier);
                    [queue finishTransaction:transaction];
                    [self firePurchaseFailedCallbacksForPayment:transaction];
                };
                
                if ([_processor respondsToSelector:@selector(purchaseManager:transactionDidFail:completionBlock:)]) {
                    [_processor purchaseManager:self transactionDidFail:transaction completionBlock:completionBlock];
                } else {
                    [_processor purchaseManager:self transactionDidFail:transaction];
                    completionBlock();
                }
            } break;

            case SKPaymentTransactionStateRestored: {
                MSDDebugLog(@"Transaction %@ for product %@ restored", transaction.transactionIdentifier, transaction.payment.productIdentifier);

                MSDPurchaseManager __weak *weak_self = self;
                MSDPurchaseCompletionBlock completionBlock = ^(void){
                    MSDPurchaseManager __strong *self = weak_self;
                    MSDCheck(self, @"MSDPurchaseManager instance was nil for completion callback.  This indicates that the purchase manager was deallocated before finishing the transaction.");
                    MSDDebugLog(@"Finish restored transaction %@ and fire notification callbacks", transaction.transactionIdentifier);
                    [queue finishTransaction:transaction];
                    [self firePurchaseRestoredCallbacksForPayment:transaction];
                };
                
                if ([_processor respondsToSelector:@selector(purchaseManager:transactionDidRestore:completionBlock:)]) {
                    [_processor purchaseManager:self transactionDidRestore:transaction completionBlock:completionBlock];
                } else {
                    [_processor purchaseManager:self transactionDidRestore:transaction];
                    completionBlock();
                }

            } break;
        }
    }
    if ([_processor respondsToSelector:@selector(purchaseManagerDidEndTransactionUpdates:)]) {
        [_processor purchaseManagerDidEndTransactionUpdates:self];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    self.restoringCompletedTransactions = NO;
    if ([_processor respondsToSelector:@selector(purchaseManager:restoreCompletedTransactionsFailedWithError:)]) {
        [_processor purchaseManager:self restoreCompletedTransactionsFailedWithError:error];
    }
    
    [self.notificationCenter postNotificationName:MSDPurchaseCompletedTransactionsRestoreDidFailNotification
                                           object:self
                                         userInfo:[NSDictionary dictionaryWithObject:error
                                                                              forKey:MSDPurchaseNotificationRequestErrorKey]];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    self.restoringCompletedTransactions = NO;
    
    if ([_processor respondsToSelector:@selector(purchaseManagerRestoreCompletedTransactionsFinished:)]) {
        [_processor purchaseManagerRestoreCompletedTransactionsFinished:self];
    }

    [self.notificationCenter postNotificationName:MSDPurchaseCompletedTransactionsRestoreDidCompleteNotification
                                           object:self
                                         userInfo:nil];
}

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    self.productsRequest.delegate = nil;
    self.productsRequest = nil;

    NSMutableDictionary *productInfoMap = [self.productsInfo mutableCopy] ?: [NSMutableDictionary dictionary];
    
    for (SKProduct *product in response.products) {
        [productInfoMap setObject:product forKey:product.productIdentifier];
    }
    
    self.productsInfo = productInfoMap;
    
    [_processor purchaseManager:self productsRequestDidComplete:response products:self.productsInfo];
    
    [self.notificationCenter postNotificationName:MSDPurchaseProductsRequestDidCompleteNotification
                                           object:self
                                         userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                   response, MSDPurchaseNotificationProductsResponseKey,
                                                   response.products, MSDPurchaseNotificationLoadedProductsKey,
                                                   nil]];
}

- (void)requestDidFinish:(SKRequest *)request {
    if (request == self.productsRequest) {
        self.productsRequest.delegate = nil;
        self.productsRequest = nil;
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    if (request == self.productsRequest) {
        self.productsRequest.delegate = nil;
        self.productsRequest = nil;
        
        [_processor purchaseManager:self productsRequestDidFail:error];
        
        [self.notificationCenter postNotificationName:MSDPurchaseProductsRequestDidFailNotification
                                               object:self
                                             userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       error, MSDPurchaseNotificationRequestErrorKey,
                                                       nil]];
    }
}

- (BOOL)isLoadingProductInfo {
    return self.productsRequest != nil;
}

#pragma mark - Purchase notification firing

- (void)firePurchaseCompleteCallbacksForPayment:(SKPaymentTransaction *)transaction {
    [self.notificationCenter postNotificationName:MSDPurchasePaymentTransactionDidCompleteNotification
                                           object:self
                                         userInfo:[NSDictionary dictionaryWithObject:transaction
                                                                              forKey:MSDPurchaseNotificationPaymentTransactionKey]];
}

- (void)firePurchaseFailedCallbacksForPayment:(SKPaymentTransaction *)transaction {
    [self.notificationCenter postNotificationName:MSDPurchasePaymentTransactionDidFailNotification
                                           object:self
                                         userInfo:[NSDictionary dictionaryWithObject:transaction
                                                                              forKey:MSDPurchaseNotificationPaymentTransactionKey]];
}

- (void)firePurchaseRestoredCallbacksForPayment:(SKPaymentTransaction *)transaction {
    [self.notificationCenter postNotificationName:MSDPurchasePaymentTransactionDidRestoreNotification
                                           object:self
                                         userInfo:[NSDictionary dictionaryWithObject:transaction
                                                                              forKey:MSDPurchaseNotificationPaymentTransactionKey]];
}

#pragma mark -

- (nullable SKPayment *)paymentInProgressForProductId:(NSString *)productId {
    NSArray *transactions = self.paymentQueue.transactions;
    for(SKPaymentTransaction *transaction in transactions) {
        if ([transaction.payment.productIdentifier isEqualToString:productId] && 
            transaction.transactionState == SKPaymentTransactionStatePurchasing) {
            return transaction.payment;
        }
    }
    return nil;
}

- (void)dealloc {
    [self.paymentQueue removeTransactionObserver:self];
    
    self.productsRequest.delegate = nil;
    
    
}

@end

NS_ASSUME_NONNULL_END
