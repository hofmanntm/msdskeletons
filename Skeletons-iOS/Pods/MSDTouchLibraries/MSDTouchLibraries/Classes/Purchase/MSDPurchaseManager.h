//
//  MSDPurchaseManager.h
//
//  Created by Mike Burke on 11-04-04.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol MSDPurchaseProcessor;

NS_ASSUME_NONNULL_BEGIN

// The notifications will be posted after calling the appropriate methods on the assigned MSDPurchaseProcessor instance.
// Perform any important processing in the MSDPurchaseProcessor, such as saving information about the purchase;
// then, the notifications will be fired to allow view controllers (etc) a chance to update their UI to reflect the new state.

// Shared notification keys
// Error key, included on MSDPurchaseProductsRequestDidFailNotification and MSDPurchaseCompletedTransactionsRestoreDidFailNotification
// (but not MSDPurchasePaymentTransactionDidFailNotification, where the error can be collected from the PaymentTransaction object)
extern NSString *const MSDPurchaseNotificationRequestErrorKey;

// Notification for product info request completion
extern NSNotificationName const MSDPurchaseProductsRequestDidBeginNotification;
extern NSNotificationName const MSDPurchaseProductsRequestDidCompleteNotification;
extern NSNotificationName const MSDPurchaseProductsRequestDidFailNotification;
// User info keys:
// Key for SKProductsResponse returned by request
extern NSString *const MSDPurchaseNotificationProductsResponseKey;
// Key containing the loaded products as an NSArray (see also the productsInfo property for a complete map of known products)
extern NSString *const MSDPurchaseNotificationLoadedProductsKey;

// Completed-transaction restoration
extern NSNotificationName const MSDPurchaseCompletedTransactionsRestoreDidBeginNotification;
extern NSNotificationName const MSDPurchaseCompletedTransactionsRestoreDidCompleteNotification;
extern NSNotificationName const MSDPurchaseCompletedTransactionsRestoreDidFailNotification;

// SKPaymentTransaction-related notifications
extern NSNotificationName const MSDPurchasePaymentTransactionDidBeginNotification;
extern NSNotificationName const MSDPurchasePaymentTransactionDidCompleteNotification;
extern NSNotificationName const MSDPurchasePaymentTransactionDidFailNotification;
extern NSNotificationName const MSDPurchasePaymentTransactionDidRestoreNotification;
// User info keys:
// Key for the SKPaymentTransaction associated with transaction complete/fail/restore notifications
extern NSString *const MSDPurchaseNotificationPaymentTransactionKey;
// Key for the SKPayment associated with transaction begin notifications
extern NSString *const MSDPurchaseNotificationPaymentKey;

// A generic completion block type used in some PurchaseProcessor callbacks
typedef void (^MSDPurchaseCompletionBlock)(void) NS_SWIFT_NAME(PurchaseCompletionBlock);


NS_SWIFT_NAME(PurchaseManager)
@interface MSDPurchaseManager : NSObject

- (instancetype)initWithProcessor:(id<MSDPurchaseProcessor>)processor NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (void)loadProductInfoForIdentifiers:(NSSet *)productIdentifiers;
- (void)restoreCompletedTransactions;
- (BOOL)isLoadingProductInfo;

- (SKPayment *)purchaseProduct:(SKProduct *)product;
- (nullable SKPayment *)paymentInProgressForProductId:(NSString *)productId;

- (nullable SKProduct*)productForIdentifier:(NSString*)productID;

// This gets populated with a mapping of product IDs to SKProduct objects when a product info request completes.
@property (nonatomic, copy, nullable, readonly) NSDictionary *productsInfo;

@property (nonatomic, strong, readonly) SKPaymentQueue *paymentQueue;
@property (nonatomic, assign, readonly, getter=isRestoringCompletedTransactions) BOOL restoringCompletedTransactions;

@end


NS_SWIFT_NAME(PurchaseProcessor)
@protocol MSDPurchaseProcessor <NSObject>

@required

@property (readonly, nonatomic) MSDPurchaseManager *purchaseManager;
- (void)purchaseManager:(MSDPurchaseManager *)manager productsRequestDidComplete:(SKProductsResponse *)response products:(NSDictionary *)products;
- (void)purchaseManager:(MSDPurchaseManager *)manager productsRequestDidFail:(NSError *)error;

@optional

- (void)purchaseManagerRestoreCompletedTransactionsFinished:(MSDPurchaseManager *)manager;
- (void)purchaseManager:(MSDPurchaseManager *)manager restoreCompletedTransactionsFailedWithError:(NSError *)error;

// Called to inform the processor that a transaction update block is beginning/ending.
// If appropriate, the transactions can be collected and then processed as a batch by purchaseManagerDidEndTransactionUpdates:.
// This can be used, for example, when receiving subscription renewals at launch, so that they can be sent to a remote server for validation in a single request.
- (void)purchaseManagerWillBeginTransactionUpdates:(MSDPurchaseManager *)manager;
- (void)purchaseManagerDidEndTransactionUpdates:(MSDPurchaseManager *)manager;

// At least one method of each pair below must be implemented.
// The completion block MUST be called once the transaction has been registered/saved/etc., to avoid future repeated callbacks for the same transaction.
// When using the non-completion-block methods, MSDPurchaseManager will do this automatically immediately after your method returns.
// Notifications will also be sent upon completion so that eg. observers can update their UI.
// NB. If you are verifying transactions asynchronously, you may want to update your UI using a custom notification that fires after you've performed all the verification.

- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidComplete:(SKPaymentTransaction *)transaction;
- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidComplete:(SKPaymentTransaction *)transaction completionBlock:(MSDPurchaseCompletionBlock)completionBlock;

- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidRestore:(SKPaymentTransaction *)transaction;
- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidRestore:(SKPaymentTransaction *)transaction completionBlock:(MSDPurchaseCompletionBlock)completionBlock;

- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidFail:(SKPaymentTransaction *)transaction;
- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidFail:(SKPaymentTransaction *)transaction completionBlock:(MSDPurchaseCompletionBlock)completionBlock;

@end

NS_ASSUME_NONNULL_END
