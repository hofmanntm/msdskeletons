//
//  MSDPurchaseProduct.h
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-04-04.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "MSDPurchaseManager.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(PurchaseProduct)
@interface MSDPurchaseProduct : NSObject

@property (readonly, copy, nonatomic) NSString *storeProductID;
@property (readonly, copy, nonatomic) NSString *localizedLongDescription;
@property (readonly, copy, nonatomic) NSString *localizedTitle;
@property (readonly, copy, nonatomic) NSString *localizedShortName;

@property (nonatomic, assign) BOOL purchased;

@property (nonatomic, weak, readonly) id<MSDPurchaseProcessor> purchaseProcessor;

- (instancetype)initWithPurchaseProcessor:(id<MSDPurchaseProcessor>)purchaseProcessor dictionary:(NSDictionary *)dictionary NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

/// @return `nil` if the product info has not been loaded.
@property (readonly, nullable, nonatomic) SKProduct *storeProduct;

/// @return `nil` if the product info has not been loaded.
@property (readonly, nullable, nonatomic) NSString *priceInLocalCurrency;

@end

NS_ASSUME_NONNULL_END
