//
//  MSDPurchaseProduct.m
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-04-04.
//
//

#import "MSDPurchaseProduct.h"
#import "MSDKeychainItem.h"
#import "MSDPurchaseManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDPurchaseProduct ()

@property (nonatomic, copy) NSString *storeProductID;
@property (nonatomic, copy) NSString *localizedTitle;
@property (nonatomic, copy) NSString *localizedLongDescription;
@property (nonatomic, copy) NSString *localizedShortName;
@property (nonatomic, copy) NSString *keychainPrefix;
@property (nonatomic, weak) id<MSDPurchaseProcessor> purchaseProcessor;

@end

@implementation MSDPurchaseProduct

- (instancetype)initWithPurchaseProcessor:(id<MSDPurchaseProcessor>)purchaseProcessor dictionary:(NSDictionary *)dictionary {
    
    self = [super init];
    
    if (self) {
        
        self.storeProductID = dictionary[@"ProductID"];
        self.localizedTitle = dictionary[@"LocalizedTitle"];
        self.localizedLongDescription = dictionary[@"LocalizedDescription"];
        self.localizedShortName = dictionary[@"LocalizedShortName"];
        self.purchaseProcessor = purchaseProcessor;
        self.keychainPrefix = [[NSBundle mainBundle] bundleIdentifier];
    }
    
    return self;
}

- (nullable SKProduct*)storeProduct {
    return [[self.purchaseProcessor purchaseManager] productForIdentifier:self.storeProductID];
}

- (nullable NSString*)priceInLocalCurrency {
    SKProduct *product = [self storeProduct];
    
    if (product) {
        static NSNumberFormatter *formatter;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            formatter = [NSNumberFormatter new];
            formatter.formatterBehavior = NSNumberFormatterBehavior10_4;
            formatter.numberStyle = NSNumberFormatterCurrencyStyle;
            formatter.locale = product.priceLocale;
        });
        
        return [formatter stringFromNumber:product.price];
    } else {
        return nil;
    }
}

- (NSString *)localizedTitle {
    SKProduct *product = [self storeProduct];
    
    if (product) {
        return product.localizedTitle;
    } else {
        return _localizedTitle;
    }
}

#pragma mark - Interface to backing store

- (NSString *)keychainItemName {
    return [NSString stringWithFormat:@"%@.%@", self.keychainPrefix, self.storeProductID];
}

- (MSDKeychainItem *)keychainItem {
    return [[MSDKeychainItem alloc] initWithIdentifier:[self keychainItemName] accessGroup:nil];
}

- (BOOL)purchased {
    return [self.keychainItem.encryptedObject boolValue];
}

- (void)setPurchased:(BOOL)purchased {
    self.keychainItem.encryptedObject = @(purchased);
}

@end

NS_ASSUME_NONNULL_END
