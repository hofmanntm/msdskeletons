//
//  MSDSimplePurchaseProcessor.m
//  Acapulco
//
//  Created by John Arnold on 2013-04-04.
//
//

#import "MSDSimplePurchaseProcessor.h"
#import "MSDNotificationBinding.h"
#import "MSDPurchaseProduct.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

NSString *const MSDPurchaseCompletedDebugFreeUnlockNotification = @"MSDPurchaseCompletedDebugFreeUnlockNotification";
static NSString *const ProductListResourceName = @"Products";

@interface MSDSimplePurchaseProcessor () <UIAlertViewDelegate>

@property (nonatomic, strong) MSDPurchaseManager *purchaseManager;
@property (nonatomic, strong) MSDNotificationBinding *notificationBinding;

@property (nonatomic, copy) NSDictionary *knownProducts;
@property (nonatomic, copy, nullable) NSDictionary *validProducts;

@property (nonatomic, strong, nullable) NSError *lastProductInfoRequestError;
@property (nonatomic, strong, nullable) NSError *lastTransactionRequestError;

#if defined(DEBUG) || defined(MSD_DEBUG_FREE_IN_APP_PURCHASE)
@property (nonatomic, strong, nullable) MSDPurchaseProduct *debugFreePurchaseProduct;
#endif

@end

@implementation MSDSimplePurchaseProcessor

MSD_SINGLETON_METHOD(MSDSimplePurchaseProcessor, sharedPurchaseProcessor)

- (instancetype)init {
    if ((self = [super init])) {
        self.purchaseManager = [[MSDPurchaseManager alloc] initWithProcessor:self];
        
        [self loadProducts];
    }
    return self;
}

- (void)loadProducts {
    NSURL *productsURL = [[NSBundle mainBundle] URLForResource:ProductListResourceName withExtension:@"plist"];
    NSDictionary *resources = [NSDictionary dictionaryWithContentsOfURL:productsURL];
    
    NSMutableSet *productIDs = [NSMutableSet set];
    
    NSMutableDictionary *loadedProducts = [NSMutableDictionary dictionary];
    for(NSDictionary *dict in [resources objectForKey:@"Products"]) {
        
        MSDPurchaseProduct *product = [[MSDPurchaseProduct alloc] initWithPurchaseProcessor:self dictionary:dict];
        loadedProducts[product.storeProductID] = product;
        
        [productIDs addObject:product.storeProductID];
    }
    
    self.knownProducts = loadedProducts;
    
    [self.purchaseManager loadProductInfoForIdentifiers:productIDs];
}

- (void)unlockItemsForTransaction:(SKPaymentTransaction *)transaction {
    NSString *productId = transaction.payment.productIdentifier;
    
    MSDPurchaseProduct *product = self.knownProducts[productId];
    product.purchased = YES;
}

- (BOOL)isProductIdentifierPurchased:(NSString*)storeProductID {
    
    MSDPurchaseProduct *product = self.knownProducts[storeProductID];
    return product.purchased;
}

#pragma mark - MSDPurchaseProcessor protocol implementation

- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidComplete:(SKPaymentTransaction *)transaction {
    self.lastTransactionRequestError = nil;
    NSString *productId = transaction.payment.productIdentifier;
    MSDDebugLog(@"Purchased product ID: %@", productId);
    
    [self unlockItemsForTransaction:transaction];
}

- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidRestore:(SKPaymentTransaction *)transaction {
    self.lastTransactionRequestError = nil;
    NSString *productId = transaction.payment.productIdentifier;
    MSDDebugLog(@"Restored product ID: %@", productId);
    
    [self unlockItemsForTransaction:transaction];
}

- (void)purchaseManager:(MSDPurchaseManager *)manager transactionDidFail:(SKPaymentTransaction *)transaction {
    self.lastTransactionRequestError = transaction.error;
    MSDDebugLog(@"Purchase failed for %@ with error: %@", transaction.payment.productIdentifier, transaction.error);
}

- (void)purchaseManager:(MSDPurchaseManager *)manager productsRequestDidComplete:(SKProductsResponse *)response products:(NSDictionary *)products {
    MSDDebugLog(@"Loaded product info");
    self.lastProductInfoRequestError = nil;
    
    NSMutableDictionary *validProducts = [NSMutableDictionary dictionary];
    for (NSString *productID in [products allKeys]) {
        
        validProducts[productID] = self.knownProducts[productID];
    }
    
    self.validProducts = validProducts;
}

- (void)purchaseManager:(MSDPurchaseManager *)manager productsRequestDidFail:(NSError *)error {
    //MSDErrorLog(@"Products request failed: %@", error);
    self.lastProductInfoRequestError = error;
}

#if defined(DEBUG) || defined(MSD_DEBUG_FREE_IN_APP_PURCHASE)
- (void)debugPurchaseProductForFree:(MSDPurchaseProduct *)product {
    self.debugFreePurchaseProduct = product;
    NSString *messageFormat = NSLocalizedString(@"Do you want to test purchase %@ for free?", @"In app purchase test flight alert view message format");
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm your In-App Purchase", @"In app purchase test flight alert view title")
                                message:[NSString stringWithFormat:messageFormat, product.localizedTitle]
                               delegate:self
                      cancelButtonTitle:NSLocalizedString(@"Cancel", @"In app purchase test flight alert view cancel button title")
                      otherButtonTitles:NSLocalizedString(@"Unlock", @"In app purchase test flight alert view conformation button title"), nil] show];
}

- (void)debugResetPurchases {
    for (MSDPurchaseProduct *product in self.knownProducts.allValues) {
        product.purchased = NO;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == alertView.firstOtherButtonIndex) {
        self.debugFreePurchaseProduct.purchased = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:MSDPurchaseCompletedDebugFreeUnlockNotification object:nil];
    }
    
    self.debugFreePurchaseProduct = nil;
}
#endif

@end

NS_ASSUME_NONNULL_END
