//
//  MSDNotificationBinding.m
//  MSDTouchLibraries
//
//  Created by Mike Burke on 12-05-23.
//  Copyright (c) 2012 MindSea Development Inc. All rights reserved.
//

#import "MSDNotificationBinding.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDNotificationBinding()

@property (nonatomic, strong, nullable) NSOperationQueue *queue;
@property (nonatomic, strong) NSNotificationCenter *notificationCenter;
@property (nonatomic, strong) NSMutableArray *observers;

@end

@implementation MSDNotificationBinding

+ (instancetype)bindingWithNotificationCenter:(nullable NSNotificationCenter *)center queue:(nullable NSOperationQueue *)queue {
    return [[self alloc] initWithNotificationCenter:center queue:queue];
}

+ (instancetype)binding {
    return [[self alloc] init];
}

- (instancetype)initWithNotificationCenter:(nullable NSNotificationCenter *)center queue:(nullable NSOperationQueue *)queueOrNil {
    if ((self = [super init])) {
        self.notificationCenter = center ?: [NSNotificationCenter defaultCenter];
        self.queue = queueOrNil;
        self.observers = [NSMutableArray array];
    }
    return self;
}

- (instancetype)init {
    return [self initWithNotificationCenter:nil queue:nil];
}

- (void)dealloc {
    [self removeAllObservers];
}

- (void)removeAllObservers {
    for(id observer in self.observers) {
        [self.notificationCenter removeObserver:observer];
    }
    [self.observers removeAllObjects];
}

- (void)addObserverForName:(nullable nullable NSString *)notificationName object:(nullable id)sender usingBlock:(void (^)(NSNotification *notification))block {
    id observer = [self.notificationCenter addObserverForName:notificationName
                                                       object:sender
                                                        queue:self.queue
                                                   usingBlock:block];
    [self.observers addObject:observer];
}

- (void)addObserver:(id)target selector:(SEL)selector name:(nullable NSString *)notificationName object:(nullable id)sender {
    __weak id unretained_target = target;
    
    [self addObserverForName:notificationName object:sender usingBlock:^(NSNotification *note) {
        // Clang normally warns us here beacuse we're calling performSelector: with a selector which
        // might have attributes, such as autoreleasing parameters or returned +1 values, etc.
        // We have little choice but to assume that the selector will be "normal" and so we ignore this.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [unretained_target performSelector:selector withObject:note];
#pragma clang diagnostic pop
    }];
}

@end

NS_ASSUME_NONNULL_END
