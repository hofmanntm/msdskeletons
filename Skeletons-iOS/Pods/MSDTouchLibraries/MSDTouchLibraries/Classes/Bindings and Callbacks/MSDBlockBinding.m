//
//  MSDBlockBinding.m
//  MSDTouchLibraries
//
//  Created by Michael Burke on 2016-07-21.
//
//

#import "MSDBlockBinding.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN


#pragma mark - MSDBlockBindingObserver implementation


@interface MSDBlockBindingObserver : NSObject
@property (nonatomic, strong) id observedObject;
@property (nonatomic, strong) NSArray<NSString*> *keyPaths;
@property (nonatomic, copy) MSDBlockBindingValueDidChangeAction action;
@end

@implementation MSDBlockBindingObserver

- (instancetype)initWithObservedObject:(__weak id)object keyPaths:(NSArray<NSString*> *)keyPaths didChangeAction:(MSDBlockBindingValueDidChangeAction)action {
    self = [super init];
    
    self.observedObject = object;
    self.keyPaths = keyPaths;
    self.action = action;
    
    return self;
}

@end


#pragma mark - MSDBlockBinding implementation


@interface MSDBlockBinding ()
@property (nonatomic, strong) NSMutableArray *observers;
@end

@implementation MSDBlockBinding

+ (MSDBlockBinding *)bindingFromObject:(id)observedObject keyPaths:(NSArray<NSString*> *)keyPaths toDidChangeAction:(MSDBlockBindingValueDidChangeAction)didChangeAction {
    return [[self alloc] initWithFromObject:observedObject keyPaths:keyPaths toDidChangeAction:didChangeAction];
}


- (instancetype)init {
    self = [super init];
    self.observers = [NSMutableArray array];
    return self;
}

- (instancetype)initWithFromObject:(id)observedObject keyPaths:(NSArray<NSString*> *)keyPaths toDidChangeAction:(MSDBlockBindingValueDidChangeAction)didChangeAction {
    self = [self init];
    [self bindObject:observedObject keyPaths:keyPaths toDidChangeAction:didChangeAction];
    return self;
}

- (void)bindObject:(id)observedObject keyPaths:(NSArray<NSString*> *)keyPaths toDidChangeAction:(MSDBlockBindingValueDidChangeAction)didChangeAction {
    MSDArgNotNil(observedObject);
    MSDArgNotNil(keyPaths);
    MSDArgNotNil(didChangeAction);
    
    MSDBlockBindingObserver *observer = [[MSDBlockBindingObserver alloc] initWithObservedObject:observedObject
                                                                                       keyPaths:keyPaths
                                                                                didChangeAction:didChangeAction];
    
    [self.observers addObject:observer];
    
    for (NSString *keyPath in keyPaths) {
        [observedObject addObserver:self
                         forKeyPath:keyPath
                            options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                            context:(__bridge void *)observer];
    }
}

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary *)change context:(nullable void *)context {
    MSDBlockBindingObserver *observer = (__bridge MSDBlockBindingObserver *)context;
    observer.action(object, keyPath);
}

- (void)detach {
    NSArray *oldObservers = [self.observers copy];
    self.observers = [NSMutableArray array];
    
    for (MSDBlockBindingObserver *observer in oldObservers) {
        for (NSString *keyPath in observer.keyPaths) {
            [observer.observedObject removeObserver:self forKeyPath:keyPath context:(__bridge void *)observer];
        }
    }
}

- (void)dealloc {
    [self detach];
}

@end

NS_ASSUME_NONNULL_END

