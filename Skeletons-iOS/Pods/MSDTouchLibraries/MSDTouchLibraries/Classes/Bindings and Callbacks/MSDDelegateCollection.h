//
//  MSDDelegateCollection.h
//  MSDTouchLibraries
//
//  Created by Raymond Edwards on 2018-08-09.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/// A collection of weakly-held delegates iterable by selector.
@interface MSDDelegateCollection<Delegate> : NSObject <NSFastEnumeration>

- (void)addDelegate:(Delegate)delegate;
- (void)removeDelegate:(Delegate)delegate;
- (void)removeAllDelegates;

- (void)enumerateDelegatesRespondingToSelector:(SEL)selector usingBlock:(__attribute__((noescape)) void (^)(Delegate delegate))block;

@end
