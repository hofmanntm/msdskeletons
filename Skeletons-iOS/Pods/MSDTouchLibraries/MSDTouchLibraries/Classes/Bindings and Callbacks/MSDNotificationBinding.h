//
//  MSDNotificationBinding.h
//  MSDTouchLibraries
//
//  Created by Mike Burke on 12-05-23.
//  Copyright (c) 2012 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/*!
 * Notification-observing helper class.
 *
 * - Makes it easier to observe a collection of notifications and perform different actions based on them.
 * 
 * - Handles removal of those observers when this class is deallocated, or at any other point if desired
 *   (via -removeAllObservers).
 * 
 * - Avoids conflicts where a subclass wants to observe the same notification(s) as a superclass, if the
 *   superclass has set itself as the target of the notification.
 *
 * - Similarly, prevents a superclass from conflicting with potential subclasses as the target of
 *   notifications.
 */
@interface MSDNotificationBinding : NSObject

/*!
 * Initialize self.
 * 
 * The initialized object will observe notifications from the specified notification center,
 * and invoke blocks/targets on the specified queue.
 *
 * @param center The notification center to observe notifications from.  If nil, [NSNotificationCenter defaultCenter] is used.
 * @param queue The operation queue used to invoke blocks or targets. If nil, they will be run synchronously on the posting thread.
 */
- (instancetype)initWithNotificationCenter:(nullable NSNotificationCenter *)center queue:(nullable NSOperationQueue *)queue NS_DESIGNATED_INITIALIZER;
/*!
 * Same as [self initWithNotificationCenter:nil queue:nil]
 */
- (instancetype)init;

/*!
 * Factory methods for initializers.
 */
+ (instancetype)bindingWithNotificationCenter:(nullable NSNotificationCenter *)center queue:(nullable NSOperationQueue *)queue;
+ (instancetype)binding;

/*!
 * Add an observer.  Analogous to -[NSNotificationCenter addObserverForName:object:queue:usingBlock:], but
 * will always use the operation queue supplied at construction.
 * 
 * @param notificationName Notification name to watch, or nil to avoid filtering on name.
 * @param sender Notification sender to watch, or nil to avoid filtering on sender.
 * @param block The block to invoke on self.queue when the notification is received.
 */
- (void)addObserverForName:(nullable NSString *)notificationName object:(nullable id)sender usingBlock:(void (^)(NSNotification *notification))block;
/*!
 * Add an observer.  Analogous to -[NSNotificationCenter addObserver:selector:name:object:].
 * 
 * @param target The target upon which the supplied selector will be invoked when a notification is received.
 * @param selector The selector to invoke on target when a notification is received.
 *                 The selector will be invoked on the operation queue supplied at construction.
 * @param notificationName Notification name to watch, or nil to avoid filtering on name.
 * @param sender Notification sender to watch, or nil to avoid filtering on sender.
 */
- (void)addObserver:(id)target selector:(SEL)selector name:(nullable NSString *)notificationName object:(nullable id)sender;

/*!
 * Remove all of the observers being managed by this object.
 */
- (void)removeAllObservers;

/*!
 * The operation queue specified at construction.  Blocks and target selectors will be invoked on this queue.
 */
@property (nonatomic, strong, nullable, readonly) NSOperationQueue *queue;
/*!
 * The notification center specified at construction that is managing the notifications being observed.
 */
@property (nonatomic, strong, readonly) NSNotificationCenter *notificationCenter;

@end

NS_ASSUME_NONNULL_END
