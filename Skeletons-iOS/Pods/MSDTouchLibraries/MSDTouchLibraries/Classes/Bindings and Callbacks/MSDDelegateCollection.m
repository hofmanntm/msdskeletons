//
//  MSDDelegateCollection.m
//  MSDTouchLibraries
//
//  Created by Raymond Edwards on 2018-08-09.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

#import "MSDDelegateCollection.h"

@implementation MSDDelegateCollection {
    NSHashTable *_delegates;
}

- (instancetype)init {
    if ((self = [super init])) {
        _delegates = [NSHashTable weakObjectsHashTable];
    }
    return self;
}

- (void)addDelegate:(id)delegate {
    [_delegates addObject:delegate];
}

- (void)removeDelegate:(id)delegate {
    [_delegates removeObject:delegate];
}

- (void)removeAllDelegates {
    [_delegates removeAllObjects];
}

- (void)enumerateDelegatesRespondingToSelector:(SEL)selector usingBlock:(__attribute__((noescape)) void (^)(id delegate))block {
    for (id delegate in _delegates) {
        if ([delegate respondsToSelector:selector]) {
            block(delegate);
        }
    }
}

#pragma mark NSFastEnumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id _Nonnull [])stackbuf count:(NSUInteger)len {
    return [_delegates countByEnumeratingWithState:state objects:stackbuf count:len];
}

@end
