//
//  MSDBlockBinding.h
//  MSDTouchLibraries
//
//  Created by Michael Burke on 2016-07-21.
//
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^MSDBlockBindingValueDidChangeAction)(id object, NSString *keyPath) NS_SWIFT_NAME(BlockBindingValueDidChangeAction);

NS_SWIFT_NAME(BlockBinding)
@interface MSDBlockBinding : NSObject

/*!
 * Same as calling -init followed by -bindObject:keyPaths:toDidChangeAction:
 */
+ (instancetype)bindingFromObject:(id)observedObject keyPaths:(NSArray<NSString*> *)keyPaths toDidChangeAction:(MSDBlockBindingValueDidChangeAction)didChangeAction;

/*!
 * Same as calling -init followed by -bindObject:keyPaths:toDidChangeAction:
 */
- (instancetype)initWithFromObject:(id)observedObject keyPaths:(NSArray<NSString*> *)keyPaths toDidChangeAction:(MSDBlockBindingValueDidChangeAction)didChangeAction;

/*!
 * Bind an object's key path(s) to a did-change action block.
 *
 * When the value of one of the key paths changes and sends a KVO event,
 * the didChangeAction block will be called with the object and key
 * path that triggered the event.
 *
 * You can bind multiple observers this way to multiple objects.
 *
 * When you're done observing, call -detach to remove the observers;
 * otherwise, if your instance of this object stays alive
 * unexpectedly, you risk having your didChangeAction block called
 * even after you're done with it.
 */
- (void)bindObject:(id)observedObject keyPaths:(NSArray<NSString*> *)keyPaths toDidChangeAction:(MSDBlockBindingValueDidChangeAction)didChangeAction;

/*!
 * Remove all bindings. This should be called before either the from object
 * or to objects are deallocated.  You can begin observing further objects
 * after detaching, but it is probably best to create a new MSDBlockBinding
 * object.
 */
- (void)detach;

@end

NS_ASSUME_NONNULL_END

