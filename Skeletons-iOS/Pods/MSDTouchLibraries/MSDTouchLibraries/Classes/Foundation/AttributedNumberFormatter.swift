//
//  AttributedNumberFormatter.swift
//  Montego
//
//  Created by Nolan Waite on 2016-07-15.
//  Copyright © 2016 MindSea. All rights reserved.
//

import Foundation

/**
 A number formatter that implements `attributedString(for:withDefaultAttributes:)`.

 `NumberFormatter` supports attributed strings on macOS. It appears that the relevant methods were brought to iOS but never actually implemented.
 */
open class AttributedNumberFormatter: NumberFormatter {
    override open func attributedString(for obj: Any, withDefaultAttributes defaultAttributes: [NSAttributedString.Key: Any]?) -> NSAttributedString? {
        guard let number = obj as? NSNumber, let plain = string(from: number) else {
            return nil
        }
        
        let activeAttributes: [NSAttributedString.Key: Any]?
        switch (number, number.compare(0)) {
        case (NSDecimalNumber.notANumber, _):
            activeAttributes = fixKeyType(textAttributesForNotANumber)
        case (kCFNumberPositiveInfinity, _):
            activeAttributes = fixKeyType(textAttributesForPositiveInfinity)
        case (kCFNumberNegativeInfinity, _):
            activeAttributes = fixKeyType(textAttributesForNegativeInfinity)
        case (_, .orderedSame):
            activeAttributes = fixKeyType(textAttributesForZero)
        case (_, .orderedAscending):
            activeAttributes = fixKeyType(textAttributesForNegativeValues)
        case (_, .orderedDescending):
            activeAttributes = fixKeyType(textAttributesForPositiveValues)
        }
        
        // Seems to be how the macOS implementation works.
        let attributes = (defaultAttributes ?? [:]).merging(activeAttributes ?? [:], uniquingKeysWith: { $1 })
        
        return NSAttributedString(string: plain, attributes: attributes)
    }
}

private func fixKeyType(_ raw: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let raw = raw else { return nil }
    return Dictionary(uniqueKeysWithValues: raw.map { (NSAttributedString.Key(rawValue: $0), $1) })
}
