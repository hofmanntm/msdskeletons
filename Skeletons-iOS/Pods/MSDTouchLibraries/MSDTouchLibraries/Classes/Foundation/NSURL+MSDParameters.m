//
//  NSURL+MSDParameters.m
//  MSDTouchLibraries
//
//  Created by Kris Luttmer on 2017-04-07.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

#import "NSURL+MSDParameters.h"

NS_ASSUME_NONNULL_BEGIN

@implementation NSURL (MSDParameters)

- (NSURL *)msd_URLByAddingParameters:(nullable NSDictionary<NSString *, id> *)parameters {
    if (parameters.count == 0) {
        return [self copy];
    }
    
    NSMutableArray *queryItems = [NSMutableArray new];
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL * stop) {
        NSURLQueryItem *queryItem = [NSURLQueryItem queryItemWithName:key
                                                                value:[obj description]];
        [queryItems addObject:queryItem];
    }];
    
    NSURLComponents *components = [NSURLComponents componentsWithString:self.absoluteString];
    if (components) {
        if (components.queryItems) {
            [queryItems addObjectsFromArray:components.queryItems];
        }
        
        if (queryItems.count > 0) {
            components.queryItems = queryItems;
            return components.URL;
        }
    }

    return [self copy];
}

@end

NS_ASSUME_NONNULL_END
