//
//  NSTimer+MSDBlock.m
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2015-12-17.
//  Copyright © 2015 MindSea Development Inc. All rights reserved.
//

#import "NSTimer+MSDBlock.h"

NS_ASSUME_NONNULL_BEGIN

@implementation NSTimer (MSDBlock)

+ (NSTimer *)msd_scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval handler:(void(^)(NSTimer *timer))handler {
    NSParameterAssert(handler);
    
    NSTimer *timer = CFBridgingRelease(CFRunLoopTimerCreateWithHandler(nil, CFAbsoluteTimeGetCurrent() + timeInterval, 0, 0, 0, ^(CFRunLoopTimerRef timer) {
        handler((__bridge NSTimer *)timer);
    }));
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
    return timer;
}

@end

NS_ASSUME_NONNULL_END
