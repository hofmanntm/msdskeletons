//
//  NSURL+MSDParameters.h
//  MSDTouchLibraries
//
//  Created by Kris Luttmer on 2017-04-07.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSURL (MSDParameters)

/**
    Adds (or extends) the URL's query string to include `parameters`.
 
    @param parameters The key-value pairs to append to the query string. Each value has `-description` called on it.
 
    @return A new URL with the altered query string, or the receiver if the query string remains unchanged.
 */
- (NSURL *)msd_URLByAddingParameters:(nullable NSDictionary<NSString *, id> *)parameters NS_SWIFT_NAME(msd_addingParameters(_:));

@end

NS_ASSUME_NONNULL_END
