//
//  NSTimer+MSDBlock.h
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2015-12-17.
//  Copyright © 2015 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSTimer (MSDBlock)

/// Creates and schedules a timer on the current run loop in the default mode that calls a block. The timer is passed to the block as its sole parameter.
+ (NSTimer *)msd_scheduledTimerWithTimeInterval:(NSTimeInterval)timeInterval handler:(void(^)(NSTimer *timer))handler;

@end

NS_ASSUME_NONNULL_END
