//
//  Debug.swift
//  MSDTouchLibraries
//
//  Created by Mike Burke on 2016-07-22.
//
//

import Foundation

/**
    Log an error message via MSDLogging, and on debug builds, also throw a fatal error.
 
    Use this to identify programming problems (eg. failed preconditions) where you generally want to fail loudly while developing but silently in distribution builds.
 */
public func fatalInDebug(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
    let fileBaseName = URL(fileURLWithPath: file).lastPathComponent
    
    Log.e(message, function: function, line: line, file: file)
    #if DEBUG
        fatalError("\(function) (\(fileBaseName):\(line)) \(message)")
    #endif
}

/**
    If `condition` is false, call fatalInDebug(failMessage).
 
    Use this instead of assert, so that assertions always get logged on both distribution and debug builds.
 */
public func fatalInDebugUnless(_ condition: Bool, _ failMessage: String, function: String = #function, line: UInt = #line, file: String = #file) {
    if !condition {
        fatalInDebug(failMessage, function: function, line: line, file: file)
    }
}
