//
//  MSDLoggingNSLogHandler.h
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-09.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDLogHandler.h"
#import "MSDLoggingTypes.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(LoggingNSLogHandler)
@interface MSDLoggingNSLogHandler : NSObject <MSDLogHandler>

/**
 * Initialize the log handler with a maximum log level of DEBUG.
 */
- (instancetype)init;
 
- (instancetype)initWithLogLevel:(MSDLogLevel)level NS_DESIGNATED_INITIALIZER;

@property (nonatomic, assign, readonly) MSDLogLevel logLevel;

/**
 * Deprecated: Moved to MSDLoggingHelper
 */
+ (NSString *)logMessageForInfoDictionary:(NSDictionary *)dictionary __attribute__ ((deprecated));

@end

NS_ASSUME_NONNULL_END
