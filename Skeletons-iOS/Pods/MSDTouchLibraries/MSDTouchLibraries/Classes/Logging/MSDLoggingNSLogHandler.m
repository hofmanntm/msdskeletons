//
//  MSDLoggingNSLogHandler.m
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-09.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDLoggingNSLogHandler.h"
#import "MSDLogger.h"
#import "MSDLoggingHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDLoggingNSLogHandler ()
@property (nonatomic, assign) MSDLogLevel logLevel;
@end

@implementation MSDLoggingNSLogHandler

- (instancetype)init {
    return (self = [self initWithLogLevel:MSDLogLevelDebug]);
}

- (instancetype)initWithLogLevel:(MSDLogLevel)level {
    if ((self = [super init])) {
        self.logLevel = level;
    }
    return self;
}

+ (NSString *)logMessageForInfoDictionary:(NSDictionary *)infoDictionary {
    return [MSDLoggingHelper logMessageForInfoDictionary:infoDictionary];
}

- (void)logger:(MSDLogger *)logger handleInfoDictionary:(NSDictionary *)infoDictionary {
    unsigned int msgLevel = [infoDictionary[MSDLoggerInfoKeyLevel] unsignedIntValue];
    
    if (msgLevel >= self.logLevel) {
        NSLog(@"%@", [MSDLoggingHelper logMessageForInfoDictionary:infoDictionary]);
    }
}

@end

NS_ASSUME_NONNULL_END
