//
//  MSDLoggingBlockHandler.h
//  MindSea
//
//  Created by Raymond Edwards on 2016-08-11.
//
//

#import <Foundation/Foundation.h>
#import "MSDLogger.h"

NS_ASSUME_NONNULL_BEGIN

/**
 *
 *  A log handler that sends formatted MSDLogging output to a provided block.
 *
 */
NS_SWIFT_NAME(LoggingBlockHandler)
@interface MSDLoggingBlockHandler : NSObject <MSDLogHandler>

@property (nonatomic, assign, readonly) MSDLogLevel logLevel;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

/**
 *  Init with log level and block.
 *
 *  A common use-case is sending MSDLogs to Crashlytics.
 *  Example:
 *  
 * [[MSDLoggingBlockHandler alloc] initWithLogLevel:MSDLogLevelDebug block:^(NSString *logMessage) { CLSLog(%@, logMessage); }];
 */
- (instancetype)initWithLogLevel:(MSDLogLevel)level block:(void (^)(NSString *))block NS_DESIGNATED_INITIALIZER;

@end

NS_ASSUME_NONNULL_END
