//
//  MSDLoggingBlockHandler.m
//  MindSea
//
//  Created by Raymond Edwards on 2016-08-11.
//
//

#import "MSDLoggingBlockHandler.h"
#import "MSDLoggingTypes.h"
#import "MSDLoggingHelper.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDLoggingBlockHandler ()

@property (nonatomic, assign) MSDLogLevel logLevel;
@property (nonatomic, copy) void (^block)(NSString *);

@end

@implementation MSDLoggingBlockHandler

- (instancetype)init {
    return nil;
}

- (instancetype)initWithLogLevel:(MSDLogLevel)level block:(void (^)(NSString *))block {
    if ((self = [super init])) {
        self.logLevel = level;
        self.block = block;
    }
    return self;
}

- (void)logger:(MSDLogger *)logger handleInfoDictionary:(NSDictionary *)infoDictionary {
    unsigned int msgLevel = [infoDictionary[MSDLoggerInfoKeyLevel] unsignedIntValue];
    
    if (msgLevel >= self.logLevel) {
        self.block([MSDLoggingHelper logMessageForInfoDictionary:infoDictionary]);
    }
}

@end

NS_ASSUME_NONNULL_END
