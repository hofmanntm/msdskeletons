//
//  NSObject+MSDLogger.m
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-09.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "NSObject+MSDLogger.h"
#import "MSDLogger.h"

NS_ASSUME_NONNULL_BEGIN

@implementation NSObject (MSDLogger)

+ (MSDLogger *)msd_logger {
    NSString *derivedName;
    NSString *className = NSStringFromClass(self);
    
    NSScanner *scanner = [NSScanner scannerWithString:className];
    NSString *classNamespace;
    if ([scanner scanCharactersFromSet:[NSCharacterSet uppercaseLetterCharacterSet]
                            intoString:&classNamespace] && 
        ![classNamespace isEqual:className] &&
        [classNamespace length] > 1) {
        
        // Trim last character, eg. the 'L' from "MSDLogger"
        classNamespace = [classNamespace substringToIndex:[classNamespace length] - 1];
        derivedName = [NSString stringWithFormat:@"%@.%@", classNamespace, [className substringFromIndex:[classNamespace length]]];
        
    } else {
        derivedName = className;
    }
    return [MSDLogger loggerNamed:derivedName];
}

- (MSDLogger *)msd_logger {
    return [[self class] msd_logger];
}

@end

NS_ASSUME_NONNULL_END
