//
//  Logging.swift
//  MSDTouchLibraries
//
//  Created by Mike Burke on 2016-07-06.
//
//

import Foundation

private func _log(_ message: String, logger: Logger, level: LogLevel, function: String = #function, line: UInt = #line, file: String = #file) {
    logger.logInfoDictionary([
        MSDLoggerInfoKey.level.rawValue: level.rawValue,
        MSDLoggerInfoKey.prettyFunction.rawValue: function,
        MSDLoggerInfoKey.message.rawValue: message,
        MSDLoggerInfoKey.lineNumber.rawValue: line,
        MSDLoggerInfoKey.file.rawValue: file])
}

open class Log {
    open class func d(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: Logger.root, level: .debug, function: function, line: line, file: file)
    }
    
    open class func i(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: Logger.root, level: .info, function: function, line: line, file: file)
    }
    
    open class func w(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: Logger.root, level: .warning, function: function, line: line, file: file)
    }
    
    open class func e(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: Logger.root, level: .error, function: function, line: line, file: file)
    }
    
    open class func c(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: Logger.root, level: .critical, function: function, line: line, file: file)
    }
}

public extension Logger {
    public func d(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: self, level: .debug, function: function, line: line, file: file)
    }
    
    public func i(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: self, level: .info, function: function, line: line, file: file)
    }
    
    public func w(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: self, level: .warning, function: function, line: line, file: file)
    }
    
    public func e(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: self, level: .error, function: function, line: line, file: file)
    }
    
    public func c(_ message: String, function: String = #function, line: UInt = #line, file: String = #file) {
        _log(message, logger: self, level: .critical, function: function, line: line, file: file)
    }
}
