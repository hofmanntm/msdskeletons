//
//  NSObject+MSDLogger.h
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-09.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MSDLogger;

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (MSDLogger)

@property (class, readonly, nonatomic) MSDLogger *msd_logger;
@property (readonly, nonatomic) MSDLogger *msd_logger;

@end

NS_ASSUME_NONNULL_END
