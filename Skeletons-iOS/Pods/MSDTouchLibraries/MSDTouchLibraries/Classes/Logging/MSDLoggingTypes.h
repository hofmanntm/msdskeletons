/*
 * Available log levels.  Associated with MSDLoggerInfoKeyLevel.
 */

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MSDLogLevel) {
    MSDLogLevelNotSet = 0,
    MSDLogLevelDebug = 10,
    MSDLogLevelInfo = 20,
    MSDLogLevelWarning = 30,
    MSDLogLevelError = 40,
    MSDLogLevelCritical = 50
} NS_SWIFT_NAME(LogLevel);

NS_ASSUME_NONNULL_END
