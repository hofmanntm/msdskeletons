//
//  MSDLogger.h
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-08.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MSDLoggingTypes.h"
#import "MSDLogHandler.h"
#import "NSObject+MSDLogger.h"

NS_ASSUME_NONNULL_BEGIN

/*
 * Log data pieces used by handlers when producing their output.
 *
 * The info dictionary passed to a handler's handleInfoDictionary: will use
 * these as keys.
 * 
 * Some of these are optional, but the following are required and ensured
 * to be present:
 *   MSDLoggerInfoKeyMessage
 *   MSDLoggerInfoKeyName
 *   MSDLoggerInfoKeyLevel
 *
 * The MSDLog macros will supply the rest if you are using them.
 */
typedef NSString *MSDLoggerInfoKey NS_EXTENSIBLE_STRING_ENUM;

// NSString: The message supplied in the call to MSDDebugLog(message, fmt) after formatting.
extern MSDLoggerInfoKey const MSDLoggerInfoKeyMessage;
// NSString: Name of the logger, ie. the category, eg. MSD.Logger, NOC.Feed, or Twitter.
extern MSDLoggerInfoKey const MSDLoggerInfoKeyName;
// NSNumber: Log level, stored in info dictionary as numberWithUnsignedInt
extern MSDLoggerInfoKey const MSDLoggerInfoKeyLevel;
// NSNumber: Line number where log called, stored in info dictionary as numberWithUnsignedInt
extern MSDLoggerInfoKey const MSDLoggerInfoKeyLineNumber;
// NSString: The 'pretty function' where log called, as retrieved by __PRETTY_FUNCTION__
extern MSDLoggerInfoKey const MSDLoggerInfoKeyPrettyFunction;
// NSString: The file where log called, as retrieved by __LINE__
extern MSDLoggerInfoKey const MSDLoggerInfoKeyFile;
// NSDate: When the log call occurred.
extern MSDLoggerInfoKey const MSDLoggerInfoKeyDate;

/*
 * Log a message using MSDLogger *logger at MSDLogLevel level.
 *
 * userInfoOrNil can be a dictionary of custom flags to be passed to the handlers,
 * or nil if none are desired.
 *
 * Arguments are a format string.
 */
#define MSDLogWithInfo(logger, level, userInfoOrNil, ...) \
    do { \
        NSMutableDictionary *_infoDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithUnsignedInt:(unsigned int)level], MSDLoggerInfoKeyLevel, \
                                                                                                 [NSString stringWithUTF8String:__PRETTY_FUNCTION__], MSDLoggerInfoKeyPrettyFunction, \
                                                                                                 [NSString stringWithFormat:__VA_ARGS__], MSDLoggerInfoKeyMessage, \
                                                                                                 [NSNumber numberWithUnsignedInt:(unsigned int)__LINE__], MSDLoggerInfoKeyLineNumber, \
                                                                                                 [NSString stringWithUTF8String:__FILE__], MSDLoggerInfoKeyFile, \
                                                                                                 nil]; \
        [_infoDictionary addEntriesFromDictionary:(userInfoOrNil ?: @{})]; \
        [logger logInfoDictionary:_infoDictionary]; \
    } while(0)

/*
 * Call MSDLogWithInfo with nil userInfo.
 */
#define MSDLog(logger, level, ...) MSDLogWithInfo(logger, level, nil, __VA_ARGS__)

/*
 * Call MSDLog at the supplied levels using the default logger for the current class.
 */
#define MSDDebugLog(...) MSDLog([self msd_logger], MSDLogLevelDebug, __VA_ARGS__)
#define MSDInfoLog(...) MSDLog([self msd_logger], MSDLogLevelInfo, __VA_ARGS__)
#define MSDWarningLog(...) MSDLog([self msd_logger], MSDLogLevelWarning, __VA_ARGS__)
#define MSDErrorLog(...) MSDLog([self msd_logger], MSDLogLevelError, __VA_ARGS__)
#define MSDCriticalLog(...) MSDLog([self msd_logger], MSDLogLevelCritical, __VA_ARGS__)

/*
 * Call MSDLog at the appropriate levels using logger named.  Can be used outside of a class.
 */
#define MSDCDebugLog(name, ...) MSDLog([MSDLogger loggerNamed:name], MSDLogLevelDebug, __VA_ARGS__)
#define MSDCInfoLog(name, ...) MSDLog([MSDLogger loggerNamed:name], MSDLogLevelInfo, __VA_ARGS__)
#define MSDCWarningLog(name, ...) MSDLog([MSDLogger loggerNamed:name], MSDLogLevelWarning, __VA_ARGS__)
#define MSDCErrorLog(name, ...) MSDLog([MSDLogger loggerNamed:name], MSDLogLevelError, __VA_ARGS__)
#define MSDCCriticalLog(name, ...) MSDLog([MSDLogger loggerNamed:name], MSDLogLevelCritical, __VA_ARGS__)

/*
 * Loggers for managing a hierarchy of categories which may have differing log filtering, levels, and handlers/outputs.
 *
 * Basic usage:
 *   // Initial setup, eg. in applicationDidFinishLaunching
 *   MSDLogger *rootLogger = [MSDLogger rootLogger]; // Fetches the root logger; default setup calls NSLog for 
 *   rootLogger.logLevel = MSDLogLevelInfo; // Display only log messages at level Info or higher (Warning, Error, Critical)
 *   
 *   // In another method/function/etc
 *   MSDInfoLog(@"I am logging here!"); // Displays 'I am logging here' with some extra data eg. method, line number...
 *   MSDDebugLog(@"I am at %p", self); // Would display 'I am at 0x...' for the memory location of self, but we set the filter level to Info so it gets suppressed.
 *   
 *   // Hierarchy example:
 *   [MSDLogger loggerNamed:@"Example.Foo"].level = MSDLogLevelWarning; // Allows logging at level Warning, Error, Critical for Example.Foo and children (eg. Example.Foo.Bar).
 *   [MSDLogger rootLogger].level = MSDLogLevelDebug; // Allows logging at Debug, Info, etc. but messages from Example.Foo will have been filtered out first.
 */
NS_SWIFT_NAME(Logger)
@interface MSDLogger : NSObject

// Fetch the root logger. The root logger by default adds an NSLog handler and, if available, a CLSLog handler.
@property (class, readonly, nonatomic) MSDLogger *rootLogger;

// Fetch a logger with the specified name (category), which is a dot-separated name in a hierarchy (eg. "MSD.Logger")
+ (MSDLogger *)loggerNamed:(NSString *)loggerName;

// Log the supplied info using this logger's handlers and potentially parent loggers.
- (void)logInfoDictionary:(NSDictionary *)infoDictionary;

// Add/remove handlers to this logger level.
- (void)addHandler:(id<MSDLogHandler>)logHandler;
- (void)removeHandler:(id<MSDLogHandler>)logHandler;

// The name of this logger as returned by loggerNamed:
@property (nonatomic, copy, readonly) NSString *name;
// The log level that this logger uses to filter.
@property (nonatomic, assign) MSDLogLevel logLevel;
// Whether this logger will propagate valid log messages to its parent.
// YES by default, but if this is a non-root logger with a handler, you may
// want to specify NO.
@property (nonatomic, assign) BOOL propagate;

@end

NS_ASSUME_NONNULL_END
