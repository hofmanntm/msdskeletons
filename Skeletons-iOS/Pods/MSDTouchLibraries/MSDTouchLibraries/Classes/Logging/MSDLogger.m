//
//  MSDLogger.m
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-08.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDLogger.h"
#import "MSDLoggingNSLogHandler.h"

NS_ASSUME_NONNULL_BEGIN

NSString *const MSDLoggerInfoKeyMessage = @"MSDLoggerInfoKeyMessage";
NSString *const MSDLoggerInfoKeyName = @"MSDLoggerInfoKeyName";
NSString *const MSDLoggerInfoKeyLevel = @"MSDLoggerInfoKeyLevel";
NSString *const MSDLoggerInfoKeyLineNumber = @"MSDLoggerInfoKeyLineNumber";
NSString *const MSDLoggerInfoKeyPrettyFunction = @"MSDLoggerInfoKeyPrettyFunction";
NSString *const MSDLoggerInfoKeyFile = @"MSDLoggerInfoKeyFile";
NSString *const MSDLoggerInfoKeyDate = @"MSDLoggerInfoKeyDate";

static MSDLogger *rootLoggerInstance;
static NSMutableArray *loggers;
static NSRecursiveLock *loggerNamedAccessLock;
static NSRecursiveLock *updateLoggerHierarchyAccessLock;

@interface MSDLogger ()
+ (nullable MSDLogger *)existingLoggerNamed:(NSString *)name;
+ (void)updateLoggerHierarchy;
- (instancetype)initWithName:(NSString *)loggerName;
- (instancetype)initRootLogger;
- (nullable MSDLogger *)findParent;
- (void)handleInfoDictionary:(NSDictionary *)infoDictionary;

@property (nonatomic, strong) NSMutableArray *handlers;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, weak) MSDLogger *parent;
@end

@implementation MSDLogger

#pragma mark - Static methods

+ (void)initialize {
    loggers = [[NSMutableArray alloc] init];
    rootLoggerInstance = [[self alloc] initRootLogger];
    loggerNamedAccessLock = [[NSRecursiveLock alloc] init];
    updateLoggerHierarchyAccessLock = [[NSRecursiveLock alloc] init];
}

+ (MSDLogger *)rootLogger {
    return rootLoggerInstance;
}

+ (MSDLogger *)loggerNamed:(NSString *)loggerName {
    MSDLogger *instance;
    
    [loggerNamedAccessLock lock];
    
    @try {
        if ((instance = [self existingLoggerNamed:loggerName])) {
            return instance;
        }
        
        instance = [[MSDLogger alloc] initWithName:loggerName];
        [loggers addObject:instance];
        
        [self updateLoggerHierarchy];
        return instance;
    }
    @finally {
        [loggerNamedAccessLock unlock];
    }
}

+ (void)updateLoggerHierarchy {
    [updateLoggerHierarchyAccessLock lock];
    
    @try {
        for(MSDLogger *instance in loggers) {
            instance.parent = [instance findParent];
        }
    }
    @finally {
        [updateLoggerHierarchyAccessLock unlock];
    }
}

#pragma mark - Lifecycle

/*
 * Prevent instantiation of loggers outside of the factory methods.
 */
- (instancetype)init {
    return nil;
}


#pragma mark - Handler management

- (void)addHandler:(id<MSDLogHandler>)logHandler {
    if (![self.handlers containsObject:logHandler]) {
        [self.handlers addObject:logHandler];
    }
}

- (void)removeHandler:(id<MSDLogHandler>)logHandler {
    [self.handlers removeObject:logHandler];
}

#pragma mark - Logging

- (void)logInfoDictionary:(NSDictionary *)infoDictionary {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:infoDictionary];
    dict[MSDLoggerInfoKeyName] = self.name;
    dict[MSDLoggerInfoKeyDate] = [NSDate date];
    
    [self handleInfoDictionary:dict];
}

#pragma mark - Private methods

#pragma mark - Private lifecycle

- (instancetype)initRootLogger {
    if ((self = [super init])) {
        self.handlers = [NSMutableArray array];
        
        self.logLevel = MSDLogLevelDebug;
        
        MSDLogLevel handlerLevel;
#ifdef DEBUG
        handlerLevel = MSDLogLevelDebug;
#else
        handlerLevel = MSDLogLevelWarning;
#endif
        [self addHandler:[[MSDLoggingNSLogHandler alloc] initWithLogLevel:handlerLevel]];
        
        self.propagate = NO;
    }
    
    return self;
}

- (instancetype)initWithName:(NSString *)loggerName {
    if ((self = [super init])) {
        self.name = loggerName;
        self.handlers = [NSMutableArray array];
        self.propagate = YES;
        
        // You can set this by adding, say, arguments in your scheme
        // like "-MSD.FileCache info" (sans quotes) which will cause the
        // file cache's debug messages to be squelched.
        self.logLevel = [self logLevelForDefaultsString:[[NSUserDefaults standardUserDefaults] stringForKey:loggerName]];
    }
    
    return self;
}

- (MSDLogLevel)logLevelForDefaultsString:(NSString *)value {
    if ([value length] == 0) {
        return MSDLogLevelDebug;
    }

    switch ([[value lowercaseString] characterAtIndex:0]) {
        case 'z': // "quiet"
            return MSDLogLevelCritical + 1;
            
        case 'c':
            return MSDLogLevelCritical;
        
        case 'e':
            return MSDLogLevelError;

        case 'w':
            return MSDLogLevelWarning;

        case 'i':
            return MSDLogLevelInfo;
            
        case 'd':
        default:
            return MSDLogLevelDebug;
    }
}

#pragma mark - Logging!

+ (nullable MSDLogger *)existingLoggerNamed:(NSString *)loggerName {
    NSUInteger instanceIndex = [loggers indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        if ([[(MSDLogger *)obj name] isEqual:loggerName]) {
            *stop = YES;
            return YES;
        } else {
            return NO;
        }
    }];
    
    if (instanceIndex != NSNotFound) {
        return loggers[instanceIndex];
    } else {
        return nil;
    }
}

- (nullable MSDLogger *)findParent {
    if (self == rootLoggerInstance) {
        return nil;
    }
    
    MSDLogger *foundParent = nil;
    
    NSArray *hierarchy = [self.name componentsSeparatedByString:@"."];
    for(NSUInteger i = [hierarchy count] - 1; (i > 0) && !foundParent; i--) {
        NSRange range;
        range.location = 0;
        range.length = i;
        foundParent = [[self class] existingLoggerNamed:[[hierarchy subarrayWithRange:range] componentsJoinedByString:@"."]];
    }
    
    if (!foundParent) {
        foundParent = [[self class] rootLogger];
    }
    
    return foundParent;
}

- (void)handleInfoDictionary:(NSDictionary *)infoDictionary {
    unsigned int msgLevel = [infoDictionary[MSDLoggerInfoKeyLevel] unsignedIntValue];
    
    if (msgLevel >= self.logLevel) {
        for(id<MSDLogHandler> handler in self.handlers) {
            [handler logger:self handleInfoDictionary:infoDictionary];
        }
        
        if (self.propagate) {
            [self.parent handleInfoDictionary:infoDictionary];
        }
    }
}

@end

NS_ASSUME_NONNULL_END
