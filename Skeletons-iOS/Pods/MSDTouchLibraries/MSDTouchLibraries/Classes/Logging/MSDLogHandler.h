//
//  MSDLogHandler.h
//  Chronicle Herald
//
//  Created by Michael Burke on 2014-09-24.
//
//

#import <Foundation/Foundation.h>
@class MSDLogger;

NS_ASSUME_NONNULL_BEGIN

/*
 * An object that can be associated with an MSDLogger instance (via addHandler:).
 *
 * Will be called for each log record passed to the associated logger that passes
 * the logger's log level filter.
 */
NS_SWIFT_NAME(LogHandler)
@protocol MSDLogHandler <NSObject>

@required

/// Will be passed an info dictionary with the data described above (MSDLoggerInfoKey...)
- (void)logger:(MSDLogger *)logger handleInfoDictionary:(NSDictionary *)infoDictionary;

@end

NS_ASSUME_NONNULL_END
