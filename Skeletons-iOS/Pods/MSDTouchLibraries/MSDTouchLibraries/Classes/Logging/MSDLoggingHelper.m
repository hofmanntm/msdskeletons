//
//  MSDLoggingHelper.m
//  Chronicle Herald
//
//  Created by Michael Burke on 2014-09-24.
//
//

#import "MSDLoggingHelper.h"
#import "MSDLogger.h"

NS_ASSUME_NONNULL_BEGIN

static NSDictionary *logLevelNameMap;

@implementation MSDLoggingHelper

+ (void)initialize {
    logLevelNameMap = @{[NSNumber numberWithUnsignedInt:MSDLogLevelNotSet]: @"NOT SET",
                        [NSNumber numberWithUnsignedInt:MSDLogLevelDebug]: @"DEBUG",
                        [NSNumber numberWithUnsignedInt:MSDLogLevelInfo]: @"INFO",
                        [NSNumber numberWithUnsignedInt:MSDLogLevelWarning]: @"WARNING",
                        [NSNumber numberWithUnsignedInt:MSDLogLevelError]: @"ERROR",
                        [NSNumber numberWithUnsignedInt:MSDLogLevelCritical]: @"CRITICAL"};
}

+ (NSString *)logMessageForInfoDictionary:(NSDictionary *)infoDictionary {
    NSString *loggerName = infoDictionary[MSDLoggerInfoKeyName];
    
    NSString *loggerNamePart;
    if (loggerName) {
        loggerNamePart = [NSString stringWithFormat:@"[%@] ", infoDictionary[MSDLoggerInfoKeyName]];
    } else {
        loggerNamePart = @"";
    }
    
    NSString *fileBaseName = [infoDictionary[MSDLoggerInfoKeyFile] lastPathComponent];
    
    return [NSString stringWithFormat:@"%@%@(%@:%d) %@ %@",
            loggerNamePart,
            infoDictionary[MSDLoggerInfoKeyPrettyFunction],
            fileBaseName,
            [infoDictionary[MSDLoggerInfoKeyLineNumber] unsignedIntValue],
            logLevelNameMap[infoDictionary[MSDLoggerInfoKeyLevel]],
            infoDictionary[MSDLoggerInfoKeyMessage]];
}

@end

NS_ASSUME_NONNULL_END
