//
//  MSDLoggingHelper.h
//  Chronicle Herald
//
//  Created by Michael Burke on 2014-09-24.
//
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(LoggingHelper)
@interface MSDLoggingHelper : NSObject

+ (NSString *)logMessageForInfoDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
