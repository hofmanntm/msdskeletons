//
//  SwipeInteractionController.swift
//
//  Created by Graham Burgsma on 2018-06-21.
//

import UIKit

@available(iOS 10, *)
open class SwipeInteractionController: UIPercentDrivenInteractiveTransition {

    private weak var transitionContext: UIViewControllerContextTransitioning?

    let panGestureRecognizer: UIPanGestureRecognizer

    lazy var longPressGestureRecognizer: UILongPressGestureRecognizer = {
        let recognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        recognizer.minimumPressDuration = 0.0
        recognizer.delegate = self
        return recognizer
    }()

    let animationController: SlideAnimationController

    public init(animationController: SlideAnimationController, panGestureRecognizer: UIPanGestureRecognizer) {
        self.panGestureRecognizer = panGestureRecognizer
        self.animationController = animationController

        super.init()

        panGestureRecognizer.delegate = self
        if panGestureRecognizer.state == .possible {
            wantsInteractiveStart = false
        }
    }

    deinit {
        longPressGestureRecognizer.view.map { $0.removeGestureRecognizer(longPressGestureRecognizer) }
        panGestureRecognizer.removeTarget(self, action: #selector(handleInteraction))
    }

    override open func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        self.transitionContext = transitionContext

        animationController.setupAnimator(using: transitionContext)

        if panGestureRecognizer.state == .possible, !wantsInteractiveStart {
            animationController.animateTransition(using: transitionContext)
        }

        switch animationController.operation {
        case .present:
            transitionContext.view(forKey: .to)?.addGestureRecognizer(longPressGestureRecognizer)
        case .dismiss:
            transitionContext.view(forKey: .from)?.addGestureRecognizer(longPressGestureRecognizer)
        }

        panGestureRecognizer.addTarget(self, action: #selector(handleInteraction))
        panGestureRecognizer.setTranslation(.zero, in: transitionContext.containerView)

        super.startInteractiveTransition(transitionContext)
    }

    @objc private func handleLongPress(_ recognizer: UILongPressGestureRecognizer) {
        let animator = animationController.animator!

        switch recognizer.state {
        case .began, .changed:
            if animator.isRunning {
                pause()
            }
        default:
            animator.startAnimation()
            update(animator.fractionComplete)
        }
    }

    @objc private func handleInteraction(_ recognizer: UIPanGestureRecognizer) {
        guard let animator = animationController.animator else { return }

        switch recognizer.state {
        case .began:
            if animator.isRunning {
                pause()
            }

        case .changed:
            let translation = recognizer.translation(in: transitionContext?.containerView)

            switch animationController.operation {
            case .present:
                animator.isReversed = true
            case .dismiss:
                animator.isReversed = false
            }

            let percentComplete: CGFloat

            switch animationController.direction {
            case .left:
                percentComplete = animator.fractionComplete - (translation.x / distance())
            case .right:
                percentComplete = animator.fractionComplete + (translation.x / distance())
            case .top:
                percentComplete = animator.fractionComplete - (translation.y / distance())
            case .bottom:
                percentComplete = animator.fractionComplete + (translation.y / distance())
            }

            animator.fractionComplete = min(percentComplete, 1.0)

            update(animator.fractionComplete)

            panGestureRecognizer.setTranslation(.zero, in: transitionContext?.containerView)
        case .ended:
            interactionEnded()

        default: break
        }
    }

    /// The total distance of the presented view from the edge of the screen.
    /// Accounts for the direction of the animation/interaction.
    private func distance() -> CGFloat {
        guard
            let context = transitionContext,
            let viewController = context.viewController(forKey: animationController.operation.contextViewControllerKey)
            else { return 0.0 }

        let frame: CGRect
        switch animationController.operation {
        case .present:
            frame = context.finalFrame(for: viewController)
        case .dismiss:
            frame = context.initialFrame(for: viewController)
        }

        switch animationController.direction {
        case .left:
            return frame.maxX
        case .right:
            return context.containerView.frame.width - frame.minX
        case .top:
            return frame.maxY
        case .bottom:
            return context.containerView.frame.height - frame.minY
        }
    }

    private func interactionEnded() {
        guard
            let animator = animationController.animator,
            let context = transitionContext
            else { return }

        let operation = animationController.operation
        let direction = animationController.direction
        let fractionComplete = animator.fractionComplete
        let totalDuration = animator.duration

        let distance = self.distance()
        let velocity = panGestureRecognizer.velocity(in: context.containerView)
        let directionalVelocity = direction.isVertical ? velocity.y : velocity.x
        let magnitude = abs(directionalVelocity)

        let shouldFinish: Bool
        switch direction {
        case .left, .top:
            shouldFinish = directionalVelocity < 0
        case .right, .bottom:
            shouldFinish = directionalVelocity > 0
        }

        switch operation {
        case .present:

            if shouldFinish {
                completionSpeed = completionSpeedPositive(duration: totalDuration, velocity: magnitude, distance: distance)
                    .clamped(to: completionSpeedRange(duration: totalDuration, fractionComplete: fractionComplete))

                cancel()
            } else {
                completionSpeed = completionSpeedNegative(duration: totalDuration, velocity: magnitude, distance: distance, fractionComplete: fractionComplete)
                    .clamped(to: completionSpeedRange(duration: totalDuration, fractionComplete: fractionComplete))

                finish()
            }
        case .dismiss:

            if shouldFinish {
                completionSpeed = completionSpeedPositive(duration: totalDuration, velocity: magnitude, distance: distance)
                    .clamped(to: completionSpeedRange(duration: totalDuration, fractionComplete: fractionComplete))

                finish()
            } else {
                completionSpeed = completionSpeedNegative(duration: totalDuration, velocity: magnitude, distance: distance, fractionComplete: fractionComplete)
                    .clamped(to: completionSpeedRange(duration: totalDuration, fractionComplete: fractionComplete))

                cancel()
            }
        }
    }

    /**
     Multiplier to slow down all completions to give a more natual feel.
     Timing curve can distort time making the animation seem too fast.
     */
    private let completionSpeedMultiplier: CGFloat = 0.95

    /**
     Calculates the completion speed for the animation taking velocity and distance into consideration.

     ```1/x ((1 - fractionComplete) duration) = ((1 - fractionComplete) distance) / velocity```

     - x: The completion speed, we are solving for this.
     - ((1 - f) t): Is how completionSpeed is calculated by UIPercentDrivenInteractiveTransition.
     - ((1 - f) d) / v: Calculates the number of seconds the animation should take.

     - Note: (completionSpeed = x), (duration = t), (fractionComplete = f), (distance = d), (velocity = v)

     Simplifies to `x = (t v) / d`
     */
    private func completionSpeedPositive(duration: TimeInterval, velocity: CGFloat, distance: CGFloat) -> CGFloat {
        let speed = (CGFloat(duration) * velocity) / distance
        return speed * completionSpeedMultiplier
    }

    /**
     Similar to `completionSpeedPositive`, difference is (1 - f) on the RHS is now just (f).
     
     ```1/x ((1 - fractionComplete) duration) = (fractionComplete distance) / velocity```

     - Note: (completionSpeed = x), (duration = t), (fractionComplete = f), (distance = d), (velocity = v)

     Simplifies to `x = -(t (f - 1) v) / (f d)`
     */
    private func completionSpeedNegative(duration: TimeInterval, velocity: CGFloat, distance: CGFloat, fractionComplete: CGFloat) -> CGFloat {
        let speed = -((CGFloat(duration) * (fractionComplete - 1) * velocity) / (fractionComplete * distance))
        return speed * completionSpeedMultiplier
    }

    private func completionSpeedRange(duration: TimeInterval, fractionComplete: CGFloat) -> ClosedRange<CGFloat> {
        return ClosedRange<CGFloat>(uncheckedBounds: (
            maximumCompletionSpeed(duration: duration, fractionComplete: fractionComplete),
            minimumCompletionSpeed(duration: duration, fractionComplete: fractionComplete))
        )
    }

    /// Calculates completionSpeed for 0.2 seconds which is the minimum completion time.
    private func minimumCompletionSpeed(duration: TimeInterval, fractionComplete: CGFloat) -> CGFloat {
        return -5 * CGFloat(duration) * (fractionComplete - 1)
    }

    /// Calculates completionSpeed for 1.5 seconds which is the maximum completion time.
    private func maximumCompletionSpeed(duration: TimeInterval, fractionComplete: CGFloat) -> CGFloat {
        return (-2 / 3) * CGFloat(duration) * (fractionComplete - 1)
    }
}

@available(iOS 10, *)
extension SwipeInteractionController: UIGestureRecognizerDelegate {

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
