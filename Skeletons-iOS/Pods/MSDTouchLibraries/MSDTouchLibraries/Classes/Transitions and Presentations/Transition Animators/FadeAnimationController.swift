//
//  FadeAnimationController.swift
//
//  Created by Graham Burgsma on 2018-06-26.
//

import UIKit

/// Transition animation that simply fades the presented view.
@available(iOS 10, *)
open class FadeAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    /// Duration for the total animation
    public var duration: TimeInterval = 0.3

    public let operation: TransitionOperation

    public init(operation: TransitionOperation) {
        self.operation = operation
    }

    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let view = transitionContext.view(forKey: operation.contextViewKey) else { return }

        let changes: () -> Void

        switch operation {
        case .present:
            transitionContext.containerView.addSubview(view)

            view.alpha = 0
            changes = {
                view.alpha = 1
            }

        case .dismiss:
            changes = {
                view.alpha = 0
            }
        }

        let duration = transitionDuration(using: transitionContext)
        let animator = UIViewPropertyAnimator(duration: duration, curve: .easeInOut, animations: changes)

        animator.addCompletion { position in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        animator.startAnimation()
    }
}
