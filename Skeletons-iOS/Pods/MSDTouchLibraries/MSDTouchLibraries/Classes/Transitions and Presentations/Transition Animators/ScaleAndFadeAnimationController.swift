//
//  ScaleAndFadeAnimationController.swift
//
//  Created by Graham Burgsma on 2018-06-26.
//

import UIKit

/// Transition animation that scales and fades similar to a UIAlertController presentation animation.
@available(iOS 10, *)
open class ScaleAndFadeAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    /// Duration for the total animation
    public var duration: TimeInterval = 0.4

    public let operation: TransitionOperation

    public init(operation: TransitionOperation) {
        self.operation = operation
    }

    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let view = transitionContext.view(forKey: operation.contextViewKey) else { return }

        let changes: () -> Void
        let duration = transitionDuration(using: transitionContext)

        switch operation {
        case .present:
            transitionContext.containerView.addSubview(view)

            view.alpha = 0
            view.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)

            changes = {
                view.transform = .identity
            }

            UIViewPropertyAnimator(duration: duration / 2, curve: .easeOut) {
                view.alpha = 1
            }.startAnimation()

        case .dismiss:
            changes = {
                view.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }

            UIViewPropertyAnimator(duration: duration / 2, curve: .easeOut) {
                view.alpha = 0
            }.startAnimation()
        }

        let animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1.0, animations: changes)

        animator.addCompletion { position in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        animator.startAnimation()
    }
}
