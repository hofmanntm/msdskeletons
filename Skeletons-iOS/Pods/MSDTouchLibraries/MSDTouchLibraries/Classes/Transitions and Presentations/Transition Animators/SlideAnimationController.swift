//
//  SlideAnimationController.swift
//
//  Created by Graham Burgsma on 2018-06-21.
//

import UIKit

/// An interruptible transition animator that moves the presented view from off screen into the frame.
@available(iOS 10, *)
open class SlideAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    /// Duration for the total animation
    public var duration: TimeInterval = 0.4

    /// The type of animation operation
    public let operation: TransitionOperation

    /// The edge the view will animate to/from
    public let direction: RectEdge

    public var animator: UIViewPropertyAnimator!

    public init(operation: TransitionOperation, direction: RectEdge) {
        self.operation = operation
        self.direction = direction
    }

    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        interruptibleAnimator(using: transitionContext).startAnimation()
    }

    public func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        if animator == nil {
            setupAnimator(using: transitionContext)
        }
        return animator
    }

    public func setupAnimator(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let viewController = transitionContext.viewController(forKey: operation.contextViewControllerKey),
            let view = transitionContext.view(forKey: operation.contextViewKey)
        else { return }

        let finalFrame = transitionContext.finalFrame(for: viewController)
        let duration = transitionDuration(using: transitionContext)
        let changes: () -> Void

        switch operation {
        case .present:
            transitionContext.containerView.addSubview(view)

            view.frame = offscreenFrame(using: transitionContext, finalFrame: finalFrame)

            changes = {
                view.frame = transitionContext.finalFrame(for: viewController)
            }
        case .dismiss:
            changes = {
                view.frame = self.offscreenFrame(using: transitionContext, finalFrame: finalFrame)
            }
        }

        animator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1.0, animations: changes)

        animator.addCompletion { position in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }

    private func offscreenFrame(using transitionContext: UIViewControllerContextTransitioning, finalFrame: CGRect) -> CGRect {
        var frame = finalFrame

        switch direction {
        case .left:
            frame.origin.x = -frame.width
        case .top:
            frame.origin.y = -frame.height
        case .bottom:
            frame.origin.y = transitionContext.containerView.frame.height
        case .right:
            frame.origin.x = transitionContext.containerView.frame.width
        }

        return frame
    }
}
