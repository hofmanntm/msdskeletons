//
//  RectEdge.swift
//
//  Created by Graham Burgsma on 2018-06-27.
//

/// Describes each side of a rectangle.
public enum RectEdge {
    case left, top, bottom, right
}

extension RectEdge {

    var isHorizontal: Bool {
        return self == .left || self == .right
    }

    var isVertical: Bool {
        return self == .top || self == .bottom
    }
}
