//
//  EdgeModalPresentationController.swift
//
//  Created by Graham Burgsma on 2018-05-23.
//
// Example usage: https://bitbucket.org/mindsea/mindseakit/src/master/

import UIKit

/// Positions a presented view "attached" to an edge of the parent view.
open class EdgeModalPresentationController: DimmingPresentationController {

    let edge: RectEdge

    public init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?, edge: RectEdge) {
        self.edge = edge
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }

    open override var frameOfPresentedViewInContainerView: CGRect {
        var margins = contentMargins

        if #available(iOS 11.0, *), let insets = containerView?.safeAreaInsets {
            margins += insets
        }

        switch edge {
        case .left: margins.left = 0
        case .top: margins.top = 0
        case .bottom: margins.bottom = 0
        case .right: margins.right = 0
        }

        let parentFrame = containerView!.bounds
        let availableSize = parentFrame.inset(by: margins).size
        let preferredSize = size(forChildContentContainer: presentedViewController,
                                 withParentContainerSize: availableSize)

        let width = min(preferredSize.width, availableSize.width)
        let height = min(preferredSize.height, availableSize.height)

        let newSize = CGSize(width: width, height: height)
        let origin: CGPoint

        func divide(_ dividend: CGFloat, by divisor: CGFloat = 2) -> CGFloat {
            return (dividend / divisor).pixelRounded()
        }

        switch edge {
        case .left:
            origin = CGPoint(x: 0,
                             y: divide(parentFrame.height - height))
        case .top:
            origin = CGPoint(x: divide(parentFrame.width - width),
                             y: 0)
        case .bottom:
            origin = CGPoint(x: divide(parentFrame.width - width),
                             y: parentFrame.height - height)
        case .right:
            origin = CGPoint(x: parentFrame.width - width,
                             y: divide(parentFrame.height - height))
        }

        return CGRect(origin: origin, size: newSize)
    }
}
