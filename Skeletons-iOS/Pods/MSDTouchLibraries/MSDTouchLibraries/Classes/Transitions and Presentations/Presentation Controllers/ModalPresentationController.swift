//
//  ModalPresentationController.swift
//
//  Created by Graham Burgsma on 2018-05-23.
//
// Example usage: https://bitbucket.org/mindsea/mindseakit/src/master/

import UIKit

/// Acts as the base controller for a modal presentations.
open class ModalPresentationController: UIPresentationController {

    /// Presented view should be placed within these insets.
    /// Defaults to UIEdgeInsets.zero
    open var contentMargins: UIEdgeInsets = .zero

    /// Defines which of the four corners receives the masking when using
    /// `cornerRadius` property. Defaults to all four corners.
    open var maskedCorners: UIRectCorner = .allCorners

    /// Corner radius applied to the presented view
    open var cornerRadius: CGFloat = 0

    open override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)

        if container === presentedViewController {
            containerView?.setNeedsLayout()
        }
    }

    open override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        return container.preferredContentSize
    }

    open override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }

    open override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()

        presentedView?.layer.round(corners: maskedCorners, radius: cornerRadius)
    }
}
