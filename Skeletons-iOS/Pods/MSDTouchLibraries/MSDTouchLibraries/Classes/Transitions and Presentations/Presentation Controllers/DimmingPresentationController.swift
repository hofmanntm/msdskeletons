//
//  DimmingPresentationController.swift
//
//  Created by Graham Burgsma on 2018-06-26.
//
// Example usage: https://bitbucket.org/mindsea/mindseakit/src/master/

import UIKit

/// A presentation controller that adds a dimming view to the transition container.
open class DimmingPresentationController: ModalPresentationController {

    /// Background color applied to the dimming view.
    /// Defaults to black with 40% alpha
    open var dimmingViewColor = UIColor(white: 0.0, alpha: 0.4)

    private lazy var tapRecognizer: UITapGestureRecognizer = {
        return UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
    }()

    /// Dismiss the presented view controller when dimming view is tapped.
    /// Defaults to `true`.
    open var dismissOnTap: Bool = true {
        didSet {
            tapRecognizer.isEnabled = dismissOnTap
        }
    }

    private let dimmingView = UIView()

    public override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)

        dimmingView.addGestureRecognizer(tapRecognizer)
    }

    @objc func handleTapGesture() {
        presentedViewController.dismiss(animated: true)
    }

    open override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()

        guard let containerView = containerView else { return }

        containerView.addSubview(dimmingView)
        dimmingView.frame = containerView.bounds
        dimmingView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        dimmingView.backgroundColor = dimmingViewColor

        dimmingView.alpha = 0.0
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 1.0
        })
    }

    open override func presentationTransitionDidEnd(_ completed: Bool) {
        if !completed {
            dimmingView.removeFromSuperview()
        }
    }

    open override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0.0
        })
    }

    open override func dismissalTransitionDidEnd(_ completed: Bool) {
        if completed {
            dimmingView.removeFromSuperview()
        }
    }
}
