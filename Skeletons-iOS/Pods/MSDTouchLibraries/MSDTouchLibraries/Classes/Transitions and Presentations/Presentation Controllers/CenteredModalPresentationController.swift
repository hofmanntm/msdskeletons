//
//  CenteredModalPresentationController.swift
//
//  Created by Graham Burgsma on 2018-06-26.
//
// Example usage: https://bitbucket.org/mindsea/mindseakit/src/master/

import UIKit

/// Useful for alert-like views or other modal views centered in the parent with a dimmed background.
open class CenteredModalPresentationController: DimmingPresentationController {

    /// Custom init to set content margins to default of (16, 16, 16, 16)
    override public init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)

        contentMargins = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }

    open override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = containerView else {
            return super.frameOfPresentedViewInContainerView
        }

        var margins = contentMargins

        if #available(iOS 11.0, *) {
            margins += containerView.safeAreaInsets
        }

        let parentFrame = containerView.bounds
        let availableSize = parentFrame.inset(by: margins).size
        let preferredSize = size(forChildContentContainer: presentedViewController,
                                 withParentContainerSize: availableSize)

        let width = min(preferredSize.width, availableSize.width)
        let height = min(preferredSize.height, availableSize.height)
        let x = (parentFrame.width - width) / 2
        let y = (parentFrame.height - height) / 2

        return CGRect(x: x.pixelRounded(),
                      y: y.pixelRounded(),
                      width: width,
                      height: height)
    }
}
