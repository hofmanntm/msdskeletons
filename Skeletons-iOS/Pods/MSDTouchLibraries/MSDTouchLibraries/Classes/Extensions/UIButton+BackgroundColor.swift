//
//  UIButton+BackgroundColor.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2019-02-26.
//  Copyright © 2019 MindSea Development Inc. All rights reserved.
//

import UIKit

extension UIButton {

    /**
     Sets the background color to use for the specified button state.

     This is more effective than other approaches of applying different background colors in different states, all of which rely on attempting to set the `backgroundColor` property at appropriate times. Come to think of it, you might get weird results if you try to set `backgroundColor` while also using `setBackgroundColor(_:for:)`.

     Internally, this makes an image out of the provided color and calls `setBackgroundImage(_:for:)`, which will wipe out any background image previously set for `state`. Because this is just a thin wrapper over setting a background image, there's no corresponding `backgroundColor(for:)` getter method.

     - Note: If you intend to (or have already) set `layer.cornerRadius` to apply a corner radius, you may need to set `layer.masksToBounds = true` to ensure that your corner radius is applied to the background you set here.
     */
    public func setBackgroundColor(_ color: UIColor?, for state: UIControl.State) {
        setBackgroundImage(color?.makeImage(), for: state)
    }
}
