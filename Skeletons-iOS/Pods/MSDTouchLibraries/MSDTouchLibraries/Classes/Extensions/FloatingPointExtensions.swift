//
//  FloatingPointExtensions.swift
//  MSDTouchLibraries
//
//  Created by Graham Burgsma on 2018-06-28.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

public enum PixelRoundingRule {
    case nearest, floor, ceiling
}

public extension FloatingPoint {

    /// Returns this value rounded to be pixel perfect using the rule and scale provided
    /// - Parameter rule: The pixel rounding rule, defaults to `nearest`
    /// - Parameter scale: The scale to use, defaults to `UIScreen.main.scale`
    func pixelRounded(_ rule: PixelRoundingRule = .nearest, scale: CGFloat = UIScreen.main.scale) -> Self {
        guard let scale = scale as? Self else {
            Log.e("Could not cast scale of type CGFloat to type \(Self.self)")
            return self
        }

        switch rule {
        case .nearest:
            return (self * scale).rounded() / scale
        case .floor:
            return floor((self + .ulpOfOne) * scale) / scale
        case .ceiling:
            return ceil((self - .ulpOfOne) * scale) / scale
        }
    }

    /// Rounds this value to be pixel perfect using the rule and scale provided
    /// - Parameter rule: The pixel rounding rule, defaults to `nearest`
    /// - Parameter scale: The scale to use, defaults to `UIScreen.main.scale`
    mutating func pixelRound(_ rule: PixelRoundingRule = .nearest, scale: CGFloat = UIScreen.main.scale) {
        self = pixelRounded(rule, scale: scale)
    }
}
