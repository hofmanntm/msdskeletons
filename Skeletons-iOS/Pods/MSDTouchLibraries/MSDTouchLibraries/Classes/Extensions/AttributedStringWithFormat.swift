//
//  AttributedStringWithFormat.swift
//  Krakow
//
//  Created by Nolan Waite on 2018-04-17.
//

import Foundation

extension NSMutableAttributedString {

    /**
     Creates an attributed string based on `format` and `attributes`, replacing each instance of `%@` with the next element in `arguments` and each instance of `%n$@` with element `n` in `arguments`.

     It is a programmer error if:

     - There are more instances of `%@` in the format string than elements in `arguments`.
     - `n` is less than `1` or greater than `arguments.count` for an instance of `%n$@`.
     */
    public convenience init(format: String, attributes: [NSAttributedString.Key: Any], arguments: NSAttributedString...) {
        self.init(string: format, attributes: attributes)

        func makeScanner(scanLocation: Int) -> Scanner {
            let scanner = Scanner(string: string)
            scanner.charactersToBeSkipped = nil
            scanner.caseSensitive = true
            scanner.scanLocation = scanLocation
            return scanner
        }

        var scanner = makeScanner(scanLocation: 0)
        var argumentIterator = arguments.makeIterator()
        while scanner.scanUpTo("%", into: nil) {
            let percentLocation = scanner.scanLocation
            scanner.scanString("%", into: nil)

            var scannedIndex: Int = NSNotFound

            if scanner.scanString("%", into: nil) {
                // replace %% with single %
                mutableString.replaceCharacters(in: NSRange(percentLocation ..< scanner.scanLocation), with: "%")
                scanner = makeScanner(scanLocation: percentLocation + 1)
            } else if scanner.scanString("@", into: nil) {
                // replace %@ with next argument
                guard let argument = argumentIterator.next() else {
                    fatalError("had at least \(arguments.count + 1) parameters in a format string but only \(arguments.count) arguments were provided")
                }

                replaceCharacters(in: NSRange(percentLocation ..< scanner.scanLocation), with: argument)
                scanner = makeScanner(scanLocation: percentLocation + argument.length)
            } else if
                scanner.scanInt(&scannedIndex),
                scanner.scanString("$", into: nil),
                scanner.scanString("@", into: nil) {

                let argument = arguments[scannedIndex - 1]
                replaceCharacters(in: NSRange(percentLocation ..< scanner.scanLocation), with: argument)
                scanner = makeScanner(scanLocation: percentLocation + argument.length)
            }
        }
    }
}
