//
//  UIColor+MakeImage.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2019-02-26.
//  Copyright © 2019 MindSea Development Inc. All rights reserved.
//

import UIKit

extension UIColor {

    /**
     Returns a 1×1 resizable image of this color.

     Useful when you just want to use a color, but some interface demands an image.

     - Seealso: `UIButton.setBackgroundColor(_:for:)`.
     */
    public func makeImage() -> UIImage {
        let size = CGSize(width: 1, height: 1)

        UIGraphicsBeginImageContextWithOptions(size, false, 1)
        defer { UIGraphicsEndImageContext() }

        setFill()
        UIRectFill(CGRect(origin: .zero, size: size))

        /*
         The documentation says "If the current context is `nil` or was not created by a call to `UIGraphicsBeginImageContext(_:)`, this function returns `nil`."

         The documentation does *not* say that that's an exhaustive list of reasons for `UIGraphicsGetImageFromCurrentImageContext()` to return `nil`.

         However, iOS 10's `UIGraphicsImageRenderer.image(actions:)` method returns a non-optional `UIImage`, so I feel reasonably safe force-unwrapping this.

         (We should probably move to `UIGraphicsImageRenderer` once we drop iOS 9 support.)
         */
        return UIGraphicsGetImageFromCurrentImageContext()!.resizableImage(withCapInsets: .zero)
    }
}
