//
//  CALayerExtensions.swift
//
//  Created by Graham Burgsma on 2018-06-27.
//

import UIKit

public extension CALayer {

    func round(corners: UIRectCorner, radius: CGFloat) {
        if #available(iOS 11.0, *) {
            maskedCorners = CACornerMask(rawValue: corners.rawValue)
            cornerRadius = radius
        } else {
            let maskPath = UIBezierPath(roundedRect: bounds,
                                        byRoundingCorners: corners,
                                        cornerRadii: CGSize(width: radius, height: radius))

            let shape = CAShapeLayer()
            shape.path = maskPath.cgPath
            mask = shape
        }

        masksToBounds = true
    }
}
