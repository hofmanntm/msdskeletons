//
//  ComparableExtensions.swift
//
//  Created by Graham Burgsma on 2018-06-26.
//

public extension Comparable {

    func clamped(to range: ClosedRange<Self>) -> Self {
        return min(max(self, range.lowerBound), range.upperBound)
    }

    mutating func clamp(to range: ClosedRange<Self>) {
        self = min(max(self, range.lowerBound), range.upperBound)
    }
}
