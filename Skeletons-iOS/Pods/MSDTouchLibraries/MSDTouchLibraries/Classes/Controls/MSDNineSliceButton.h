//
//  MSDNineSliceButton.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 10-11-04.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/*!
 * This is a simple class that treats its background image
 * in all control states to be a nine-sliced image with the
 * middle pixel stretched in each direction.
 *
 * Useful to make a stretchable button in IB.
 */
@interface MSDNineSliceButton : UIButton

@end

NS_ASSUME_NONNULL_END
