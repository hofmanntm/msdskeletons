//
//  MSDNineSliceButton.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 10-11-04.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDNineSliceButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDNineSliceButton()


- (void)prepareImageForState:(UIControlState)state;

@end



@implementation MSDNineSliceButton

- (nullable id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self prepareImageForState:UIControlStateNormal];
        [self prepareImageForState:UIControlStateHighlighted];
        [self prepareImageForState:UIControlStateSelected];
        [self prepareImageForState:UIControlStateDisabled];
        [self setBackgroundImage:[self backgroundImageForState:UIControlStateSelected]
                        forState:UIControlStateSelected | UIControlStateHighlighted];
    }
    return self;
}

- (void)prepareImageForState:(UIControlState)state {
    UIImage *image = [self backgroundImageForState:state];
    if (image.leftCapWidth || image.topCapHeight) {
        return;
    }
    CGFloat leftCap = roundf((image.size.width - 1) / 2.0f);
    CGFloat topCap = roundf((image.size.height - 1) / 2.0f);
    if (leftCap == 0 || topCap == 0) {
        return;
    }
    
    UIImage *nineSliceImage = [image stretchableImageWithLeftCapWidth:leftCap topCapHeight:topCap];
    
    [self setBackgroundImage:nineSliceImage forState:state];
}

- (void)setBackgroundImage:(nullable UIImage *)image forState:(UIControlState)state {
    [super setBackgroundImage:image forState:state];
    if (!image || image.leftCapWidth || image.topCapHeight) {
        return;
    }
    [self prepareImageForState:state];
}


@end

NS_ASSUME_NONNULL_END
