//
//  StringWithEmboldeningAsterisks.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2018-02-07.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

public extension NSMutableAttributedString {

    /**
     Creates an attributed string by turning text surrounded by double-asterisks into bold text. Said asterisks are removed from the string.

     For example, the string `**hello**` turns into the attributed string `hello` with an `NSAttributedStringKey.font` set to a bold font for the entire string.

     Emboldening works on pairs of double-asterisks, so the string `hello **friend** it **is** me` will turn into an attributed string with two bold chunks and no asterisks.
     */
    convenience init(stringWithEmboldeningAsterisks string: String, attributes: [NSAttributedString.Key: Any] = [:], boldFont: UIFont? = nil) {
        var asteriskedRanges: [NSRange] = []
        let scanner = Scanner(string: string)
        scanner.charactersToBeSkipped = nil

        while !scanner.isAtEnd {
            scanner.scanUpTo("**", into: nil)
            let start = scanner.scanLocation
            guard scanner.scanString("**", into: nil) else { break }

            scanner.scanUpTo("**", into: nil)
            guard scanner.scanString("**", into: nil) else { break }
            let end = scanner.scanLocation

            asteriskedRanges.append(NSRange(location: start, length: end - start))
        }

        guard !asteriskedRanges.isEmpty else {
            self.init(string: string, attributes: attributes)
            return
        }

        let string = NSMutableString(string: string)
        for range in asteriskedRanges.reversed() {
            let withoutAsterisks = NSRange(location: range.location + 2, length: range.length - 4)
            string.replaceCharacters(in: range, with: string.substring(with: withoutAsterisks))
        }

        self.init(string: string as String, attributes: attributes)

        guard !self.string.isEmpty else { return }

        let font = attribute(.font, at: 0, effectiveRange: nil) as? UIFont
            ?? UIFont.systemFont(ofSize: UIFont.systemFontSize)
        let bold: UIFont = {
            if let specifiedBoldFont = boldFont {
                return specifiedBoldFont
            } else {
                return font.fontDescriptor.withSymbolicTraits(.traitBold)
                    .map { UIFont(descriptor: $0, size: font.pointSize) }
                    ?? UIFont.boldSystemFont(ofSize: font.pointSize)
            }
        }()

        var offset = 0
        for range in asteriskedRanges {
            let boldRange = NSRange(location: range.location + offset, length: range.length - 4)
            addAttribute(.font, value: bold, range: boldRange)
            offset -= 4
        }
    }
}
