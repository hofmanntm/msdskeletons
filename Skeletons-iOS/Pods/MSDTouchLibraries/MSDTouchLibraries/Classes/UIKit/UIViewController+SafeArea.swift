//
//  UIViewController+SafeArea.swift
//  MSDTouchLibraries
//
//  Created by Graham Burgsma on 2018-06-20.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

extension UIView {
    
    var hasSafeArea: Bool {
        if #available(iOS 11.0, *) {
            return safeAreaInsets != .zero
        } else {
            return false
        }
    }
    
    var effectiveSafeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return safeAreaInsets
        } else {
            return .zero
        }
    }
}
