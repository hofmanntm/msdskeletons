//
//  MSDPullToRefreshStatusView.m
//
//  Created by Bill Wilson on 11-03-04.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDPullToRefreshStatusView.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDPullToRefreshStatusView

- (void)layoutForStatus:(nullable MSDStatus *)status
                visible:(BOOL)visible {
    
    NSString *pullStatus = (status.userInfo)[MSDPullToRefreshStatusKey];
    CGAffineTransform transform = CGAffineTransformMakeScale(-1, 1);
    transform = CGAffineTransformRotate(transform, M_PI/2);
    if ([pullStatus isEqual:MSDPullToRefreshStatusLetGo]) {
        transform = CGAffineTransformRotate(transform, M_PI);
    }
    self.arrowView.transform = transform;
    
    if (status.inProgress) {
        self.arrowView.alpha = 0.0;
    }
    else {
        self.arrowView.alpha = 1.0;
    }
    [super layoutForStatus:status visible:visible];
}

@end

NS_ASSUME_NONNULL_END
