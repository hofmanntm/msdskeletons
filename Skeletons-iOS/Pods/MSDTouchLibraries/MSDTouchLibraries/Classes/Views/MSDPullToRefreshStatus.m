//
//  MSDPullToRefreshStatus.h
//  HIOClient
//
//  Created by Bill Wilson on 11-03-04.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

NSString * const MSDPullToRefreshStatusKey = @"MSDPullToRefreshStatusKey";

NSString * const MSDPullToRefreshStatusPullMore = @"MSDPullToRefreshStatusPullMore";
NSString * const MSDPullToRefreshStatusLetGo = @"MSDPullToRefreshStatusLetGo";

NS_ASSUME_NONNULL_END
