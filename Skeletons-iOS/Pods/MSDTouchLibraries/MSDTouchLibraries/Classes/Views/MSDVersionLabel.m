//
//  MSDVersionLabel.m
//
//  Created by Jesse Rusak on 11-03-05.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDVersionLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDVersionLabel ()

@property (nonatomic, assign) BOOL commitHashVisible;

@end

@implementation MSDVersionLabel

- (instancetype)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self addTapGestureRecognizer];
        self.text = [[self class] versionText];
    }
    return self;
}

- (nullable id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self addTapGestureRecognizer];
        self.text = [[self class] versionText];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.text = [[self class] versionText];
}

- (void)addTapGestureRecognizer {
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleCommitHash)]];
    self.userInteractionEnabled = YES;
}

- (void)toggleCommitHash {
    self.commitHashVisible = !self.commitHashVisible;
    
    if (self.commitHashVisible) {
        NSString *commitHash = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MSDVCSCommit"];
        if (commitHash) {
            if ([commitHash hasSuffix:@"-dirty"]) {
                commitHash = [[commitHash substringToIndex:MIN(7, commitHash.length)] stringByAppendingString:@"-dirty"];
            } else {
                commitHash = [commitHash substringToIndex:MIN(7, commitHash.length)];
            }
            
            self.text = [[[self class] versionText] stringByAppendingFormat:@" (%@)", commitHash];
        }
    } else {
        NSRange range = [self.text rangeOfString:@"(" options:NSBackwardsSearch];
        self.text = [[[self class] versionText] substringWithRange:NSMakeRange(0, range.location-1)];
    }
}

+ (NSString *)versionText {
    NSBundle *mainBundle = [NSBundle mainBundle];
    
    NSString *appName = [mainBundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *friendlyVersion = [mainBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    NSString *rawVersion = [mainBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *buildDate = [mainBundle objectForInfoDictionaryKey:@"MSDBuildDate"];
    
    if (!friendlyVersion) {
        friendlyVersion = rawVersion;
        rawVersion = nil;
    }
    
    NSString *version;
    if (rawVersion && friendlyVersion) {
        version = [NSString stringWithFormat:@"%@ %@ (%@)", appName, friendlyVersion, rawVersion];
    } else if (friendlyVersion) {
        version = [NSString stringWithFormat:@"%@ %@", appName, friendlyVersion];
    } else {
        version = appName;
    }
    
    if (buildDate) {
        version = [version stringByAppendingFormat:@", %@", buildDate];
    }
    
    return version;
}

@end

NS_ASSUME_NONNULL_END
