//
//  HybridWebViewDelegate.swift
//  Galveston
//
//  Created by Michael Burke on 2016-07-19.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

import CoreGraphics

public protocol HybridWebViewDelegate: class {
    
    /// Optional; default implementation does nothing.
    func hybridWebView(_ hybridWebView: HybridWebView, nativeViewNamed name: String, didChangeSize newSize: CGSize)
    
    /// Optional; default implementation does nothing.
    func hybridWebView(_ hybridWebView: HybridWebView, didAddNativeViewNamed name: String, withSize newSize: CGSize)
    
    /// Optional; default implementation does nothing.
    func hybridWebView(_ hybridWebView: HybridWebView, didRemoveNativeViewNamed name: String)

    /// Optional; default implementation does nothing.
    func hybridWebView(_ hybridWebView: HybridWebView, willDisplayNativeViewNamed name: String)

    /// Optional; default implementation does nothing.
    func hybridWebView(_ hybridWebView: HybridWebView, didEndDisplayingNativeViewNamed name: String)
}

public extension HybridWebViewDelegate {
    
    func hybridWebView(_ hybridWebView: HybridWebView, nativeViewNamed name: String, didChangeSize newSize: CGSize) {}
    func hybridWebView(_ hybridWebView: HybridWebView, didAddNativeViewNamed name: String, withSize newSize: CGSize) {}
    func hybridWebView(_ hybridWebView: HybridWebView, didRemoveNativeViewNamed name: String) {}
    func hybridWebView(_ hybridWebView: HybridWebView, willDisplayNativeViewNamed name: String) {}
    func hybridWebView(_ hybridWebView: HybridWebView, didEndDisplayingNativeViewNamed name: String) {}
}
