//
//  MSDVersionLabel.h
//
//  Created by Jesse Rusak on 11-03-05.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/*!
 * This is a UILabel that by default shows the current app
 * version as APP_NAME VERSION_STRING (VERSION_NUMBER)
 */
NS_SWIFT_NAME(VersionLabel)
@interface MSDVersionLabel : UILabel

@property (class, readonly, nonatomic) NSString *versionText;

@end

NS_ASSUME_NONNULL_END
