//
//  UITextView+MSDFontName.h
//  Pods
//
//  Created by Joel Glanfield on 2014-05-28.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName;

@end

NS_ASSUME_NONNULL_END
