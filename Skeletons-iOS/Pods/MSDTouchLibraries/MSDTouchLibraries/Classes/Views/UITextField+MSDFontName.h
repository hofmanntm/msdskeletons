//
//  UITextField+MSDFontName.h
//
//  Created by Matthew FitzGerald-Chamberlain on 2/11/2014.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName;

@end

NS_ASSUME_NONNULL_END
