//
//  UITextField+MSDFontName.m
//
//  Created by Matthew FitzGerald-Chamberlain on 2/11/2014.
//
//

#import "UITextField+MSDFontName.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UITextField (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end

NS_ASSUME_NONNULL_END
