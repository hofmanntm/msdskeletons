//
//  MSDLoadingHUD.h
//
//  Created by Jesse Rusak on 10-04-25.
//  Copyright 2010 MindSea Development Inc.. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDStatusView.h"

NS_ASSUME_NONNULL_BEGIN

@class MSDLoadingHUD;

typedef void(^MSDLoadingHUDCompletion)(MSDLoadingHUD * _Nonnull);

NS_SWIFT_NAME(LoadingHUD)
@interface MSDLoadingHUD : MSDStatusView

- (instancetype)initWithSuperview:(UIView *)superview;

- (void)beginLoadWithMessage:(NSString *)loadingMessage;
- (void)beginLoadWithMessage:(NSString *)loadingMessage animated:(BOOL)animated;

- (void)endLoadAnimated:(BOOL)animated;

- (void)endLoadAnimated:(BOOL)animated withIcon:(nullable UIImage *)icon message:(nullable NSString *)message completion:(nullable MSDLoadingHUDCompletion)completion;

/// Shows the specified message and icon briefly.
- (void)flashMessage:(nullable NSString *)message withIcon:(nullable UIImage *)icon animated:(BOOL)animated;

@property (class, readonly, nonatomic) UIImage *checkIcon;
@property (class, readonly, nonatomic) UIImage *xIcon;

@property (nonatomic, strong, nullable) IBOutlet UIImageView *iconView;

/// The amount of time we show a message for while ending a load
@property (nonatomic, assign) NSTimeInterval messageShowingInterval;

/**
 Loads from contentNibName, with standard background.

 You can override this in a subclass to change where the content is loaded from.
*/
- (void)loadContent;

@property (readonly, nonatomic) NSString *contentNibName;

/// Default is MSDTouchLibraries bundle.
@property (readonly, nonatomic) NSBundle *contentNibBundle;

@end

NS_ASSUME_NONNULL_END
