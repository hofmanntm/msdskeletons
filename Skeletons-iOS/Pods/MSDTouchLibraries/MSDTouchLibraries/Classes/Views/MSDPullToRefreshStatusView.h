//
//  MSDPullToRefreshStatusView.h
//
//  Created by Bill Wilson on 11-03-04.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDStatusView.h"
#import "MSDPullToRefreshStatus.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(PullToRefreshStatusView)
@interface MSDPullToRefreshStatusView : MSDStatusView

@property (nonatomic, strong, nullable) IBOutlet UIView *arrowView;

@end

NS_ASSUME_NONNULL_END
