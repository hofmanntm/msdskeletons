//
//  MSDMapView.h
//
//  Created by Jesse Rusak on 10-08-26.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MSDMapView : MKMapView<MKMapViewDelegate>

- (void)removeAllAnnotationsIncludingUserLocation:(BOOL)removeUserAnnotation;  
- (NSArray*)allAnnotationsIncludingUserLocation:(BOOL)includeUserAnnotaion;

- (MKCoordinateRegion)regionThatIncludesAllAnnotationsWithPadding;
- (MKCoordinateRegion)regionThatIncludesAnnotationsWithPadding:(NSArray*)annotations_;

- (void)setRegionThatIncludesAllAnnotationsWithViewPadding:(CGSize)padding animated:(BOOL)animated;
- (void)setRegionThatIncludesAnnotations:(NSArray*)annotations withViewPadding:(CGSize)padding animated:(BOOL)animated;

- (MKCoordinateRegion)regionThatIncludesAllAnnotations;
- (MKCoordinateRegion)regionThatIncludesAnnotations:(NSArray*)annotations_;

// WARNING: This only works when we're zoomed in enough
// that changing the center coordinate doesn't affect the span 
// We can fix this by adding more projection math, or doing the calculations in
// map points (iOS 4 only)
- (void)setCenterCoordinateToShowViewForAnnotation:(id <MKAnnotation>)annotation withPadding:(CGSize)padding animated:(BOOL)animated;

// This acts just like MKMapView's selectAnnotation:animated, except that if you pass YES
// for centeringIfNotVisible, and the annotation is not visible, 
// it will move the map to center the passed annotation, and select it when it is able
- (void)selectAnnotation:(id <MKAnnotation>)annotation centeringIfNotVisible:(BOOL)centering animated:(BOOL)animated;

@property(nonatomic, assign) CLLocationDegrees msdMinimumPaddingDegrees;

@property(nonatomic, weak) IBOutlet id<MKMapViewDelegate> msdMapViewDelegate;

@end

NS_ASSUME_NONNULL_END
