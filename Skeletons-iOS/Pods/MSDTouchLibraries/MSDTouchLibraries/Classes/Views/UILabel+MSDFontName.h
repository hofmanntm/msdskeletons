//
//  UILabel+MSDFontName.h
//  Sable
//
//  Created by John Arnold on 2013-01-18.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// This category provides a way to set custom fonts from IB using User Defined Runtime Attributes.
@interface UILabel (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName;

@end

NS_ASSUME_NONNULL_END
