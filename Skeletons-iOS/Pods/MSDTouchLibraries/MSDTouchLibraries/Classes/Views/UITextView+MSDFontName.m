//
//  UITextView+MSDFontName.m
//  Pods
//
//  Created by Joel Glanfield on 2014-05-28.
//
//

#import "UITextView+MSDFontName.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UITextView (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName {

    BOOL editable = self.editable;

    self.editable = YES;
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
    self.editable = editable;
}

@end

NS_ASSUME_NONNULL_END
