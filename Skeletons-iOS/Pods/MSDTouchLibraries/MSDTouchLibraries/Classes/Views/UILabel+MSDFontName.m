//
//  UILabel+MSDFontName.m
//  Sable
//
//  Created by John Arnold on 2013-01-18.
//
//

#import "UILabel+MSDFontName.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UILabel (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName {
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end

NS_ASSUME_NONNULL_END
