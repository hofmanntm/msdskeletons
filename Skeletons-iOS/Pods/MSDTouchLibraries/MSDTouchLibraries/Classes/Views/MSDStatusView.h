//
//  MSDStatusView.h
//
//  Created by Jesse Rusak on 11-01-27.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDStatus.h"
@class MSDStatusView;

NS_ASSUME_NONNULL_BEGIN

// Common keys recognized by MSDStatusViews in userInfo

// An NSNumber with either YES or NO, indicating 
// if the status view should show success or failure.
extern NSString * const MSDStatusViewSuccessKey;

// An NSNumber indicating how long this status should
// be displayed for, at a minimum. Overrides the status view's
// own minimum display time.
extern NSString * const MSDStatusViewMinimumDisplayTimeKey;


/*!
 * Delegate for the MSDStatusView class
 */
NS_SWIFT_NAME(StatusViewDelegate)
@protocol MSDStatusViewDelegate<NSObject>

@optional
/*!
 * Called when the status view lays itself out based
 * on the content to be displayed. You can use this to
 * re-position or otherwise modify the status view based
 * on its new bounds and internal layout.
 *
 * Note that there is no guarantee that this will be called
 * for a status unless it was shown immediately.
 *
 * This may be called in a UIView animation block, so you should
 * use animatable properties if possible.
 */
- (void)statusViewDidLayout:(MSDStatusView *)statusView;

@end


/*!
 * @abstract Lets you show a status message with an optional button and spinner.
 *
 * @discussion Call a "show" method to make the view appear.
 * If there is an action callback and text, it will also show a button 
 * that can be tapped to run that callback. If the status is marked as
 * inProgress, it will also show a spinner.
 *
 * When you show the status view, it may delay based on previous
 * calls to show. 	
 *
 * The status view should be instantiated in a XIB with its outlets
 * assigned to subviews. If you use the same view repeatedly, you 
 * might want to make a separate XIB that contains only the view and
 * its contents.
 *
 * By default, subviews are simply set to alpha = 0 when not in use.
 * Subclasses might choose to resize the subviews or their own bounds depending 
 * on contents by overriding layoutForShowingStatus.
 */
NS_SWIFT_NAME(StatusView)
@interface MSDStatusView : UIView

/*!
 * Shows the given status, after optionally waiting for the minimumDisplayTime to
 * pass with any statuses that we previously shown.
 *
 * Note that there's no guarantee the status will actually be shown if immediately
 * is NO, as statuses can be skipped if the view is removed from the window or 
 * another status is shown immediately before this one has a chance to display.
 *
 * @param status The status to display
 * @param immediately If NO, we wait for all previous statuses to show for their 
 *                    minimum display time first.
 * @param animated If YES, we animate any changes to the view.
 */
- (void)showStatus:(nullable MSDStatus *)status
       immediately:(BOOL)immediately
          animated:(BOOL)animated;

/*!
 * Convenience method for showing a status. Animated.
 */
- (void)showStatusWithLocalizedTitle:(nullable NSString *)title
                    localizedMessage:(nullable NSString *)message
                          inProgress:(BOOL)inProgress;

/*!
 * Convenience method for showing a status. Animated.
 */
- (void)showStatusWithLocalizedTitle:(nullable NSString *)title
                    localizedMessage:(nullable NSString *)message
                 localizedActionText:(nullable NSString *)actionText
                              action:(nullable void (^)(MSDStatus *status))actionBlock;

/*!
 * Hides this status view. 
 * Equivalent to [self showStatus:nil immediately:NO animated:animated]
 */
- (void)hideAnimated:(BOOL)animated;

/*!
 * Called as needed to layout our subviews and change our own bounds
 * when status is to be displayed. 
 * The default implementation fills in the information from the
 * showingStatus to the various views and sets unused views to an alpha of 0,
 * (except for the activity indicator, which it simply starts or stops)
 * and our own alpha to 0 if visible == NO. (Note that you can have a status
 * and also be invisible if we're laying out for appearing or disappearing.)
 * You can override this to change subview position or the
 * status view's own bounds/alpha as desired.
 * 
 * Do not override layoutSubviews, as it is used to fire appropriate
 * delegate callbacks. Override this instead, which is called in 
 * layoutSubviews.
 */
- (void)layoutForStatus:(nullable MSDStatus *)status visible:(BOOL)visible;

/*!
 * Called when an animation wrapped around layoutForStatus:visible:
 * finishes, passing the status and visible flag used. Note that
 * another status may already have been layed out at this point.
 */
- (void)layoutAnimationDidFinishForStatus:(nullable MSDStatus *)status visible:(BOOL)visible;

/*!
 * Can be overridden in subclasses to do something after a status is
 * hidden or skipped. This will always be called for every status, even
 * if they never appear. 
 */
- (void)statusDidEnd:(nullable MSDStatus *)status;

/*!
 * The minimum time to show a status. Defaults to a reasonable value.
 */
@property (nonatomic, assign) NSTimeInterval minimumDisplayTime;

/*!
 * The duration used for appearing/disappearing/reconfiguring animations.
 */
@property (nonatomic, assign) NSTimeInterval animationDuration;

/*!
 * The status currently visible, or nil. KVO-compliant.
 */
@property (nonatomic, readonly, strong, nullable) MSDStatus *showingStatus;

/*! 
 * The last status which will be displayed after waiting
 * for other statuses to meet the minimum time. KVO-compliant.
 */
@property (nonatomic, readonly, nullable) MSDStatus *mostRecentStatus;

/*!
 * True iff showingStatus != nil. KVO-compliant.
 */
@property (nonatomic, readonly, getter=isShowing) BOOL showing;

// Set these in your XIB
@property (nonatomic, weak) IBOutlet id<MSDStatusViewDelegate> delegate;

@property (nonatomic, strong, nullable) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong, nullable) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong, nullable) IBOutlet UIButton *actionButton;
@property (nonatomic, strong, nullable) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

NS_ASSUME_NONNULL_END
