//
//  UITableViewCell+MSDSelectedBackgroundColor.m
//  MSDTouchLibraries
//
//  Created by Matthew FitzGerald-Chamberlain on 2014-07-09.
//  Copyright 2014 MindSea Development Inc. All rights reserved.
//

#import "UITableViewCell+MSDSelectedBackgroundColor.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UITableViewCell (MSDSelectedBackgroundColor)

- (nullable UIColor *)selectedBackgroundColor_msd {
    return self.selectedBackgroundView.backgroundColor;
}

- (void)setSelectedBackgroundColor_msd:(nullable UIColor *)backgroundColor {
    if (backgroundColor) {
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        backgroundView.backgroundColor = backgroundColor;
        backgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.selectedBackgroundView = backgroundView;
    } else {
        self.selectedBackgroundView = nil;
    }
}

@end

NS_ASSUME_NONNULL_END
