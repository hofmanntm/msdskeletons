//
//  MSDTouchForwardingView.m
//
//  Created by Jesse Rusak on 10-07-29.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDTouchForwardingView.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDTouchForwardingView

- (nullable UIView *)hitTest:(CGPoint)point withEvent:(nullable UIEvent *)event {
	UIView *hitTestResult = [super hitTest:point withEvent:event];
    
	if (self.targetView && hitTestResult == self) {
        // Follow the superclass implementation of hitTest:withEvent: and avoid
        // returning views that shouldn't be getting events
        if (self.targetView.userInteractionEnabled == NO ||
            self.targetView.hidden ||
            self.targetView.alpha < 0.01) {
            return nil;
        }
		// check target sub-views to see who wants this, if anyone
		for (UIView *subview in self.targetView.subviews) {
			CGPoint subviewPoint = [subview convertPoint:point fromView:self];
			UIView *subviewHitTestResult = [subview hitTest:subviewPoint withEvent:event];
			if (subviewHitTestResult) {
				return subviewHitTestResult;
			}
		}
		// no subviews of targetview want this, send it to targetview
		return self.targetView;			
	}
	
	return hitTestResult;
}

@end

NS_ASSUME_NONNULL_END
