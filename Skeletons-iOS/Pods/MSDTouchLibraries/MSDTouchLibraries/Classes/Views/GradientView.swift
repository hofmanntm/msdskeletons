//
//  GradientView.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2017-02-10.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
 A view that draws a gradient across its bounds, from top to bottom.
 
 Because the gradient is drawn by a `CAGradientLayer` acting as the backing layer of the view, UIKit animations work as expected when resizing the view or changing its colors. (As opposed to simply adding a `CAGradientLayer` as a sublayer, which animates outside of UIKit.)
 
 This should really be `@IBDesignable`, and it would be except for some [rude interaction with CocoaPods](https://github.com/CocoaPods/CocoaPods/issues/5678).
 */
open class GradientView: UIView {
    
    @IBInspectable open var topColor: UIColor? {
        didSet { updateColors() }
    }
    
    @IBInspectable open var bottomColor: UIColor? {
        didSet { updateColors() }
    }
    
    open override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        updateColors()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        updateColors()
    }
    
    private var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    private func updateColors() {
        let colors = [
            topColor ?? bottomColor?.withAlphaComponent(0) ?? .clear,
            bottomColor ?? topColor?.withAlphaComponent(0) ?? .clear,
            ]
        gradientLayer.colors = colors.map { $0.cgColor }
    }
}
