//
//  MSDStatusView.m
//
//  Created by Jesse Rusak on 11-01-27.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDStatusView.h"

NS_ASSUME_NONNULL_BEGIN

NSString * const MSDStatusViewSuccessKey = @"MSDStatusViewSuccessKey";
NSString * const MSDStatusViewMinimumDisplayTimeKey = @"MSDStatusViewMinimumDisplayTimeKey";

@interface MSDStatusView() {
    BOOL shouldAnimateChanges;
}

@property (nonatomic, strong, nullable) MSDStatus *showingStatus;
@property (nonatomic, strong) NSMutableArray *pendingStatusInfoQueue;

@property (nonatomic, strong, nullable) NSTimer *nextShowTimer;

// These are used to pass to layoutForStatus:visible:; they are usually
// the same as showingStatus and (showingStatus != nil), but they are
// modified during animations to produce nice appearance and disappearance
// (ie they allow the view to prepare its internals before appearing, and
// to leave them unchanged when disappearing)
@property (nonatomic, strong, nullable) MSDStatus *statusForLayout;
@property (nonatomic, assign) BOOL visibleForLayout;

- (void)clearPendingStatuses;

- (void)cancelNextShowTimer;
- (void)startNextShowTimer;

@end

static const NSTimeInterval StatusBubbleMinimumDisplayTime = 0.75;
static const NSTimeInterval StatusBubbleAnimationDuration = 0.3;

static const NSString *StatusBubbleStatusKey = @"StatusBubbleStatusKey";
static const NSString *StatusBubbleAnimatedKey = @"StatusBubbleAnimatedKey";

static const NSString *AnimationStatusKey = @"status";
static const NSString *AnimationVisibleKey = @"visible";


@implementation MSDStatusView

#pragma mark -
#pragma mark KVO

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
    
    if ([key isEqualToString:@"showing"]) {
        keyPaths = [[NSSet setWithObject:@"showingStatus"] setByAddingObjectsFromSet:keyPaths];
    } else if ([key isEqualToString:@"mostRecentStatus"]) {
        keyPaths = [[NSSet setWithObject:@"status"] setByAddingObjectsFromSet:keyPaths];
    }
    return keyPaths;
}

#pragma mark -
#pragma mark Lifecycle

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        self.minimumDisplayTime = StatusBubbleMinimumDisplayTime;
        self.animationDuration = StatusBubbleAnimationDuration;
        self.pendingStatusInfoQueue = [NSMutableArray array];
        self.alpha = 0.0f;
        self.actionButton.hidden = YES;
        self.titleLabel.hidden = YES;
        self.messageLabel.hidden = YES;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.minimumDisplayTime = StatusBubbleMinimumDisplayTime;
        self.animationDuration = StatusBubbleAnimationDuration;
        self.pendingStatusInfoQueue = [NSMutableArray array];
        self.alpha = 0.0f;
        self.actionButton.hidden = YES;
        self.titleLabel.hidden = YES;
        self.messageLabel.hidden = YES;
    }
    return self;
}

#pragma mark -
#pragma mark Core API

- (void)showStatus:(nullable MSDStatus *)status
       immediately:(BOOL)immediately
          animated:(BOOL)animated {

    NSArray *statusesNeedingDidEndCallbacks = nil;
    
    if (immediately) {
        // We defer these until after the internalShowStatus call below so that
        // the self.showingStatus gets its statusDidEnd first.
        statusesNeedingDidEndCallbacks = [self.pendingStatusInfoQueue copy];

        [self clearPendingStatuses];
        [self cancelNextShowTimer];
    } 
    
    if(self.nextShowTimer == nil && [self.pendingStatusInfoQueue count] == 0) {
        [self internalShowStatus:status animated:animated];
    } else {
        [self queueStatus:status animated:animated];
    }
    
    for (MSDStatus *status in statusesNeedingDidEndCallbacks) {
        [self statusDidEnd:status];
    }
}

- (void)showStatusWithLocalizedTitle:(nullable NSString *)title
                    localizedMessage:(nullable NSString *)message
                          inProgress:(BOOL)inProgress {
    [self showStatus:[MSDStatus statusWithLocalizedTitle:title
                                        localizedMessage:message
                                     localizedActionText:nil
                                                  action:nil
                                              inProgress:inProgress
                                                userInfo:nil]
         immediately:NO
            animated:YES];
}

- (void)showStatusWithLocalizedTitle:(nullable NSString *)title
                    localizedMessage:(nullable NSString *)message
                 localizedActionText:(nullable NSString *)actionText
                              action:(nullable void (^)(MSDStatus *status))actionBlock{
    [self showStatus:[MSDStatus statusWithLocalizedTitle:title
                                        localizedMessage:message
                                     localizedActionText:actionText
                                                  action:actionBlock
                                              inProgress:NO
                                                userInfo:nil]
         immediately:NO
            animated:YES];
}


- (void)hideAnimated:(BOOL)animated {
    [self showStatus:nil immediately:NO animated:animated];
}

#pragma mark -
#pragma mark Layout

- (void)layoutForStatus:(nullable MSDStatus *)status
                visible:(BOOL)visible {
    self.alpha = visible ? 1.0f : 0.0f;
    
    if (status.localizedTitle) {
        self.titleLabel.text = status.localizedTitle;
        self.titleLabel.alpha = 1.0f;
        self.titleLabel.hidden = NO;
    } else {
        self.titleLabel.alpha = 0.0f;
    }

    if (status.localizedMessage) {
        self.messageLabel.text = status.localizedMessage;
        self.messageLabel.alpha = 1.0f;
        self.messageLabel.hidden = NO;
    } else {
        self.messageLabel.alpha = 0.0f;
    }
    
    if (status.localizedActionText) {
        [self.actionButton setTitle:status.localizedActionText forState:UIControlStateNormal];
        self.actionButton.alpha = 1.0f;
        self.actionButton.hidden = NO;
    } else {
        self.actionButton.alpha = 0.0f;
    }

    if (status.inProgress) {
        [self.activityIndicator startAnimating];
    } else {
        [self.activityIndicator stopAnimating];
    }
}

- (void)layoutSubviews {
    void(^animations)(void) = ^(void) {
        [self layoutForStatus:self.statusForLayout
                      visible:self.visibleForLayout];
        [super layoutSubviews];
        
        if ([self.delegate respondsToSelector:@selector(statusViewDidLayout:)]) {
            [self.delegate statusViewDidLayout:self];
        }
    };

    void (^completion)(BOOL) = ^(BOOL finished) {
        if (finished) {
            if (self.titleLabel.alpha <= 0) {
                self.titleLabel.hidden = YES;
            }

            if (self.messageLabel.alpha <= 0) {
                self.messageLabel.hidden = YES;
            }

            if (self.actionButton.alpha <= 0) {
                self.actionButton.hidden = YES;
            }
        }
    };
    
    BOOL animating = shouldAnimateChanges;
    if (animating) {
        
        NSMutableDictionary *context = [NSMutableDictionary dictionaryWithDictionary:@{AnimationVisibleKey: @(self.visibleForLayout)}];
        if (self.statusForLayout) {
            context[AnimationStatusKey] = self.statusForLayout;
        }
        
        [UIView animateWithDuration:self.animationDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionBeginFromCurrentState animations:animations completion:^(BOOL finished) {

            [self layoutAnimationDidStop:nil finished:@(finished) context:context];

            completion(finished);
        }];

    } else {
        animations();
        completion(YES);
    }
}

- (void)layoutAnimationDidStop:(nullable NSString *)animation finished:(NSNumber *)finished context:(NSDictionary *)contextDict {
    
    MSDStatus *status = contextDict[AnimationStatusKey];
    BOOL visible = [contextDict[AnimationVisibleKey] boolValue];
    
    [self layoutAnimationDidFinishForStatus:status visible:visible];
}

- (void)layoutAnimationDidFinishForStatus:(nullable MSDStatus *)status visible:(BOOL)visible {
    // for overriding
}

- (void)statusDidEnd:(nullable MSDStatus *)status {
    // for overriding
}

#pragma mark -
#pragma mark MSDStatus Management

- (void)internalShowStatus:(nullable MSDStatus *)status
                  animated:(BOOL)animated {
    
    [self statusDidEnd:self.showingStatus];
    
    if (!self.showingStatus) {
        // We're appearing; we should position ourselves
        // before animating
        self.showingStatus = status;
        self.statusForLayout = status;
        self.visibleForLayout = NO;
        [self setNeedsLayout];
        [self layoutIfNeeded];
        
        if (self.showingStatus != status) {
            return; // during our callback, we changed what we're showing
        }
    }
    
    self.showingStatus = status;

    if (self.showingStatus) {
        self.statusForLayout = status;
        self.visibleForLayout = YES;
    } else {
        // statusForLayout remains as the old one when disappearing,
        // so we animate to invisible without changing our content
        self.visibleForLayout = NO;
    }
    
    if (animated) {
        shouldAnimateChanges = YES;
        [self setNeedsLayout];
        [self layoutIfNeeded];
        shouldAnimateChanges = NO;
    } else {
        [self setNeedsLayout];
    }
    
    self.statusForLayout = self.showingStatus;
    
    [self startNextShowTimer];
}

- (void)queueStatus:(nullable MSDStatus *)status animated:(BOOL)animated {
    NSMutableDictionary *statusInfo = [NSMutableDictionary dictionary];
    statusInfo[StatusBubbleAnimatedKey] = @(animated);
    if (status) {
        statusInfo[StatusBubbleStatusKey] = status;
    }
    
    [self willChangeValueForKey:@"mostRecentStatus"];
    [self.pendingStatusInfoQueue addObject:statusInfo];
    [self didChangeValueForKey:@"mostRecentStatus"];
}

- (void)clearPendingStatuses {
    [self willChangeValueForKey:@"mostRecentStatus"];
    [self.pendingStatusInfoQueue removeAllObjects];
    [self didChangeValueForKey:@"mostRecentStatus"];
}

- (void)showNextIfNeeded {
    if ([self.pendingStatusInfoQueue count]) {
        [self willChangeValueForKey:@"mostRecentStatus"];
        
        NSDictionary *statusInfo = (self.pendingStatusInfoQueue)[0];
        [self internalShowStatus:statusInfo[StatusBubbleStatusKey]
                        animated:[statusInfo[StatusBubbleAnimatedKey] boolValue]];
        [self.pendingStatusInfoQueue removeObjectAtIndex:0];
        
        [self didChangeValueForKey:@"mostRecentStatus"];
    }
}

#pragma mark -
#pragma mark Timer

- (void)cancelNextShowTimer {
    [self.nextShowTimer invalidate];
    self.nextShowTimer = nil;
}

- (void)startNextShowTimer {
    [self cancelNextShowTimer];
    
    NSTimeInterval toWait;
    
    NSNumber *displayTimeOverride = (self.showingStatus.userInfo)[MSDStatusViewMinimumDisplayTimeKey];
    if (displayTimeOverride) {
        toWait = [displayTimeOverride doubleValue];
    } else {
        toWait = self.minimumDisplayTime;
    }

    if (toWait < 0) {
        toWait = 0;
    }
    
    self.nextShowTimer = [NSTimer scheduledTimerWithTimeInterval:toWait
                                                          target:self
                                                        selector:@selector(nextShowTimerFired:)
                                                        userInfo:nil
                                                         repeats:NO];
}

- (void)nextShowTimerFired:(NSTimer *)timer {
    self.nextShowTimer = nil;
    [self showNextIfNeeded];
}

#pragma mark -
#pragma mark Properties

- (nullable MSDStatus *)mostRecentStatus {
    if ([self.pendingStatusInfoQueue count]) {
        return [self.pendingStatusInfoQueue lastObject][StatusBubbleStatusKey];
    }
    return self.showingStatus;
}

- (BOOL)isShowing {
    return self.showingStatus != nil;
}

#pragma mark -
#pragma mark Actions

- (void)setActionButton:(nullable UIButton *)newButton {
    if (newButton != _actionButton) {
        [_actionButton removeTarget:self action:@selector(actionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        _actionButton = newButton;
        [_actionButton addTarget:self action:@selector(actionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)actionButtonTapped:(UIButton *)button {
    // We ignore button presses if we're about to switch away,
    // because this is confusing for the target of the callback
    if (self.showingStatus == self.mostRecentStatus) {
        [self.showingStatus performAction];
    }
}

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    self.actionButton = nil;
}

@end

NS_ASSUME_NONNULL_END
