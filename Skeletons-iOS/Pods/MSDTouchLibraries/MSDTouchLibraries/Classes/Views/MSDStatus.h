//
//  MSDStatus.h
//  CanaDream
//
//  Created by Jesse Rusak on 11-01-29.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/*!
 * @abstract A status to show the user.
 * @discussion Represents a status to be displayed in a MSDStatusView:
 * a title, message, optional action and an indicator if the status
 * is in progress or not.
 *
 * The action will recieve the sender of the action as its first parameter.
 *
 * You don't usually need to create these directly; instead you can
 * use the convenience methods on MSDStatusView.
 */
@interface MSDStatus : NSObject

+ (instancetype)statusWithLocalizedTitle:(nullable NSString *)title
                        localizedMessage:(nullable NSString *)message
                     localizedActionText:(nullable NSString *)actionText
                                  action:(nullable void (^)(MSDStatus *status))actionBlock
                              inProgress:(BOOL)inProgress
                                userInfo:(nullable NSDictionary *)userInfo;

- (instancetype)initWithLocalizedTitle:(nullable NSString *)title
                      localizedMessage:(nullable NSString *)message
                   localizedActionText:(nullable NSString *)actionText
                                action:(nullable void (^)(MSDStatus *status))actionBlock
                            inProgress:(BOOL)inProgress
                              userInfo:(nullable NSDictionary *)userInfo NS_DESIGNATED_INITIALIZER;

@property (nonatomic, copy, nullable) NSString *localizedTitle;
@property (nonatomic, copy, nullable) NSString *localizedMessage;

@property (nonatomic, copy, nullable) NSString *localizedActionText;
@property (nonatomic, copy, nullable) void (^actionBlock)(MSDStatus *status);
- (void)performAction;

@property (nonatomic, assign, getter=isInProgress) BOOL inProgress;

@property (nonatomic, copy, nullable) NSDictionary *userInfo;

@end

NS_ASSUME_NONNULL_END
