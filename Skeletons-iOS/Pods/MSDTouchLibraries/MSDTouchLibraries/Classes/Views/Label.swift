//
//  Label.swift
//  MSDTouchLibraries
//
//  Created by Aidan Waite on 2017-11-23.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
 Allows for compact line wrapping which draws the text at the minimum width possible without adding new lines
 
 - Seealso: https://trello.com/c/7LezJ9ho
 - Seealso: Based on https://github.com/square/Paralayout
 */
open class Label: UILabel {
    
    public enum LineWrapBehavior {
        /// The same wrapping behaviour as UILabel
        case standard
        /// Draws the text at the minimum width possible without adding new lines
        case compact
    }
    
    open var lineWrapBehavior: LineWrapBehavior = .compact {
        didSet{
            setNeedsDisplay()
        }
    }
    
    override open func drawText(in rect: CGRect) {
        if shouldWrapCompact {
            super.drawText(in: compactTextRect(forBounds: rect))
        } else {
            super.drawText(in: rect)
        }
    }
    
    /// Calculates text bounds for the compact line wrapping
    public func compactTextRect(forBounds bounds: CGRect) -> CGRect {
        var textBounds = bounds
        let sizeThatFits = super.sizeThatFits(textBounds.size)
        
        // If it's a single line label just return
        if numberOfLines == 1 {
            textBounds.size = sizeThatFits
            return textBounds
        }
        
        // If height is unlimited, return the measured height
        if textBounds.height == CGFloat.greatestFiniteMagnitude {
            textBounds.size.height = sizeThatFits.height
            return textBounds
        }
        
        // If the text will get truncated, don't modify the text rect
        if sizeThatFits.height > textBounds.height {
            return textBounds
        }
        
        // If there's no text at all just return
        guard let text = attributedText, text.length > 0 else {
            return textBounds
        }
        
        let maxHeight = textBounds.height
        
        // The minWidth is the longest word that's narrower than the originalWidth
        let originalWidth = bounds.width
        var minWidth: CGFloat = 0.0
        let words = text.nonEmptyComponentsSeparated(by: CharacterSet.whitespacesAndNewlines)
        for word in words {
            let wordWidth = ceil(word.size().width)
            if wordWidth <= originalWidth && wordWidth > minWidth {
                minWidth = wordWidth
            }
        }
        
        // Do a binary search to find the narrowest width that fits the maxHeight
        var bestWidth = originalWidth
        var maxWidth = originalWidth
        
        while minWidth < maxWidth - 1.0 {
            let midWidth: CGFloat = CGFloat(round((Double(minWidth) + Double(maxWidth)) / 2.0))
            let heightAtMidWidth = textHeight(forWidth: midWidth)
            
            if heightAtMidWidth > maxHeight {
                // We got an extra line get bigger
                minWidth = midWidth
            } else {
                // No extra lines get smaller
                maxWidth = midWidth
                bestWidth = midWidth
            }
        }
        
        // Align the text rect within the bounds based on the text alignment
        if textAlignment == .center {
            textBounds.origin.x = textBounds.minX + floor((textBounds.width - bestWidth) / 2.0)
        } else if textAlignment == .right {
            textBounds.origin.x = textBounds.maxX - bestWidth
        }
        
        textBounds.size.width = bestWidth
        return textBounds
    }
    
    private var shouldWrapCompact: Bool {
        guard numberOfLines != 1 else {
            return false
        }
        
        return lineWrapBehavior == .compact
    }
    
    private func textHeight(forWidth width: CGFloat) -> CGFloat {
        let sizeToFit = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        // Change and restore our number of lines to measure the unliimted height of the text
        let numberOfLines = self.numberOfLines
        super.numberOfLines = 0
        
        let height = super.sizeThatFits(sizeToFit).height
        
        super.numberOfLines = numberOfLines
        return height
        
    }
}

private extension NSAttributedString {
    func nonEmptyComponentsSeparated(by separatorSet: CharacterSet) -> [NSAttributedString] {
        var components = [NSAttributedString]()
        let string = self.string as NSString
        let stringLength = string.length
        var searchRange = NSRange(location: 0, length: stringLength)
        
        while searchRange.length > 0 {
            let rangeOfNextSeparator = string.rangeOfCharacter(from: separatorSet, options: [], range: searchRange)
            
            if rangeOfNextSeparator.length == 0 {
                // No more separators remain so add the rest of the string and you're finished
                components.append(attributedSubstring(from: searchRange))
                break
            } else if rangeOfNextSeparator.location > searchRange.location {
                // A separator is further ahead so add the substring up to that point
                let rangeToNextSeparator = NSRange(location: searchRange.location, length: rangeOfNextSeparator.location - searchRange.location)
                components.append((attributedSubstring(from: rangeToNextSeparator)))
            }
            
            // Continue the search from the end of the separator range
            let endOfNextSeparator = NSMaxRange(rangeOfNextSeparator)
            searchRange = NSRange(location: endOfNextSeparator, length: stringLength - endOfNextSeparator)
        }
        
        return components
    }
}
