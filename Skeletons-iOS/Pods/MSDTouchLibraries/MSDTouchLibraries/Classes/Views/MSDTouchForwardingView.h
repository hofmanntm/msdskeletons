//
//  MSDTouchForwardingView.h
//
//  Created by Jesse Rusak on 10-07-29.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(TouchForwardingView)
@interface MSDTouchForwardingView : UIView

@property (nonatomic, strong) IBOutlet UIView *targetView;

@end

NS_ASSUME_NONNULL_END
