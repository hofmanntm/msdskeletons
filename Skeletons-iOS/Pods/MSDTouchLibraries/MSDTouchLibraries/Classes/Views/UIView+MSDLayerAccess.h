//
//  UIView+MSDLayerAccess.h
//  Acapulco
//
//  Created by John Arnold on 2012-12-06.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// This category provides easy access to CALayer attributes so you can use them
// in XIBs, or without having to include QuartzCore.
@interface UIView (MSDLayerAccess)

@property (nullable) UIColor *borderColor_msd;
@property CGFloat borderWidth_msd;
@property CGFloat cornerRadius_msd;
@property (nullable) UIColor *shadowColor_msd;
@property CGSize shadowOffset_msd;
@property float shadowOpacity_msd;
@property CGFloat shadowRadius_msd;

@end

NS_ASSUME_NONNULL_END
