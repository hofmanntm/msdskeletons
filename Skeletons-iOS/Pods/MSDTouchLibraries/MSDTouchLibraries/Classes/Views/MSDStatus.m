//
//  MSDStatus.m
//  CanaDream
//
//  Created by Jesse Rusak on 11-01-29.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDStatus.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDStatus

+ (instancetype)statusWithLocalizedTitle:(nullable NSString *)title
                        localizedMessage:(nullable NSString *)message
                     localizedActionText:(nullable NSString *)actionText
                                  action:(nullable void (^)(MSDStatus *))actionBlock
                              inProgress:(BOOL)inProgress
                                userInfo:(nullable NSDictionary *)userInfo {

    return [[self alloc] initWithLocalizedTitle:title
                               localizedMessage:message
                            localizedActionText:actionText
                                         action:actionBlock
                                     inProgress:inProgress
                                       userInfo:userInfo];
}

- (instancetype)initWithLocalizedTitle:(nullable NSString *)title
                      localizedMessage:(nullable NSString *)message
                   localizedActionText:(nullable NSString *)actionText
                                action:(nullable void (^)(MSDStatus *))actionBlock
                            inProgress:(BOOL)inProgress
                              userInfo:(nullable NSDictionary *)userInfo {

    if (!(self = [super init])) {
        return nil;
    }

    _localizedTitle = [title copy];
    _localizedMessage = [message copy];
    _localizedActionText = [actionText copy];
    _actionBlock = [actionBlock copy];
    _inProgress = inProgress;
    _userInfo = [userInfo copy];

    return self;
}

- (instancetype)init {
    return [self initWithLocalizedTitle:nil
                       localizedMessage:nil
                    localizedActionText:nil
                                 action:nil
                             inProgress:NO
                               userInfo:nil];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p; title = %@; message = %@; actionText = %@; actionBlock = %@; inProgress = %d; userInfo = %@>",
            [self class],
            self,
            self.localizedTitle,
            self.localizedMessage,
            self.localizedActionText,
            self.actionBlock,
            self.inProgress,
            self.userInfo];
}

- (void)performAction {
    if (!self.actionBlock) {
        return;
    }

    self.actionBlock(self);
}

@end

NS_ASSUME_NONNULL_END
