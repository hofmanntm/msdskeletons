//
//  UIButton+MSDFontName.m
//  Sable
//
//  Created by John Arnold on 2013-01-18.
//
//

#import "UIButton+MSDFontName.h"
#import "UILabel+MSDFontName.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UIButton (MSDFontName)

- (void)setFontName_msd:(NSString *)fontName {
    UILabel *label = self.titleLabel;
    [label setFontName_msd:fontName];
}

@end

NS_ASSUME_NONNULL_END
