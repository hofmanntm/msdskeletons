//
//  MSDHairlineView.m
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2015-12-17.
//  Copyright © 2015 MindSea Development Inc. All rights reserved.
//

#import "MSDHairlineView.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDHairlineView

- (CGSize)intrinsicContentSize {
    return CGSizeMake(UIViewNoIntrinsicMetric, CalculateHeight(self));
}

- (CGSize)sizeThatFits:(CGSize)size {
    size.height = CalculateHeight(self);
    return size;
}

static CGFloat CalculateHeight(UIView *view) {
    UIScreen *screen = view.window.screen ?: [UIScreen mainScreen];
    return 1 / MAX(screen.scale, 1);
}

@end

NS_ASSUME_NONNULL_END
