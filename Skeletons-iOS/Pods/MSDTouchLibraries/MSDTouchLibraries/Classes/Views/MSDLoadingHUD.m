//
//  MSDLoadingHUD.m
//
//  Created by Jesse Rusak on 10-04-25.
//  Copyright 2010 MindSea Development Inc.. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "MSDLoadingHUD.h"

NS_ASSUME_NONNULL_BEGIN

static const CGFloat CornerRadius = 20.0f;
static const CGFloat MinimumPadding = 20.0f;

static const NSTimeInterval DefaultMessageShowingInterval = 1.5;
static const NSTimeInterval DefaultAnimationDuration = 0.4;

static NSString * const MSDLoadingHUDIconKey = @"MSDLoadingHUDIconKey";

@interface MSDLoadingHUD()

- (void)loadContent;

@property (nonatomic, copy, nullable) MSDLoadingHUDCompletion endCompletionBlock;
@property (nonatomic, strong) IBOutlet UIView *contentView;


@end

@implementation MSDLoadingHUD {
	CGSize _normalContentSize;
    BOOL _currentlyVisible;
    
    // We want to fire our endCompletionBlock when we disappear,
    // but we don't know if we're going animate or not at layout time,
    // so we keep track of whether we expect an animation and
    // only fire the callback at layout if we're not animating
    BOOL _callCompletionAfterAnimation;
}

#pragma mark -
#pragma mark Initialization

static void commonInit(MSDLoadingHUD *self) {
    [self loadContent];
    self.messageShowingInterval = DefaultMessageShowingInterval;
    self.animationDuration = DefaultAnimationDuration;
}

- (instancetype)initWithSuperview:(UIView *)theSuperview {
    self = [self initWithFrame:theSuperview.bounds];
    [theSuperview addSubview:self];
    return self;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    commonInit(self);
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    commonInit(self);
    return self;
}

- (NSString *)contentNibName {
    if([UIVisualEffectView class]) {
        return @"MSDLoadingHUDContentBlur";
    }
    return @"MSDLoadingHUDContent";
}

- (NSBundle *)contentNibBundle {
    return [NSBundle bundleForClass:[MSDLoadingHUD class]];
}

- (void)loadContent {
    self.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    if ([UIVisualEffectView class]) {
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0];
    }
    
    self.userInteractionEnabled = YES;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.contentNibBundle loadNibNamed:self.contentNibName owner:self options:nil];
    
	_normalContentSize = self.contentView.frame.size;
    [self addSubview:self.contentView];
    self.contentView.layer.cornerRadius = CornerRadius;
}

- (void)layoutSubviews {
	[super layoutSubviews];
	CGFloat contentScale = 1.0f;
	
	CGSize contentViewMaxSize = CGRectInset(self.bounds, MinimumPadding, MinimumPadding).size;
	if (contentViewMaxSize.width < _normalContentSize.width ||
		contentViewMaxSize.height < _normalContentSize.height) {
		contentScale = MIN(contentViewMaxSize.width / _normalContentSize.width,
						   contentViewMaxSize.height / _normalContentSize.height);
	}
	self.contentView.bounds = CGRectIntegral(CGRectMake(0, 0,
                                                        _normalContentSize.width * contentScale,
                                                        _normalContentSize.height * contentScale));
    
	self.contentView.center = CGPointMake(roundf(CGRectGetMidX(self.bounds)), roundf(CGRectGetMidY(self.bounds)));
}

- (void)layoutForStatus:(nullable MSDStatus *)status visible:(BOOL)visible {
    [super layoutForStatus:status visible:visible];

    if (visible) {
        self.contentView.transform = CGAffineTransformIdentity;
    } else {
        // we're hidden; but are we going away, or not showing yet?
        if (_currentlyVisible) {
            // we're going away; animate outwards
            self.contentView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        } else {
            // we're coming in; animated inwards
            self.contentView.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }
    }
    _currentlyVisible = visible;
    
    UIImage *icon = (status.userInfo)[MSDLoadingHUDIconKey];
    if (!icon) {
        NSNumber *success = (status.userInfo)[MSDStatusViewSuccessKey];
        if (success) {
            if ([success boolValue]) {
                icon = [[self class] checkIcon];
            } else {
                icon = [[self class] xIcon];
            }
        }
    }
    
    self.iconView.image = icon;
    
    if (!_callCompletionAfterAnimation && !visible && self.endCompletionBlock) {
        self.endCompletionBlock(self);
        self.endCompletionBlock = nil;
    }
}


- (void)flashMessage:(nullable NSString *)message withIcon:(nullable UIImage *)icon animated:(BOOL)animated {
    if (icon || message) {
        // Flash a message first
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        if (icon) {
            userInfo[MSDLoadingHUDIconKey] = icon;
        }
        userInfo[MSDStatusViewMinimumDisplayTimeKey] = @(self.messageShowingInterval);
        
        MSDStatus *status = [MSDStatus statusWithLocalizedTitle:message
                                               localizedMessage:nil
                                            localizedActionText:nil
                                                         action:nil
                                                     inProgress:NO
                                                       userInfo:userInfo];
        
        [self showStatus:status immediately:NO animated:YES];
    }
    
    [self showStatus:nil immediately:NO animated:animated];

}

#pragma mark -
#pragma mark Begin Loading

- (void)beginLoadWithMessage:(NSString *)loadingMessage animated:(BOOL)animated {
    MSDStatus *status = [MSDStatus statusWithLocalizedTitle:loadingMessage
                                           localizedMessage:nil
                                        localizedActionText:nil
                                                     action:nil
                                                 inProgress:YES
                                                   userInfo:nil];

    [self showStatus:status immediately:NO animated:animated];
}

- (void)beginLoadWithMessage:(NSString *)loadingMessage {
    [self beginLoadWithMessage:loadingMessage animated:NO];
}

#pragma mark -
#pragma mark End Animations

- (void)endLoadAnimated:(BOOL)animated {
    [self endLoadAnimated:animated withIcon:nil message:nil completion:nil];
}

- (void)endLoadAnimated:(BOOL)animated withIcon:(nullable UIImage *)icon message:(nullable NSString *)message completion:(nullable MSDLoadingHUDCompletion)completion {
    if (self.mostRecentStatus) {
        _callCompletionAfterAnimation = animated;
        self.endCompletionBlock = completion;
        [self flashMessage:message withIcon:icon animated:animated];
    }
}

#pragma mark - Capturing when we disappear or something else takes over

- (void)layoutAnimationDidFinishForStatus:(nullable MSDStatus *)status visible:(BOOL)visible {
    if (visible == NO && self.endCompletionBlock) {
        self.endCompletionBlock(self);
        self.endCompletionBlock = nil;
    }
}


#pragma mark -
#pragma mark Misc

static UIImage * LoadImageFromMDSTLBundle(NSString *imageName) {
    if ([[UIImage class] respondsToSelector:@selector(imageNamed:inBundle:compatibleWithTraitCollection:)]) {
        return [UIImage imageNamed:imageName inBundle:[NSBundle bundleForClass:[MSDLoadingHUD class]] compatibleWithTraitCollection:nil];
    } else {
        return [UIImage imageNamed:imageName];
    }
}

+ (UIImage *)checkIcon {
    UIImage *baseImage = LoadImageFromMDSTLBundle(@"MSDLoadingHUDCheckIcon.png");
    return [MSDLoadingHUD tintableBaseImage:baseImage];
}

+ (UIImage *)xIcon {
    UIImage *baseImage = LoadImageFromMDSTLBundle(@"MSDLoadingHUDXIcon.png");
    return [MSDLoadingHUD tintableBaseImage:baseImage];
}

+ (UIImage*)tintableBaseImage:(UIImage *)image {
    if ([UIVisualEffectView class]) {
        image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    
    return image;
}

@end

NS_ASSUME_NONNULL_END
