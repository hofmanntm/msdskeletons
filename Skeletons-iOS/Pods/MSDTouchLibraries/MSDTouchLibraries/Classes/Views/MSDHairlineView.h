//
//  MSDHairlineView.h
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2015-12-17.
//  Copyright © 2015 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 A view whose intrinsic height is one device pixel.
 
 This should really be `IB_DESIGNABLE`, and it would be except for some [rude interaction with CocoaPods](https://github.com/CocoaPods/CocoaPods/issues/5678).
 */
NS_SWIFT_NAME(HairlineView)
@interface MSDHairlineView : UIView

@end

NS_ASSUME_NONNULL_END
