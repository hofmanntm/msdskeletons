//
//  UITableViewCell+MSDSelectedBackgroundColor.h
//  MSDTouchLibraries
//
//  Created by Matthew FitzGerald-Chamberlain on 2014-07-09.
//  Copyright 2014 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableViewCell (MSDSelectedBackgroundColor)

/// Usable from Interface Builder by setting a user-defined runtime attribute.
@property (nullable, nonatomic) UIColor *selectedBackgroundColor_msd;

@end

NS_ASSUME_NONNULL_END
