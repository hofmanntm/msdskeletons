//
//  UIView+MSDLayerAccess.m
//  Acapulco
//
//  Created by John Arnold on 2012-12-06.
//
//

#import "UIView+MSDLayerAccess.h"
#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@implementation UIView (MSDLayerAccess)

- (nullable UIColor *)borderColor_msd {
    CGColorRef borderColor = self.layer.borderColor;
    return borderColor ? [UIColor colorWithCGColor:borderColor] : nil;
}

- (void)setBorderColor_msd:(nullable UIColor *)borderColor_msd {
    self.layer.borderColor = borderColor_msd.CGColor;
}

- (CGFloat)borderWidth_msd {
    return self.layer.borderWidth;
}

- (void)setBorderWidth_msd:(CGFloat)borderWidth_msd {
    self.layer.borderWidth = borderWidth_msd;
}

- (CGFloat)cornerRadius_msd {
    return self.layer.cornerRadius;
}

- (void)setCornerRadius_msd:(CGFloat)cornerRadius_msd {
    self.layer.cornerRadius = cornerRadius_msd;
}

- (nullable UIColor *)shadowColor_msd {
    CGColorRef shadowColor = self.layer.shadowColor;
    return shadowColor ? [UIColor colorWithCGColor:shadowColor] : nil;
}

- (void)setShadowColor_msd:(nullable UIColor *)shadowColor_msd {
    self.layer.shadowColor = shadowColor_msd.CGColor;
}

- (CGSize)shadowOffset_msd {
    return self.layer.shadowOffset;
}

- (void)setShadowOffset_msd:(CGSize)shadowOffset_msd {
    self.layer.shadowOffset = shadowOffset_msd;
}

- (float)shadowOpacity_msd {
    return self.layer.shadowOpacity;
}

- (void)setShadowOpacity_msd:(float)shadowOpacity_msd {
    self.layer.shadowOpacity = shadowOpacity_msd;
}

- (CGFloat)shadowRadius_msd {
    return self.layer.shadowRadius;
}

- (void)setShadowRadius_msd:(CGFloat)shadowRadius_msd {
    self.layer.shadowRadius = shadowRadius_msd;
}

@end

NS_ASSUME_NONNULL_END
