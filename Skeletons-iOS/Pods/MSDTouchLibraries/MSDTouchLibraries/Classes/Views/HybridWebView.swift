//
//  HybridWebView.swift
//  Galveston
//
//  Created by Michael Burke on 2016-07-12.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

import UIKit
import WebKit

/**
    - Note: `HybridWebView` depends on calls to `UIScrollViewDelegate.scrollViewDidScroll`. Please forward these calls if you take over as the web view's scroll view delegate.
 */
public final class HybridWebView: UIView {
    
    fileprivate static let UpdateNativeViewHoldersMessageName = "updateNativeViewHolders"
    
    private(set) public var webView: WKWebView!
    private var webViewConstraints: [NSLayoutConstraint] = []
    
    private var nativeViewHolders: [String: CGRect] = [:]
    private var nativeViews: [String: NativeSubview] = [:]
    private var previouslyVisibleNativeViewNames: Set<String> = []
    
    private var webviewContentHeight: Int = -1
    private var webViewHeightBinding: BlockBinding?
    
    public weak var delegate: HybridWebViewDelegate?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        
        configureWebView()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        configureWebView()
    }
    
    deinit {
        webView.scrollView.delegate = nil
        webViewHeightBinding?.detach()
    }
    
    private func configureWebView() {
        let jsPath = Bundle(for: HybridWebView.self).url(forResource: "MSDHybridWebView", withExtension: "js")!
        let js = try! String(contentsOf: jsPath, encoding: .utf8)
        
        let userContentController = WKUserContentController()
        userContentController.addUserScript(WKUserScript(source: js, injectionTime: .atDocumentStart, forMainFrameOnly: true))
        userContentController.add(WeakScriptMessageHandler(self), name: HybridWebView.UpdateNativeViewHoldersMessageName)
        
        let webViewConfiguration = WKWebViewConfiguration()
        webViewConfiguration.userContentController = userContentController
        
        // WKWebView can't be created from interface builder, so we add it here in code.
        webView = WKWebView(frame: bounds, configuration: webViewConfiguration)
        webView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(webView)
        
        webViewConstraints = [
            NSLayoutConstraint(item: self,
                               attribute: .top,
                               relatedBy: .equal,
                               toItem: webView,
                               attribute: .top,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: self,
                               attribute: .bottom,
                               relatedBy: .equal,
                               toItem: webView,
                               attribute: .bottom,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: self,
                               attribute: .left,
                               relatedBy: .equal,
                               toItem: webView,
                               attribute: .left,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: self,
                               attribute: .right,
                               relatedBy: .equal,
                               toItem: webView,
                               attribute: .right,
                               multiplier: 1,
                               constant: 0)]
        
        NSLayoutConstraint.activate(webViewConstraints)
        webView.scrollView.delegate = self
        webView.scrollView.scrollsToTop = true
        
        webViewHeightBinding = BlockBinding(from: webView.scrollView, keyPaths: [#keyPath(UIScrollView.contentSize)]) { [weak self] (object, keypath) in
            guard let sself = self else { return }

            let currentHeight = Int(sself.webView.scrollView.contentSize.height)

            guard sself.webviewContentHeight != currentHeight else { return }

            sself.webviewContentHeight = currentHeight
            sself.webView.evaluateJavaScript("msdUpdateNativeViewHolders()", completionHandler: nil)
        }
    }

    /// Breaks the retain cycle HybridWebView -> WKWebView -> WKUserContentController -> HybridWebView.
    private final class WeakScriptMessageHandler: NSObject, WKScriptMessageHandler {
        private weak var target: WKScriptMessageHandler?

        init(_ target: WKScriptMessageHandler) {
            self.target = target
        }

        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            target?.userContentController(userContentController, didReceive: message)
        }
    }

    fileprivate func updateVisibleNativeViews() {
        let wasVisible = previouslyVisibleNativeViewNames
        let nowVisible = visibleNativeViewNames

        for name in wasVisible.subtracting(nowVisible) {
            delegate?.hybridWebView(self, didEndDisplayingNativeViewNamed: name)
        }

        for name in nowVisible.subtracting(wasVisible) {
            delegate?.hybridWebView(self, willDisplayNativeViewNamed: name)
        }

        previouslyVisibleNativeViewNames = nowVisible
    }

    private var visibleNativeViewNames: Set<String> {
        let visibleBounds = webView.scrollView.bounds
        var names: Set<String> = []
        for (name, info) in nativeViews where info.view.frame.intersects(visibleBounds) {
            names.insert(name)
        }
        return names
    }
    
    // MARK: Subview management
    
    fileprivate func updateNativeViewHolders(newHolders: [String: [String: Double]]) {
        var updatedNativeViewHolders: [String: CGRect] = [:]
        var newViewHolderNames: [String] = []
        
        for (name, rectDict) in newHolders {
            let newRect = CGRect(clientBoundingRect: rectDict)
            
            if let existingHolderRect = nativeViewHolders[name] {
                // We know about this one already.
                if existingHolderRect != newRect {
                    // This existing holder has a new rect in the DOM.
                    
                    moveViewForViewHolderNamed(name: name, toPosition: newRect)
                    
                    if newRect.size != existingHolderRect.size {
                        delegate?.hybridWebView(self, nativeViewNamed: name, didChangeSize: newRect.size)
                    }
                }
            } else {
                // This is a new native view holder added to the DOM since last call.
                addViewForViewHolderNamed(name: name, atPosition: newRect)
                
                
                delegate?.hybridWebView(self, didAddNativeViewNamed: name, withSize: newRect.size)
                
                newViewHolderNames.append(name)
            }
            
            updatedNativeViewHolders[name] = newRect
        }
        
        let nativeViewHolderKeys = Set(nativeViewHolders.keys)
        let newHolderKeys = Set(newHolders.keys)
        let removedViewHolderNames = nativeViewHolderKeys.subtracting(newHolderKeys)
        for name in removedViewHolderNames {
            
            removeViewFromViewHolderNamed(name)
            delegate?.hybridWebView(self, didRemoveNativeViewNamed: name)
        }

        nativeViewHolders = updatedNativeViewHolders
        
        for name in nativeViewHolders.keys {
            if let alreadyRegistered = nativeViews[name] {
                updateElementSizeIfNecessary(forView: alreadyRegistered.view, named: name)
            }
        }
    }
    
    // MARK: Placing subviews over webview
    
    private func addNativeViewSubview(nativeView: NativeSubview, initialRect: CGRect) {
        nativeView.constraints = NativeSubviewConstraints(view: nativeView.view,
                                                          inView: self.webView.scrollView,
                                                          rect: initialRect,
                                                          options: nativeView.options)
        nativeView.view.isHidden = true
        webView.scrollView.addSubview(nativeView.view)
        NSLayoutConstraint.activate(nativeView.constraints!.constraints)
    }
    
    private func moveViewForViewHolderNamed(name: String, toPosition position: CGRect) {
        guard let nativeView = nativeViews[name] else { return }

        // A UIView actually exists for this view, so let's move it

        guard nativeView.view.superview != nil else {
            fatalInDebug("Attempted to move native view named \(name) but it has no superview")
            return
        }

        guard let constraints = nativeView.constraints else {
            fatalInDebug("Attempted to move native view named \(name) but it has no constraints")
            return
        }

        constraints.xConstraint.constant = position.origin.x
        constraints.yConstraint.constant = position.origin.y
        constraints.widthConstraint?.constant = position.size.width
        constraints.heightConstraint?.constant = position.size.height
        nativeView.view.isHidden = false
    }
    
    private func addViewForViewHolderNamed(name: String, atPosition position: CGRect) {
        guard let nativeView = nativeViews[name] else { return }

        // A UIView actually exists for this view, so let's put it in place

        guard nativeView.view.superview == nil else {
            fatalInDebug("Attempted to place native view named \(name) but it already has a superview")
            return
        }

        guard nativeView.constraints == nil else {
            fatalInDebug("Attempted to place native view named \(name) but it already has constraints")
            return
        }

        addNativeViewSubview(nativeView: nativeView, initialRect: position)
    }
    
    private func removeViewFromViewHolderNamed(_ name: String) {
        guard let nativeView = nativeViews[name] else { return }

        // A UIView actually exists for this view, so let's get rid of it

        guard nativeView.view.superview != nil else {
            fatalInDebug("Attempted to remove native view named \(name) but it has no superview")
            return
        }

        guard nativeView.constraints != nil else {
            fatalInDebug("Attempted to remove native view named \(name) but it has no constraints")
            return
        }

        nativeView.view.removeFromSuperview()
        nativeView.constraints = nil
    }

    private var isAlreadyUpdatingElementSize = false

    private func updateElementSizeIfNecessary(forView view: UIView, named name: String) {
        guard !isAlreadyUpdatingElementSize, let nativeViewHolderRect = nativeViewHolders[name] else { return }

        isAlreadyUpdatingElementSize = true
        defer { isAlreadyUpdatingElementSize = false }

        view.layoutIfNeeded()

        let newSize = view.bounds.size
        let newWidth = newSize.width
        let newHeight = newSize.height

        let jsSafeName = name.escapingForJavaScript
        
        let options = nativeViews[name]!.options
        
        if newWidth != nativeViewHolderRect.size.width
            && (options.contains(.allowNativeViewToOverrideHolderWidth)
                || options.contains(.useOnlyNativeViewWidth))
        {
            let js = "msdSetNativeViewHolderWidth(\(jsSafeName), '\(newWidth)px');"
            webView.evaluateJavaScript(js, completionHandler: { (result, error) in
                if let error = error {
                    Log.w("Error setting native view holder width: \(error)")
                }
            })
        }
        
        if newHeight != nativeViewHolderRect.size.height
            && (options.contains(.allowNativeViewToOverrideHolderHeight)
                || options.contains(.useOnlyNativeViewHeight))
        {
            let js = "msdSetNativeViewHolderHeight(\(jsSafeName), '\(newHeight)px');"
            webView.evaluateJavaScript(js, completionHandler: { (result, error) in
                if let error = error {
                    Log.w("Error setting native view holder height: \(error)")
                }
            })
        }
    }
    
    // MARK: Subview interface
    
    /**
     Normally, the added view will track the size of the native view holder HTML element that it's been added to.

     You can add w/h constraints with priority > 500 to your view and override that behaviour. If you do, then changes to the view's bounds.size will cause HybridWebView to apply width/height CSS styles to the native view holder to match.

     If you don't add w/h constraints to the view, then changes to the HTML element's size will cause the view to resize instead.

     - Note: No delegate methods are called as a result of calling `addSubview(…)`.
     */
    public func addSubview(view: UIView, inNativeViewHolderNamed name: String, options: NativeViewOptions = []) {
        guard nativeViews[name] == nil else {
            fatalInDebug("Requested to add a native view named \(name) but a view with that name already exists")
            return
        }

        // We use constraints to position the view. Maybe not the most neighbourly thing to foricbly turn off autoresizing mask constraints but it avoids otherwise annoying issues. The caller can always restore it.
        view.translatesAutoresizingMaskIntoConstraints = false

        // This binding fires when the view changes, and updates the view holder html element to match the new size of the view.
        // It only does that if the sizes are different, though, so you need to override the w/h constraints of the view or else
        // it will always follow the html element's size.
        let sizeBinding = BlockBinding(from: view.layer, keyPaths: [#keyPath(CALayer.bounds)]) { [weak self, weak view] (object, keypath) in
            guard let view = view else { return }
            self?.updateElementSizeIfNecessary(forView: view, named: name)
        }
        
        let nativeView = NativeSubview(view: view, sizeBinding: sizeBinding, options: options, constraints: nil)
        
        if let rect = nativeViewHolders[name] {
            // A holder already exists, so we can add a subview immediately
            addNativeViewSubview(nativeView: nativeView, initialRect: rect)
        }
        
        nativeViews[name] = nativeView
    }

    /// - Note: No delegate methods are called as a result of calling `addSubview(…)`.
    public func removeSubviewFromNativeViewHolderNamed(name: String) {
        guard let nativeView = nativeViews[name] else {
            fatalInDebug("Requested to remove a native view named \(name) but no such view exists")
            return
        }
        
        if nativeView.view.superview != nil {
            nativeView.view.removeFromSuperview()
        }
        
        nativeView.sizeBinding.detach()
        
        nativeViews[name] = nil
    }

    // MARK: Manual updating

    /**
        Updates position and size for all native views.
     
        HybridWebView attempts to update automatically, but sometimes it can't quite get it right and you might want to manually trigger an update (e.g. after device rotation).
     */
    public func updateNativeViewHolders() {
        webView.evaluateJavaScript("msdUpdateNativeViewHolders()")
    }
}

extension HybridWebView: WKScriptMessageHandler {

    public func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case HybridWebView.UpdateNativeViewHoldersMessageName:
            if let body = message.body as? [String: [String: Double]] {
                updateNativeViewHolders(newHolders: body)
                setNeedsLayout()
                layoutIfNeeded()
            }

            updateVisibleNativeViews()
            
        default:
            Log.w("Received unrecognized script message: \(message.name)")
        }
    }
}

extension HybridWebView: UIScrollViewDelegate {

    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        scrollView.decelerationRate = .normal
    }

    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateVisibleNativeViews()
    }
    
    // Prevent pinch to zoom
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
}

/**
 `NativeViewOptions` indicate how the width and height of a native view should be handled.


 * `allowNativeViewToOverrideHolderWidth` and `allowNativeViewToOverrideHolderHeight`:

     `HybridWebView` will add a low-priority constraint (@ `defaultNativeSubviewConstraintPriority`)
     for the width using the size of the view holder in HTML.

     If the resolved width of the view is different from the HTML view holder size after a
     layout pass, then a 'width' style will be set on the view holder for the new width,
     and updated if the native view's width changes.

     In short: The size specified/computed in HTML is used, but can be overridden.


 * `useOnlyNativeViewWidth` (and `useOnlyNativeViewHeight`):

     `HybridWebView` not add any constraints for width; the native view must maintain its own
     width.

     If the resolved width of the view is different from the HTML view holder size after a
     layout pass, then a 'width' style will be set on the view holder for the new width,
     and updated if the native view's width changes.

     In short: The native view MUST specify a size.  The size computed in HTML is ignored
               and overridden.


 If you don't specify any options for a particular dimension, `HybridWebView` will add a
 required-priority constraint to the view holder, and change it if the HTML view
 holder changes size.  It will never apply a style in HTML, so the HTML element gets
 the final say over what the size will be.

 In short: If you specify no options, the size computed in HTML is enforced.
 */
public struct NativeViewOptions: OptionSet {
    public let rawValue: Int
    public init(rawValue: Int) { self.rawValue = rawValue }
    
    public static let allowNativeViewToOverrideHolderWidth = NativeViewOptions(rawValue: 1)
    public static let allowNativeViewToOverrideHolderHeight = NativeViewOptions(rawValue: 2)
    public static let useOnlyNativeViewWidth = NativeViewOptions(rawValue: 4)
    public static let useOnlyNativeViewHeight = NativeViewOptions(rawValue: 8)
}


private extension CGRect {
    init(clientBoundingRect: [String: Double]) {
        self.init(
            x: clientBoundingRect["x"]!,
            y: clientBoundingRect["y"]!,
            width: clientBoundingRect["width"]!,
            height: clientBoundingRect["height"]!)
    }
}

private let defaultNativeSubviewConstraintPriority = 500

private class NativeSubviewConstraints {
    let xConstraint: NSLayoutConstraint
    let yConstraint: NSLayoutConstraint
    let widthConstraint: NSLayoutConstraint?
    let heightConstraint: NSLayoutConstraint?
    
    var constraints: [NSLayoutConstraint] {
        var constraints = [xConstraint, yConstraint]
        
        if let c = widthConstraint {
            constraints.append(c)
        }
        
        if let c = heightConstraint {
            constraints.append(c)
        }
        
        return constraints
    }
    
    init(view: UIView, inView superview: UIView, rect: CGRect, options: NativeViewOptions) {
        xConstraint = NSLayoutConstraint(item: view,
                                         attribute: .left,
                                         relatedBy: .equal,
                                         toItem: superview,
                                         attribute: .left,
                                         multiplier: 1,
                                         constant: rect.origin.x)
        xConstraint.identifier = "HybridWebView-left"
        
        yConstraint = NSLayoutConstraint(item: view,
                                         attribute: .top,
                                         relatedBy: .equal,
                                         toItem: superview,
                                         attribute: .top,
                                         multiplier: 1,
                                         constant: rect.origin.y)
        yConstraint.identifier = "HybridWebView-top"
        
        if !options.contains(.useOnlyNativeViewWidth) {
            widthConstraint = NSLayoutConstraint(item: view,
                                                 attribute: .width,
                                                 relatedBy: .equal,
                                                 toItem: nil,
                                                 attribute: .notAnAttribute,
                                                 multiplier: 1,
                                                 constant: rect.size.width)
            widthConstraint?.identifier = "HybridWebView-width"
        } else {
            widthConstraint = nil
        }
        
        if !options.contains(.useOnlyNativeViewHeight) {
            heightConstraint = NSLayoutConstraint(item: view,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: nil,
                                                  attribute: .notAnAttribute,
                                                  multiplier: 1,
                                                  constant: rect.size.height)
            heightConstraint?.identifier = "HybridWebView-height"
        } else {
            heightConstraint = nil
        }
        
        
        #if swift(>=4.0)
            xConstraint.priority = UILayoutPriority(Float(defaultNativeSubviewConstraintPriority))
            yConstraint.priority = UILayoutPriority(Float(defaultNativeSubviewConstraintPriority))
            
            let widthConstraintPriority = options.contains(.allowNativeViewToOverrideHolderWidth) ? defaultNativeSubviewConstraintPriority : 1000
            let heightConstraintPriority = options.contains(.allowNativeViewToOverrideHolderHeight) ? defaultNativeSubviewConstraintPriority : 1000
            
            widthConstraint?.priority = UILayoutPriority(Float(widthConstraintPriority))
            heightConstraint?.priority = UILayoutPriority(Float(heightConstraintPriority))
        #else
            xConstraint.priority = Float(defaultNativeSubviewConstraintPriority)
            yConstraint.priority = Float(defaultNativeSubviewConstraintPriority)
            
            let widthConstraintPriority = options.contains(.allowNativeViewToOverrideHolderWidth) ? defaultNativeSubviewConstraintPriority : 1000
            let heightConstraintPriority = options.contains(.allowNativeViewToOverrideHolderHeight) ? defaultNativeSubviewConstraintPriority : 1000
            
            widthConstraint?.priority = Float(widthConstraintPriority)
            heightConstraint?.priority = Float(heightConstraintPriority)
        #endif
    }
}

private class NativeSubview {
    let view: UIView
    let sizeBinding: BlockBinding
    var constraints: NativeSubviewConstraints?
    let options: NativeViewOptions
    
    init(view: UIView, sizeBinding: BlockBinding, options: NativeViewOptions, constraints: NativeSubviewConstraints?) {
        self.view = view
        self.sizeBinding = sizeBinding
        self.constraints = constraints
        self.options = options
    }
}
