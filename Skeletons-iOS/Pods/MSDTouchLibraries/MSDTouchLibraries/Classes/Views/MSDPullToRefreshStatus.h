//
//  MSDPullToRefreshStatus.h
//  HIOClient
//
//  Created by Bill Wilson on 11-03-04.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString * const MSDPullToRefreshStatusKey;

typedef NSString *MSDPullToRefreshStatus NS_EXTENSIBLE_STRING_ENUM;

extern MSDPullToRefreshStatus const MSDPullToRefreshStatusPullMore;
extern MSDPullToRefreshStatus const MSDPullToRefreshStatusLetGo;

NS_ASSUME_NONNULL_END
