//
//  MSDMapView.m
//
//  Created by Jesse Rusak on 10-08-26.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDMapView.h"
#import "MSDTouchLibraryDefines.h"


@interface MSDMapView()

@property (nonatomic, weak, nullable) id<MKAnnotation> annotationToSelect;

@end

@implementation MSDMapView {
    // Delayed selection
    BOOL _shouldAnimatedAnnotationToSelect;
}

#pragma mark -
#pragma mark Delegate Mechanics

- (BOOL)respondsToSelector:(SEL)aSelector {
    BOOL responds = [super respondsToSelector:aSelector] || [self.msdMapViewDelegate respondsToSelector:aSelector];
    return responds;
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    if ([self.msdMapViewDelegate respondsToSelector:[anInvocation selector]]) {
        [anInvocation invokeWithTarget:self.msdMapViewDelegate];
    } else {
        [super forwardInvocation:anInvocation];
    }
}

// We ensure we're always our own delegate
- (void)setDelegate:(nullable id <MKMapViewDelegate>)newDelegate {
    MSDCheck(newDelegate == self || newDelegate == nil, @"The map view delegate must be itself; use msdMapViewDelegate to recieve delegate messages");
    [super setDelegate:newDelegate];
}

- (void)setMsdMapViewDelegate:(id <MKMapViewDelegate>)ourNewDelegate {
    _msdMapViewDelegate = ourNewDelegate;

    // we do this to ensure that the superclass re-caches the
    // results of respondsToSelector: with our new delegate
    [self setDelegate:nil];
    [self setDelegate:self];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views {
    for (MKAnnotationView *view in views) {
        if (view.annotation && view.annotation == self.annotationToSelect) {
            id<MKAnnotation> annotation = self.annotationToSelect;
            self.annotationToSelect = nil;
            [self selectAnnotation:annotation animated:_shouldAnimatedAnnotationToSelect];
        }
    }
    
    if ([self.msdMapViewDelegate respondsToSelector:@selector(mapView:didAddAnnotationViews:)]) {
        [self.msdMapViewDelegate mapView:mapView didAddAnnotationViews:views];
    }
}
         
#pragma mark -
#pragma mark Init

- (void)commonInit {
  //  this needs to be delayed until we get msdMapViewDelegate set; otherwise we
//    don't respond to respondsToSelector: correctly, which is called inside
//    of [super setDelegate:]. Or else we need to call super setDelegate:nil, then
//    with self so that it re-caches the delegate methods when we get a new msdMapViewDelegate
    [super setDelegate:self];
    
    self.msdMinimumPaddingDegrees = 0.05f;
}

- (instancetype)init {
    if ((self = [super init])) {
        [self commonInit];
    }
    return self;
}

- (nullable id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self commonInit];
    }
    return self;
}
         
#pragma mark -
#pragma mark Public API

- (NSArray*)allAnnotationsIncludingUserLocation:(BOOL)includeUserAnnotaion {
    NSMutableArray *filteredAnnotations = [[NSMutableArray alloc] initWithCapacity:[self.annotations count]];
	for (NSObject<MKAnnotation> *annotation in self.annotations) {
		if (includeUserAnnotaion || ![annotation isKindOfClass:[MKUserLocation class]]) {
			[filteredAnnotations addObject:annotation];
		}
	}
    return filteredAnnotations;
}

- (void)removeAllAnnotationsIncludingUserLocation:(BOOL)removeUserAnnotation {
	[self removeAnnotations:[self allAnnotationsIncludingUserLocation:removeUserAnnotation]];
}

- (MKCoordinateRegion)sanityCheckedRegion:(MKCoordinateRegion)region {
    
    // Ensure the center is reasonable
    if (region.center.latitude > 90) {
        MSDDebugLog(@"Latitude out of range: %lf", region.center.latitude);
        region.center.latitude = 90;
    } else if (region.center.latitude < -90) {
        MSDDebugLog(@"Latitude out of range: %lf", region.center.latitude);
        region.center.latitude = -90;
    }
    
    if (region.center.longitude > 180) {
        MSDDebugLog(@"Longitude out of range: %lf", region.center.longitude);
        region.center.longitude = 180;
    } else if (region.center.longitude < -180) {
        MSDDebugLog(@"Longitude out of range: %lf", region.center.longitude);
        region.center.longitude = -180;
    }
    
    // Ensure the span is sane
    if (region.span.latitudeDelta <= 0) {
        MSDDebugLog(@"Latitude delta out of range: %lf", region.span.latitudeDelta);
        region.span.latitudeDelta = self.msdMinimumPaddingDegrees;
    } else if (region.span.latitudeDelta > 180) {
        MSDDebugLog(@"Latitude delta out of range: %lf", region.span.latitudeDelta);
        region.span.latitudeDelta = 180;
    }
    
    if (region.span.longitudeDelta <= 0) {
        MSDDebugLog(@"Longitude delta out of range: %lf", region.span.longitudeDelta);
        region.span.longitudeDelta = self.msdMinimumPaddingDegrees;
    } else if (region.span.longitudeDelta > 360) {
        MSDDebugLog(@"Longitude delta out of range: %lf", region.span.longitudeDelta);
        region.span.longitudeDelta = 360;
    }
    
    // Ensure the span does not go off the edge of the world
    if (region.center.latitude - (region.span.latitudeDelta / 2.0) < -90) {
        // Too low, fit it to -90
        MSDDebugLog(@"Latitude delta off the edge: center %lf, delta %lf", region.center.latitude, region.span.latitudeDelta);
        region.span.latitudeDelta = 2.0 * (90.0 + region.center.latitude);
    } else if (region.center.latitude + (region.span.latitudeDelta / 2.0) > 90) {
        // Too high, fit it to +90
        MSDDebugLog(@"Latitude delta off the edge: center %lf, delta %lf", region.center.latitude, region.span.latitudeDelta);
        region.span.latitudeDelta = 2.0 * (90.0 - region.center.latitude);
    }
    
    return region;
}

- (MKCoordinateRegion)regionThatIncludesAnnotations:(NSArray*)annotations_ {
    CLLocationCoordinate2D topLeftCoordinate;
    topLeftCoordinate.latitude = -90;
    topLeftCoordinate.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoordinate;
    bottomRightCoordinate.latitude = 90;
    bottomRightCoordinate.longitude = -180;
    
    for (NSObject<MKAnnotation> *annotation in annotations_) {
        topLeftCoordinate.longitude = fmin(topLeftCoordinate.longitude, annotation.coordinate.longitude);
        topLeftCoordinate.latitude = fmax(topLeftCoordinate.latitude, annotation.coordinate.latitude);
        
        bottomRightCoordinate.longitude = fmax(bottomRightCoordinate.longitude, annotation.coordinate.longitude);
        bottomRightCoordinate.latitude = fmin(bottomRightCoordinate.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoordinate.latitude - (topLeftCoordinate.latitude - bottomRightCoordinate.latitude) * 0.5;
    region.center.longitude = topLeftCoordinate.longitude + (bottomRightCoordinate.longitude - topLeftCoordinate.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoordinate.latitude - bottomRightCoordinate.latitude); 
    region.span.longitudeDelta = fabs(bottomRightCoordinate.longitude - topLeftCoordinate.longitude); 
    
    region = [self sanityCheckedRegion:region];
    
    return region;
}

- (MKCoordinateRegion)regionThatIncludesAllAnnotations {
    return [self regionThatIncludesAnnotations:self.annotations];
}

- (MKCoordinateRegion)regionThatIncludesAnnotationsWithPadding:(NSArray*)annotations_ {
    MKCoordinateRegion region = [self regionThatIncludesAnnotations:annotations_];
    region.span.latitudeDelta = MAX(self.msdMinimumPaddingDegrees, 1.1 * region.span.latitudeDelta);
    region.span.longitudeDelta = MAX(self.msdMinimumPaddingDegrees, 1.1 * region.span.longitudeDelta);
    
    region = [self sanityCheckedRegion:region];
    
    return region;
}

- (MKCoordinateRegion)regionThatIncludesAllAnnotationsWithPadding {
    return [self regionThatIncludesAnnotationsWithPadding:self.annotations];
}

- (void)setRegionThatIncludesAllAnnotationsWithViewPadding:(CGSize)padding animated:(BOOL)animated {
    [self setRegionThatIncludesAnnotations:self.annotations withViewPadding:padding animated:animated];
}

- (void)setRegionThatIncludesAnnotations:(NSArray*)annotations withViewPadding:(CGSize)padding animated:(BOOL)animated {
    
    MKCoordinateRegion region = [self regionThatIncludesAnnotations:annotations];
    
    // Set our region so that we can calculate padding in view coordinates. If we're animating, we need to set it back
    // to the old region before animating to the final result.
    MKCoordinateRegion oldRegion = self.region;
    
    // Apply padding in view coordinates
    CGRect rect = [self convertRegion:region toRectToView:self];
    rect = CGRectInset(rect, -padding.width, -padding.height);
    region = [self convertRect:rect toRegionFromView:self];
    
    region = [self sanityCheckedRegion:region];
    
    if (animated) {
        [self setRegion:oldRegion animated:NO];
    }
    
    [self setRegion:region animated:animated];
}

- (void)setCenterCoordinateToShowViewForAnnotation:(id<MKAnnotation>)annotation withPadding:(CGSize)padding animated:(BOOL)animated {
    MKAnnotationView *view = [self viewForAnnotation:annotation];
    if (view) {
        // We convert it to our bounds first, so the padding is applied in our coordinate system
        CGRect targetBounds = [self convertRect:view.bounds fromView:view];
        targetBounds.origin.x -= padding.width / 2;
        targetBounds.size.width += padding.width;
        
        targetBounds.origin.y -= padding.height / 2;
        targetBounds.size.height += padding.height;
        
        MKCoordinateRegion requiredRegion = [self convertRect:targetBounds toRegionFromView:self];
        [self setRegion:requiredRegion animated:YES];
        
        // This seems to be different than [self region] for some reason
        MKCoordinateRegion currentRegion = [self convertRect:self.bounds toRegionFromView:self];
        
        // Find change required; may not need any
        CLLocationDegrees latitudeDelta = 0, longitudeDelta = 0;
        if (requiredRegion.center.latitude > currentRegion.center.latitude) {
            // might need to increase latitude
            CLLocationDegrees requiredMaxLatitude = requiredRegion.center.latitude + requiredRegion.span.latitudeDelta / 2.0f;
            CLLocationDegrees currentMaxLatitude = currentRegion.center.latitude + currentRegion.span.latitudeDelta / 2.0f;
            
            latitudeDelta = MAX(0, requiredMaxLatitude - currentMaxLatitude);
        } else {
            // might need to decrease latitude
            CLLocationDegrees requiredMinLatitude = requiredRegion.center.latitude - requiredRegion.span.latitudeDelta / 2.0f;
            CLLocationDegrees currentMinLatitude = currentRegion.center.latitude - currentRegion.span.latitudeDelta / 2.0f;
            latitudeDelta = MIN(0, requiredMinLatitude - currentMinLatitude);
        }
        
        if (requiredRegion.center.longitude > currentRegion.center.longitude) {
            // might need to increase longitude
            CLLocationDegrees requiredMaxLongitude = requiredRegion.center.longitude + requiredRegion.span.longitudeDelta / 2.0f;
            CLLocationDegrees currentMaxLongitude = currentRegion.center.longitude + currentRegion.span.longitudeDelta / 2.0f;
            
            longitudeDelta = MAX(0, requiredMaxLongitude - currentMaxLongitude);
        } else {
            // might need to decrease longitude
            CLLocationDegrees requiredMinLongitude = requiredRegion.center.longitude - requiredRegion.span.longitudeDelta / 2.0f;
            CLLocationDegrees currentMinLongitude = currentRegion.center.longitude - currentRegion.span.longitudeDelta / 2.0f;
            longitudeDelta = MIN(0, requiredMinLongitude - currentMinLongitude);
        }
        
        if (latitudeDelta || longitudeDelta) {
            CLLocationCoordinate2D centerCoordinate = self.centerCoordinate;
            centerCoordinate.latitude += latitudeDelta;
            centerCoordinate.longitude += longitudeDelta;
            
            [self setCenterCoordinate:centerCoordinate animated:animated];
        }
    }
}

- (void)selectAnnotation:(id <MKAnnotation>)annotation centeringIfNotVisible:(BOOL)centering animated:(BOOL)animated {
    MKAnnotationView *annotationView = [self viewForAnnotation:annotation];
    // When we're not asked to center, or if the annotation is present, we act just like selectAnnotation:animated:
    if (!centering || annotationView) {
        [self selectAnnotation:annotation animated:animated];
    } else {
        // We need to center on this annotation and select it when it appears
        self.annotationToSelect = annotation;
        _shouldAnimatedAnnotationToSelect = animated;
        [self setCenterCoordinate:annotation.coordinate animated:animated];
    }
}


@end


