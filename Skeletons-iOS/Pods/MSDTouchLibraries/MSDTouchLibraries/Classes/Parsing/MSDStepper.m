//
//  MSDStepper.m
//  MSDTouchLibraries
//
//  Created by Michael Burke on 2014-09-02.
//
//

#import "MSDStepper.h"
#import "MSDBlockParser.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

NSString * const MSDStepperErrorDomain = @"com.mindsea.msdtouchlibraries.verifyingParser.ErrorDomain";

@interface MSDStepper()

@property (nonatomic, strong) id context;
@property (nonatomic, strong) NSError *error;

- (id)runBlock:(id(^)(MSDStepper *stepper))block error:(NSError **)error;

@end


@implementation MSDStepper

+ (id<MSDParser>)stepParserWithBlock:(id(^)(MSDStepper *stepper))block {
    return [[MSDBlockParser alloc] initWithBlock:^id(id input, NSError **error) {
        return [MSDStepper stepWithContext:input block:block error:error];
    }];
}

+ (nullable id)stepWithContext:(id)context
                         block:(id __nullable(^)(MSDStepper *stepper))block
                         error:(NSError **)error {
    MSDStepper *stepper = [[MSDStepper alloc] initWithContext:context];
    return [stepper runBlock:block error:error];
}

+ (NSArray *)stepEnumeratingContextsInArray:(NSArray *)contexts
                                      block:(id __nullable(^)(MSDStepper *stepper, NSUInteger index))block
                                      error:(NSError **)error {
    MSDStepper *stepper = [[MSDStepper alloc] init];

    return [stepper runBlock:^id(MSDStepper *stepper) {
        return [stepper enumerateContextsInArray:contexts
                                      usingBlock:block];
    } error:error];
}

+ (instancetype)stepperWithContext:(id)context {
    return [[self alloc] initWithContext:context];
}

- (instancetype)initWithContext:(id)context {
    if ((self = [super init])) {
        self.context = context;
        self.treatNullAsMissing = YES;
    }
    return self;
}

- (id)runBlock:(id (^)(MSDStepper *))block error:(NSError **)error {
    id result = block(self);
    if (error) {
        *error = self.error;
    }
    return result;
}

- (void)failWithError:(NSError *)error {
    self.error = error;
}

- (void)failWithLocalizedDescription:(NSString *)message {
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: message};
    NSError *error = [NSError errorWithDomain:MSDStepperErrorDomain
                                         code:MSDStepperGenericError
                                     userInfo:userInfo];
    [self failWithError:error];
}

- (void)failWithLocalizedFormat:(NSString *)message, ... {
    va_list args;
    va_start(args, message);
    NSString *description = [[NSString alloc] initWithFormat:message arguments:args];
    va_end(args);
    [self failWithLocalizedDescription:description];
}

#pragma mark - Contexts

- (nullable id)useContext:(id)theContext
                 forBlock:(id __nullable(^)(MSDStepper *stepper))block {
    [self requiredObjectValue:theContext];
    if (!self.error) {
        NSError *subError;
        id result = [MSDStepper stepWithContext:theContext
                                          block:block
                                          error:&subError];
        if (subError) {
            self.error = subError;
            return nil;
        }
        return result;
    } else {
        return nil;
    }
}

- (nullable NSArray *)enumerateContextsInArray:(NSArray *)contexts
                                    usingBlock:(id __nullable(^)(MSDStepper *stepper, NSUInteger index))block {
    
    [self requiredArrayValue:contexts];
    
    if (!self.error) {
        NSMutableArray *results = [NSMutableArray array];
        NSUInteger index = 0;
        
        for (id theContext in [contexts copy]) {
            
            id singleResult = [self useContext:theContext
                                      forBlock:^id(MSDStepper *stepper) {
                                          @autoreleasepool {
                                              return block(stepper, index);
                                          }
                                      }];
            if (self.error) {
                return nil;
            }
            [results addObject:MSDObjectOrNull(singleResult)];
            index++;
        }
        return results;
    }
    return nil;
}

#pragma mark -
#pragma mark Internal Failing Methods

- (void)failForRequiredValueWithDescription:(NSString *)valueDescription {
    NSString *description = [NSString stringWithFormat:NSLocalizedString(@"Parsing error: %@ is required", @"Message shown when a required value is not found while parsing."), valueDescription];
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: description};
    NSError *error = [NSError errorWithDomain:MSDStepperErrorDomain
                                         code:MSDStepperRequiredValueError
                                     userInfo:userInfo];
    [self failWithError:error];
}

- (void)failForTypeWithExpectedType:(Class)expected
                         foundValue:(id)value
                                key:(nullable NSString *)key {
    
    NSString *description;
    if (key) {
        description = [NSString stringWithFormat:NSLocalizedString(@"Parsing error: %@ is not a %@ (got %@, a %@)", @"Message shown when a value is the wrong type."), key, expected, value, [value class]];
    } else {
        description = [NSString stringWithFormat:NSLocalizedString(@"Parsing error: got %@ (a %@) where %@ expected", @"Message shown when a value is the wrong type."), value, [value class], expected];
    }
    
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: description};
    NSError *error = [NSError errorWithDomain:MSDStepperErrorDomain
                                         code:MSDStepperTypeError
                                     userInfo:userInfo];
    [self failWithError:error];
}

- (nullable NSNumber *)numberForString:(NSString *)string key:(nullable NSString *)key {
    NSScanner *scanner = [NSScanner scannerWithString:string];
    NSDecimal decimal;
    if (![scanner scanDecimal:&decimal]) {
        [self failForTypeWithExpectedType:[NSNumber class] foundValue:string key:key];
        return nil;
    }
    return [NSDecimalNumber decimalNumberWithDecimal:decimal];
}

#define CHECK_KEY_BODY(theKey, theClass, required, theTransformMessage, defaultValue, tryNumberConvert, errorReturn) \
if (self.error) { \
    return errorReturn; \
} \
id computedValue = [self.context valueForKey:theKey]; \
if (self.treatNullAsMissing && computedValue == [NSNull null]) { \
computedValue = nil; \
} \
if (required && computedValue == nil) { \
[self failForRequiredValueWithDescription:theKey]; \
return errorReturn; \
} \
if (self.convertsStringsToNumbers && tryNumberConvert && [computedValue isKindOfClass:[NSString class]]) { \
computedValue = [self numberForString:computedValue key:theKey]; \
    if (self.error) { \
        return errorReturn; \
    } \
} \
if (computedValue && ![computedValue isKindOfClass:theClass]) { \
[self failForTypeWithExpectedType:theClass foundValue:computedValue key:theKey]; \
return errorReturn; \
} \
return computedValue ? [computedValue theTransformMessage] : defaultValue; \
/* */


#pragma mark -
#pragma mark Required Keys

- (nullable NSString *)requiredStringForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSString class], YES, self, nil, NO, nil);
}

- (nullable NSNumber *)requiredNumberForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSNumber class], YES, self, nil, YES, nil);
}

- (nullable NSArray *)requiredArrayForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSArray class], YES, self, nil, NO, nil);
}

- (nullable NSDictionary *)requiredDictionaryForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSDictionary class], YES, self, nil, NO, nil);
}

- (int)requiredIntForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSNumber class], YES, intValue, 0, YES, 0);
}

- (BOOL)requiredBoolForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSNumber class], YES, boolValue, 0, YES, NO);
}

- (float)requiredFloatForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSNumber class], YES, floatValue, 0, YES, 0.0f);
}

- (double)requiredDoubleForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSNumber class], YES, doubleValue, 0, YES, 0.0);
}

- (nullable NSURL *)requiredURLForKey:(NSString *)key baseURL:(nullable NSURL *)baseURL {
    NSURL *url = [self optionalURLForKey:key baseURL:baseURL];
    if (!url) {
        [self failForRequiredValueWithDescription:key];
        return nil;
    }
    return url;
}

- (nullable NSURL *)requiredURLForKey:(NSString *)key {
    return [self requiredURLForKey:key baseURL:nil];
}

- (nullable id)requiredObjectForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSObject class], YES, self, nil, NO, nil);
}

- (nullable id)requiredObjectForKey:(NSString *)key ofClass:(Class)theClass {
    CHECK_KEY_BODY(key, theClass, YES, self, nil, NO, nil);
}

#pragma mark -
#pragma mark Optional Keys

- (nullable NSString *)optionalStringForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSString class], NO, self, nil, NO, nil);
}

- (nullable NSNumber *)optionalNumberForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSNumber class], NO, self, nil, YES, nil);
}

- (nullable NSArray *)optionalArrayForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSArray class], NO, self, nil, NO, nil);
}

- (nullable NSDictionary *)optionalDictionaryForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSDictionary class], NO, self, nil, NO, nil);
}

- (int)optionalIntForKey:(NSString *)key withDefault:(int)theDefault {
    CHECK_KEY_BODY(key, [NSNumber class], NO, intValue, theDefault, YES, 0);
}

- (BOOL)optionalBoolForKey:(NSString *)key withDefault:(BOOL)theDefault {
    CHECK_KEY_BODY(key, [NSNumber class], NO, boolValue, theDefault, YES, NO);
}

- (float)optionalFloatForKey:(NSString *)key withDefault:(float)theDefault {
    CHECK_KEY_BODY(key, [NSNumber class], NO, floatValue, theDefault, YES, 0.0f);
}

- (double)optionalDoubleForKey:(NSString *)key withDefault:(double)theDefault {
    CHECK_KEY_BODY(key, [NSNumber class], NO, doubleValue, theDefault, YES, 0.0);
}

- (nullable NSURL *)optionalURLForKey:(NSString *)key baseURL:(nullable NSURL *)baseURL {
    id initialValue = [self optionalObjectForKey:key];
    if (!initialValue) {
        return nil;
    }
    
    if ([initialValue isKindOfClass:[NSURL class]]) {
        return initialValue;
    }
    
    if (![initialValue isKindOfClass:[NSString class]]) {
        [self failForTypeWithExpectedType:[NSURL class] foundValue:initialValue key:key];
        return nil;
    }
    
    // Special-case spaces here, because they're commonly in otherwise-valid URLs
    // Note that we expect things to be valid URLs otherwise.
    initialValue = [initialValue stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:initialValue relativeToURL:baseURL];
    if (!url) {
        [self failWithLocalizedFormat:NSLocalizedString(@"Parsing Error: expected a URL, but got: %@", @"Error when a malformed URL is found"), initialValue];
        return nil;
    } 
    return url;
}

- (nullable NSURL *)optionalURLForKey:(NSString *)key {
    return [self optionalURLForKey:key baseURL:nil];
}

- (nullable id)optionalObjectForKey:(NSString *)key {
    CHECK_KEY_BODY(key, [NSObject class], NO, self, nil, NO, nil);
}

- (nullable id)optionalObjectForKey:(NSString *)key ofClass:(Class)theClass {
    CHECK_KEY_BODY(key, theClass, NO, self, nil, NO, nil);
}

#undef CHECK_KEY_BODY


#define CHECK_VALUE_BODY(theValue, theClass, required, theTransformMessage, defaultValue, tryNumberConvert, errorReturn) \
id localValue = theValue; \
if (self.error) { \
    return errorReturn; \
} \
if (self.treatNullAsMissing && localValue == [NSNull null]) { \
    localValue = nil; \
} \
if (required && localValue == nil) { \
    [self failForRequiredValueWithDescription:[theClass description]]; \
    return errorReturn; \
} \
if (self.convertsStringsToNumbers && tryNumberConvert && [localValue isKindOfClass:[NSString class]]) { \
    localValue = [self numberForString:localValue key:nil]; \
    if (self.error) { \
        return errorReturn; \
    } \
} \
if (localValue && ![localValue isKindOfClass:(theClass)]) { \
    [self failForTypeWithExpectedType:theClass foundValue:localValue key:nil]; \
    return errorReturn; \
}\
return localValue ? [localValue theTransformMessage] : defaultValue; \
/* */

#pragma mark -
#pragma mark Required Values

- (nullable NSString *)requiredStringValue:(id)object {
    CHECK_VALUE_BODY(object, [NSString class], YES, self, nil, NO, nil);
}

- (nullable NSNumber *)requiredNumberValue:(id)object {
    CHECK_VALUE_BODY(object, [NSNumber class], YES, self, nil, YES, nil);
}

- (nullable NSArray *)requiredArrayValue:(id)object {
    CHECK_VALUE_BODY(object, [NSArray class], YES, self, nil, NO, nil);
}

- (nullable NSDictionary *)requiredDictionaryValue:(id)object {
    CHECK_VALUE_BODY(object, [NSDictionary class], YES, self, nil, NO, nil);
}

- (int)requiredIntValue:(id)object {
    CHECK_VALUE_BODY(object, [NSNumber class], YES, intValue, 0, YES, 0);
}

- (BOOL)requiredBoolValue:(id)object {
    CHECK_VALUE_BODY(object, [NSNumber class], YES, boolValue, 0, YES, NO);    
}

- (float)requiredFloatValue:(id)object {
    CHECK_VALUE_BODY(object, [NSNumber class], YES, floatValue, 0, YES, 0.0f);        
}

- (double)requiredDoubleValue:(id)object {
    CHECK_VALUE_BODY(object, [NSNumber class], YES, doubleValue, 0, YES, 0.0);
}

- (nullable NSURL *)requiredURLValue:(id)initialValue baseURL:(nullable NSURL *)baseURL {
    NSURL *url = [self optionalURLValue:initialValue baseURL:baseURL];
    if (!url) {
        [self failForRequiredValueWithDescription:[NSURL description]];
    }
    return url;
}

- (nullable NSURL *)requiredURLValue:(id)initialValue {
    return [self requiredURLValue:initialValue baseURL:nil];
}

- (nullable id)requiredObjectValue:(id)object {
    CHECK_VALUE_BODY(object, [NSObject class], YES, self, nil, NO, nil);
}

- (nullable id)requiredObjectValue:(id)object ofClass:(Class)theClass {
    CHECK_VALUE_BODY(object, theClass, YES, self, nil, NO, nil);
}

#pragma mark -
#pragma mark Optional Values

- (nullable NSString *)optionalStringValue:(id)object {
    CHECK_VALUE_BODY(object, [NSString class], NO, self, nil, NO, nil);
}

- (nullable NSNumber *)optionalNumberValue:(id)object {
    CHECK_VALUE_BODY(object, [NSNumber class], NO, self, nil, YES, nil);
}

- (nullable NSArray *)optionalArrayValue:(id)object {
    CHECK_VALUE_BODY(object, [NSArray class], NO, self, nil, NO, nil);
}

- (nullable NSDictionary *)optionalDictionaryValue:(id)object {
    CHECK_VALUE_BODY(object, [NSDictionary class], NO, self, nil, NO, nil);
}

- (int)optionalIntValue:(id)object withDefault:(int)theDefault {
    CHECK_VALUE_BODY(object, [NSNumber class], NO, intValue, theDefault, YES, 0);
}

- (BOOL)optionalBoolValue:(id)object withDefault:(BOOL)theDefault {
    CHECK_VALUE_BODY(object, [NSNumber class], NO, boolValue, theDefault, YES, NO);
}

- (float)optionalFloatValue:(id)object withDefault:(float)theDefault {
    CHECK_VALUE_BODY(object, [NSNumber class], NO, floatValue, theDefault, YES, 0.0f);
}

- (double)optionalDoubleValue:(id)object withDefault:(double)theDefault {
    CHECK_VALUE_BODY(object, [NSNumber class], NO, doubleValue, theDefault, YES, 0.0);
}

- (nullable NSURL *)optionalURLValue:(id)initialValue baseURL:(nullable NSURL *)baseURL {
    if ([initialValue isKindOfClass:[NSURL class]]) {
        return initialValue;
    }
    
    if (![initialValue isKindOfClass:[NSString class]]) {
        [self failForTypeWithExpectedType:[NSURL class] foundValue:initialValue key:nil];
    };
    
    // Special-case spaces here, because they're commonly in otherwise-valid URLs
    // Note that we expect things to be valid URLs otherwise.
    initialValue = [initialValue stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:initialValue relativeToURL:baseURL];
    if (!url) {
        [self failWithLocalizedFormat:NSLocalizedString(@"Parsing Error: expected a URL, but got: %@", @"Error when a malformed URL is found"), initialValue];
    } 
    return url;
}

- (nullable NSURL *)optionalURLValue:(id)initialValue {
    return [self optionalURLValue:initialValue baseURL:nil];
}

- (nullable id)optionalObjectValue:(id)object ofClass:(Class)theClass {
    CHECK_VALUE_BODY(object, theClass, NO, self, nil, NO, nil);
}

#undef CHECK_VALUE_BODY

@end

NS_ASSUME_NONNULL_END
