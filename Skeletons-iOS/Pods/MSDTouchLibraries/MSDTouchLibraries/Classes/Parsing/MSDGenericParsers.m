//
//  MSDGenericParsers.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-05-02.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDGenericParsers.h"
#import <stdarg.h>

#import "MSDBlockParser.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

NSString * const MSDGenericParsersErrorDomain = @"com.mindsea.msdtouchlibraries.msdgenericparsers.errors";

@implementation MSDGenericParsers

+ (id<MSDParser>)parserByApplyingParsersInArray:(NSArray<id<MSDParser>> *)parsers {
    return [MSDBlockParser parserWithBlock:^id(id input, NSError **error) {
        for (id<MSDParser> parser in parsers) {
            input = [parser parseInput:input error:error];
            if (!input) {
                return nil;
            }
        }
        return input;
    }];
}

+ (id<MSDParser>)parserByApplyingParsers:(id<MSDParser>)firstParser, ... {
    NSMutableArray *parsers = [NSMutableArray array];
    if (firstParser) {
        [parsers addObject:firstParser];
        va_list args;
        va_start(args, firstParser);
        id parser;
        while ((parser = va_arg(args, id))) {
            [parsers addObject:parser];
        }
    }
    
    return [self parserByApplyingParsersInArray:parsers];
}

+ (id<MSDParser>)propertyListParser {
    return [MSDBlockParser parserWithBlock:^id(id input, NSError **error) {
        MSDCheck([input isKindOfClass:[NSData class]], @"Expected NSData. Do you need to set parseRawData on your parsing request?");
        return [NSPropertyListSerialization propertyListWithData:input options:0 format:NULL error:error];
    }];
}

+ (id<MSDParser>)JSONParser {
    return [MSDBlockParser parserWithBlock:^id(id input, NSError **error) {
        if ([input isKindOfClass:[NSData class]]) {
            return [NSJSONSerialization JSONObjectWithData:input options:NSJSONReadingAllowFragments error:error];
        } else if ([input isKindOfClass:[NSString class]]) {
            NSData *data = [input dataUsingEncoding:NSUTF8StringEncoding];
            return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
        } else {
            if (error) {
                NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Expected data or string.", @"Error when passed something to a JSON parser we don't recognize")};
                *error = [NSError errorWithDomain:MSDGenericParsersErrorDomain code:MSDGenericParsersErrorUnknownJSONInput userInfo:userInfo];
            }
            return nil;
        }
    }];
}


+ (id<MSDParser>)dataToStringParserWithEncoding:(NSStringEncoding)encoding {
    return [MSDBlockParser parserWithBlock:^id(id input, NSError **error) {
        NSString *result = [[NSString alloc] initWithData:input encoding:encoding];
        if (!result && error) {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedString(@"Cannot decode string data.", @"Error when we can't parse some data into a string")};
            *error = [NSError errorWithDomain:MSDGenericParsersErrorDomain code:MSDGenericParsersErrorInvalidEncoding userInfo:userInfo];
        }
        return result;
    }];
}

@end

NS_ASSUME_NONNULL_END
