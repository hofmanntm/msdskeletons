//
//  MSDBlockParser.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-05-01.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDParser.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(BlockParser)
@interface MSDBlockParser : NSObject<MSDParser>

/*!
 * Method to create a parser that overrides parseObject:error: with the 
 * given block. The passed block should not reference (and thus retain)
 * the block parser.
 */
+ (instancetype)parserWithBlock:(id(^)(id input, NSError **error))parseBlock;
- (instancetype)initWithBlock:(id (^)(id input, NSError **error))theParseBlock NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
