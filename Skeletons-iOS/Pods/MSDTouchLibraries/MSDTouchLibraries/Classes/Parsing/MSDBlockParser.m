//
//  MSDBlockParser.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-05-01.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDBlockParser.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDBlockParser() 

@property (nonatomic, copy) id(^parseBlock)(id, NSError **);

@end

@implementation MSDBlockParser

+ (instancetype)parserWithBlock:(id(^)(id input, NSError **error))parseBlock {
    return [[self alloc] initWithBlock:parseBlock];
}

- (instancetype)initWithBlock:(id (^)(id input, NSError **error))theParseBlock {
    MSDArgNotNil(theParseBlock);
    if ((self = [super init])) {
        self.parseBlock = theParseBlock;
    }
    return self;
}

- (nullable id)parseInput:(id)input error:(NSError **)error {
    return self.parseBlock(input, error);
}

@end

NS_ASSUME_NONNULL_END
