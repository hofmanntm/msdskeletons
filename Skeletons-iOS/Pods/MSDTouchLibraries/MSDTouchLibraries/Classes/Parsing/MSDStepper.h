//
//  MSDStepper.h
//  MSDTouchLibraries
//
//  Created by Michael Burke on 2014-09-02.
//
//

#import <Foundation/Foundation.h>
#import "MSDCompatibility.h"
#import "MSDParser.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString * const MSDStepperErrorDomain;
typedef NS_ERROR_ENUM(MSDStepperErrorDomain, MSDStepperError) {
    MSDStepperGenericError,
    MSDStepperRequiredValueError,
    MSDStepperTypeError,
    MSDStepperUnexpectedError,
};


/*!
 * Provides an interface to parse while allowing easy type checking
 * and failing.
 *
 * In general, if any call fails, the MSDStepper instance will
 * fail and any further calls will be no-ops.  You can get the
 * error from the error property at any point to see if the instance
 * is in a failed state.
 *
 * Most likely, you will want to check error after you finish parsing.
 * The class methods will return error so you can check it from the
 * highest level.
 */
NS_SWIFT_NAME(Stepper)
@interface MSDStepper : NSObject

/*!
 * Returns an MSDParser that will run the given block using 
 * a stepper with the context set to the parser's input.
 */
+ (id<MSDParser>)stepParserWithBlock:(id(^)(MSDStepper *stepper))block;

/*!
 * Runs the given block, passing a stepper that can be used to
 * set a context and verify types. The context is the given object;
 * will cause an error if nil.
 *
 * @param context A context to operate on; must not be nil.
 * @param block The block to call for operating on the given context.
 *
 * @return The result from calling block on the given context with a stepper.
 *         NB. This does not guarantee nil return on failure; you need to
 *             check error separately.
 */
+ (nullable id)stepWithContext:(id)context
                         block:(id __nullable(^)(MSDStepper *stepper))block
                         error:(NSError **)error;

/*!
 * Runs the given block for each element in an array and collects results.
 *
 * Note that we may create autorelease pools for each context in this
 * iteration, so don't rely on any values created to exist after this
 * block runs.
 *
 * This has the same semantics as enumerateContextsInArray:usingBlock:
 *
 * @param contexts An array of contexts; must not be nil.
 * @param block The block to call for each context, and the index of that context.
 *
 * @return An array containing the results of calling block repeatedly for
 *         each context (with nil return values represented as NSNull objects).
 */
+ (NSArray *)stepEnumeratingContextsInArray:(NSArray *)contexts
                                      block:(id __nullable(^)(MSDStepper *stepper, NSUInteger index))block
                                      error:(NSError **)error;

/*!
 * Initialize the MSDStepper with a context.  This is the basic
 * initializer.
 *
 * Consider using stepWithContext:block:error: or
 * stepEnumeratingContextsInArray:block:error:, which will return
 * any error that may happen during stepping.
 */
- (instancetype)initWithContext:(id)context;

/*!
 * Convenience method for [[MSDStepper alloc] initWithContext:context];
 */
+ (instancetype)stepperWithContext:(id)context;

/*!
 * Within a stepping block, call this to run a block with a new context.
 * The context must not be nil. Returns the result of the block.
 */
- (nullable id)useContext:(id)theContext
                 forBlock:(id __nullable(^)(MSDStepper *stepper))block;

/*!
 * Within a stepping block, call this to run a block for each element
 * in an array and collect the results. The resulting array with have
 * an NSNull in it for any nil values returned.
 *
 * If any sub-enumeration fails, this MSDStepper instance will capture
 * the error and be considered to have failed.
 *
 * Note that we may create autorelease pools for each context in this
 * iteration, so don't rely on any values created to exist after this 
 * block runs, other than the return value.
 *
 * @param contexts An array of contexts; must not be nil.
 * @param block The block to call for each context, and the index of that context.
 */
- (nullable NSArray *)enumerateContextsInArray:(NSArray *)contexts
                                    usingBlock:(id __nullable(^)(MSDStepper *stepper, NSUInteger index))block;

/*! 
 * The context, set at construction.
 */
@property (nonatomic, readonly, strong) id context;

/*! 
 * The error that has caused the stepper to fail.
 *
 * If this is non-nil, then calling any methods that operate on a context will
 * become no-ops, preventing further processing.  Calls that use blocks will
 * not execute at all.
 *
 * This can be set manually by calling failWith... in order to cause the stepper
 * (and any parent steppers) to fail as well.  It will be set automatically when
 * calls on this stepper fail; you can use this to stop processing if desired.
 */
@property (nonatomic, readonly, strong, nullable) NSError *error;

/*!
 * Call to indicate failure during stepping.
 *
 * This sets self.error, which will cause further validation calls to 
 * become no-ops.
 */
- (void)failWithError:(NSError *)error;

/*!
 * Call to indicate failure during stepping.
 *
 * This sets self.error, which will cause further validation calls to 
 * become no-ops.
 *
 * @discussion Uses MSDStepperErrorDomain and MSDStepperGenericError
 * for the error domain and code, respectively.
 */
- (void)failWithLocalizedDescription:(NSString *)message;

/*!
 * Call to indicate failure during stepping.
 *
 * This sets self.error, which will cause further validation calls to 
 * become no-ops.
 *
 * @discussion Uses MSDStepperErrorDomain and MSDStepperGenericError
 * for the error domain and code, respectively.
 */
- (void)failWithLocalizedFormat:(NSString *)message, ... NS_FORMAT_FUNCTION(1,2);

/*!
 * If this is YES (defaults to YES) then values passed which are
 * NSNulls are assumed to be "missing" in terms of required/optional
 * checks.
 */
@property (nonatomic, assign) BOOL treatNullAsMissing;

/*!
 * If this is YES (defaults to NO) then we will treat numeric strings
 * as numbers when asked, using non-localized values for separators.
 * (As defined by NSScanner.)
 *
 * Note this only affects the accessors that ask for numbers or
 * primitive values, not, for example, requiredObjectForKey:ofClass:
 * if you pass [NSNumber class].
 */
@property (nonatomic, assign) BOOL convertsStringsToNumbers;

// These return the valueForKey: of the context,
// failing if the value is the wrong type or nil.
- (nullable NSString *)requiredStringForKey:(NSString *)key;
- (nullable NSNumber *)requiredNumberForKey:(NSString *)key;
- (nullable NSArray *)requiredArrayForKey:(NSString *)key;
- (nullable NSDictionary *)requiredDictionaryForKey:(NSString *)key;
- (int)requiredIntForKey:(NSString *)key;
- (BOOL)requiredBoolForKey:(NSString *)key;
- (float)requiredFloatForKey:(NSString *)key;
- (double)requiredDoubleForKey:(NSString *)key;
- (nullable NSURL *)requiredURLForKey:(NSString *)key; // Converts from string if needed
- (nullable NSURL *)requiredURLForKey:(NSString *)key baseURL:(nullable NSURL *)url; // Converts from string if needed
- (nullable id)requiredObjectForKey:(NSString *)key;
- (nullable id)requiredObjectForKey:(NSString *)key ofClass:(Class)theClass;

// These return the valueForKey: of the context,
// failing if the value is the wrong type. 
// Returns nil if the value is nil.
- (nullable NSString *)optionalStringForKey:(NSString *)key;
- (nullable NSNumber *)optionalNumberForKey:(NSString *)key;
- (nullable NSArray *)optionalArrayForKey:(NSString *)key;
- (nullable NSDictionary *)optionalDictionaryForKey:(NSString *)key;
- (int)optionalIntForKey:(NSString *)key withDefault:(int)theDefault;
- (BOOL)optionalBoolForKey:(NSString *)key withDefault:(BOOL)theDefault;
- (float)optionalFloatForKey:(NSString *)key withDefault:(float)theDefault;
- (double)optionalDoubleForKey:(NSString *)key withDefault:(double)theDefault;
- (nullable NSURL *)optionalURLForKey:(NSString *)key; // Converts from string if needed
- (nullable NSURL *)optionalURLForKey:(NSString *)key baseURL:(nullable NSURL *)url; // Converts from string if needed
- (nullable id)optionalObjectForKey:(NSString *)key;
- (nullable id)optionalObjectForKey:(NSString *)key ofClass:(Class)theClass;

// These allow you to test arbitrary values for correct types,
// and fail if the passed object is not the correct type or nil.
- (nullable NSString *)requiredStringValue:(id)object;
- (nullable NSNumber *)requiredNumberValue:(id)object;
- (nullable NSArray *)requiredArrayValue:(id)object;
- (nullable NSDictionary *)requiredDictionaryValue:(id)object;
- (int)requiredIntValue:(id)object;
- (BOOL)requiredBoolValue:(id)object;
- (float)requiredFloatValue:(id)object;
- (double)requiredDoubleValue:(id)object;
- (nullable NSURL *)requiredURLValue:(id)url; // Converts from string if needed
- (nullable NSURL *)requiredURLValue:(id)url baseURL:(nullable NSURL *)baseURL; // Converts from string if needed
- (nullable id)requiredObjectValue:(id)object;
- (nullable id)requiredObjectValue:(id)object ofClass:(Class)theClass;

// These allow you to test arbitrary values for correct types,
// and fail if the passed object is not of the correct type.
// Returns nil if the object is nil.
- (nullable NSString *)optionalStringValue:(id)object;
- (nullable NSNumber *)optionalNumberValue:(id)object;
- (nullable NSArray *)optionalArrayValue:(id)object;
- (nullable NSDictionary *)optionalDictionaryValue:(id)object;
- (int)optionalIntValue:(id)object withDefault:(int)theDefault;
- (BOOL)optionalBoolValue:(id)object withDefault:(BOOL)theDefault;
- (float)optionalFloatValue:(id)object withDefault:(float)theDefault;
- (double)optionalDoubleValue:(id)object withDefault:(double)theDefault;
- (nullable NSURL *)optionalURLValue:(id)url; // Converts from string if needed
- (nullable NSURL *)optionalURLValue:(id)url baseURL:(nullable NSURL *)baseURL; // Converts from string if needed
- (nullable id)optionalObjectValue:(id)object ofClass:(Class)theClass;

@end

NS_ASSUME_NONNULL_END
