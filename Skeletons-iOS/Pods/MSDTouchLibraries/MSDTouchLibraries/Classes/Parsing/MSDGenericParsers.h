//
//  MSDGenericParsers.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-05-02.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDCompatibility.h"
#import "MSDParser.h"

NS_ASSUME_NONNULL_BEGIN

extern NSString * const MSDGenericParsersErrorDomain;
typedef NS_ERROR_ENUM(MSDGenericParsersErrorDomain, MSDGenericParsersError) {
    MSDGenericParsersErrorInvalidEncoding,
    MSDGenericParsersErrorUnknownJSONInput,
};


NS_SWIFT_NAME(GenericParsers)
@interface MSDGenericParsers : NSObject

#pragma mark - Combinators

/// Returns a parser that runs each parser in the array in turn, passing the output of one as the input of the next.
+ (id<MSDParser>)parserByApplyingParsersInArray:(NSArray<id<MSDParser>> *)parsers;

/*!
 * Returns a parser that runs the first parser on the given input,
 * and passes the results to the next parser and so on, returning the
 * results of the last parser.
 */
+ (id<MSDParser>)parserByApplyingParsers:(id<MSDParser>)firstParser, ... NS_REQUIRES_NIL_TERMINATION;

#pragma mark - Data Formats

/*!
 * Returns a parser that takes an NSData input and returns an NSArray
 * or NSDictionary result by treating it as a plist.
 */
+ (id<MSDParser>)propertyListParser;

/*!
 * Returns a parser that takes an NSString input and returns the 
 * result of parsing that as JSON, typically an NSDictionary.
 * Can also be passed an NSData object, which must be UTF-8 encoded text.
 */
+ (id<MSDParser>)JSONParser;

/*!
 * Returns a parser that takes an NSData object and produces
 * an NSString, using the given encoding for conversion.
 */
+ (id<MSDParser>)dataToStringParserWithEncoding:(NSStringEncoding)encoding;

@end

NS_ASSUME_NONNULL_END
