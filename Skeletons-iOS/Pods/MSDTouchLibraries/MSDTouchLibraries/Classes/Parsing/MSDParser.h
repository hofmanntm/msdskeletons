//
//  MSDParser.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-20.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(Parser)
@protocol MSDParser<NSObject>

- (nullable id)parseInput:(id)input error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
