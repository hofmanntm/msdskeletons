//
//  MSDContentChangeIdentifier.h
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-28.
//  Copyright (c) 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MSDContentChangeIdentifier : NSObject

/**
    @param isEqualBlock A block that returns YES iff its two parameters are considered equal for the diff. If nil, -isEqual: is used to compare contents.
 
    @return A set of indices in `newContent` pointing to the values that were moved or added.
 */
+ (NSIndexSet *)diffOldContent:(NSArray *)oldContent
             againstNewContent:(NSArray *)newContent
                  isEqualBlock:(nullable __attribute__((noescape)) BOOL (^)(id lhs, id rhs))isEqualBlock
         itemDeletedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))deletedBlock
           itemAddedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))addedBlock
           itemMovedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger fromIndex, NSUInteger toIndex, id item))movedBlock;

/**
    Calls `+diffOldContent:againstNewContent:isEqualBlock:itemDeletedBlockOrNil:itemAddedBlockOrNil:itemMovedBlockOrNil:` with a `nil` `isEqualBlock`.
 */
+ (NSIndexSet *)diffOldContent:(NSArray *)oldContent
             againstNewContent:(NSArray *)newContent
         itemDeletedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))deletedBlock
           itemAddedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))addedBlock
           itemMovedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger fromIndex, NSUInteger toIndex, id item))movedBlock;

@end

NS_ASSUME_NONNULL_END
