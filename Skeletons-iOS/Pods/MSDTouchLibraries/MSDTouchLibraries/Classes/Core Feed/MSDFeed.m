//
//  MSDFeed.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-19.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDFeed.h"
#import "MSDContentChangeIdentifier.h"
#import "MSDDelegateCollection.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDFeed ()

@property (nonatomic) MSDDelegateCollection<id<MSDFeedDelegate>> *delegates;

@end

@implementation MSDFeed

- (instancetype)init {
    if ((self = [super init])) {
        _delegates = [MSDDelegateCollection<MSDFeedDelegate> new];
    }
    return self;
}

- (NSArray *)content {
    return @[];
}

- (NSUInteger)count {
    return self.content.count;
}

- (id)objectAtIndexedSubscript:(NSUInteger)index {
    return self.content[index];
}

- (void)addDelegate:(id<MSDFeedDelegate>)delegate {
    [self.delegates addDelegate:delegate];
}

- (void)removeDelegate:(id<MSDFeedDelegate>)delegate {
    [self.delegates removeDelegate:delegate];
}

- (id<NSFastEnumeration>)delegatesRespondingToSelector:(SEL)selector {
    NSMutableArray *responsiveDelegates = [NSMutableArray new];
    [self.delegates enumerateDelegatesRespondingToSelector:selector usingBlock:^(id<MSDFeedDelegate> delegate) {
        [responsiveDelegates addObject:delegate];
    }];
    return responsiveDelegates;
}

- (NSIndexSet *)dispatchContentChangeNotificationsForContentChanged:(NSArray *)oldContent toContent:(NSArray *)newContent isEqualBlock:(nullable __attribute__((noescape)) BOOL(^)(id lhs, id rhs))isEqualBlock {
    return [MSDContentChangeIdentifier diffOldContent:oldContent
                                    againstNewContent:newContent
                                         isEqualBlock:isEqualBlock
                                itemDeletedBlockOrNil:^(NSUInteger index, id item) {
                                    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feed:didDeleteItemAtIndex:)]) {
                                        [delegate feed:self didDeleteItemAtIndex:index];
                                    }
                                }
                                  itemAddedBlockOrNil:^(NSUInteger index, id item) {
                                      for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feed:didAddItemAtIndex:)]) {
                                          [delegate feed:self didAddItemAtIndex:index];
                                      }
                                  }
                                  itemMovedBlockOrNil:^(NSUInteger fromIndex, NSUInteger toIndex, id item) {
                                      for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feed:didMoveItemAtIndex:toIndex:)]) {
                                          [delegate feed:self didMoveItemAtIndex:fromIndex toIndex:toIndex];
                                      }
                                  }];
}

- (NSIndexSet *)dispatchContentChangeNotificationsForContentChanged:(NSArray *)oldContent toContent:(NSArray *)newContent {
    return [self dispatchContentChangeNotificationsForContentChanged:oldContent toContent:newContent isEqualBlock:nil];
}

- (BOOL)canReload {
    return NO;
}

- (void)reload {
    
}

- (void)cancelReload {
    
}

- (BOOL)canLoadMore {
    return NO;
}

- (void)loadMore {
    
}

- (void)cancelLoadMore {
    
}

- (BOOL)isReloading {
    return NO;
}

- (BOOL)isLoadingMore {
    return NO;
}

@end

NS_ASSUME_NONNULL_END
