//
//  MSDFeed.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-19.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol MSDFeedDelegate;

NS_ASSUME_NONNULL_BEGIN

/*!
 * @abstract A list of items that can be loaded.
 */
@protocol MSDFeed<NSObject>

// Not expected to be KVO-compliant; use the delegate callbacks instead
@property (nonatomic, readonly) NSArray *content;

/**
    Some feeds can optimize returning a count without returning a whole array of content.
 
    Callers: If all you need is a count, call count.
 
    Implementers: If you can get the count faster than through the content array, do that. Otherwise just return `content.count`.
 */
@property (nonatomic, readonly) NSUInteger count;

/**
    Some feeds can optimize returning one or a handful of objects without returning a whole array of content.
 
    Throws an exception if index is out of bounds.
 
    Callers: If you need only a small number of objects, try to subscript into the feed.
 
    Implementers: If you can get at a particular object faster than through the content array, do that. Otherwise just return `content[index]`.
 */
- (id)objectAtIndexedSubscript:(NSUInteger)index;

- (void)addDelegate:(id<MSDFeedDelegate>)delegate NS_SWIFT_NAME(addDelegate(_:));
- (void)removeDelegate:(id<MSDFeedDelegate>)delegate NS_SWIFT_NAME(removeDelegate(_:));

@property (nonatomic, readonly) BOOL canReload; // should be KVO-compliant
- (void)reload;
- (void)cancelReload;

@property (nonatomic, readonly) BOOL canLoadMore; // should be KVO-compliant
- (void)loadMore;
- (void)cancelLoadMore;

@property (nonatomic, readonly, getter=isReloading) BOOL reloading; // should be KVO-compliant
@property (nonatomic, readonly, getter=isLoadingMore) BOOL loadingMore; // should be KVO-compliant

@end


@protocol MSDFeedDelegate<NSObject>

@optional

// These are called just after a reload or loadMore actually starts
// (ie calling reload doesn't always call this unless it actually works.)
- (void)feedDidBeginReload:(id<MSDFeed>)feed;
- (void)feedDidBeginLoadMore:(id<MSDFeed>)feed;

// These are called on cancel and on success, but not failure
- (void)feedDidFinishReload:(id<MSDFeed>)feed;
- (void)feedDidFinishLoadMore:(id<MSDFeed>)feed;

// These are called only on failure
- (void)feedDidFailReload:(id<MSDFeed>)feed withError:(NSError *)error;
- (void)feedDidFailLoadMore:(id<MSDFeed>)feed withError:(NSError *)error;

// These are called before/after the didUpdate/Add/Delete/Move callbacks
- (void)feedWillChangeContent:(id<MSDFeed>)feed;
- (void)feedDidChangeContent:(id<MSDFeed>)feed;

- (void)feed:(id<MSDFeed>)feed didUpdateItemAtIndex:(NSUInteger)index;
- (void)feed:(id<MSDFeed>)feed didDeleteItemAtIndex:(NSUInteger)index;
- (void)feed:(id<MSDFeed>)feed didAddItemAtIndex:(NSUInteger)index;
- (void)feed:(id<MSDFeed>)feed didMoveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

@end


/*!
 * A class than handles MSDFeed's delegates and provides default implementations
 * and convenience for delegate handling.
 */
@interface MSDFeed : NSObject<MSDFeed>

// For subclasses to use
- (id<NSFastEnumeration>)delegatesRespondingToSelector:(SEL)selector;

/**
    @param isEqualBlock A block that returns YES iff its two parameters should be considered equal between the old array and the new array. If nil, -isEqual: is used.
    
    @return A set of indices in `newContent` pointing to the values that were moved or added.
 */
- (NSIndexSet *)dispatchContentChangeNotificationsForContentChanged:(NSArray *)oldContent toContent:(NSArray *)newContent isEqualBlock:(nullable __attribute__((noescape)) BOOL(^)(id lhs, id rhs))isEqualBlock;

/// Calls -dispatchContentChangeNotificationsForContentChanged:toContent:isEqualBlock: with a nil isEqualBlock.
- (NSIndexSet *)dispatchContentChangeNotificationsForContentChanged:(NSArray *)oldContent toContent:(NSArray *)newContent;

@end

NS_ASSUME_NONNULL_END
