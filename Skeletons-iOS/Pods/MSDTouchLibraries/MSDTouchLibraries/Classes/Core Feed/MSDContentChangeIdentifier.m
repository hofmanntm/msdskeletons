//
//  MSDContentChangeIdentifier.m
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-11-28.
//  Copyright (c) 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDContentChangeIdentifier.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDContentChangeIdentifier

+ (NSIndexSet *)diffOldContent:(NSArray *)oldContent
             againstNewContent:(NSArray *)newContent
         itemDeletedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))deletedBlock
           itemAddedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))addedBlock
           itemMovedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger fromIndex, NSUInteger toIndex, id item))movedBlock {
    
    return [self diffOldContent:oldContent
              againstNewContent:newContent
                   isEqualBlock:nil
          itemDeletedBlockOrNil:deletedBlock
            itemAddedBlockOrNil:addedBlock
            itemMovedBlockOrNil:movedBlock];
}

+ (NSIndexSet *)diffOldContent:(NSArray *)oldContent
             againstNewContent:(NSArray *)newContent
                  isEqualBlock:(nullable __attribute__((noescape)) BOOL (^)(id lhs, id rhs))isEqualBlock
         itemDeletedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))deletedBlock
           itemAddedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger index, id item))addedBlock
           itemMovedBlockOrNil:(nullable __attribute__((noescape)) void (^)(NSUInteger fromIndex, NSUInteger toIndex, id item))movedBlock {
    
    if (!isEqualBlock) {
        isEqualBlock = ^(id lhs, id rhs) { return [lhs isEqual:rhs]; };
    }
    
    // the list of indexes we need to delete
    NSMutableArray *indexesToDelete = [NSMutableArray array];
    
    // The items to insert, item->index (items stored as NSValues)
    NSMutableDictionary *itemsToInsert = [NSMutableDictionary dictionary];
    
    // A set of newContent indices for items that we've already asked the table to either insert
    // or update
    NSMutableIndexSet *itemsAlreadyUpdated = [NSMutableIndexSet new];
    
    // Table views run all deletions first, against the original content,
    // then all insertions. This is the content after all deletions have run.
    NSMutableArray *postDeletionContent = [NSMutableArray array];
    
    // Go through original content, and decide what we can leave in place
    // and what we need to delete/move. This is a greedy version of this 
    // and is not a minimal transform. For example, if the first item in 
    // the old feed went to the very bottom, this will delete and re-insert
    // all other items.
    NSUInteger oldCount = [oldContent count];
    NSUInteger lastInsertIndex = 0;
    for (NSUInteger oldIndex = 0; oldIndex < oldCount; oldIndex++) {
        id item = oldContent[oldIndex];
        NSUInteger foundIndex = [newContent indexOfObjectPassingTest:^BOOL(id obj, NSUInteger i, BOOL *stop) {
            return isEqualBlock(obj, item);
        }];
        
        if (foundIndex != NSNotFound && foundIndex >= lastInsertIndex) {
            // we don't need to move this item; it will fall into place
            [postDeletionContent addObject:item];
            lastInsertIndex = foundIndex;
        } else {
            // we need to delete this item
            [indexesToDelete addObject:@(oldIndex)];
        }
    }
    
    // We now have a list of post-deletion content. We'll now run insertions 
    // and transform the content to get to the final state.
    NSMutableArray *newContentSoFar = [postDeletionContent mutableCopy];
    NSUInteger newCount = [newContent count];
    for (NSUInteger newIndex = 0; newIndex < newCount; newIndex++) {
        id itemInNewContent = newContent[newIndex];
        if ([newContentSoFar count] > newIndex && isEqualBlock(newContentSoFar[newIndex], itemInNewContent)) {
            // nothing to be done; the desired content matches the content we've got
        } else {
            // we need to insert an item here
            itemsToInsert[[NSValue valueWithNonretainedObject:itemInNewContent]] = @(newIndex);
            [newContentSoFar insertObject:itemInNewContent atIndex:newIndex];
        }
    }
    
    MSDCheck(({
        BOOL allGood = newContentSoFar.count == newContent.count;
        for (NSUInteger i = 0, end = newContent.count; i < end && allGood; i++) {
            allGood = isEqualBlock(newContentSoFar[i], newContent[i]);
        }
        allGood;
    }), @"Transform failed: didn't recreate new content");
    
    // Tell delegates about deletes
    for (NSNumber *deleteIndexNumber in indexesToDelete) {
        NSUInteger deleteIndex = deleteIndexNumber.unsignedIntegerValue;
        id itemBeingDeleted = oldContent[deleteIndex];
        NSNumber *insertIndexNumber = itemsToInsert[[NSValue valueWithNonretainedObject:itemBeingDeleted]];
        if (insertIndexNumber) {
            // this is a move
            NSUInteger insertIndex = insertIndexNumber.unsignedIntegerValue;
            if (movedBlock) {
                movedBlock(deleteIndex, insertIndex, itemBeingDeleted);
            }
            [itemsAlreadyUpdated addIndex:insertIndex]; // we've already inserted this
        } else {
            // normal delete
            if (deletedBlock) {
                deletedBlock(deleteIndex, itemBeingDeleted);
            }
        }
    }
    
    // And about inserts
    for (NSValue *itemValueToInsert in itemsToInsert) {
        id itemToInsert = [itemValueToInsert nonretainedObjectValue];
        NSNumber *indexNumber = itemsToInsert[itemValueToInsert];
        NSUInteger index = indexNumber.unsignedIntegerValue;
        if (![itemsAlreadyUpdated containsIndex:indexNumber.unsignedIntegerValue]) {
            if (addedBlock) {
                addedBlock(index, itemToInsert);
            }
            [itemsAlreadyUpdated addIndex:index];
        }
    }
    
    return itemsAlreadyUpdated;
}
             
@end

NS_ASSUME_NONNULL_END
