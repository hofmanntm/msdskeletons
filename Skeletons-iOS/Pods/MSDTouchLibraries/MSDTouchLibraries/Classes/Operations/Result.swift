//
//  Result.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2016-06-14.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

/// Either a failure with a reason, or a success with a value.
public enum Result<Value> {
    case success(Value)
    case failure(Error)
    
    /// Creates a success.
    public init(value: Value) {
        self = .success(value)
    }
    
    /// Creates a failure.
    public init(error: Error) {
        self = .failure(error)
    }
    
    /// Creates a result from a function that can `throw`.
    public init(_ block: @autoclosure () throws -> Value) {
        self.init(attempt: block)
    }
    
    /// Creates a result from a function that can `throw`.
    public init(attempt block: () throws -> Value) {
        do {
            self = .success(try block())
        } catch {
            self = .failure(error)
        }
    }
    
    /// Returns the value on success, or `nil` on failure.
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Returns the error on failure, or `nil` on success.
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
    
    /// Returns a new result by mapping a successful value using `transform`, or by passing the failure error along.
    
    public func map<U>(_ transform: (Value) -> U) -> Result<U> {
        return flatMap { .success(transform($0)) }
    }
    
    /// Returns a successful result applied by `transform`, or by passing the failure error along.
    
    public func flatMap<U>(_ transform: (Value) -> Result<U>) -> Result<U> {
        switch self {
        case .success(let value):
            return transform(value)
        case .failure(let error):
            return .failure(error)
        }
    }
}

extension Result: CustomStringConvertible {
    public var description: String {
        switch self {
        case .success(let value):
            return ".Success(\(value))"
        case .failure(let error):
            return ".Failure(\(error))"
        }
    }
}

extension Result: CustomDebugStringConvertible {
    public var debugDescription: String {
        return description
    }
}
