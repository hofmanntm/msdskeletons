//
//  AsynchronousOperation.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2016-06-14.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

import Foundation

/**
    An asynchronous NSOperation.
 
    To use, subclass and override `main()` to kick off whatever asynchronous task. When finished, you must call `finish()`.
 
    Subclasses should feel free to override `cancel()` and do whatever they would like.
 */
open class AsynchronousOperation: Operation {
    
    /**
        Subclasses should override to start an asynchronous task. The operation is *not* finished once `main()` returns; rather, it's the subclass's responsibility to call `finish()` when the task is complete.
     
        If the operation is cancelled before it starts, `main()` is not called.
     */
    open override func main() {
        assertionFailure("\(type(of: self)) should override \(#function)")
        finish()
    }
    
    /// Subclasses *must* call `finish()` when the operation is complete, otherwise the operation will never leave the queue and dependencies will never start.
    public final func finish() {
        guard !isFinished else { return }
        state = .finished
        didFinish()
    }
    
    open func didFinish() {
        // Subclasses can override, but we don't need to do anything.
    }
    
    // MARK: Implementation details
    
    private let lock = NSLock()
    
    public enum State {
        case waiting, executing, finished
    }
    private var _state: State = .waiting
    open private(set) var state: State {
        get {
            return lock.withCriticalSection {
                self._state
            }
        }
        set {
            let oldValue = state
            
            // KVO needs to happen outside lock to avoid deadlock.
            switch (oldValue, newValue) {
            case (.waiting, .executing):
                willChangeValue(forKey: Key.isExecuting)
            case (.executing, .finished):
                willChangeValue(forKey: Key.isFinished)
            case (.waiting, .finished):
                willChangeValue(forKey: Key.isExecuting)
                willChangeValue(forKey: Key.isFinished)
            default:
                fatalError("invalid state change from \(oldValue) to \(newValue)")
            }
            
            lock.withCriticalSection {
                self._state = newValue
            }
            
            // KVO needs to happen outside lock to avoid deadlock.
            switch (oldValue, newValue) {
            case (.waiting, .executing):
                didChangeValue(forKey: Key.isExecuting)
            case (.executing, .finished):
                didChangeValue(forKey: Key.isFinished)
            case (.waiting, .finished):
                didChangeValue(forKey: Key.isExecuting)
                didChangeValue(forKey: Key.isFinished)
            default:
                fatalError("how was this not already a fatal error")
            }
        }
    }
    
    open override var isAsynchronous: Bool {
        return true
    }
    
    open override var isExecuting: Bool {
        return state == .executing
    }
    
    open override var isFinished: Bool {
        return state == .finished
    }
    
    public final override func start() {
        guard !isCancelled else { return finish() }
        
        main()
    }
}

private enum Key {
    static let isExecuting = "isExecuting"
    static let isFinished = "isFinished"
}


private extension NSLocking {
    func withCriticalSection<T>(_ block: () -> T) -> T {
        lock()
        let value = block()
        unlock()
        return value
    }
}
