//
//  ModelFetchFunctions.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2016-06-14.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

/// A bundle of associated functions for fetching, parsing, and saving model objects.
public protocol ModelFetchFunctions {
    
    /// An intermediary between fetching and parsing.
    associatedtype Data
    
    /// The result of parsing.
    associatedtype Model
    
    /// The result of saving. Ok to use `Void` if there's nothing worth passing on alongside `Success`.
    associatedtype Save
    
    /**
        Retrieves (asynchronously, if desired) some data for further processing.
     
        - Parameter completion: A block to call when the data is available, or an error occurs when attempting to retrieve it.
     
        - Returns: A token that can cancel the operation. If cancellation is impossible or does not make sense, simply return `nil`.
     */
    func fetch(_ completion: @escaping (_ result: Result<Data>) -> Void) -> Cancellable?
    
    /**
        - Returns: The parsed model instance.
     
        - Throws: A validation error encountered while parsing data.
     
        - Note: Implementer can assume `parse(_:)` is called off the main queue, so don't worry about blocking.
     */
    func parse(_ data: Data) throws -> Model
    
    /**
        Saves (asynchronously, if desired) the parsed model instance. A good place to make your database call.
     
        - Parameter model: The parsed model.
        - Parameter completion: A block to call when saving is complete or fails. On failure, the error is passed into the block.
     */
    func save(_ model: Model, completion: @escaping (_ result: Result<Save>) -> Void)
}


/// Wraps ModelFetchFunctions in an NSOperation for handy queueing.
open class ModelFetchOperation<Functions: ModelFetchFunctions>: AsynchronousOperation {
    /// On completion, either a `Success` along with the result of the successful save, or a `Failure` with the error that occurred during the operation's execution.
    open private(set) var result: Result<Functions.Save>?
    
    private let functions: Functions
    private let queue = DispatchQueue(label: "FPSTOperation internal queue")
    private var fetchCancelToken: Cancellable?
    
    public init(_ functions: Functions) {
        self.functions = functions
    }
    
    open override func main() {
        fetchCancelToken = functions.fetch(didFetch)
    }
    
    open override func cancel() {
        fetchCancelToken?.cancel()
        super.cancel()
    }
    
    private func didFetch(_ result: Result<Functions.Data>) {
        guard !isCancelled else {
            self.result = .failure(NSError(domain: NSCocoaErrorDomain, code: NSUserCancelledError, userInfo: nil))
            return self.finish()
        }
        
        switch result {
        case .failure(let error):
            self.result = .failure(error)
            return self.finish()
            
        case .success(let data):
            queue.async { self.parseInBackground(data) }
        }
    }
    
    private func parseInBackground(_ data: Functions.Data) {
        let model: Functions.Model
        do {
            model = try functions.parse(data)
        } catch {
            self.result = .failure(error)
            return finish()
        }
        
        functions.save(model) { (result) in
            self.result = result
            self.finish()
        }
    }
}


public protocol Cancellable {
    /// Attempts to cancel the represented task. It's ok to call `cancel()` multiple times, though only the first call can do anything.
    func cancel()
}
