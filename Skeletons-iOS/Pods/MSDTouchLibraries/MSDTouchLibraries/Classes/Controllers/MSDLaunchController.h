//
//  MSDLaunchController.h
//  MSDTouchLibraries
//

#import <Foundation/Foundation.h>

@class MSDAppUpgrade;
@class MSDLaunchMessage;
@protocol MSDLaunchControllerDelegate;

NS_ASSUME_NONNULL_BEGIN

/**
 * This class provides some miscellaneous launch-time services.
 * It keeps track of current and last-launch version, as well as the
 * latest app version.
 *
 * It also provides a standard mechanism by which a message can be
 * displayed at launch-time.
 */
NS_SWIFT_NAME(LaunchController)
@interface MSDLaunchController : NSObject

@property (class, readonly, nonatomic) MSDLaunchController *sharedLaunchController;

/**
 * Updates last launch version, then fetches the latest version & message info.
 * Those are also fetched occasionally afterwards.
 *
 * You can postpone calling this if you don't want to update the lastLaunchVersion 
 * for future runs immediately.
 */
- (void)completeLaunch;

/**
 * The delegate for this controller. This must be set before calling lastLaunchVersion.
 */
@property (nonatomic, weak) id<MSDLaunchControllerDelegate> delegate;


#pragma mark - Current & Previous Versions

/**
 * YES if the app has not previously been run.
 */
@property (readonly, nonatomic) BOOL isFirstLaunch;

/**
 * The current numeric bundle version.
 */
@property (readonly, nonatomic) NSUInteger currentVersion;

/**
 * The version of the app during the previous run.
 * 
 * When you call completeLaunch, the currentVersion is saved
 * for the next run of the app; the lastLaunchVersion is unchanged until
 * the app is killed and re-launched.
 *
 * Will be 0 if the app was not previously run.
 * This must be called after setting a delegate on this object, if one is needed.
 */
@property (readonly, nonatomic) NSUInteger lastLaunchVersion;

#pragma mark - App Upgrades

/**
 * The upgrade info for the latest version of the app available in the app store.
 * You can use KVO on this property to be notified of changes.
 * Defaults to nil.
 */
@property (nonatomic, readonly, strong, nullable) MSDAppUpgrade *upgrade;

/**
 * Shows the default UI for the latest upgrade.
 */
- (void)showDefaultUpgradeUI;

#pragma mark - Launch Messages

/**
 * The current message, or nil.
 * You can use KVO on this property to be notified of changes.
 */
@property (nonatomic, readonly, strong, nullable) MSDLaunchMessage *message;

/**
 * Get and set the last displayed date for a message.
 * This will be set by showing the default message UI; if you show
 * your own UI, you should set this yourself. 
 *
 * This is stored per message-identifier, so if another message comes from
 * the server with the same identifier, this date will apply to both.
 *
 * Defaults to nil if the message has never been seen.
 */
- (nullable NSDate *)lastDisplayDateForMessage:(MSDLaunchMessage *)message;
- (void)setLastDisplayDate:(nullable NSDate *)date forMessage:(MSDLaunchMessage *)message;

/**
 * Shows the default UI for the current message.
 */
- (void)showDefaultMessageUI;

@end


NS_SWIFT_NAME(LaunchControllerDelegate)
@protocol MSDLaunchControllerDelegate <NSObject>

@optional

/**
 * Implement this if you want to use a custom endpoint for updates.
 * If you return nil, this will not update version or message.
 */
- (nullable NSURL *)launchControllerUpdateURL:(MSDLaunchController *)launchController;

/**
 * If a previous version of this app stored the last-launch version in a different
 * user-defaults key, you can return that key here and we'll use it for backwards-compatability.
 */
- (NSString *)launchControllerUserDefaultsKeyForLastLaunchVersion:(MSDLaunchController *)launchController;

/**
 * This is called during launch and occasionally afterwards if there is a message saved or
 * retrieved from the server.
 * Return YES to have a default alert appear.
 *
 * If unimplemented, defaults to returning YES if the lastDisplayDateForMessage: is nil.
 * (That is, it defaults to showing a message with a given identifier once.)
 */
- (BOOL)launchController:(MSDLaunchController *)launchController shouldDisplayAlertForMessage:(MSDLaunchMessage *)message;

/**
 * This is called during launch and occasionally afterwards if there is an upgrade saved or
 * retrieved from the server.
 * Return YES to have a default alert appear.
 *
 * If unimplemented, defaults to returning YES if upgrade version is higher than the current version.
 * (That is, defaults to always showing an alert for applicable upgrades.)
 */
- (BOOL)launchController:(MSDLaunchController *)launchController shouldDisplayAlertForUpgrade:(MSDAppUpgrade *)upgrade;

@end

/**
 * Represents the latest version of the app, as well as
 * information to present to the user about it.
 */
@interface MSDAppUpgrade : NSObject

@property (nonatomic, readonly) NSUInteger version;
@property (nonatomic, readonly, copy, nullable) NSString *title;
@property (nonatomic, readonly, copy, nullable) NSString *body;
@property (nonatomic, readonly, copy, nullable) NSURL *upgradeURL;
@property (nonatomic, readonly) BOOL shouldForceUpgrade;
@property (nonatomic, readonly, copy, nullable) NSDictionary *userInfo; // extra keys from server

@end

/**
 * Represents a message from the server, as well as
 * information to present to the user about it.
 */
@interface MSDLaunchMessage : NSObject

// This identifier allows the client to identify the same message
// coming from the server more than once.
@property (nonatomic, readonly, copy) NSString *identifier;
@property (nonatomic, readonly, copy, nullable) NSString *title;
@property (nonatomic, readonly, copy, nullable) NSString *body;
@property (nonatomic, readonly, copy, nullable) NSString *actionName;
@property (nonatomic, readonly, copy, nullable) NSURL *actionURL;
@property (nonatomic, readonly, copy, nullable) NSDictionary *userInfo; // extra keys from server

@end

NS_ASSUME_NONNULL_END
