//
//  SwitchCollectionCellController.swift
//  MSDTouchLibraries
//
//  Created by Aidan Waite on 2018-06-27.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
A collection cell controller for making a collection cell with an attributed string title and a switch button. When the user taps the switch, the current state of the switch is passed to the action block. Initial value is set using the `SwitchDataSource`. Use the default nib or pass in your own. If passing in your own, its custom class must be set to `SwitchCollectionViewCell`.

## Example

You've got an account screen with two toggleable options: Integration and Notifications.

Make a `SwitchCellViewModel` for each case with an enum `Switch` as the `ContextType` and add them to your feed:

```
enum Switch {
  case integration
  case notifications
}

var feed: MSDFeedProtocol = {
  ArrayFeed(content: [
    SwitchCellViewModel(
      context: Switch.integration,
      title: NSAttributedString(string: "Integration", attributes: [.font: Theme.font.body])),
    SwitchCellViewModel(
      context: Switch.notifications,
      title: NSAttributedString(string: "Notifications", attributes: [.font: Theme.font.body])),
  ])
}()
```

Add a `SwitchCollectionCellController` to your `MSDMultiCollectionCellController`'s cellControllers. Here no custom nib is specified so the default nib will be used.

```
let cellController = MSDMultiCollectionCellController()
cellController.cellControllers = [
  SwitchCollectionCellController(
    for: collectionView,
    switchDataSource: { [weak self] content in
      self?.currentSwitchValue(for: content.context)
    },
    actionBlock: { [weak self] (content: SwitchCellViewModel<Switch>, isOn: Bool) in
      self?.didToggleSwitch(content, isOn)
  })
]
```

Where `currentSwitchValue` is used to set the initial value for each switch

```
private func currentSwitchValue(for content: Switch) -> Bool {
  switch content {
  case .notifications:
    return NotificationController.shared.isEnabled
  case .integration:
    return IntegrationController.shared.isEnabled
  }
}
```

And `didToggleSwitch` handles a switch being tapped

```
private func didToggleSwitch(_ content: SwitchCellViewModel<Switch>, _ isNowOn: Bool) {
  switch content.context {
  case .integration:
    IntegrationController.shared.setStatus(to: isNowOn)
  case .notifications:
    NotificationController.shared.setStatus(to: isNowOn)
  }
}
```
 */
open class SwitchCollectionCellController<ContextType: Hashable>: NSObject, MSDCollectionCellController {

    public weak var delegate: MSDCollectionCellControllerDelegate?

    private let reuseIdentifier: String

    private let actionBlock: ActionBlock
    private let switchDataSource: SwitchDataSource<ContextType>
    private let templateCell: SwitchCollectionViewCell

    public typealias ActionBlock = (_ content: SwitchCellViewModel<ContextType>, _ switchIsOn: Bool) -> Void
    public typealias SwitchDataSource<ContextType: Hashable> = (_ viewModel: SwitchCellViewModel<ContextType>) -> Bool?

    public init(
        for collectionView: UICollectionView,
        switchDataSource: @escaping SwitchDataSource<ContextType>,
        actionBlock: @escaping ActionBlock,
        nibName: String = "SwitchCollectionViewCell",
        bundle: Bundle? = Bundle(for: SwitchCollectionViewCell.self)) {

        self.templateCell = UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: nil, options: [:]).first as! SwitchCollectionViewCell
        self.reuseIdentifier = String(describing: SwitchCollectionViewCell.self)
        self.actionBlock = actionBlock
        self.switchDataSource = switchDataSource
        collectionView.register(UINib(nibName: nibName, bundle: bundle), forCellWithReuseIdentifier: self.reuseIdentifier)
    }

    public func acceptsContent(_ content: Any) -> Bool {
        return content is SwitchCellViewModel<ContextType>
    }

    public func reuseIdentifier(forContent content: Any, at indexPath: IndexPath) -> String {
        return self.reuseIdentifier
    }

    public func configureCell(_ untypedCell: UICollectionViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? SwitchCollectionViewCell, let content = untypedContent as? SwitchCellViewModel<ContextType> else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }

        cell.actionBlock = { [weak self] isOn in self?.actionBlock(content, isOn) }
        cell.label.attributedText = content.title
        cell.switchControl.isOn = switchDataSource(content) ?? false
    }

    public func cell(forContent content: Any, in collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseIdentifier, for: indexPath) as! SwitchCollectionViewCell
    }

    public func cellSize(forContent untypedContent: Any, in collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, at indexPath: IndexPath) -> CGSize {
        guard let content = untypedContent as? SwitchCellViewModel<ContextType> else {
            fatalInDebug(fatalInDebugMessage(for: nil, content: untypedContent))
            return .zero
        }

        self.configureCell(self.templateCell, forContent: content)

        let templateSize = CGSize(width: self.templateCell.frame.width, height: self.templateCell.frame.height)
        self.templateCell.bounds = CGRect(origin: .zero, size: templateSize)

        self.templateCell.setNeedsLayout()
        self.templateCell.layoutIfNeeded()

        return self.templateCell.systemLayoutSizeFitting(
            UIView.layoutFittingCompressedSize,
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .fittingSizeLevel)
    }

    private func fatalInDebugMessage(for cell: UICollectionViewCell?, content: Any) -> String {
        if let cell = cell {
            return "Wrong types sent to \(type(of: self)). Got cell of type \(type(of: cell)) while expecting \(SwitchCollectionViewCell.self) and content of type \(type(of: content)) when expecting \(SwitchCellViewModel<ContextType>.self)"
        }
        return "Wrong types sent to \(type(of: self)). Got content of type \(type(of: content)) when expecting \(SwitchCellViewModel<ContextType>.self)"
    }
}

/// A collection cell that is a label and switch button
open class SwitchCollectionViewCell: UICollectionViewCell {
    // This is private(set) because we may need to change the value of the switch after our switch button collection cell has already been initialized (and switch "isOn" is not part of the view model)
    @IBOutlet private(set) var switchControl: UISwitch!
    @IBOutlet var label: UILabel!

    var actionBlock: ((Bool) -> Void)?

    @IBAction private func valueChanged(_ sender: Any) {
        actionBlock?(switchControl.isOn)
    }
}
