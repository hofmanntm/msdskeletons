//
//  MSDSimpleCollectionCellController.h
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-06-11.
//
//

#import "MSDCollectionCellController.h"
#import "MSDCollectionCellControllerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

/*!
 This provides a simple way to implement MSDCollectionCellController.
 
 You can either create one of these directly and set contentClass and
 the various blocks, or you can subclass.
 
 You probably want to override the following methods in your subclass:
 - (instancetype)init; // (to call one of MSDCollectionCellController's inits, or use the default)
 - (BOOL)acceptsContent:(id)content; // defaults to YES
 - (void)configureCell:(UICollectionViewCell *)cell forContent:(id)content atIndexPath:(NSIndexPath*)indexPath;
 - (void)performSelectActionForContent:(id)content cell:(UICollectionViewCell *)cell;
 
 */

@interface MSDSimpleCollectionCellController : NSObject<MSDCollectionCellController>

/*!
 The class of content to accept. Defaults to NSObject.
 */
@property (nonatomic, strong) Class contentClass;

/*!
 A single reuse identifier to use.
 */
@property (nonatomic, copy, nullable) NSString *reuseIdentifier;

/*!
 Set these blocks to configure the cells, and run them on user action.
 */
@property (nonatomic, copy, nullable) void(^configureBlock)(id cell, id content);
@property (nonatomic, copy, nullable) BOOL(^shouldAllowSelectionBlock)(id cell, id content);
@property (nonatomic, copy, nullable) void(^selectActionBlock)(id cell, id content, id<MSDCollectionCellControllerDelegate> delegate);
@property (nonatomic, copy, nullable) void(^deselectActionBlock)(id cell, id content, id<MSDCollectionCellControllerDelegate> delegate);
@property (nonatomic, copy, nullable) void(^configureSupplementaryViewBlock)(id view, NSString *kind);

- (instancetype)initWithCellSize:(CGSize)cellSize headerReferenceSize:(CGSize)headerReferenceSize;
- (instancetype)initWithCellSize:(CGSize)cellSize headerReferenceSize:(CGSize)headerReferenceSize footerReferenceSize:(CGSize)footerReferenceSize NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (void)setReuseIdentifier:(nullable NSString *)reuseIdentifier forSupplementaryViewsOfKind:(NSString *)kind;

@end

NS_ASSUME_NONNULL_END
