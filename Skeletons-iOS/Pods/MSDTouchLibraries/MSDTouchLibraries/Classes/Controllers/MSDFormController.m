//
//  MSDFormController.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 10-06-01.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDFormController.h"

#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDTextElementValidator {
    NSString *_errorTitle;
    NSString *_errorMessage;
    id _textElement;
}

@synthesize invalidViews = _invalidViews;

- (instancetype)initWithTextElement:(UIView *)theTextElement title:(nullable NSString *)theTitle message:(nullable NSString *)theMessage {
    if ((self = [super init])) {
        MSDArgNotNil(theTextElement);
        
        _errorTitle = [theTitle copy];
        _errorMessage = [theMessage copy];
        _textElement = theTextElement;

        // We need to monitor these because user edits don't fire KVO messages reliably
        if ([_textElement isKindOfClass:[UITextView class]]) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInvalidViews) name:UITextViewTextDidChangeNotification object:_textElement];
        } else if ([_textElement isKindOfClass:[UITextField class]]) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateInvalidViews) name:UITextFieldTextDidChangeNotification object:_textElement];
        }
        
        // We need this, too, because the notifications aren't sent if we just set the text in code
        [_textElement addObserver:self forKeyPath:@"text" options:0 context:(__bridge void *)([MSDTextElementValidator class])];
        
        [self updateInvalidViews];
    }
    return self;
}

- (void)setInvalidViews:(NSArray *)newInvalidViews {
    _invalidViews = [newInvalidViews copy];
}

- (void)updateInvalidViews {
    NSArray *oldInvalidViews = self.invalidViews;
    NSArray *newInvalidViews;
    if ([self isTextValid:[_textElement text]]) {
        newInvalidViews = @[];
    } else {
        newInvalidViews = @[_textElement];
    }
    
    if (![newInvalidViews isEqual:oldInvalidViews]) {
        [self setInvalidViews:newInvalidViews];
    }    
}

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary *)change context:(nullable void *)context {
    if (context == (__bridge void *)[MSDTextElementValidator class]) {
        [self updateInvalidViews];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (NSString *)errorTitleForInvalidView:(UIView *)view {
    return _errorTitle;
}

- (NSString *)errorMessageForInvalidView:(UIView *)view {
    return _errorMessage;
}

// for subclasses to implement
- (BOOL)isTextValid:(NSString *)textContent {
    return YES;
}

- (void)dealloc {
    [_textElement removeObserver:self forKeyPath:@"text"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end



@interface MSDTextElementHasContentValidator : MSDTextElementValidator

@end

@implementation MSDTextElementHasContentValidator

- (BOOL)isTextValid:(NSString *)textContent {
    textContent = [textContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (textContent == nil || [textContent isEqualToString:@""]) {
        return NO;
    }
    return YES;
}

@end

@interface MSDTextElementMaxLengthValidator : MSDTextElementValidator {
    NSUInteger maxLength;
}

- (instancetype)initWithTextElement:(UIView *)textElement maxLength:(NSUInteger)maxLength title:(NSString *)title message:(NSString *)message;
@end

@implementation MSDTextElementMaxLengthValidator

- (instancetype)initWithTextElement:(UIView *)theTextElement maxLength:(NSUInteger)theMaxLength title:(NSString *)theTitle message:(NSString *)theMessage {
    if ((self = [super initWithTextElement:theTextElement title:theTitle message:theMessage])) {
        maxLength = theMaxLength;
        [self updateInvalidViews]; // we need to do this after we set our max length
    }
    return self;
}

- (BOOL)isTextValid:(NSString *)textContent {
    if ([textContent length] > maxLength) {
        return NO;
    }
    return YES;
}

@end


@interface MSDFormController () <UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate>

@property (nonatomic, assign, getter=isValid) BOOL valid;
- (BOOL)validateOrShowError:(BOOL)showError;
@end


@implementation MSDFormController {
    NSMutableArray *_validators;
    
    UIView *_errorView; // set while showing error dialog
}

- (instancetype)init {
    if ((self = [super init])) {
        _valid = YES;
    }
    return self;
}

#pragma mark -
#pragma mark Validation

- (void)updateValidState {
    BOOL validNow = [self validateOrShowError:NO];
    if (validNow != self.valid) {
        self.valid = validNow;
    }    
}

- (void)addValidator:(id<MSDFormValidator>)validator {
    [(NSObject *)validator addObserver:self forKeyPath:@"invalidViews" options:0 context:(__bridge void *)([MSDFormController class])];
    
    if (!_validators) {
        _validators = [[NSMutableArray alloc] init];
    }
    [_validators addObject:validator];
    [self updateValidState];
}

- (id<MSDFormValidator>)validateTextField:(UITextField *)field hasContentWithErrorTitle:(nullable NSString *)title message:(nullable NSString *)message {
    MSDArgNotNil(field);
   
    id<MSDFormValidator> validator = [[MSDTextElementHasContentValidator alloc] initWithTextElement:field title:title message:message];
    [self addValidator:validator];
    return validator;
}

- (id<MSDFormValidator>)validateTextView:(UITextView *)view hasContentWithErrorTitle:(nullable NSString *)title message:(nullable NSString *)message {
    MSDArgNotNil(view);
    
    id<MSDFormValidator> validator = [[MSDTextElementHasContentValidator alloc] initWithTextElement:view title:title message:message];
    [self addValidator:validator];
    return validator;
}

- (id<MSDFormValidator>)validateTextField:(UITextField *)field
                   hasContentLengthAtMost:(NSUInteger)length
                           withErrorTitle:(nullable NSString *)title
                                  message:(nullable NSString *)message {
    MSDArgNotNil(field);
    
    id<MSDFormValidator> validator = [[MSDTextElementMaxLengthValidator alloc] initWithTextElement:field
                                                                                         maxLength:length
                                                                                             title:title
                                                                                           message:message];
    [self addValidator:validator];
    return validator;
}

- (id<MSDFormValidator>)validateTextView:(UITextView *)view
                  hasContentLengthAtMost:(NSUInteger)length
                          withErrorTitle:(nullable NSString *)title
                                 message:(nullable NSString *)message {
    MSDArgNotNil(view);
    
    id<MSDFormValidator> validator = [[MSDTextElementMaxLengthValidator alloc] initWithTextElement:view
                                                                                          maxLength:length
                                                                                              title:title 
                                                                                            message:message];
    [self addValidator:validator];
    return validator;
}

- (void)removeValidator:(id<MSDFormValidator>)validator {
    if ([_validators containsObject:validator]) {
        [(NSObject*)validator removeObserver:self forKeyPath:@"invalidViews"];
        
        [_validators removeObject:validator];
        [self updateValidState];
    }
}

- (BOOL)validateOrShowError:(BOOL)showError {
    for (id<MSDFormValidator> validator in _validators) {
        NSArray *invalidViews = [validator invalidViews];
        if ([invalidViews count]) {
            if (showError) {
                UIView *newErrorView = invalidViews[0];
                _errorView = newErrorView;
            
                NSString *title = [validator errorTitleForInvalidView:_errorView];
                NSString *message = [validator errorMessageForInvalidView:_errorView];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"Button when validation fails in MSDTextFieldFormController") otherButtonTitles:nil];
                [alert show];
            }
            
            return NO;
        }
    }
    return YES;
}

- (void)observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary *)change context:(nullable void *)context {
    if (context == (__bridge void *)([MSDFormController class])) {
        BOOL newValid = [self validateOrShowError:NO];
        if (newValid != self.valid) {
            self.valid = newValid;
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - Firing the form

- (IBAction)fireForm:(nullable id)sender {
    for (UITextField *field in self.orderedViews) {
        if ([field isFirstResponder]) {
            if (![field endEditing:NO]) {
                return; // couldn't end editing
            }
        }
    }
    
    if ([self validateOrShowError:YES]) {
        [self.delegate formControllerDidFire:self];
    }    
}

#pragma mark - Return Handling

- (BOOL)textElementShouldReturn:(UIView *)view {
    NSUInteger index = [self.orderedViews indexOfObject:view];
    if (index != NSNotFound) {
        NSUInteger nextIndex = index + 1;
        if (nextIndex == [self.orderedViews count]) {
            [self fireForm:self];
        } else {
            UITextField *nextField = self.orderedViews[nextIndex];
            if ([self.delegate respondsToSelector:@selector(formController:willMakeViewFirstResponder:)]) {
                [self.delegate formController:self willMakeViewFirstResponder:nextField];
            }
            [nextField becomeFirstResponder];
        }
        return NO;
    }
    return YES;    
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [self textElementShouldReturn:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([self.textFieldDelegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.textFieldDelegate textFieldDidEndEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([self.textFieldDelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        return [self.textFieldDelegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if([self.textFieldDelegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.textFieldDelegate textFieldShouldClear:textField];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if([self.textFieldDelegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.textFieldDelegate textFieldDidBeginEditing:textField];
    }
}


#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)theTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)string {
	
    if ([string isEqualToString:@"\n"]) {
        return [self textElementShouldReturn:theTextView];
	}
    
    if([self.textViewDelegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
        return [self.textViewDelegate textView:theTextView shouldChangeTextInRange:range replacementText:string];
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    if([self.textViewDelegate respondsToSelector:@selector(textViewDidChange:)]) {
        [self.textViewDelegate textViewDidChange:textView];
    }
}

- (void)textViewDidChangeSelection:(UITextView *)textView {
    if([self.textViewDelegate respondsToSelector:@selector(textViewDidChangeSelection:)]) {
        [self.textViewDelegate textViewDidChangeSelection:textView];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (_errorView) {
        
        if ([_errorView isKindOfClass:[UITextView class]] || [_errorView isKindOfClass:[UITextField class]]) {
            
            if ([self.delegate respondsToSelector:@selector(formController:willMakeViewFirstResponder:)]) {
                [self.delegate formController:self willMakeViewFirstResponder:_errorView];
            }
            
            [_errorView becomeFirstResponder];
        }
        
        _errorView = nil;
    }
}

#pragma mark - Memory Management

- (void)dealloc {
    for (NSObject<MSDFormValidator> *validator in _validators) {
        [validator removeObserver:self forKeyPath:@"invalidViews"];
    }
    
}

@end

NS_ASSUME_NONNULL_END
