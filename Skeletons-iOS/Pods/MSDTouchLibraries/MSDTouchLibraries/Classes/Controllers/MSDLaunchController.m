//
//  MSDLaunchController.m
//  MSDTouchLibraries
//

#import "MSDLaunchController.h"
#import "MSDKeychainItem.h"
#import "MSDGenericParsers.h"
#import "MSDStepper.h"
#import "MSDFileCache.h"
#import "MSDTouchLibraryDefines.h"
#import "MSDAlertQueue.h"


static NSString * const MessageSeenDatesKey = @"MSDLaunchControllerMessageSeenDates";

static NSString * const UniqueIdentifierKeychainPrefix = @"MSDLaunchControllerUniqueIdentifier";

static const NSTimeInterval MinBackgroundTimeForUpdate = 5 * 60; // 5 minutes

static NSString * const DefaultUpdateURL = @"https://ms-launch.appspot.com/api/1/launch.json";

@interface MSDAppUpgrade ()

@property (nonatomic) NSUInteger version;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSURL *upgradeURL;
@property (nonatomic) BOOL shouldForceUpgrade;
@property (nonatomic, copy) NSDictionary *userInfo;

@end

@implementation MSDAppUpgrade


@end

@interface MSDLaunchMessage ()

@property (nonatomic, copy) NSString *identifier;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSString *actionName;
@property (nonatomic, copy) NSURL *actionURL;
@property (nonatomic, copy) NSDictionary *userInfo; // extra keys from server

@end

@implementation MSDLaunchMessage


@end


@interface MSDLaunchController () <UIAlertViewDelegate, MSDFileCacheDelegate>

@property (nonatomic, strong) MSDAppUpgrade *upgrade;
@property (nonatomic, strong) MSDLaunchMessage *message;

@property (nonatomic, strong) MSDAlertQueue *alertQueue;
@property (nonatomic, assign) BOOL isOpeningURLExternally;

@property (nonatomic, strong) NSDate *lastBackgroundDate;

@end

@implementation MSDLaunchController {
    BOOL _completedLaunch;
    BOOL _computedLastLaunch;
    MSDFileCache *_fileCache;
    NSNumber *_lastLaunchVersion;
    BOOL _shouldShowUpgradeIfNeeded;
}

MSD_SINGLETON_METHOD(MSDLaunchController, sharedLaunchController);

- (instancetype)init {
    if (self = [super init]) {
        _alertQueue = [MSDAlertQueue new];

        _fileCache = [[MSDFileCache alloc] initWithName:@"MSDLaunchController"
                                                 bundle:nil
                                           subdirectory:nil
                                            storageType:MSDFileCacheStorageTypeOffline];
        _fileCache.delegate = self;
    }
    return self;
}

- (void)completeLaunch {
    if (_completedLaunch) {
        return;
    }
    _completedLaunch = YES;
    [self lastLaunchVersion];
    [[NSUserDefaults standardUserDefaults] setInteger:[self currentVersion]
                                              forKey:[self lastLaunchVersionKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(willEnterForegroundNotification:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterBackgroundNotification:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [self fetchLatestInfo];
}

- (void)dealloc {
    _fileCache.delegate = nil;
}

#pragma mark - Versions

- (NSString *)lastLaunchVersionKey {
    if ([self.delegate respondsToSelector:@selector(launchControllerUserDefaultsKeyForLastLaunchVersion:)]) {
        return [self.delegate launchControllerUserDefaultsKeyForLastLaunchVersion:self];
    }
    return @"MSDLaunchControllerLastLaunchVersion";
}

- (NSUInteger)currentVersion {
    return [[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey] intValue];
}

- (NSUInteger)lastLaunchVersion {
    if (!_computedLastLaunch) {
        _lastLaunchVersion = [[NSUserDefaults standardUserDefaults] objectForKey:[self lastLaunchVersionKey]];
        _computedLastLaunch = YES;
    }
    
    return _lastLaunchVersion.unsignedIntegerValue;
}

- (void)setDelegate:(id<MSDLaunchControllerDelegate>)delegate {
    if (delegate) {
        MSDCheck(!_computedLastLaunch || ![delegate respondsToSelector:@selector(launchControllerUserDefaultsKeyForLastLaunchVersion:)],
                 @"setDelegate: called after lastLaunchVersion already computed. This is unsafe, since the delegate's launchControllerUserDefaultsKeyForLastLaunchVersion: method would have been ignored.");
    }
    _delegate = delegate;
}

- (BOOL)isFirstLaunch {
    return [self lastLaunchVersion] == 0 && _lastLaunchVersion == nil;
}

#pragma mark - Upgrade

- (void)showDefaultUpgradeUI {
    if (!self.upgrade) {
        return;
    }

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:self.upgrade.title
                                                                   message:self.upgrade.body
                                                            preferredStyle:UIAlertControllerStyleAlert];
    __weak __typeof__(self) welf = self;
    __weak __typeof__(alert) walert = alert;
    if (self.upgrade.upgradeURL) {


        [alert addAction:
         [UIAlertAction actionWithTitle:NSLocalizedString(@"Update", @"Button to update app to the latest version")
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
          {
              __typeof__(self) self = welf;
              __typeof__(alert) alert = walert;
              if (alert) {
                  [self.alertQueue didDismissAlert:alert];
              }

              // open upgrade
              NSURL *url = self.upgrade.upgradeURL;
              if (url) {
                  self.isOpeningURLExternally = YES;
                  [[UIApplication sharedApplication] openURL:url];
              }
          }]];
    }

    if (!self.upgrade.shouldForceUpgrade) {
        [alert addAction:
         [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"Continue-without-updating button on launch.")
                                  style:UIAlertActionStyleCancel
                                handler:^(UIAlertAction *action)
          {
              __typeof__(self) self = welf;
              __typeof__(alert) alert = walert;
              if (alert) {
                  [self.alertQueue didDismissAlert:alert];
              }
          }]];
    }

    [self.alertQueue showAlert:alert];
}

#pragma mark - Messages

- (void)showDefaultMessageUI {
    if (!self.message) {
        return;
    }

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:self.message.title
                                                                   message:self.message.body
                                                            preferredStyle:UIAlertControllerStyleAlert];
    __weak __typeof__(self) welf = self;
    __weak __typeof__(alert) walert = alert;

    if (self.message.actionURL) {
        [alert addAction:
         [UIAlertAction actionWithTitle:self.message.actionName ?: NSLocalizedString(@"View", @"Default message action button name")
                                  style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction *action)
          {
              __typeof__(self) self = welf;
              __typeof__(alert) alert = walert;

              [self setLastDisplayDate:[NSDate date] forMessage:self.message];

              if (alert) {
                  [self.alertQueue didDismissAlert:alert];
              }

              // perform action
              NSURL *url = self.message.actionURL;
              if (url) {
                  self.isOpeningURLExternally = YES;
                  [[UIApplication sharedApplication] openURL:url];
              }
          }]];
    }

    [alert addAction:
     [UIAlertAction actionWithTitle:NSLocalizedString(@"Close", @"Continue-without-updating button on launch.")
                              style:UIAlertActionStyleCancel
                            handler:^(UIAlertAction *action)
      {
          __typeof__(self) self = welf;
          __typeof__(alert) alert = walert;

          [self setLastDisplayDate:[NSDate date] forMessage:self.message];

          if (alert) {
              [self.alertQueue didDismissAlert:alert];
          }
      }]];

    [self.alertQueue showAlert:alert];
}



- (nullable NSDate *)lastDisplayDateForMessage:(MSDLaunchMessage *)message {
    NSDictionary *messageSeenDates = [[NSUserDefaults standardUserDefaults] objectForKey:MessageSeenDatesKey];
    return [messageSeenDates objectForKey:message.identifier];
}

- (void)setLastDisplayDate:(nullable NSDate *)date forMessage:(MSDLaunchMessage *)message {
    NSParameterAssert(message != nil);
    
    NSMutableDictionary *messageSeenDates = [[[NSUserDefaults standardUserDefaults] objectForKey:MessageSeenDatesKey] mutableCopy];
    if (!messageSeenDates) {
        messageSeenDates = [NSMutableDictionary dictionary];
    }
    
    if (date) {
        [messageSeenDates setObject:date forKey:message.identifier];
    } else {
        [messageSeenDates removeObjectForKey:message.identifier];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:messageSeenDates forKey:MessageSeenDatesKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Notifications

- (void)willEnterForegroundNotification:(NSNotification *)notification {
    NSDate *compareDate = [self.lastBackgroundDate dateByAddingTimeInterval:MinBackgroundTimeForUpdate];
    if (self.isOpeningURLExternally || [compareDate compare:[NSDate date]] == NSOrderedAscending) {
        self.isOpeningURLExternally = NO;
        [self fetchLatestInfo];
    }
}

- (void)didEnterBackgroundNotification:(NSNotification *)notification {
    self.lastBackgroundDate = [NSDate date];
}

#pragma mark - Fetching
- (NSURL *)fileCache:(MSDFileCache *)cache remoteURLForFileNamed:(NSString *)filename {
   
    NSURL *url = [NSURL URLWithString:DefaultUpdateURL];
    
    if ([self.delegate respondsToSelector:@selector(launchControllerUpdateURL:)]) {
        url = [self.delegate launchControllerUpdateURL:self];
    }
    
    if (!url) {
        MSDDebugLog(@"Not updating launch data; launchControllerUpdateURL: returned nil");
    }

    return url;
}

- (NSDictionary *)fileCache:(MSDFileCache *)cache remoteRequestParametersForFileNamed:(NSString *)filename {

    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:@"bundleIdentifier"];
    [parameters setObject:@([self currentVersion]) forKey:@"bundleVersion"];
    
#ifdef DEBUG
    [parameters setObject:@YES forKey:@"debug"];
#else
    [parameters setObject:@NO forKey:@"debug"];
#endif
    
#ifdef DISTRIBUTION
    [parameters setObject:@YES forKey:@"distribution"];
#else
    [parameters setObject:@NO forKey:@"distribution"];
#endif
    
    [parameters setObject:[[UIDevice currentDevice] model] forKey:@"deviceModel"];
    [parameters setObject:@"ios" forKey:@"platform"];
    [parameters setObject:[[UIDevice currentDevice] systemVersion] forKey:@"systemVersion"];
    [parameters setObject:[self uniqueIdentifier] forKey:@"uniqueIdentifier"];
    
    return parameters;
}

- (NSString *)uniqueIdentifier {
    
    MSDKeychainItem *item = [[MSDKeychainItem alloc] initWithIdentifier:UniqueIdentifierKeychainPrefix accessGroup:nil];
    if (!item.encryptedObject) {
        item.encryptedObject = [NSUUID UUID].UUIDString;
    }
    return item.encryptedObject;
}

- (id)fileCache:(MSDFileCache *)cache contentsForFileNamed:(NSString *)filename fromPath:(NSString *)path error:(NSError **)error {
    NSDictionary *contents = nil;
    
    NSData *data = [NSData dataWithContentsOfFile:path options:NSDataReadingMappedIfSafe error:error];
    if (data) {
        id<MSDParser> parser = [MSDGenericParsers parserByApplyingParsers:[MSDGenericParsers JSONParser], [MSDStepper stepParserWithBlock:^id(MSDStepper *stepper) {
            [stepper requiredDictionaryValue:[stepper context]];

            NSMutableDictionary *result = [NSMutableDictionary dictionary];
            
            NSDictionary *messageInfo = [stepper optionalDictionaryForKey:@"message"];
            if (messageInfo) {
                MSDLaunchMessage *message = [[MSDLaunchMessage alloc] init];
                [stepper useContext:messageInfo
                          forBlock:^id _Nullable(MSDStepper *stepper) {
                              message.identifier = [stepper requiredStringForKey:@"identifier"];
                              message.title = [stepper optionalStringForKey:@"title"];
                              message.body = [stepper optionalStringForKey:@"body"];
                              message.actionName = [stepper optionalStringForKey:@"actionName"];
                              message.actionURL = [stepper optionalURLForKey:@"actionURL"];
                              message.userInfo = [stepper optionalDictionaryForKey:@"userInfo"];
                              return nil;
                          }];
                [result setObject:message forKey:@"message"];
            }
            
            NSDictionary *latest = [stepper optionalDictionaryForKey:@"upgrade"];
            if (latest) {
                MSDAppUpgrade *upgrade = [[MSDAppUpgrade alloc] init];
                [stepper useContext:latest
                          forBlock:^id _Nullable(MSDStepper *stepper) {
                              upgrade.version = [stepper requiredIntForKey:@"version"];
                              upgrade.title = [stepper optionalStringForKey:@"title"];
                              upgrade.body = [stepper optionalStringForKey:@"body"];
                              upgrade.upgradeURL = [stepper optionalURLForKey:@"upgradeURL"];
                              upgrade.shouldForceUpgrade = [stepper optionalBoolForKey:@"force" withDefault:NO];
                              upgrade.userInfo = [stepper optionalDictionaryForKey:@"userInfo"];
                              return nil;
                          }];
                [result setObject:upgrade forKey:@"upgrade"];
            }
            return result;
        }], nil];
        contents = [parser parseInput:data error:error];
    }
    return contents;
}

-(NSString *)fileCache:(MSDFileCache *)cache remoteRequestMethodForFileNamed:(NSString *)filename {
    return @"POST";
}

- (void)fetchLatestInfo {
    [_fileCache loadContentsOfFileNamed:@"launch.json"
                      completionHandler:^(NSDictionary *contents, NSError *error) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              if (!contents) {
                                  MSDErrorLog(@"Couldn't parse launch response: %@", error);
                              } else {
                                  self.message = [contents objectForKey:@"message"];
                                  self.upgrade = [contents objectForKey:@"upgrade"];
                                  
                                  [self showMessageIfNeeded];
                                  [self showUpgradeIfNeeded];
                              }
                          });
                      }];
}

- (void)showMessageIfNeeded {
    BOOL shouldShow = self.message && [self lastDisplayDateForMessage:self.message] == nil;
    if (self.message && [self.delegate respondsToSelector:@selector(launchController:shouldDisplayAlertForMessage:)]) {
        shouldShow = [self.delegate launchController:self shouldDisplayAlertForMessage:self.message];
    }

    if (shouldShow) {
        [self showDefaultMessageUI];
    }
}

- (void)showUpgradeIfNeeded {
    BOOL shouldShow = self.upgrade && self.upgrade.version > self.currentVersion;
    if (self.upgrade && [self.delegate respondsToSelector:@selector(launchController:shouldDisplayAlertForUpgrade:)]) {
        shouldShow = [self.delegate launchController:self shouldDisplayAlertForUpgrade:self.upgrade];
    }
    
    if (shouldShow) {
        [self showDefaultUpgradeUI];
    }
}

@end
