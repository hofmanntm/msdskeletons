//
//  MSDWaitingLocationController.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 10-05-18.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


NS_ASSUME_NONNULL_BEGIN

typedef void(^MSDWaitingLocationFoundCompletion)(CLLocation * _Nullable , NSError * _Nullable);

/*!
 * Provides a mechanism where you can ask for a location
 * with a desired accuracy and wait for some time to get it.
 */
@interface MSDWaitingLocationController : NSObject<CLLocationManagerDelegate>

/*! 
 Call to start a location search, which ends with the desired accuracy or waits for the given delay.
 The completion block will receive a location, and possibly an error if the location couldn't be found.
 If the location was found, but with a lower accuracy, this will not cause an error. Check the location's accuracy
 information if you want to detect this situation.
 */
- (void)findLocationWithAccuracy:(CLLocationAccuracy)accuracy waitingUpToDelay:(NSTimeInterval)delay completion:(nonnull MSDWaitingLocationFoundCompletion)completion;

/*!
 Call this to cancel a previous findLocationWithAccuracy:waitingUpToDelay:completion: request.
 */
- (void)cancelFindLocation;

/*! 
 This indicates that it is OK if the controller begins a location
 search before it is actually needed.
 
 This will not prompt the user to use their location, but will start a search
 if they've already permitted use of thier location.
 */
- (void)beginPreflightIfLocationAllowedWithAccuracy:(CLLocationAccuracy)accuracy;

/*! 
 This indicates that it is not OK if the controller is running a location
 search before requested.
 */
- (void)cancelPreflight;

/*!
 * Convinience method for [self cancelPreflight]; [self cancelFindLocation];
 */
- (void)cancelAll;

/*!
 * How long we should keep a location of the desired accuracy
 * around before asking for a new one.
 * Defaults to 0.0f, meaning we always ask for a fresh location when you
 * ask for one. (Note that if the timeout is too short, you could
 * still get a location more stale than this.)
 */
@property(nonatomic, assign) NSTimeInterval maximumLocationStaleness;

/*!
 * Returns if we're currently finding a location;
 * rethrns NO if only a preflight is going on.
 */
@property(nonatomic, readonly, getter=isFindingLocation) BOOL findingLocation;

/*!
 * See -[CLLocationManager requestWhenInUseAuthorization].  On if iOS version is < 8, do nothing.
 */
- (void)requestWhenInUseAuthorization;

/*!
 * See -[CLLocationManager requestAlwaysAuthorization].  On if iOS version is < 8, do nothing.
 */
- (void)requestAlwaysAuthorization;

@end

NS_ASSUME_NONNULL_END
