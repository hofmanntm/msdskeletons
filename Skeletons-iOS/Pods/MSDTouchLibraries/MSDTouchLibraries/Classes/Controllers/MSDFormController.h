//
//  MSDFormController.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 10-06-01.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MSDFormController;

NS_ASSUME_NONNULL_BEGIN

@protocol MSDFormControllerDelegate<NSObject>

- (void)formControllerDidFire:(MSDFormController *)controller;

@optional
- (void)formController:(MSDFormController *)controller willMakeViewFirstResponder:(UIView *)view;

@end


/*!
 * This protocol is implemented by all validators.
 * These are usually created internally, but are returned
 * so they can be removed.
 */
@protocol MSDFormValidator<NSObject>

// invalidViews must be KVO-compliant
@property (nonatomic, readonly) NSArray *invalidViews;
- (NSString *)errorTitleForInvalidView:(UIView *)view;
- (NSString *)errorMessageForInvalidView:(UIView *)view;

@end

@interface MSDTextElementValidator : NSObject<MSDFormValidator>

- (instancetype)initWithTextElement:(UIView *)textElement title:(nullable NSString *)title message:(nullable NSString *)message NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

// for subclasses to implement
- (BOOL)isTextValid:(NSString *)textContent;
- (void)updateInvalidViews; // call internally as needed

@end


/*!
 This class manages a form of text fields and views. When set as 
 the delegate of those views, pressing the return key will advance
 to the next one in the list. Pressing the return key on the final
 view will fire the @p MSDFormControllerDelegate method
 @p formControllerDidFire:
 
 You can also specify validation functions that are called before
 @p formControllerDidFire: is invoked; if validation fails, an error is shown
 and the offending field is given first responder status.
 
 This controller also ensures that endEditing: is called before calling
 @ formControllerDidFire: , so the values in the fields are up-to-date.
 */
@interface MSDFormController : NSObject

/*! 
 Indicates that the specified text field must contain characters other than whitespace and newlines before firing.
 
 @param field The field whose content we should validate.
 @param title The title shown for an error message.
 @param message The message shown for an error message.
 */
- (id<MSDFormValidator>)validateTextField:(UITextField *)field hasContentWithErrorTitle:(nullable NSString *)title message:(nullable NSString *)message;

/*! 
 Indicates that the specified text view must contain characters other than whitespace and newlines before firing.
 
 @param view The view whose content we should validate.
 @param title The title shown for an error message.
 @param message The message shown for an error message.
 */
- (id<MSDFormValidator>)validateTextView:(UITextView *)view hasContentWithErrorTitle:(nullable NSString *)title message:(nullable NSString *)message;

/*!
 Indicates that the specified text field must have fewer than a
 certain number of characters before firing.
 
 @param field The field whose content we should validate.
 @param length The maximum number of characters we permit.
 @param title The title shown for an error message.
 @param message The message shown for an error message.
 */
- (id<MSDFormValidator>)validateTextField:(UITextField *)field
                   hasContentLengthAtMost:(NSUInteger)length
                           withErrorTitle:(nullable NSString *)title
                                  message:(nullable NSString *)message;

/*! 
 Indicates that the specified text view must have fewer than a 
 certain number of characters before firing.
 
 @param view The view whose content we should validate.
 @param length The maximum number of characters we permit.
 @param title The title shown for an error message.
 @param message The message shown for an error message.
 */
- (id<MSDFormValidator>)validateTextView:(UITextView *)view 
                  hasContentLengthAtMost:(NSUInteger)length
                          withErrorTitle:(nullable NSString *)title
                                 message:(nullable NSString *)message;

/*!
 Removes a validator previously added.
 */
- (void)removeValidator:(id<MSDFormValidator>)validator;

- (void)addValidator:(id<MSDFormValidator>)validator;

/*! 
 An IBAction that is equivalent to pressing return on the form.
 You can use this to hook other buttons up to the same validation mechanics
 and callback as the return key.
 */
- (IBAction)fireForm:(nullable id)sender;

/*! 
 Returns whether all form elements are valid.
 KVO-compliant.
 */
@property (nonatomic, readonly, getter=isValid) BOOL valid;

/*! The form fields, in traversal order. KVO-compliant. */
@property (nonatomic, copy) NSArray *orderedViews;


@property (nonatomic, weak) IBOutlet id<MSDFormControllerDelegate> delegate;

@property (nonatomic, weak) IBOutlet id<UITextFieldDelegate> textFieldDelegate; // forwarding delegate
@property (nonatomic, weak) IBOutlet id<UITextViewDelegate> textViewDelegate; // forwarding delegate

@end

NS_ASSUME_NONNULL_END
