//
//  MSDSimpleCollectionCellController.m
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-06-11.
//
//

#import "MSDSimpleCollectionCellController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDSimpleCollectionCellController ()
@property (nonatomic, assign) CGSize cellSize;
@property (nonatomic, assign) CGSize headerReferenceSize;
@property (nonatomic, assign) CGSize footerReferenceSize;
@property (nonatomic, strong) NSMutableDictionary *supplementaryReuseIdentifiers;
@end

@implementation MSDSimpleCollectionCellController

@synthesize delegate = _delegate;

- (instancetype)initWithCellSize:(CGSize)cellSize headerReferenceSize:(CGSize)headerReferenceSize {
    return [self initWithCellSize:cellSize headerReferenceSize:headerReferenceSize footerReferenceSize:CGSizeZero];
}

- (instancetype)initWithCellSize:(CGSize)cellSize headerReferenceSize:(CGSize)headerReferenceSize footerReferenceSize:(CGSize)footerReferenceSize {
    self = [super init];
    
    if (self) {
        self.cellSize = cellSize;
        self.headerReferenceSize = headerReferenceSize;
        self.footerReferenceSize = footerReferenceSize;
    }
    
    return self;
}

- (BOOL)acceptsContent:(id)content {
    return [content isKindOfClass:self.contentClass];
}

- (NSString *)reuseIdentifierForContent:(id)content atIndexPath:(NSIndexPath *)indexPath {
    if (self.reuseIdentifier) {
        return self.reuseIdentifier;
    } else {
        return NSStringFromClass([self class]);
    }
}

- (void)setReuseIdentifier:(nullable NSString*)reuseIdentifier forSupplementaryViewsOfKind:(NSString*)kind {
    if (!self.supplementaryReuseIdentifiers) {
        self.supplementaryReuseIdentifiers = [NSMutableDictionary new];
    }
    
    if (reuseIdentifier) {
        self.supplementaryReuseIdentifiers[kind] = reuseIdentifier;
    } else {
        [self.supplementaryReuseIdentifiers removeObjectForKey:kind];
    }
}

- (nullable NSString *)reuseIdentifierForSupplementaryElementOfKind:(NSString *)kind {
    NSString *result = self.supplementaryReuseIdentifiers[kind];
    
    return result;
}

- (UICollectionViewCell *)cellForContent:(id)content inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *dequeuedCell = [collectionView dequeueReusableCellWithReuseIdentifier:[self reuseIdentifierForContent:content atIndexPath:indexPath] forIndexPath:indexPath];
    return dequeuedCell;
}

- (CGSize)cellSizeForContent:(id)content inCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout atIndexPath:(NSIndexPath *)indexPath {
    
    if (CGSizeEqualToSize(self.cellSize, CGSizeZero) && [collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionViewLayout;
        return layout.itemSize;
    } else {
        return self.cellSize;
    }
}

- (void)configureCell:(UICollectionViewCell *)cell forContent:(id)content {
    if (self.configureBlock) {
        self.configureBlock(cell, content);
    }
}

- (nullable UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)kind inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    
    NSString *reuseIdentifier = [self reuseIdentifierForSupplementaryElementOfKind:kind];
    if (reuseIdentifier) {
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        return view;
    } else {
        return nil;
    }
}

- (CGSize)referenceSectionHeaderSizeinCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout {
    if (CGSizeEqualToSize(self.headerReferenceSize, CGSizeZero) && [collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionViewLayout;
        return layout.headerReferenceSize;
    } else {
        return self.headerReferenceSize;
    }
}

- (CGSize)referenceSectionFooterSizeinCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout {
    if (CGSizeEqualToSize(self.footerReferenceSize, CGSizeZero) && [collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionViewLayout;
        return layout.footerReferenceSize;
    } else {
        return self.footerReferenceSize;
    }
}

- (void)configureSupplementaryView:(UICollectionReusableView *)view ofKind:(NSString *)kind {
    if (self.configureSupplementaryViewBlock) {
        self.configureSupplementaryViewBlock(view, kind);
    }
}

- (BOOL)shouldAllowSelectionForContent:(id)content cell:(nullable UICollectionViewCell *)cell {
    if (self.shouldAllowSelectionBlock) {
        return self.shouldAllowSelectionBlock(cell, content);
    }
    
    return YES;
}

- (void)performSelectActionForContent:(id)content cell:(nullable UICollectionViewCell *)cell {
    if (self.selectActionBlock) {
        self.selectActionBlock(cell, content, self.delegate);
    } else if ([self.delegate respondsToSelector:@selector(collectionCellController:performSelectActionForContent:)]) {
        [self.delegate collectionCellController:self performSelectActionForContent:content];
    }
}

- (void)performDeselectActionForContent:(id)content cell:(nullable UICollectionViewCell *)cell {
    if (self.deselectActionBlock) {
        self.deselectActionBlock(cell, content, self.delegate);
    } else if ([self.delegate respondsToSelector:@selector(collectionCellController:performDeselectActionForContent:)]) {
        [self.delegate collectionCellController:self performDeselectActionForContent:content];
    }
}

@end

NS_ASSUME_NONNULL_END
