//
//  SpacingCollectionCellController.swift
//  MSDTouchLibraries
//
//  Created by Aidan Waite on 2018-06-26.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
 A collection cell controller for making a blank collection cell with a specified height and optional background color.

 ## Example use case

 Mocks show an 8pt gap between the bottom of one cell and the top of another. Rather than wrestling with view section insets or some such, simply add a `SpacingCollectionCellViewModel` to your feed with a height of 8pt. Now the space should be 10pt instead of 8pt? No problem, update the height value give to `SpacingCollectionCellViewModel`'s initializer to 10pt. Now the background should be light grey instead of clear? No problem, add a background color in your call to `SpacingCollectionCellViewModel`'s initializer.
 */
open class SpacingCollectionCellController: NSObject, MSDCollectionCellController {

    public weak var delegate: MSDCollectionCellControllerDelegate?

    private let reuseIdentifier: String

    public init(for collectionView: UICollectionView) {
        self.reuseIdentifier = String(describing: SpacingCollectionCellController.self)
        collectionView.register(SpacingCollectionViewCell.self, forCellWithReuseIdentifier: self.reuseIdentifier)
    }

    public func acceptsContent(_ content: Any) -> Bool {
        return content is SpacingCollectionCellViewModel
    }

    public func reuseIdentifier(forContent content: Any, at indexPath: IndexPath) -> String {
        return reuseIdentifier
    }

    public func configureCell(_ untypedCell: UICollectionViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? SpacingCollectionViewCell, let content = untypedContent as? SpacingCollectionCellViewModel else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }

        cell.backgroundColor = content.color
    }

    public func shouldAllowSelection(forContent content: Any, cell: UICollectionViewCell?) -> Bool {
        return false
    }

    public func cell(forContent content: Any, in collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    }

    public func cellSize(forContent untypedContent: Any, in collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, at indexPath: IndexPath) -> CGSize {
        guard let content = untypedContent as? SpacingCollectionCellViewModel else {
            fatalInDebug(fatalInDebugMessage(for: nil, content: untypedContent))
            return .zero
        }

        return content.size
    }

    private func fatalInDebugMessage(for cell: UICollectionViewCell?, content: Any) -> String {
        if let cell = cell {
            return "Wrong types sent to \(type(of: self)). Got cell of type \(type(of: cell)) while expecting \(SpacingCollectionViewCell.self) and content of type \(type(of: content)) when expecting \(SpacingCollectionCellViewModel.self)"
        }
        return "Wrong types sent to \(type(of: self)). Got content of type \(type(of: content)) when expecting \(SpacingCollectionCellViewModel.self)"
    }
}

/// A collection cell that is a blank space of a specified height and background color
open class SpacingCollectionViewCell: UICollectionViewCell {
}

/// ViewModel used by `SpacingCollectionCellController`
public struct SpacingCollectionCellViewModel: Hashable {
    let identifier: String
    let size: CGSize
    let color: UIColor

    /**
     Initializes a `SpacingCollectionCellViewModel` for `SpacingCollectionCellController`

     - Parameter identifier: An identifier that uniquely identifies this space
     - Parameter size: The size of the spacing cell
     - Parameter color: The background color of the cell (optional, defaults to clear)
     */
    public init(identifier: String, size: CGSize, color: UIColor = .clear) {
        self.identifier = identifier
        self.size = size
        self.color = color
    }

    public var hashValue: Int {
        return identifier.hashValue
    }
}
