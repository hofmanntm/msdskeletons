//
//  MSDAlertQueue.h
//  MSDTouchLibraries
//
//  Created by Raymond Edwards on 2018-04-20.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Presents multiple alerts consecutively.
 
 Harkening back to the days of `UIAlertView`, an `MSDAlertQueue`:
 
 * Will show the next alert after the previous is dismissed.
 * Does not care about which view controller is presenting the alert.
 */
NS_SWIFT_NAME(AlertQueue)
@interface MSDAlertQueue : NSObject

/**
 Enqueues an alert to be shown after any previously queued alert, or immediately if no alert is queued.
 
 If your alert has no actions, a default "OK" action will be provided for you.
 
 If you define your own actions on an alert, you must pass it to `didDismiss(_:)` when your action completes.
 
 - Seealso: `AlertQueue.showAlert(title:message:)`.
 */
- (void)showAlert:(nonnull UIAlertController *)alert NS_SWIFT_NAME(show(_:));


/// Convenience method that shows an alert with the provided title and string, along with a default "OK" action that will dismiss the alert when tapped.
- (void)showAlert:(nonnull NSString *)title message:(nullable NSString *)message NS_SWIFT_NAME(showAlert(title:message:));


- (void)didDismissAlert:(nonnull UIAlertController *)alert NS_SWIFT_NAME(didDismiss(_:));

@end
