//
//  MSDTableCellControllerDelegate.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-19.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol MSDTableCellController;

NS_ASSUME_NONNULL_BEGIN

@protocol MSDTableCellControllerDelegate<NSObject>
@optional

/*!
 Return YES if you present the view controller.
 */
- (BOOL)tableCellController:(id<MSDTableCellController>)cellController 
      presentViewController:(UIViewController *)viewController
                 forContent:(id)content;

/*!
 Return YES if you open the URL.
 */
- (BOOL)tableCellController:(id<MSDTableCellController>)cellController 
                    openURL:(NSURL *)URL
                 forContent:(id)content;

- (void)tableCellController:(id<MSDTableCellController>)cellController 
performSelectActionForContent:(id)content;


@end

NS_ASSUME_NONNULL_END
