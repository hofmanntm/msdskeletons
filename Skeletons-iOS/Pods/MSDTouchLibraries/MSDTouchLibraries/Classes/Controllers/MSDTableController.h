//
//  MSDTableController.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-08-15.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MSDFeed.h"
#import "MSDTableCellController.h"
#import "MSDTableCellControllerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@class MSDTableController;

@protocol MSDTableControllerDelegate <MSDTableCellControllerDelegate>

@optional

/**
    Called when a feed will change. Return NO to skip this batch of feed updates, in which case the table view is not informed of updates and the delegate does not receive -willChangeTableContentForFeed: or -didChangeTableContentForFeed:.
 
    If the user can rearrange cells in your table, this is probably the method you want to implement.
 */
- (BOOL)tableController:(MSDTableController *)controller shouldChangeTableContentForFeed:(id<MSDFeed>)feed inSection:(NSInteger)section;

- (void)tableController:(MSDTableController *)controller willChangeTableContentForFeed:(id<MSDFeed>)feed;
- (void)tableController:(MSDTableController *)controller didChangeTableContentForFeed:(id<MSDFeed>)feed;

- (UITableViewRowAnimation)tableController:(MSDTableController *)controller rowAnimationForChangesInFeed:(id<MSDFeed>)feed;

- (void)scrollViewDidScroll:(UIScrollView *)scrollView forTableController:(MSDTableController *)controller __deprecated_msg("Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView forTableController:(MSDTableController *)controller __deprecated_msg("Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate forTableController:(MSDTableController *)controller __deprecated_msg("Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView forTableController:(MSDTableController *)controller __deprecated_msg("Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");

/**
    Can make section headers disappear by conditionally returning 0.
 
    @param suggestedHeight The height that would've been returned if you did not implement this delegate method.
 */
- (CGFloat)tableController:(MSDTableController *)controller heightForHeaderInSection:(NSInteger)section suggestedHeight:(CGFloat)suggestedHeight;

/**
 When the table view sends this we get an index path, but if the cell did end displaying due to feed updates then it's a pre-feed-update index path which is not very useful, so we don't pass it on to the delegate.

 This is why this method can't be added to cell controllers: we get a pre-update index path after the feed updates, so we can't easily find the content associated with the cell, and that prevents the multi cell controllers from figuring out who to send the message to.
 */
- (void)tableController:(MSDTableController *)controller didEndDisplayingCell:(UITableViewCell *)cell;

@end

@interface MSDTableController : NSObject<UITableViewDelegate, UITableViewDataSource, MSDFeedDelegate>

- (instancetype)initWithTableView:(nullable UITableView *)tableView;
- (instancetype)initWithTableView:(nullable UITableView *)theTableView selfSizingSupport:(BOOL)shouldSupportSelfSizingCells NS_DESIGNATED_INITIALIZER;

- (void)removeAllSections;

- (void)addSectionWithSingleCell:(UITableViewCell *)cell;
- (void)addSectionWithCellArray:(NSArray<UITableViewCell *> *)cellArray;
- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDTableCellController>)cellController;
- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDTableCellController>)cellController;

- (void)addSectionWithSingleCell:(UITableViewCell *)cell headerTitle:(nullable NSString *)title;
- (void)addSectionWithCellArray:(NSArray<UITableViewCell *> *)cellArray headerTitle:(nullable NSString *)title;
- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDTableCellController>)cellController headerTitle:(nullable NSString *)title;
- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDTableCellController>)cellController headerTitle:(nullable NSString *)title caching:(BOOL)caching;
- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDTableCellController>)cellController headerTitle:(nullable NSString *)title;


- (void)addSectionWithSingleCell:(UITableViewCell *)cell headerView:(nullable UIView *)view;
- (void)addSectionWithCellArray:(NSArray<UITableViewCell *> *)cellArray headerView:(nullable UIView *)view;
- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDTableCellController>)cellController headerView:(nullable UIView *)view;
- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDTableCellController>)cellController headerView:(nullable UIView *)headerView caching:(BOOL)caching;
- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDTableCellController>)cellController headerView:(nullable UIView *)headerView;

//only works with table controllers that have shouldSupportSelfSizingCells = TRUE
- (void)addSectionWithSelfSizingCell:(UITableViewCell *)cell headerTitle:(nullable NSString *)title;
- (void)addSectionWithSelfSizingCellArray:(NSArray<UITableViewCell *> *)cellArray headerTitle:(nullable NSString *)title;
- (void)addSectionWithSelfSizingCell:(UITableViewCell *)cell headerView:(nullable UIView *)view;
- (void)addSectionWithSelfSizingCellArray:(NSArray<UITableViewCell *> *)cellArray headerView:(nullable UIView *)view;

// Returns the index path for the row containing this view, if any
- (nullable NSIndexPath *)indexPathForView:(UIView *)view;

// Returns the index path for the piece of content passed as one of the item
// in an array given to addSectionWithContent:... If this content appears
// repeatedly, returns the first one.
- (nullable NSIndexPath *)indexPathForContent:(id)content;

- (nullable id)contentForIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, weak, nullable) IBOutlet id<MSDTableControllerDelegate> delegate;
@property (nonatomic, weak) IBOutlet id<UIScrollViewDelegate> scrollViewDelegate;
@property (nonatomic, strong, nullable) IBOutlet UITableView *tableView;

@property (nonatomic) NSInteger maxFeedChangesBeforeReloadData;

@property (nonatomic, strong) NSMutableArray *sections;

@end

NS_ASSUME_NONNULL_END
