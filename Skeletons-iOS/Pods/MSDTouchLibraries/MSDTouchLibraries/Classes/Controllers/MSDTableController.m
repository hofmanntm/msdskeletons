//
//  MSDTableController.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-08-15.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDTableController.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

static NSString * const SectionSingleCellKey = @"SingleCell";
static NSString * const SectionCellArrayKey = @"CellArray";
static NSString * const SectionSelfSizingCellKey = @"SelfSizingCell";
static NSString * const SectionContentKey = @"Content";
static NSString * const SectionFeedKey = @"Feed";
static NSString * const SectionCellControllerKey = @"CellController";
static NSString * const SectionHeaderTitleKey = @"SectionHeaderTitle";
static NSString * const SectionHeaderViewKey = @"SectionHeaderView";

@interface MSDTableControllerFeedChange : NSObject

@property (nonatomic, strong) NSIndexPath *reloadIndexPath;
@property (nonatomic, strong) NSIndexPath *deleteIndexPath;
@property (nonatomic, strong) NSIndexPath *insertIndexPath;
@property (nonatomic, strong) NSIndexPath *moveFromIndexPath;
@property (nonatomic, strong) NSIndexPath *moveToIndexPath;

@end

@implementation MSDTableControllerFeedChange

- (NSString *)debugDescription {
    NSMutableArray *paths = [NSMutableArray new];
    void (^append)(NSString *, NSIndexPath *) = ^(NSString *what, NSIndexPath *indexPath) {
        [paths addObject:[NSString stringWithFormat:@"%@ = (%@, %@)", what, @(indexPath.section), @(indexPath.item)]];
    };
    if (self.reloadIndexPath) {
        append(@"reload", self.reloadIndexPath);
    }
    if (self.deleteIndexPath) {
        append(@"delete", self.deleteIndexPath);
    }
    if (self.insertIndexPath) {
        append(@"insert", self.insertIndexPath);
    }
    if (self.moveFromIndexPath) {
        append(@"from", self.moveFromIndexPath);
    }
    if (self.moveToIndexPath) {
        append(@"to", self.moveToIndexPath);
    }
    NSString *together = [paths componentsJoinedByString:@"; "];
    return [NSString stringWithFormat:@"<%@: %p %@>", self.class, self, together];
}

@end


@interface MSDTableController ()

@property (nonatomic) NSInteger changingFeedCount;
@property (nonatomic, strong, nullable) NSMutableArray<MSDTableControllerFeedChange *> *feedChanges;
@property (nonatomic) NSMutableIndexSet *sectionsThatChanged;
@property (nonatomic) BOOL shouldSupportSelfSizingCells;
@property (nonatomic, strong) NSMutableSet *temporarilyIgnoredFeeds;

@end

#pragma mark - Init & Properties
@implementation MSDTableController

- (instancetype)init {
    return [self initWithTableView:nil selfSizingSupport:false];
}

- (instancetype)initWithTableView:(nullable UITableView *)theTableView {
    return [self initWithTableView:theTableView selfSizingSupport:false];
}


- (instancetype)initWithTableView:(nullable UITableView *)theTableView selfSizingSupport:(BOOL)shouldSupportSelfSizingCells {
    self = [super init];
    
    self.sections = [[NSMutableArray alloc] init];
    self.temporarilyIgnoredFeeds = [[NSMutableSet alloc] init];
    
    self.shouldSupportSelfSizingCells = shouldSupportSelfSizingCells;
    self.tableView = theTableView;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.maxFeedChangesBeforeReloadData = 50;
    
    return self;
}

- (void)setTableCellControllerDelegate:(id<MSDTableCellControllerDelegate>)theTableCellControllerDelegate {
    MSDDebugFail(@"Use setDelegate: instead of setTableCellControllerDelegate");

    // we can cast this because all MSDTableControllerDelegate methods are optional
    [self setDelegate:(id<MSDTableControllerDelegate>)theTableCellControllerDelegate];
}

- (void)setDelegate:(nullable id<MSDTableControllerDelegate>)theDelegate {
    _delegate = theDelegate;
    
    for (NSDictionary *section in _sections) {
        id<MSDTableCellController> cellController = section[SectionCellControllerKey];
        if ([cellController respondsToSelector:@selector(setDelegate:)]) {
            cellController.delegate = _delegate;
        }
    }
}

- (void)setTableView:(nullable UITableView *)theTableView {
    if (_tableView != theTableView) {
        if (_tableView.delegate == self) {
            _tableView.delegate = nil;
        }
        
        if (_tableView.dataSource == self) {
            _tableView.dataSource = nil;
        }

        _tableView = theTableView;
        
        // Default to us, but allow others to get between if desired
        if (!_tableView.delegate) {
            _tableView.delegate = self;
        }

        if (!_tableView.dataSource) {
            _tableView.dataSource = self;
        }
        
    }
}

#pragma mark - responds to selector
- (BOOL)respondsToSelector:(SEL)aSelector {
    if (aSelector == @selector(tableView:estimatedHeightForRowAtIndexPath:)) {
        return self.shouldSupportSelfSizingCells;
    }
    return [super respondsToSelector:aSelector];
}


#pragma mark - Sections

- (void)removeAllSections {
    // Note: If we ever allow removing a single section, we need to
    // ensure we don't clear the delegate of a cell controller that's 
    // still in use with another section
    for (NSDictionary *section in _sections) {
        id<MSDTableCellController> cellController = section[SectionCellControllerKey];
        if ([cellController respondsToSelector:@selector(delegate)]) {
            MSDCheck(cellController.delegate == self.delegate || cellController.delegate == nil, @"Cell controller's delegate modified after being added to a section.");
            cellController.delegate = nil;
        }
        
        id<MSDFeed> feed = section[SectionFeedKey];
        [feed removeDelegate:self];
    }
    [_sections removeAllObjects];
}

- (void)addSectionWithSingleCell:(UITableViewCell *)cell {
    [self addSectionWithSingleCell:cell headerView:nil];
}

- (void)addSectionWithCellArray:(NSArray<UITableViewCell *> *)cellArray {
    [self addSectionWithCellArray:cellArray headerView:nil];
}

- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDTableCellController>)cellController {
    [self addSectionWithContent:content cellController:cellController headerView:nil];
}

- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDTableCellController>)cellController {
    [self addSectionWithFeed:feed cellController:cellController headerView:nil];
}

- (void)addSectionWithSingleCell:(UITableViewCell *)cell headerTitle:(nullable NSString *)title {
    MSDArgNotNil(cell);
    
    [_sections addObject:@{SectionSingleCellKey: cell,
                           SectionHeaderTitleKey: MSDObjectOrNull(title)}];
}

- (void)addSectionWithCellArray:(NSArray *)cellArray headerTitle:(nullable NSString *)title {
    MSDArgNotNil(cellArray);
    
    [_sections addObject:@{SectionCellArrayKey: cellArray,
                           SectionHeaderTitleKey: MSDObjectOrNull(title)}];
}

- (void)addSectionWithSelfSizingCell:(UITableViewCell *)cell headerTitle:(nullable NSString *)title {
    MSDArgNotNil(cell);
    MSDCheck(self.shouldSupportSelfSizingCells, @"To add self sizing cells you need your table controller to support self sizing cells.");
    
    [_sections addObject:@{SectionSingleCellKey: cell,
                           SectionHeaderTitleKey: MSDObjectOrNull(title),
                           SectionSelfSizingCellKey: @YES}];
}

- (void)addSectionWithSelfSizingCellArray:(NSArray *)cellArray headerTitle:(nullable NSString *)title {
    MSDArgNotNil(cellArray);
    MSDCheck(self.shouldSupportSelfSizingCells, @"To add self sizing cells you need your table controller to support self sizing cells.");
    
    [_sections addObject:@{SectionCellArrayKey: cellArray,
                           SectionHeaderTitleKey: MSDObjectOrNull(title),
                           SectionSelfSizingCellKey: @YES}];
}


- (void)addSectionWithContent:(NSArray *)content 
               cellController:(id<MSDTableCellController>)cellController
                  headerTitle:(nullable NSString *)title
                      caching:(BOOL)caching {
    MSDArgNotNil(content);
    MSDArgNotNil(cellController);
    MSDCheck(![cellController respondsToSelector:@selector(delegate)] || cellController.delegate == nil || cellController.delegate == self.delegate, @"Cell controller shouldn't have another object as a delegate when added to a section.");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    MSDCheck(![cellController respondsToSelector:@selector(cellHeightForContent:)],
             @"Cell controller %@ implements cellHeightForContent:, but is now expected to implement cellHeightForContent:inTableView: instead.", cellController);
    MSDCheck(![cellController respondsToSelector:@selector(cellForContent:)], 
             @"Cell controller %@ implements cellForContent:, but is now expected to implement cellForContent:inTableView: instead.", cellController);
#pragma clang diagnostic pop

    if ([cellController respondsToSelector:@selector(setDelegate:)]) {
        cellController.delegate = self.delegate;
    }
    
    for (id contentItem in content) {
        if (![cellController acceptsContent:contentItem]) {
            MSDDebugFail(@"Cell controller %@ doesn't accept some passed content (eg %@)", cellController, contentItem);
            break;
        }
    }
    
    if (caching) {
        NSMutableArray *cells  = [[NSMutableArray alloc] initWithCapacity:[content count]];
        for (id contentItem in content) {
            UITableViewCell *cell = [cellController cellForContent:contentItem inTableView:_tableView];
            [cellController configureCell:cell forContent:contentItem];
            [cells addObject:cell];
        }
        [_sections addObject:@{SectionContentKey: content,
                               SectionCellControllerKey: cellController,
                               SectionCellArrayKey: cells,
                               SectionHeaderTitleKey: MSDObjectOrNull(title)}];
    }
    else {
        [_sections addObject:@{SectionContentKey: content,
                               SectionCellControllerKey: cellController,
                               SectionHeaderTitleKey: MSDObjectOrNull(title)}];
    }
    
}

- (void)addSectionWithContent:(NSArray *)content 
               cellController:(id<MSDTableCellController>)cellController
                  headerTitle:(nullable NSString *)title {
    
    [self addSectionWithContent:content cellController:cellController headerTitle:title caching:NO];
}


- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDTableCellController>)cellController headerTitle:(nullable NSString *)title {
    MSDArgNotNil(feed);
    MSDArgNotNil(cellController);
    MSDCheck(![cellController respondsToSelector:@selector(delegate)] || cellController.delegate == nil || cellController.delegate == self.delegate, @"Cell controller shouldn't have another object as a delegate when added to a section.");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    MSDCheck(![cellController respondsToSelector:@selector(cellHeightForContent:)],
             @"Cell controller %@ implements cellHeightForContent:, but is now expected to implement cellHeightForContent:inTableView: instead.", cellController);
    MSDCheck(![cellController respondsToSelector:@selector(cellForContent:)], 
             @"Cell controller %@ implements cellForContent:, but is now expected to implement cellForContent:inTableView: instead.", cellController);
#pragma clang diagnostic pop
    
    [feed addDelegate:self];
    
    cellController.delegate = self.delegate;

    [_sections addObject:@{SectionFeedKey: feed,
                           SectionCellControllerKey: cellController,
                           SectionHeaderTitleKey: MSDObjectOrNull(title)}];
}

- (void)addSectionWithSingleCell:(UITableViewCell *)cell headerView:(nullable UIView *)headerView {
    MSDArgNotNil(cell);
    
    [_sections addObject:@{SectionSingleCellKey: cell,
                           SectionHeaderViewKey: MSDObjectOrNull(headerView)}];
}

- (void)addSectionWithCellArray:(NSArray *)cellArray headerView:(nullable UIView *)headerView {
    MSDArgNotNil(cellArray);
    
    [_sections addObject:@{SectionCellArrayKey: cellArray,
                           SectionHeaderViewKey: MSDObjectOrNull(headerView)}];
}

- (void)addSectionWithSelfSizingCell:(UITableViewCell *)cell headerView:(nullable UIView *)headerView {
    MSDArgNotNil(cell);
    MSDCheck(self.shouldSupportSelfSizingCells, @"To add self sizing cells you need your table controller to support self sizing cells.");
    
    [_sections addObject:@{SectionSingleCellKey: cell,
                           SectionHeaderViewKey: MSDObjectOrNull(headerView),
                           SectionSelfSizingCellKey: @YES}];
}

- (void)addSectionWithSelfSizingCellArray:(NSArray<UITableViewCell *> *)cellArray headerView:(nullable UIView *)headerView {
    MSDArgNotNil(cellArray);
    MSDCheck(self.shouldSupportSelfSizingCells, @"To add self sizing cells you need your table controller to support self sizing cells.");
    
    [_sections addObject:@{SectionCellArrayKey: cellArray,
                           SectionHeaderViewKey: MSDObjectOrNull(headerView),
                           SectionSelfSizingCellKey: @YES}];
}

- (void)addSectionWithContent:(NSArray *)content 
               cellController:(id<MSDTableCellController>)cellController
                   headerView:(nullable UIView *)headerView
                      caching:(BOOL)caching{
    MSDArgNotNil(content);
    MSDArgNotNil(cellController);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    MSDCheck(![cellController respondsToSelector:@selector(cellHeightForContent:)],
             @"Cell controller %@ implements cellHeightForContent:, but is now expected to implement cellHeightForContent:inTableView: instead.", cellController);
    MSDCheck(![cellController respondsToSelector:@selector(cellForContent:)], 
             @"Cell controller %@ implements cellForContent:, but is now expected to implement cellForContent:inTableView: instead.", cellController);
#pragma clang diagnostic pop
    
    for (id contentItem in content) {
        if (![cellController acceptsContent:contentItem]) {
            MSDDebugFail(@"Cell controller %@ doesn't accept some passed content (eg %@)", cellController, contentItem);
            break;
        }
    }

    if ([cellController respondsToSelector:@selector(setDelegate:)]) {
        cellController.delegate = self.delegate;
    }
    
    if (caching) {
        NSMutableArray *cells  = [[NSMutableArray alloc] initWithCapacity:[content count]];
        for (id contentItem in content) {
            UITableViewCell *cell = [cellController cellForContent:contentItem inTableView:_tableView];
            [cellController configureCell:cell forContent:contentItem];
            [cells addObject:cell];
        }
        [_sections addObject:@{SectionContentKey: content,
                               SectionCellControllerKey: cellController,
                               SectionCellArrayKey: cells,
                               SectionHeaderViewKey: MSDObjectOrNull(headerView)}];
    }
    else {
        [_sections addObject:@{SectionContentKey: content,
                               SectionCellControllerKey: cellController,
                               SectionHeaderViewKey: MSDObjectOrNull(headerView)}];
    }
}

- (void)addSectionWithContent:(NSArray *)content 
               cellController:(id<MSDTableCellController>)cellController
                   headerView:(nullable UIView *)headerView {
    
    [self addSectionWithContent:content 
                 cellController:cellController 
                     headerView:headerView caching:NO];
    
}

- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDTableCellController>)cellController headerView:(nullable UIView *)headerView {
    MSDArgNotNil(feed);
    MSDArgNotNil(cellController);
    MSDCheck(![cellController respondsToSelector:@selector(delegate)] || cellController.delegate == nil || cellController.delegate == self.delegate, @"Cell controller shouldn't have another object as a delegate when added to a section.");
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    MSDCheck(![cellController respondsToSelector:@selector(cellHeightForContent:)],
             @"Cell controller %@ implements cellHeightForContent:, but is now expected to implement cellHeightForContent:inTableView: instead.", cellController);
    MSDCheck(![cellController respondsToSelector:@selector(cellForContent:)], 
             @"Cell controller %@ implements cellForContent:, but is now expected to implement cellForContent:inTableView: instead.", cellController);
#pragma clang diagnostic pop

    [feed addDelegate:self];

    if ([cellController respondsToSelector:@selector(setDelegate:)]) {
        cellController.delegate = self.delegate;
    }
    
    [_sections addObject:@{SectionFeedKey: feed,
                           SectionCellControllerKey: cellController,
                           SectionHeaderViewKey: MSDObjectOrNull(headerView)}];
}

#pragma mark - Table view data source

- (void)checkTableView:(UITableView *)theTableView {
    MSDCheck(self.tableView, @"You need to assign the tableView member of this controller before showing its table view.");
    MSDCheck(self.tableView == theTableView, @"Got a table view callback from the wrong table view");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView {
    [self checkTableView:theTableView];
    return [_sections count];
}


- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section {
    [self checkTableView:theTableView];
    NSDictionary *sectionInfo = _sections[section];
    NSArray *cellArray = sectionInfo[SectionCellArrayKey];
    if (cellArray) {
        return [cellArray count];
    }
    NSArray *contentArray = sectionInfo[SectionContentKey];
    if (contentArray) {
        return [contentArray count];
    }
    
    id<MSDFeed>feed = sectionInfo[SectionFeedKey];
    if (feed) {
        return feed.count;
    }
    
    return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:theTableView];
    NSDictionary *sectionInfo = _sections[indexPath.section];
    UITableViewCell *cell = sectionInfo[SectionSingleCellKey];
    if (cell) {
        return cell;
    }
    
    NSArray *cellArray = sectionInfo[SectionCellArrayKey];
    if (cellArray) {
        return cellArray[indexPath.row];
    }
    
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        
        NSString *reuseIdentifier = [cellController reuseIdentifierForContent:content];
        UITableViewCell *cell;
        
        cell = [cellController cellForContent:content inTableView:_tableView];
        
        MSDCheck([cell.reuseIdentifier isEqual:reuseIdentifier], @"Cell returned from cell controller (%@) had reuseIdentifier %@, which must be equal to the cell controller's reuseIdentifierForContent, which returned %@", cellController, cell.reuseIdentifier, reuseIdentifier);
        [cellController configureCell:cell forContent:content];
        return cell;
    }
    
    MSDDebugFail(@"couldn't figure out how to make a cell");
    return [UITableViewCell new];
}

- (nullable NSString*)tableView:(UITableView *)theTableView titleForHeaderInSection:(NSInteger)section {
    [self checkTableView:theTableView];
    NSDictionary *sectionInfo = _sections[section];
    id header = sectionInfo[SectionHeaderTitleKey];
    if (header == [NSNull null]) {
        return nil;
    }
    
    return header;
}

- (CGFloat)tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:theTableView];
    NSDictionary *sectionInfo = _sections[indexPath.section];

    if (self.shouldSupportSelfSizingCells && [sectionInfo[SectionSelfSizingCellKey] isEqual:@YES]) {
        return UITableViewAutomaticDimension;
    }

    
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        
        return [cellController cellHeightForContent:content inTableView:theTableView];
    }
    
    return [self tableView:theTableView cellForRowAtIndexPath:indexPath].frame.size.height;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self checkTableView:tableView];
    NSDictionary *sectionInfo = _sections[indexPath.section];
    
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        if([cellController respondsToSelector:@selector(willDisplayCell:forContent:inTableView:)]) {
            return [cellController willDisplayCell:cell forContent:content inTableView:tableView];
        }
    }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:tableView];

    if ([self.delegate respondsToSelector:@selector(tableController:didEndDisplayingCell:)]) {
        [self.delegate tableController:self didEndDisplayingCell:cell];
    }
}

- (nullable UIView *)tableView:(UITableView *)theTableView viewForHeaderInSection:(NSInteger)section {
    [self checkTableView:theTableView];
    NSDictionary *sectionInfo = _sections[section];
    id headerView = sectionInfo[SectionHeaderViewKey];
    if (!headerView || headerView == [NSNull null]) {
        return nil;
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)theTableView heightForHeaderInSection:(NSInteger)section {
    [self checkTableView:theTableView];

    CGFloat height = 0;

    NSDictionary *sectionInfo = _sections[section];
    id headerTitle = sectionInfo[SectionHeaderTitleKey];
    UIView *headerView = sectionInfo[SectionHeaderViewKey];

    if (headerView && ![[NSNull null] isEqual:headerView]) {
        height = CGRectGetHeight(headerView.bounds);
    } else if (headerTitle && headerTitle != [NSNull null]) {
        height = 21;
    }

    if ([self.delegate respondsToSelector:@selector(tableController:heightForHeaderInSection:suggestedHeight:)]) {
        return [self.delegate tableController:self heightForHeaderInSection:section suggestedHeight:height];
    }

    return height;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:tableView];

    id content = [self contentForIndexPath:indexPath];

    if (!content) {
        return YES;
    }

    NSDictionary *sectionInfo = _sections[indexPath.section];
    id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
    MSDCheck(cellController, @"Section with content, but no cell controller found");

    if ([cellController respondsToSelector:@selector(shouldAllowHighlightForContent:cell:)]) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        return [cellController shouldAllowHighlightForContent:content cell:cell];
    }

    return YES;
}

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:tableView];
    
    id content = [self contentForIndexPath:indexPath];
    
    if (!content) {
        return indexPath;
    }
    
    NSDictionary *sectionInfo = _sections[indexPath.section];
    id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
    MSDCheck(cellController, @"Section with content, but no cell controller found");
    
    if ([cellController respondsToSelector:@selector(shouldAllowSelectionForContent:cell:)]) {
        if (![cellController shouldAllowSelectionForContent:content cell:[self.tableView cellForRowAtIndexPath:indexPath]]) {
            return nil;
        }
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:theTableView];
    
    id content = [self contentForIndexPath:indexPath];
    
    if (content) {
        NSDictionary *sectionInfo = _sections[indexPath.section];
        id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        
        if ([cellController respondsToSelector:@selector(performSelectActionForContent:cell:)]) {
            [cellController performSelectActionForContent:content cell:[self.tableView cellForRowAtIndexPath:indexPath]];
        }
    }    
}

- (void)tableView:(UITableView *)theTableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [self checkTableView:theTableView];
    
    id content = [self contentForIndexPath:indexPath];
    
    if (content) {
        NSDictionary *sectionInfo = _sections[indexPath.section];
        id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        
        if ([cellController respondsToSelector:@selector(performAccessoryActionForContent:cell:)]) {
            [cellController performAccessoryActionForContent:content cell:[self.tableView cellForRowAtIndexPath:indexPath]];
        }
    }        
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(scrollViewDidScroll:forTableController:)]) {
        MSDErrorLog(@"scrollViewDidScroll:forTableController: is deprecated. Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"

        [self.delegate scrollViewDidScroll:scrollView forTableController:self];

        #pragma clang diagnostic pop
    }
    
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.scrollViewDelegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(scrollViewWillBeginDragging:forTableController:)]) {
        MSDErrorLog(@"scrollViewWillBeginDragging:forTableController: is deprecated. Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"

        [self.delegate scrollViewWillBeginDragging:scrollView forTableController:self];

        #pragma clang diagnostic pop
    }
    
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)]) {
        [self.scrollViewDelegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)]) {
        [self.scrollViewDelegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)willDecelerate {
    if ([self.delegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:forTableController:)]) {
        MSDErrorLog(@"scrollViewDidEndDragging:willDecelerate:forTableController: is deprecated. Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"

        [self.delegate scrollViewWillBeginDragging:scrollView forTableController:self];

        [self.delegate scrollViewDidEndDragging:scrollView willDecelerate:willDecelerate forTableController:self];

        #pragma clang diagnostic pop
    }
    
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]) {
        [self.scrollViewDelegate scrollViewDidEndDragging:scrollView willDecelerate:willDecelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillBeginDecelerating:)]) {
        [self.scrollViewDelegate scrollViewWillBeginDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(scrollViewDidEndDecelerating:forTableController:)]) {
        MSDErrorLog(@"scrollViewDidEndDecelerating:forTableController: is deprecated. Please respond to the equivalent UIScrollViewDelegate method by setting the scrollViewDelegate for MSDTableController.");

        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Wdeprecated-declarations"

        [self.delegate scrollViewDidEndDecelerating:scrollView forTableController:self];

        #pragma clang diagnostic pop
    }
    
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)]) {
        [self.scrollViewDelegate scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)]) {
        [self.scrollViewDelegate scrollViewDidEndScrollingAnimation:scrollView];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewShouldScrollToTop:)]) {
        return [self.scrollViewDelegate scrollViewShouldScrollToTop:scrollView];
    }
    
    return YES;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidScrollToTop:)]) {
        [self.scrollViewDelegate scrollViewDidScrollToTop:scrollView];
    }
}

#pragma mark - Feed Support

- (NSUInteger)sectionIndexForFeed:(id<MSDFeed>)feed {
    __block NSUInteger result = NSNotFound;
    
    [_sections enumerateObjectsUsingBlock:^(NSDictionary *section, NSUInteger idx, BOOL *stop) {
        id<MSDFeed> sectionFeed = section[SectionFeedKey];
        if (sectionFeed == feed) {
            result = idx;
        }
    }];
    
    MSDCheck(result != NSNotFound, @"Couldn't find feed %@ in list of feeds", feed);
    return result;
}

- (nullable id<MSDFeed>)feedForSectionIndex:(NSUInteger)sectionIndex {
    return _sections[sectionIndex][SectionFeedKey];
}

- (UITableViewRowAnimation)rowAnimationForFeed:(id<MSDFeed>)feed {
    if ([self.delegate respondsToSelector:@selector(tableController:rowAnimationForChangesInFeed:)]) {
        return [self.delegate tableController:self rowAnimationForChangesInFeed:feed];
    }
    return UITableViewRowAnimationFade;
}

- (void)feedWillChangeContent:(id<MSDFeed>)feed {
    if ([self.delegate respondsToSelector:@selector(tableController:shouldChangeTableContentForFeed:inSection:)]) {
        if (![self.delegate tableController:self shouldChangeTableContentForFeed:feed inSection:[self sectionIndexForFeed:feed]]) {
            [_temporarilyIgnoredFeeds addObject:feed];
            return;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(tableController:willChangeTableContentForFeed:)]) {
        [self.delegate tableController:self willChangeTableContentForFeed:feed];
    }

    if (self.changingFeedCount == 0) {
        self.feedChanges = [NSMutableArray new];
        self.sectionsThatChanged = [NSMutableIndexSet new];
    }
    self.changingFeedCount += 1;
    [self.sectionsThatChanged addIndex:[self sectionIndexForFeed:feed]];
}

- (void)feedDidChangeContent:(id<MSDFeed>)feed {
    if ([_temporarilyIgnoredFeeds containsObject:feed]) {
        [_temporarilyIgnoredFeeds removeObject:feed];
        return;
    }

    self.changingFeedCount -= 1;

    if (self.changingFeedCount == 0) {
        // Queuing up a large number of animations can really hurt performance. To prevent this performance issue we reload all data if the number of changes is high enough.
        if (!self.tableView.window || self.feedChanges.count > self.maxFeedChangesBeforeReloadData) {
            [self.tableView reloadData];
        } else {
            [self.tableView beginUpdates];

            for (MSDTableControllerFeedChange *change in self.feedChanges) {
                if (change.insertIndexPath) {
                    [self.tableView insertRowsAtIndexPaths:@[change.insertIndexPath]
                                          withRowAnimation:[self rowAnimationForFeed:[self feedForSectionIndex:change.insertIndexPath.section]]];
                }
                if (change.deleteIndexPath) {
                    [self.tableView deleteRowsAtIndexPaths:@[change.deleteIndexPath]
                                          withRowAnimation:[self rowAnimationForFeed:[self feedForSectionIndex:change.insertIndexPath.section]]];
                }
                if (change.moveFromIndexPath && change.moveToIndexPath) {
                    [self.tableView deleteRowsAtIndexPaths:@[change.moveFromIndexPath]
                                          withRowAnimation:[self rowAnimationForFeed:[self feedForSectionIndex:change.moveFromIndexPath.section]]];
                    [self.tableView insertRowsAtIndexPaths:@[change.moveToIndexPath]
                                          withRowAnimation:[self rowAnimationForFeed:[self feedForSectionIndex:change.moveToIndexPath.section]]];
                }
                if (change.reloadIndexPath) {
                    [self.tableView reloadRowsAtIndexPaths:@[change.reloadIndexPath]
                                          withRowAnimation:[self rowAnimationForFeed:[self feedForSectionIndex:change.reloadIndexPath.section]]];
                }
            }

            [self.tableView endUpdates];
        }

        if ([self.delegate respondsToSelector:@selector(tableController:didChangeTableContentForFeed:)]) {
            [self.sections enumerateObjectsAtIndexes:self.sectionsThatChanged options:0 usingBlock:^(NSDictionary *section, NSUInteger i, BOOL *stop) {
                id<MSDFeed> feed = section[SectionFeedKey];
                if (feed) {
                    [self.delegate tableController:self didChangeTableContentForFeed:feed];
                }
            }];
        }

        [self.feedChanges removeAllObjects];
        [self.sectionsThatChanged removeAllIndexes];
    }
}

- (void)feed:(id<MSDFeed>)feed didUpdateItemAtIndex:(NSUInteger)index {
    if ([_temporarilyIgnoredFeeds containsObject:feed]) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self sectionIndexForFeed:feed]];
    MSDTableControllerFeedChange *change = [MSDTableControllerFeedChange new];
    change.reloadIndexPath = indexPath;
    [self.feedChanges addObject:change];
}

- (void)feed:(id<MSDFeed>)feed didDeleteItemAtIndex:(NSUInteger)index {
    if ([_temporarilyIgnoredFeeds containsObject:feed]) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self sectionIndexForFeed:feed]];
    MSDTableControllerFeedChange *change = [MSDTableControllerFeedChange new];
    change.deleteIndexPath = indexPath;
    [self.feedChanges addObject:change];
}

- (void)feed:(id<MSDFeed>)feed didAddItemAtIndex:(NSUInteger)index {
    if ([_temporarilyIgnoredFeeds containsObject:feed]) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self sectionIndexForFeed:feed]];
    MSDTableControllerFeedChange *change = [MSDTableControllerFeedChange new];
    change.insertIndexPath = indexPath;
    [self.feedChanges addObject:change];
}

- (void)feed:(id<MSDFeed>)feed didMoveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex {
    if ([_temporarilyIgnoredFeeds containsObject:feed]) {
        return;
    }
    
    NSIndexPath *fromPath = [NSIndexPath indexPathForRow:fromIndex inSection:[self sectionIndexForFeed:feed]];
    NSIndexPath *toPath = [NSIndexPath indexPathForRow:toIndex inSection:[self sectionIndexForFeed:feed]];
    MSDTableControllerFeedChange *change = [MSDTableControllerFeedChange new];
    change.moveFromIndexPath = fromPath;
    change.moveToIndexPath = toPath;
    [self.feedChanges addObject:change];
}


#pragma mark - Utilities

- (nullable id)contentForIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *sectionInfo = _sections[indexPath.section];
    
    NSArray *contentArray = sectionInfo[SectionContentKey];
    if (contentArray) {
        return contentArray[indexPath.row];
    }
    
    id<MSDFeed> feed = sectionInfo[SectionFeedKey];
    if (feed) {
        return feed[indexPath.row];
    }
    return nil;
}

- (nullable NSIndexPath *)indexPathForView:(UIView *)view {
    while (view && ![view isKindOfClass:[UITableViewCell class]]) {
        view = view.superview;
    }
    
    if (!view) {
        return nil;
    }
    
    // Look through all static cells
    for (NSUInteger sectionIndex = 0; sectionIndex < [_sections count]; sectionIndex++) {
        NSDictionary *sectionInfo = _sections[sectionIndex];
        if (sectionInfo[SectionSingleCellKey] == view) {
            return [NSIndexPath indexPathForRow:0 inSection:sectionIndex];
        } else {
            NSArray *cells = sectionInfo[SectionCellArrayKey];
            if (cells) {
                NSUInteger arrayIndex = [cells indexOfObject:view];
                if (arrayIndex != NSNotFound) {
                    return [NSIndexPath indexPathForRow:arrayIndex inSection:sectionIndex];
                }
            }
        }
    }
    
    // If we didn't find it in static cells, check if it is a controller-based cell
    return [self.tableView indexPathForCell:(UITableViewCell *)view];
}

- (nullable NSIndexPath *)indexPathForContent:(id)targetContent {
    for (NSUInteger section = 0; section < [_sections count]; section++) {
        NSDictionary *sectionInfo = _sections[section];
        NSArray *sectionContent = sectionInfo[SectionContentKey];
        if (!sectionContent) {
            // try from the feed
            sectionContent = [sectionInfo[SectionFeedKey] content];
        }
        if (sectionContent) {
            NSUInteger row = [sectionContent indexOfObject:targetContent];
            if (row != NSNotFound) {
                return [NSIndexPath indexPathForRow:row inSection:section];
            }
        }
    }
    return nil;
}


#pragma mark - Estimated Height
- (CGFloat)tableView:(UITableView *)theTableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self checkTableView:theTableView];
    NSDictionary *sectionInfo = self.sections[indexPath.section];
    
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDTableCellController> cellController = sectionInfo[SectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        if([cellController respondsToSelector:@selector(estimatedCellHeightForContent:inTableView:)]) {
            return [cellController estimatedCellHeightForContent:content inTableView:theTableView];
        }
    }
    
    if ([sectionInfo[SectionSelfSizingCellKey] isEqual:@YES]) {
        return [self tableView:theTableView cellForRowAtIndexPath:indexPath].frame.size.height;
    }
    
    return -1;
}


#pragma mark -
#pragma mark Memory management

- (void)dealloc {
    [self removeAllSections];

    // also clears delegate and dataSource
    self.tableView = nil;
}

@end

NS_ASSUME_NONNULL_END
