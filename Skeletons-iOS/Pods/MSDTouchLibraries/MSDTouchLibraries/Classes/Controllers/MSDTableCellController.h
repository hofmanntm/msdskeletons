//
//  MSDTableCellController.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-19.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDTableCellControllerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MSDTableCellController<NSObject>

- (BOOL)acceptsContent:(id)content;

- (NSString *)reuseIdentifierForContent:(id)content;
- (void)configureCell:(UITableViewCell *)cell forContent:(id)content;

/*!
 * Return a table view cell for the supplied content.  The cell will later be
 * passed in a call to configureCell:forContent: in order to configure it, so
 * it does not need to be configured here.
 *
 * You are expected to dequeue reusable cells from tableView in your cell
 * controller.
 */
- (UITableViewCell *)cellForContent:(id)content inTableView:(UITableView *)tableView;

- (CGFloat)cellHeightForContent:(id)content inTableView:(UITableView *)tableView;

@optional

- (BOOL)shouldAllowHighlightForContent:(id)content cell:(UITableViewCell *)cell;
- (BOOL)shouldAllowSelectionForContent:(id)content cell:(UITableViewCell *)cell;

- (void)performSelectActionForContent:(id)content cell:(UITableViewCell *)cell;
- (void)performAccessoryActionForContent:(id)content cell:(UITableViewCell *)cell;

- (CGFloat)estimatedCellHeightForContent:(id)content inTableView:(UITableView *)tableView;
- (void)willDisplayCell:(UITableViewCell *)cell forContent:(id)content inTableView:(UITableView *)tableView;

/*!
 * This is typically the view controller holding the table view.
 */
@property (nonatomic, weak, nullable) id<MSDTableCellControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
