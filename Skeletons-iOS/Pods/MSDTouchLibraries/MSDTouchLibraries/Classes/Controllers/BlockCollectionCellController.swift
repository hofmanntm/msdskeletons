//
//  BlockCollectionCellController.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2018-03-09.
//

import UIKit

/**
 A collection cell controller that forwards to blocks, saving you from implementing an MSDCollectionCellController type in common cases.

 The cell controller works both with nibs and with plain cell classes.

 - Seealso: `TemplateCollectionCellController`.
 */
public final class BlockCollectionCellController<CellType: UICollectionViewCell, ContentType>: NSObject, MSDCollectionCellController {

    public weak var delegate: MSDCollectionCellControllerDelegate?

    private let configureBlock: ConfigureBlock
    private let reuseIdentifier: String
    private let selectActionBlock: ActionBlock?
    private let sizeBlock: SizeBlock
    private let willDisplayBlock: WillDisplayBlock?

    public typealias ActionBlock = (_ cell: CellType, _ content: ContentType) -> Void
    public typealias ConfigureBlock = (_ cell: CellType, _ content: ContentType) -> Void
    public typealias SizeBlock = (_ content: ContentType, _ indexPath: IndexPath, _ collectionView: UICollectionView) -> CGSize
    public typealias WillDisplayBlock = (_ cell: CellType, _ content: ContentType) -> Void

    /**
     Initializes a cell controller that loads cells from a nib.

     - Parameter cellNibBundle: The bundle that has the nib. If `nil`, searches the main bundle.
     */
    public init(
        cellNibName: String,
        cellNibBundle: Bundle?,
        for collectionView: UICollectionView,
        sizeBlock: @escaping SizeBlock,
        configureBlock: @escaping ConfigureBlock,
        selectActionBlock: ActionBlock? = nil,
        willDisplayBlock: WillDisplayBlock? = nil)
    {
        self.configureBlock = configureBlock
        reuseIdentifier = cellNibName
        self.selectActionBlock = selectActionBlock
        self.sizeBlock = sizeBlock
        self.willDisplayBlock = willDisplayBlock
        super.init()

        collectionView.register(UINib(nibName: cellNibName, bundle: cellNibBundle), forCellWithReuseIdentifier: reuseIdentifier)
    }

    /**
     Initializes a cell controller that creates cells without a nib.
     */
    public init(
        cellType: CellType.Type,
        for collectionView: UICollectionView,
        sizeBlock: @escaping SizeBlock,
        configureBlock: @escaping ConfigureBlock,
        selectActionBlock: ActionBlock? = nil,
        willDisplayBlock: WillDisplayBlock? = nil)
    {
        self.configureBlock = configureBlock
        reuseIdentifier = "\(cellType)"
        self.selectActionBlock = selectActionBlock
        self.sizeBlock = sizeBlock
        self.willDisplayBlock = willDisplayBlock
        collectionView.register(cellType, forCellWithReuseIdentifier: reuseIdentifier)
    }

    public func acceptsContent(_ content: Any) -> Bool {
        return content is ContentType
    }

    public func cellSize(forContent untypedContent: Any, in collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, at indexPath: IndexPath) -> CGSize {
        guard let content = untypedContent as? ContentType else {
            fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
            return .zero
        }

        return sizeBlock(content, indexPath, collectionView)
    }

    public func reuseIdentifier(forContent content: Any, at indexPath: IndexPath) -> String {
        return reuseIdentifier
    }

    public func cell(forContent content: Any, in collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    }

    public func configureCell(_ untypedCell: UICollectionViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? CellType else {
            return fatalInDebug("expected cell of type \(CellType.self) but got \(type(of: untypedCell))")
        }

        guard let content = untypedContent as? ContentType else {
            return fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
        }

        configureBlock(cell, content)
    }

    public func willDisplay(_ untypedCell: UICollectionViewCell, forContent untypedContent: Any, in collectionView: UICollectionView) {
        guard let cell = untypedCell as? CellType else {
            return fatalInDebug("expected cell of type \(CellType.self) but got \(type(of: untypedCell))")
        }

        guard let content = untypedContent as? ContentType else {
            return fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
        }

        willDisplayBlock?(cell, content)
    }

    public func performSelectAction(forContent untypedContent: Any, cell untypedCell: UICollectionViewCell?) {
        guard let cell = untypedCell as? CellType else {
            return fatalInDebug("expected cell of type \(CellType.self) but got \(type(of: untypedCell))")
        }

        guard let content = untypedContent as? ContentType else {
            return fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
        }

        if let selectActionBlock = selectActionBlock {
            selectActionBlock(cell, content)
        } else {
            delegate?.collectionCellController?(self, performSelectActionForContent: content)
        }
    }
}
