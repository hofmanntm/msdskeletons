//
//  MSDCollectionCellController.h
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-06-11.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MSDCollectionCellControllerDelegate;

@protocol MSDCollectionCellController <NSObject>

- (BOOL)acceptsContent:(id)content;

- (NSString *)reuseIdentifierForContent:(id)content atIndexPath:(NSIndexPath *)indexPath;
- (void)configureCell:(UICollectionViewCell *)cell forContent:(id)content;

/*!
 * Return a collection view cell for the supplied content.  The cell will later be
 * passed in a call to configureCell:forContent: in order to configure it, so
 * it does not need to be configured here.
 *
 * You are expected to dequeue reusable cells from collectionView in your cell
 * controller.
 */
- (UICollectionViewCell *)cellForContent:(id)content inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;

- (CGSize)cellSizeForContent:(id)content inCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout atIndexPath:(NSIndexPath *)indexPath;

@optional

/// MSDCollectionController calls cellForItemAtIndexPath: which can return nil if the cell is offscreen
- (BOOL)shouldAllowSelectionForContent:(id)content cell:(nullable UICollectionViewCell *)cell;
- (void)performSelectActionForContent:(id)content cell:(nullable UICollectionViewCell *)cell;
- (void)performDeselectActionForContent:(id)content cell:(nullable UICollectionViewCell *)cell;

- (void)willDisplayCell:(nonnull UICollectionViewCell *)cell forContent:(nonnull id)content inCollectionView:(nonnull UICollectionView*)collectionView;

- (void)didHighlightCellForContent:(nonnull id)content atIndexPath:(nonnull NSIndexPath *)indexPath inCollectionView:(nonnull UICollectionView*)collectionView;
- (void)didUnhighlightCellForContent:(nonnull id)content atIndexPath:(nonnull NSIndexPath *)indexPath inCollectionView:(nonnull UICollectionView*)collectionView;

- (nullable NSString *)reuseIdentifierForSupplementaryElementOfKind:(NSString *)kind;
- (void)configureSupplementaryView:(UICollectionReusableView *)view ofKind:(NSString *)kind;

- (nullable UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)kind inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;
- (CGSize)referenceSectionHeaderSizeinCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout;
- (CGSize)referenceSectionFooterSizeinCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout;

- (void)willDisplaySupplementaryView:(__kindof UICollectionReusableView *)view
                      forElementKind:(NSString *)kind
                         atIndexPath:(NSIndexPath *)indexPath
                    inCollectionView:(UICollectionView *)collectionView;

/*!
 * This is typically the view controller holding the collection view.
 */
@property (nonatomic, weak, nullable) id<MSDCollectionCellControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
