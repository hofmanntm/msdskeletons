//
//  TemplateCollectionCellController.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2018-03-09.
//

import UIKit

/**
 A cell controller that forwards to blocks, sizing cells that know how to size themselves.

 All cells will be set to the full width of the collection view (minus section and content insets).
 */
public final class TemplateCollectionCellController<CellType: UICollectionViewCell, ContentType>: NSObject, MSDCollectionCellController {

    public weak var delegate: MSDCollectionCellControllerDelegate?

    private let configureBlock: ConfigureBlock
    private let reuseIdentifier: String
    private let selectActionBlock: ActionBlock?
    private let templateCell: CellType
    private let willDisplayBlock: WillDisplayBlock?

    public typealias ActionBlock = (_ cell: CellType, _ content: ContentType) -> Void
    public typealias ConfigureBlock = (_ cell: CellType, _ content: ContentType, _ reason: ConfigurationReason) -> Void
    public typealias WillDisplayBlock = (_ cell: CellType, _ content: ContentType) -> Void

    /**
     Used in the configure block to notifiy the user whether the cell is configured in order to be displayed or for calculating size
     */
    public enum ConfigurationReason {

        /// The configuration is for sizing purposes and the results will not be shown to the user. Consider skipping network fetches for images and similar.
        case sizing

        /// The configuration is for showing in the collection view. Make sure it's ready to show to the user.
        case showing
    }

    /**
     Instantiates a cell controller that loads cells from a nib.

     - Parameter cellNibBundle: The bundle that has the nib. If `nil`, searches the main bundle.
     */
    public init(
        cellNibName: String,
        cellNibBundle: Bundle?,
        for collectionView: UICollectionView,
        configureBlock: @escaping ConfigureBlock,
        selectActionBlock: ActionBlock? = nil,
        willDisplayBlock: WillDisplayBlock? = nil)
    {
        self.configureBlock = configureBlock
        reuseIdentifier = cellNibName
        self.selectActionBlock = selectActionBlock

        let nib = UINib(nibName: cellNibName, bundle: cellNibBundle)
        templateCell = {
            let objects = nib.instantiate(withOwner: nil, options: nil)
            guard objects.count == 1 else {
                fatalError("must have exactly one top-level object in nib \(nib)")
            }
            guard let cell = objects[0] as? CellType else {
                fatalError("expected \(CellType.self) view but found \(type(of: objects[0])) in nib \(nib)")
            }
            return cell
        }()

        self.willDisplayBlock = willDisplayBlock
        super.init()

        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }

    /**
     Instantiates a cell controller that makes cells without a nib.
     */
    public init(
        cellType: CellType.Type,
        for collectionView: UICollectionView,
        configureBlock: @escaping ConfigureBlock,
        selectActionBlock: ActionBlock? = nil,
        willDisplayBlock: WillDisplayBlock? = nil)
    {
        self.configureBlock = configureBlock
        reuseIdentifier = "\(cellType)"
        self.selectActionBlock = selectActionBlock
        templateCell = cellType.init()
        self.willDisplayBlock = willDisplayBlock
        super.init()

        collectionView.register(cellType, forCellWithReuseIdentifier: reuseIdentifier)
    }

    public func acceptsContent(_ content: Any) -> Bool {
        return content is ContentType
    }

    public func reuseIdentifier(forContent content: Any, at indexPath: IndexPath) -> String {
        return reuseIdentifier
    }

    public func cellSize(forContent untypedContent: Any, in collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, at indexPath: IndexPath) -> CGSize {
        
        guard let content = untypedContent as? ContentType else {
            fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
            return .zero
        }

        var width = collectionView.bounds.width - collectionView.contentInset.left - collectionView.contentInset.right

        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            let flowDelegate = collectionView.delegate as? UICollectionViewDelegateFlowLayout
            let delegateInset = flowDelegate?.collectionView?(collectionView, layout: layout, insetForSectionAt: indexPath.section)
            let sectionInset = delegateInset ?? layout.sectionInset
            width -= sectionInset.left
            width -= sectionInset.right
        }

        templateCell.bounds = CGRect(x: 0, y: 0, width: width, height: templateCell.bounds.height)

        configureBlock(templateCell, content, .sizing)

        templateCell.setNeedsLayout()
        templateCell.layoutIfNeeded()

        let targetSize = CGSize(width: width, height: UIView.layoutFittingCompressedSize.height)
        let fittingSize = templateCell.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
        return CGSize(width: width, height: fittingSize.height)
    }

    public func cell(forContent content: Any, in collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    }

    public func configureCell(_ untypedCell: UICollectionViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? CellType else {
            return fatalInDebug("expected cell of type \(CellType.self) but got \(type(of: untypedCell))")
        }

        guard let content = untypedContent as? ContentType else {
            return fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
        }

        configureBlock(cell, content, .showing)
    }

    public func performSelectAction(forContent untypedContent: Any, cell untypedCell: UICollectionViewCell?) {
        guard let cell = untypedCell as? CellType else {
            return fatalInDebug("expected cell of type \(CellType.self) but got \(type(of: untypedCell))")
        }

        guard let content = untypedContent as? ContentType else {
            return fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
        }

        if let selectActionBlock = selectActionBlock {
            selectActionBlock(cell, content)
        } else {
            delegate?.collectionCellController?(self, performSelectActionForContent: content)
        }
    }

    public func willDisplay(_ untypedCell: UICollectionViewCell, forContent untypedContent: Any, in collectionView: UICollectionView) {
        guard let cell = untypedCell as? CellType else {
            return fatalInDebug("expected cell of type \(CellType.self) but got \(type(of: untypedCell))")
        }

        guard let content = untypedContent as? ContentType else {
            return fatalInDebug("expected content of type \(ContentType.self) but got \(type(of: untypedContent))")
        }

        willDisplayBlock?(cell, content)
    }
}
