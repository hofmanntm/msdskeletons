//
//  MSDWaitingLocationController.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 10-05-18.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDWaitingLocationController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDWaitingLocationController()

- (void)finishFindWithLocation;
- (void)finishFindWithError;
- (void)finishFindWithTimeout;

@property (nonatomic, copy) MSDWaitingLocationFoundCompletion locationFoundCompletion;

@end

@implementation MSDWaitingLocationController {
    CLLocationManager *_locationManager;
    
    BOOL _preflighting;
    BOOL _finding;
    BOOL _timeoutPassed;

    CLLocationAccuracy _findAccuracy;
    
    NSError *_lastError;
    CLLocation *_lastLocation;
    NSDate *_lastLocationFoundDate;
    
    BOOL _needsPermission;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _maximumLocationStaleness = 0.0f;
    }
    return self;
}

#pragma mark -
#pragma mark Starting

- (void)syncWithManager {
    BOOL shouldDetect = _finding || _preflighting;
    if (shouldDetect) {
        [_locationManager startUpdatingLocation];
    } else {
        [_locationManager stopUpdatingLocation];
    }
}

- (void)findLocationWithAccuracy:(CLLocationAccuracy)accuracy waitingUpToDelay:(NSTimeInterval)delay completion:(nonnull MSDWaitingLocationFoundCompletion)completion {
    if (_finding) {
        @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Must finish previous find before starting a new one" userInfo:nil]; 
    }
    
    _finding = YES;
    _locationFoundCompletion = completion;
    _findAccuracy = accuracy;
	_timeoutPassed = NO;
    
    BOOL lastLocationSufficient = (_lastLocation != nil
                                   && _lastLocation.horizontalAccuracy <= _findAccuracy
                                   && [[NSDate date] timeIntervalSinceDate:_lastLocationFoundDate] < self.maximumLocationStaleness);
    
    if (lastLocationSufficient) {
        [self finishFindWithLocation];
    } else {
        [self performSelector:@selector(finishFindWithTimeout) withObject:nil afterDelay:delay];
        [self syncWithManager];
    }
}

- (void)beginPreflightIfLocationAllowedWithAccuracy:(CLLocationAccuracy)accuracy {
    [self beginPreflightIfLocationAllowed];
}

- (void)beginPreflightIfLocationAllowed {
    
    _needsPermission = YES;
    
    BOOL enabled = [[CLLocationManager class] locationServicesEnabled];
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (status != kCLAuthorizationStatusAuthorizedAlways && status != kCLAuthorizationStatusAuthorizedWhenInUse) {
        enabled = NO;
    }
    
    if (enabled) {
        _preflighting = YES;
        [self syncWithManager];
    }
    else {
        _needsPermission = YES;
    }
}

- (BOOL)isFindingLocation {
	return _finding;
}
	

#pragma mark -
#pragma mark Finishing

- (void)cancelFindLocation {
    _finding = NO;
    [self syncWithManager];
    
    _locationFoundCompletion = nil;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(finishFindWithTimeout) object:nil];
}

- (void)cancelPreflight {
    _preflighting = NO;
    [self syncWithManager];
}

- (void)cancelAll {
    [self cancelPreflight];
    [self cancelFindLocation];
}

- (void)finishFindWithError {
    //TODO: Nicer localized descriptions for kCLErrorLocationUnknown, kCLErrorDeined, kCLErrorNetwork, 
    if (_finding) {
        self.locationFoundCompletion(nil, _lastError);
    }
    [self cancelFindLocation];
}

- (void)finishFindWithLocation {
    if (_finding) {
        self.locationFoundCompletion(_lastLocation, nil);
    }
    [self cancelFindLocation];
}

- (void)finishFindWithTimeout {
    if (_lastLocation) {
        [self finishFindWithLocation];
    } else if (_lastError) {
        [self finishFindWithError];
	} else {
		// We haven't got either an error or a location from the manager; likely we're waiting for the user 
		// to agree to allow location updates, so we just ignore this; we will either get a location or an error later
		_timeoutPassed = YES; // this signals that we should use the next location update or error
    }
}

#pragma mark - Permissions

- (void)requestWhenInUseAuthorization {
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
}

- (void)requestAlwaysAuthorization {
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
}

#pragma mark -
#pragma mark Callbacks & Delegates

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *newLocation = locations.lastObject;
    _lastLocation = newLocation;
    
    _lastLocationFoundDate = [[NSDate alloc] init];
    
    _lastError = nil;
    
    if (_finding) {
        if (newLocation.horizontalAccuracy <= _findAccuracy || _timeoutPassed) {
            [self finishFindWithLocation];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    _lastError = error;
    if (_finding) {
		if ([_lastError code] != kCLErrorLocationUnknown || _timeoutPassed) {
			[self finishFindWithError];
			[self cancelPreflight]; // if we got a real error, stop trying to preflight
		}
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (_needsPermission) {
        [self beginPreflightIfLocationAllowed];
    }
}

#pragma mark - Memory Management

- (void)dealloc {
    [self cancelAll];
}

@end

NS_ASSUME_NONNULL_END
