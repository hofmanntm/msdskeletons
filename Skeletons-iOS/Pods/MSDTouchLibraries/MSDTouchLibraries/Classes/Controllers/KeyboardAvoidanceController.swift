//
//  KeyboardAvoidanceController.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2017-02-10.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
    Adjusts a scroll view's `contentInset` to keep content visible when the keyboard appears.
 
    The `contentInset` adjustment is animated to match the keyboard movement.
 */
public final class KeyboardAvoidanceController: NSObject {
    private weak var scrollView: UIScrollView?

    /**
        A closure to call after the `contentInset` is adjusted.
     
        This is where you might ensure that the currently-selected content is visible in the scroll view.
     
        - Parameter scrollView: The scroll view that was just adjusted.
     */
    public var didChangeCompletion: ((_ scrollView: UIScrollView) -> Void)?

    public init(scrollView: UIScrollView) {
        self.scrollView = scrollView
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameDidChange), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc private func keyboardFrameDidChange(_ notification: Notification) {
        guard
            let screenFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let scrollView = scrollView,
            let contentView = scrollView.superview,
            let screen = contentView.window?.screen,
            let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let rawCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt
            else { return }

        // Undocumented: Starting in iOS 7 the curve here is not one of the values in UIViewAnimationCurve, and there's no corresponding value in UIViewAnimationOptions, but things Just Work if you twiddle the bits yourself.
        let options = UIView.AnimationOptions(rawValue: rawCurve << 16)

        let endFrame = screen.coordinateSpace.convert(screenFrame, to: contentView)
        let animations: () -> Void = {
            // Frame is undefined when a view is tranformed. So we do this instead
            let scrollViewFrame = CGRect(x: scrollView.center.x - scrollView.bounds.width/2,
                                         y: scrollView.center.y - scrollView.bounds.height/2,
                                         width: scrollView.bounds.width,
                                         height: scrollView.bounds.height)
            let bottom = scrollViewFrame.intersection(endFrame).height
            scrollView.contentInset.bottom = bottom
            scrollView.scrollIndicatorInsets.bottom = bottom
        }

        UIView.animate(withDuration: duration, delay: 0, options: options, animations: animations, completion: { (finished) in
            self.didChangeCompletion?(scrollView)
        })
    }
}
