//
//  SwitchTableCellController.swift
//  MSDTouchLibraries
//
//  Created by Aidan Waite on 2018-06-26.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
A table cell controller for making a table cell with an attributed string title and a switch button. When the user taps the switch, the current state of the switch is passed to the action block. Initial value is set using the `SwitchDataSource`. Use the default nib or pass in your own. If passing in your own, its custom class must be set to `SwitchTableViewCell`.

## Example

You've got an account screen with two toggleable options: Integration and Notifications.

Make a `SwitchCellViewModel` for each case with an enum `Switch` as the `ContextType` and add them to your feed:

```
enum Switch {
  case integration
  case notifications
}

var feed: MSDFeedProtocol = {
  ArrayFeed(content: [
    SwitchCellViewModel(
      context: Switch.integration,
      title: NSAttributedString(string: "Integration", attributes: [.font: Theme.font.body])),
    SwitchCellViewModel(
      context: Switch.notifications,
      title: NSAttributedString(string: "Notifications", attributes: [.font: Theme.font.body])),
  ])
}()
```

Add a `SwitchTableCellController` to your `MSDMultiTableCellController`'s cellControllers.  Here no custom nib is specified so the default nib will be used.

```
let cellController = MSDMultiTableCellController()
cellController.cellControllers = [
  SwitchTableCellController(
    for: tableView,
    switchDataSource: { [weak self] content in
      self?.currentSwitchValue(for: content.context)
    },
    actionBlock: { [weak self] (content: SwitchCellViewModel<Switch>, isOn: Bool) in
      self?.didToggleSwitch(content, isOn)
  })
]
```

Where `currentSwitchValue` is used to set the initial value for each switch

```
private func currentSwitchValue(for content: Switch) -> Bool {
  switch content {
  case .notifications:
    return NotificationController.shared.isEnabled
  case .integration:
    return IntegrationController.shared.isEnabled
  }
}
```

And `didToggleSwitch` handles a switch being tapped

```
private func didToggleSwitch(_ content: SwitchCellViewModel<Switch>, _ isNowOn: Bool) {
  switch content.context {
  case .integration:
    IntegrationController.shared.setStatus(to: isNowOn)
  case .notifications:
    NotificationController.shared.setStatus(to: isNowOn)
  }
}
```
 */
open class SwitchTableCellController<ContextType: Hashable>: NSObject, MSDTableCellController {

    public weak var delegate: MSDTableCellControllerDelegate?

    private let reuseIdentifier: String

    private let actionBlock: ActionBlock
    private let switchDataSource: SwitchDataSource<ContextType>
    private let templateCell: SwitchTableViewCell

    public typealias ActionBlock = (_ content: SwitchCellViewModel<ContextType>, _ switchIsOn: Bool) -> Void
    public typealias SwitchDataSource<ContextType: Hashable> = (_ viewModel: SwitchCellViewModel<ContextType>) -> Bool?

    public init(
        for tableView: UITableView,
        switchDataSource: @escaping SwitchDataSource<ContextType>,
        actionBlock: @escaping ActionBlock,
        nibName: String = "SwitchTableViewCell",
        bundle: Bundle? = Bundle(for: SwitchTableViewCell.self)) {

        self.templateCell = UINib(nibName: nibName, bundle: bundle).instantiate(withOwner: nil, options: [:]).first as! SwitchTableViewCell
        self.reuseIdentifier = String(describing: SwitchTableViewCell.self)
        self.actionBlock = actionBlock
        self.switchDataSource = switchDataSource
        tableView.register(UINib(nibName: nibName, bundle: bundle), forCellReuseIdentifier: self.reuseIdentifier)
    }

    public func acceptsContent(_ content: Any) -> Bool {
        return content is SwitchCellViewModel<ContextType>
    }

    public func reuseIdentifier(forContent content: Any) -> String {
        return self.reuseIdentifier
    }

    public func configureCell(_ untypedCell: UITableViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? SwitchTableViewCell, let content = untypedContent as? SwitchCellViewModel<ContextType> else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }

        cell.actionBlock = { [weak self] isOn in self?.actionBlock(content, isOn) }
        cell.label.attributedText = content.title
        cell.switchControl.isOn = switchDataSource(content) ?? false
    }

    public func shouldAllowHighlight(forContent content: Any, cell: UITableViewCell) -> Bool {
        return false
    }

    public func shouldAllowSelection(forContent content: Any, cell: UITableViewCell) -> Bool {
        return false
    }

    public func cell(forContent content: Any, in tableView: UITableView) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! SwitchTableViewCell
    }

    public func cellHeight(forContent untypedContent: Any, in tableView: UITableView) -> CGFloat {
        guard let content = untypedContent as? SwitchCellViewModel<ContextType> else {
            fatalInDebug(fatalInDebugMessage(for: nil, content: untypedContent))
            return 0
        }
        self.configureCell(self.templateCell, forContent: content)
        let tableWidth = tableView.bounds.size.width
        let templateSize = CGSize(width: tableView.frame.width, height: self.templateCell.frame.height)
        self.templateCell.bounds = CGRect(origin: .zero, size: templateSize)

        self.templateCell.setNeedsLayout()
        self.templateCell.layoutIfNeeded()

        let size = self.templateCell.systemLayoutSizeFitting(
            CGSize(width: tableWidth, height: UIView.layoutFittingCompressedSize.height),
            withHorizontalFittingPriority: .required,
            verticalFittingPriority: .fittingSizeLevel)
        return size.height
    }

    private func fatalInDebugMessage(for cell: UITableViewCell?, content: Any) -> String {
        if let cell = cell {
            return "Wrong types sent to \(type(of: self)). Got cell of type \(type(of: cell)) while expecting \(SwitchTableViewCell.self) and content of type \(type(of: content)) when expecting \(SwitchCellViewModel<ContextType>.self)"
        }
        return "Wrong types sent to \(type(of: self)). Got content of type \(type(of: content)) when expecting \(SwitchCellViewModel<ContextType>.self)"
    }
}

/// A table cell that is a label and switch button
open class SwitchTableViewCell: UITableViewCell {
    // This is private(set) because we may need to change the value of the switch after our switch button table cell has already been initialized (and switch "isOn" is not part of the view model)
    @IBOutlet private(set) public var switchControl: UISwitch!
    @IBOutlet var label: UILabel!

    var actionBlock: ((Bool) -> Void)?

    @IBAction private func valueChanged(_ sender: Any) {
        actionBlock?(switchControl.isOn)
    }
}

/// ViewModel used by `SwitchTableCellController`
public struct SwitchCellViewModel<ContextType: Hashable>: Hashable {
    public let context: ContextType
    let title: NSAttributedString

    /**
     Initializes a `SwitchCellViewModel` for `SwitchTableCellController`

     - Parameter context: The type of switch (used to handle value change)
     - Parameter title: The attributed string title of the switch button cell
     - Parameter height: The height of the switch button cell
    */
    public init(context: ContextType, title: NSAttributedString) {
        self.context = context
        self.title = title
    }
}
