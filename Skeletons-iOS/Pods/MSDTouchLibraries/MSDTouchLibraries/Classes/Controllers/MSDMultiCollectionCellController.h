//
//  MSDMultiCollectionCellController.h
//  MSDTouchLibraries
//
//  Created by Kris Luttmer on 16-07-13.
//  Copyright 2016 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDCollectionCellController.h"
#import "MSDCollectionCellControllerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

/**
    Forwards `MSDCollectionCellController` methods to a collection of cell controllers.
 
    @note Supplementary view methods (e.g. `-configureSupplementaryView:ofKind:`) are called on all cell controllers that implement them. Cell controllers must be resiliant against unexpected calls to these methods. For example, a cell controller that only handles a section header may nevertheless be given a chance to configure a section footer (if it happens to share a multi collection cell controller with a cell controller that actually wants to configure a section footer).
 
    @note The first non-zero size returned by a cell controller for `-referenceSectionHeaderSizeinCollectionView:layout:` and for `-referenceSectionFooterSizeinCollectionView:layout:` is the size returned by a multi collection cell controller.
 */
@interface MSDMultiCollectionCellController : NSObject <MSDCollectionCellController>

@property (nonatomic, copy) IBOutletCollection(id) NSArray<id<MSDCollectionCellController>> *cellControllers;

- (instancetype)initWithCellControllers:(NSArray<id<MSDCollectionCellController>> *)cellControllers;

- (id<MSDCollectionCellController>)controllerForContent:(id)content;

@end

NS_ASSUME_NONNULL_END
