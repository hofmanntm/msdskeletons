//
//  SpacingTableCellController.swift
//  MSDTouchLibraries
//
//  Created by Aidan Waite on 2018-06-25.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import UIKit

/**
 A table cell controller for making a blank table cell with a specified height and optional background color.

 ## Example use case

 Mocks show an 8pt gap between the bottom of one cell and the top of another. Rather than wrestling with view section insets or some such, simply add a `SpacingTableCellViewModel` to your feed with a height of 8pt. Now the space should be 10pt instead of 8pt? No problem, update the height value give to `SpacingTableCellViewModel`'s initializer to 10pt. Now the background should be light grey instead of clear? No problem, add a background color in your call to `SpacingTableCellViewModel`'s initializer.
 */
open class SpacingTableCellController: NSObject, MSDTableCellController {

    public weak var delegate: MSDTableCellControllerDelegate?

    private let reuseIdentifier: String

    public init(for tableView: UITableView) {
        self.reuseIdentifier = String(describing: SpacingTableCellController.self)
        tableView.register(SpacingTableViewCell.self, forCellReuseIdentifier: self.reuseIdentifier)
    }

    public func acceptsContent(_ content: Any) -> Bool {
        return content is SpacingTableCellViewModel
    }

    public func reuseIdentifier(forContent content: Any) -> String {
        return self.reuseIdentifier
    }

    public func configureCell(_ untypedCell: UITableViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? SpacingTableViewCell, let content = untypedContent as? SpacingTableCellViewModel else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }

        cell.backgroundColor = content.color
    }

    public func shouldAllowHighlight(forContent content: Any, cell: UITableViewCell) -> Bool {
        return false
    }

    public func shouldAllowSelection(forContent content: Any, cell: UITableViewCell) -> Bool {
        return false
    }

    public func cell(forContent content: Any, in tableView: UITableView) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! SpacingTableViewCell
    }

    public func cellHeight(forContent untypedContent: Any, in tableView: UITableView) -> CGFloat {
        guard let content = untypedContent as? SpacingTableCellViewModel else {
            fatalInDebug(fatalInDebugMessage(for: nil, content: untypedContent))
            return 0
        }

        return content.height
    }

    private func fatalInDebugMessage(for cell: UITableViewCell?, content: Any) -> String {
        if let cell = cell {
            return "Wrong types sent to \(type(of: self)). Got cell of type \(type(of: cell)) while expecting \(SpacingTableViewCell.self) and content of type \(type(of: content)) when expecting \(SpacingTableCellViewModel.self)"
        }
        return "Wrong types sent to \(type(of: self)). Got content of type \(type(of: content)) when expecting \(SpacingTableCellViewModel.self)"
    }
}

/// A table cell that is a blank space of a specified height and background color
open class SpacingTableViewCell: UITableViewCell {
}

/// ViewModel used by `SpacingTableCellController`
public struct SpacingTableCellViewModel: Hashable {
    let identifier: String
    let height: CGFloat
    let color: UIColor

    /**
     Initializes a `SpacingTableCellViewModel` for `SpacingTableCellController`

     - Parameter identifier: An identifier that uniquely identifies this space
     - Parameter height: The height of the spacing cell
     - Parameter color: The background color of the cell (optional, defaults to clear)
     */
    public init(identifier: String, height: CGFloat, color: UIColor = .clear) {
        self.identifier = identifier
        self.height = height
        self.color = color
    }
}
