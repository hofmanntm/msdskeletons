//
//  MSDCollectionCellControllerDelegate.h
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-06-11.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MSDCollectionCellController;

@protocol MSDCollectionCellControllerDelegate <NSObject>

@optional

/*!
 Return YES if you present the view controller.
 */
- (BOOL)collectionCellController:(id<MSDCollectionCellController>)cellController
           presentViewController:(UIViewController *)viewController
                      forContent:(id)content;

/*!
 Return YES if you open the URL.
 */
- (BOOL)collectionCellController:(id<MSDCollectionCellController>)cellController
                         openURL:(NSURL *)URL
                      forContent:(id)content;

- (void)collectionCellController:(id<MSDCollectionCellController>)cellController performSelectActionForContent:(id)content;
- (void)collectionCellController:(id<MSDCollectionCellController>)cellController performDeselectActionForContent:(id)content;

@end

NS_ASSUME_NONNULL_END
