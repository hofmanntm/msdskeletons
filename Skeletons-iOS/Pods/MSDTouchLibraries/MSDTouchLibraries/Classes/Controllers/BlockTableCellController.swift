//
//  BlockTableCellController.swift
//  MSDTouchLibraries
//
//  Created by Tomas Hofmann on 2018-03-05.
//

import UIKit

public final class BlockTableCellController<CellType: UITableViewCell, ContentType> : NSObject, MSDTableCellController {

    public weak var delegate: MSDTableCellControllerDelegate?

    private let actionBlock: ActionBlock?
    private let configureBlock: ConfigureBlock
    private let heightBlock: HeightBlock
    private let reuseIdentifier: String
    private let willDisplayBlock: WillDisplayBlock?

    public typealias ActionBlock = (_ cell: CellType, _ content: ContentType) -> Void
    public typealias ConfigureBlock = (_ cell: CellType, _ content: ContentType) -> Void
    public typealias HeightBlock = (_ content: ContentType) -> CGFloat
    public typealias WillDisplayBlock = (_ cell: CellType, _ content: ContentType) -> Void
    
    /**
     Creates a BlockTableCellController which assumes that the nib's Indentifier matches the name of the class
     The BlockTableCellController will register the nib with the table
     */
    public convenience init(
        for tableView: UITableView,
        heightBlock: @escaping HeightBlock,
        configureBlock: @escaping ConfigureBlock)
    {
        self.init(cellNibName: String(describing: CellType.self), for: tableView, heightBlock: heightBlock, configureBlock: configureBlock, actionBlock: nil, willDisplayBlock: nil)
    }
    
    /**
     Creates a BlockTableCellController that uses nibs
     The BlockTableCellController will register the nib with the table
     */
    public convenience init(
        cellNibName: String,
        for tableView: UITableView,
        heightBlock: @escaping HeightBlock,
        configureBlock: @escaping ConfigureBlock)
    {
        self.init(cellNibName: cellNibName, for: tableView, heightBlock: heightBlock, configureBlock: configureBlock, actionBlock: nil, willDisplayBlock: nil)
    }
    
    /**
     Creates a BlockTableCellController which assumes that the nib's Indentifier matches the name of the class
     The BlockTableCellController will register the nib with the table
     */
    public convenience init(
        for tableView: UITableView,
        heightBlock: @escaping HeightBlock,
        configureBlock: @escaping ConfigureBlock,
        actionBlock: ActionBlock?,
        willDisplayBlock: WillDisplayBlock?)
    {
        self.init(cellNibName: String(describing: CellType.self), for: tableView, heightBlock: heightBlock, configureBlock: configureBlock, actionBlock: actionBlock, willDisplayBlock: willDisplayBlock)
    }
    
    /**
     Creates a BlockTableCellController that uses nibs
     The BlockTableCellController will register the nib with the table
     */
    public init(
        cellNibName: String,
        for tableView: UITableView,
        heightBlock: @escaping HeightBlock,
        configureBlock: @escaping ConfigureBlock,
        actionBlock: ActionBlock?,
        willDisplayBlock: WillDisplayBlock?)
    {
        self.reuseIdentifier = cellNibName
        self.heightBlock = heightBlock
        self.configureBlock = configureBlock
        self.actionBlock = actionBlock
        self.willDisplayBlock = willDisplayBlock
        tableView.register(UINib(nibName: cellNibName, bundle: nil), forCellReuseIdentifier: cellNibName)
    }
    
    /**
     Creates a BlockTableCellController without a nib
     The BlockTableCellController will register the nib with the table
     */
    public convenience init(
        cellType: CellType.Type,
        for tableView: UITableView,
        heightBlock: @escaping HeightBlock,
        configureBlock: @escaping ConfigureBlock)
    {
        self.init(cellType: cellType, for: tableView, heightBlock: heightBlock, configureBlock: configureBlock, actionBlock: nil, willDisplayBlock: nil)
    }
    
    /**
     Creates a BlockTableCellController without a nib
     The BlockTableCellController will register the nib with the table
     */
    public init(
        cellType: CellType.Type,
        for tableView: UITableView,
        heightBlock: @escaping HeightBlock,
        configureBlock: @escaping ConfigureBlock,
        actionBlock: ActionBlock?,
        willDisplayBlock: WillDisplayBlock?)
    {
        self.reuseIdentifier = String(describing: cellType)
        self.heightBlock = heightBlock
        self.configureBlock = configureBlock
        self.actionBlock = actionBlock
        self.willDisplayBlock = willDisplayBlock
        tableView.register(cellType, forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    // MARK: MSDTableCellController
    
    public func acceptsContent(_ content: Any) -> Bool {
        return content is ContentType
    }
    
    public func reuseIdentifier(forContent content: Any) -> String {
        return self.reuseIdentifier
    }
    
    public func configureCell(_ untypedCell: UITableViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? CellType, let content = untypedContent as? ContentType else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }
        
        configureBlock(cell, content)
    }

    public func shouldAllowHighlight(forContent content: Any, cell: UITableViewCell) -> Bool {
        return shouldAllowHighlightOrSelection(for: content, cell: cell)
    }

    public func shouldAllowSelection(forContent content: Any, cell: UITableViewCell) -> Bool {
        return shouldAllowHighlightOrSelection(for: content, cell: cell)
    }

    private func shouldAllowHighlightOrSelection(for content: Any, cell: UITableViewCell) -> Bool {
        if actionBlock != nil {
            return true
        } else if let delegate = delegate, delegate.responds(to: #selector(MSDTableCellControllerDelegate.tableCellController(_:performSelectActionForContent:))) {
            return true
        } else {
            return false
        }
    }
    
    public func performSelectAction(forContent content: Any, cell: UITableViewCell) {
        guard let content = content as? ContentType, let cell = cell as? CellType else {
            return
        }
        if let actionBlock = self.actionBlock {
            actionBlock(cell, content)
        } else {
            self.delegate?.tableCellController?(self, performSelectActionForContent: content)
        }
    }
    
    public func willDisplay(_ untypedCell: UITableViewCell, forContent untypedContent: Any, in tableView: UITableView) {
        guard let cell = untypedCell as? CellType, let content = untypedContent as? ContentType else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }
        
        self.willDisplayBlock?(cell, content)
    }
    
    public func cell(forContent content: Any, in tableView: UITableView) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! CellType
    }
    
    public func cellHeight(forContent untypedContent: Any, in tableView: UITableView) -> CGFloat {
        guard let content = untypedContent as? ContentType else {
            fatalInDebug(fatalInDebugMessage(for: nil, content: untypedContent))
            return 0
        }
        
        return self.heightBlock(content)
    }
    
    private func fatalInDebugMessage(for cell: UITableViewCell?, content: Any) -> String {
        if let cell = cell {
            return "Wrong types sent to \(type(of: self)). Got cell of type \(type(of: cell)) while expecting \(CellType.self) and content of type \(type(of: content)) when expecting \(ContentType.self)"
        }
        return "Wrong types sent to \(type(of: self)). Got content of type \(type(of: content)) when expecting \(ContentType.self)"
    }
}
