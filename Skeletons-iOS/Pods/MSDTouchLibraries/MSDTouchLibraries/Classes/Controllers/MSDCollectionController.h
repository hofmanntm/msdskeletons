//
//  MSDCollectionController.h
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-06-11.
//
//

#import "MSDCollectionCellController.h"
#import "MSDFeed.h"

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXTERN NSString * const MSDCollectionSectionContentKey;
FOUNDATION_EXTERN NSString * const MSDCollectionSectionCellControllerKey;

@class MSDCollectionController;

@protocol MSDCollectionControllerDelegate <MSDCollectionCellControllerDelegate>

@optional
- (void)collectionController:(MSDCollectionController *)controller willChangeCollectionContentForFeed:(id<MSDFeed>)feed;
- (void)collectionController:(MSDCollectionController *)controller didChangeCollectionContentForFeed:(id<MSDFeed>)feed;

- (BOOL)collectionController:(MSDCollectionController *)controller shouldDisplaySectionForFeed:(id<MSDFeed>)feed;

- (void)collectionController:(MSDCollectionController *)controller collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface MSDCollectionController : NSObject <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, MSDFeedDelegate>

@property (nonatomic, copy, readonly) NSArray *sections;

- (instancetype)initWithCollectionView:(UICollectionView*)collectionView;

- (void)removeAllSections;

- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDCollectionCellController>)cellController;
- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDCollectionCellController>)cellController headerView:(nullable UIView *)view;

- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDCollectionCellController>)cellController;
- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDCollectionCellController>)cellController headerView:(nullable UIView *)headerView;


// Returns the index path for the cell containing this view, if any
- (nullable NSIndexPath *)indexPathForView:(UIView *)view;

// Returns the index path for the piece of content passed as one of the item
// in an array given to addSectionWithContent:... If this content appears
// repeatedly, returns the first one.
- (nullable NSIndexPath *)indexPathForContent:(id)content;

- (nullable id)contentForIndexPath:(NSIndexPath *)indexPath;

@property (nonatomic, weak) IBOutlet id<MSDCollectionControllerDelegate> delegate;

// forwards some UIScrollViewDelegate calls. if you need another, add it.
@property (nonatomic, weak) IBOutlet id<UIScrollViewDelegate> scrollViewDelegate;

@property (nonatomic, strong, nullable) IBOutlet UICollectionView *collectionView;

@end

NS_ASSUME_NONNULL_END
