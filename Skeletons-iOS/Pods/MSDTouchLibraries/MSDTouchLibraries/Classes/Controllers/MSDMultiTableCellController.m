//
//  MSDMultiTableCellController.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-24.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDMultiTableCellController.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

/// A table cell controller that does nothing.
@interface MSDFailureTableCellController : NSObject <MSDTableCellController>

@end

@implementation MSDMultiTableCellController

@synthesize delegate = _delegate;

- (instancetype)init {
    if ((self = [super init])) {
        _cellControllers = @[];
    }
    return self;
}

- (instancetype)initWithCellControllers:(NSArray<id<MSDTableCellController>> *)cellControllers {
    if ((self = [self init])) {
        _cellControllers = [cellControllers copy];

        for (id<MSDTableCellController> controller in _cellControllers) {
            controller.delegate = _delegate;
        }
    }
    return self;
}

static void UnsetDelegates(NSArray<id<MSDTableCellController>> *controllers) {
    for (id<MSDTableCellController> controller in controllers) {
        controller.delegate = nil;
    }
}

- (void)setCellControllers:(NSArray<id<MSDTableCellController>> *)newControllers {
    UnsetDelegates(_cellControllers);

    newControllers = [newControllers copy] ?: @[];
    _cellControllers = newControllers;
    
    for (id<MSDTableCellController> controller in _cellControllers) {
        controller.delegate = self.delegate;
    }
}

- (void)setDelegate:(nullable id<MSDTableCellControllerDelegate>)newDelegate {
    _delegate = newDelegate;

    for (id<MSDTableCellController> controller in _cellControllers) {
        controller.delegate = _delegate;
    }
}

- (id<MSDTableCellController>)controllerForContent:(id)content {
    for (id<MSDTableCellController> controller in self.cellControllers) {
        if ([controller acceptsContent:content]) {
            return controller;
        }
    }

    MSDDebugFail(@"couldn't find a controller for the content");
    return [MSDFailureTableCellController new];
}

- (BOOL)acceptsContent:(id)content {
    return [self controllerForContent:content] != nil;
}

- (NSString *)reuseIdentifierForContent:(id)content {
    return [[self controllerForContent:content] reuseIdentifierForContent:content];
}

- (UITableViewCell *)cellForContent:(id)content inTableView:(UITableView *)tableView {
    return [[self controllerForContent:content] cellForContent:content inTableView:tableView];
}

- (void)configureCell:(UITableViewCell *)cell forContent:(id)content {
    return [[self controllerForContent:content] configureCell:cell forContent:content];
}

- (CGFloat)cellHeightForContent:(id)content inTableView:(UITableView *)tableView {
    return [[self controllerForContent:content] cellHeightForContent:content inTableView:tableView];
}

- (CGFloat)estimatedCellHeightForContent:(id)content inTableView:(UITableView *)tableView {
    id<MSDTableCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(estimatedCellHeightForContent:inTableView:)]) {
        return [controller estimatedCellHeightForContent:content inTableView:tableView];
    }
    return -1;
}

- (void)willDisplayCell:(UITableViewCell*)cell forContent:(id)content inTableView:(UITableView *)tableView {
    id<MSDTableCellController> controller = [self controllerForContent:content];
    if([controller respondsToSelector:@selector(willDisplayCell:forContent:inTableView:)]) {
        [controller willDisplayCell:cell forContent:content inTableView:tableView];
    }
}

- (BOOL)shouldAllowHighlightForContent:(id)content cell:(UITableViewCell *)cell {
    id<MSDTableCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(shouldAllowHighlightForContent:cell:)]) {
        return [controller shouldAllowHighlightForContent:content cell:cell];
    }
    return YES;
}

- (BOOL)shouldAllowSelectionForContent:(id)content cell:(UITableViewCell *)cell {
    id<MSDTableCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(shouldAllowSelectionForContent:cell:)]) {
        return [controller shouldAllowSelectionForContent:content cell:cell];
    }
    return YES;
}

- (void)performSelectActionForContent:(id)content cell:(UITableViewCell *)cell {
    id<MSDTableCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(performSelectActionForContent:cell:)]) {
        [controller performSelectActionForContent:content cell:cell];
    }
}

- (void)performAccessoryActionForContent:(id)content cell:(UITableViewCell *)cell {
    id<MSDTableCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(performAccessoryActionForContent:cell:)]) {
        [controller performAccessoryActionForContent:content cell:cell];
    }
}

- (void)dealloc {
    UnsetDelegates(_cellControllers);
}

@end


@implementation MSDFailureTableCellController

- (BOOL)acceptsContent:(id)content {
    return NO;
}

- (NSString *)reuseIdentifierForContent:(id)content {
    MSDDebugFail(@"please do not talk to this cell controller");
    return @"";
}

- (void)configureCell:(UITableViewCell *)cell forContent:(id)content {
    MSDDebugFail(@"please do not talk to this cell controller");
}

- (void)performSelectActionForContent:(id)content cell:(UITableViewCell *)cell {
    MSDDebugFail(@"please do not talk to this cell controller");
}

- (void)performAccessoryActionForContent:(id)content cell:(UITableViewCell *)cell {
    MSDDebugFail(@"please do not talk to this cell controller");
}

- (UITableViewCell *)cellForContent:(id)content inTableView:(UITableView *)tableView {
    MSDDebugFail(@"please do not talk to this cell controller");
    return [UITableViewCell new];
}

- (CGFloat)cellHeightForContent:(id)content inTableView:(UITableView *)tableView {
    MSDDebugFail(@"please do not talk to this cell controller");
    return 0;
}

@end

NS_ASSUME_NONNULL_END
