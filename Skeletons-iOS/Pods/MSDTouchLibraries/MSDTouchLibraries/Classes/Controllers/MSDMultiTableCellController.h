//
//  MSDMultiTableCellController.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-24.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDTableCellController.h"
#import "MSDTableCellControllerDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDMultiTableCellController : NSObject <MSDTableCellController>

@property (nonatomic, copy) IBOutletCollection(id) NSArray<id<MSDTableCellController>> *cellControllers;

- (instancetype)initWithCellControllers:(NSArray<id<MSDTableCellController>> *)cellControllers;

@end

NS_ASSUME_NONNULL_END
