//
//  MSDAlertQueue.m
//  MSDTouchLibraries
//
//  Created by Raymond Edwards on 2018-04-20.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

#import "MSDAlertQueue.h"
#import "MSDTouchLibraryDefines.h"

@interface MSDEmptyViewController: UIViewController
@end

@implementation MSDEmptyViewController

- (BOOL)prefersStatusBarHidden {
    return UIApplication.sharedApplication.isStatusBarHidden;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIApplication.sharedApplication.statusBarStyle;
}

@end

@interface MSDAlertQueue ()

@property (nonatomic, strong) UIWindow *alertWindow;

@end

@implementation MSDAlertQueue {
    NSMutableArray<UIAlertController *> *_queue;
    BOOL _isShowingAnAlert;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _queue = [[NSMutableArray alloc] init];
        _isShowingAnAlert = NO;
    }
    return self;
}

- (UIWindow *)alertWindow {
    if (!_alertWindow) {
        UIWindow *newWindow = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
        newWindow.windowLevel = UIWindowLevelAlert;
        
        newWindow.rootViewController = [[MSDEmptyViewController alloc] init];
        newWindow.rootViewController.view.backgroundColor = UIColor.clearColor;
        
        [newWindow makeKeyAndVisible];
        _alertWindow = newWindow;
    }
    return _alertWindow;
}

- (void)presentNextInQueueIfNecessary {
    if (_isShowingAnAlert) {
        return;
    }
    
    UIAlertController *alert = _queue.firstObject;
    
    if (!alert) {
        _isShowingAnAlert = NO;
        self.alertWindow.hidden = YES;
        self.alertWindow = nil;
        return;
    }
    
    _isShowingAnAlert = YES;
    [self.alertWindow.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)showAlert:(UIAlertController *)alert {
    if (alert.actions.count < 1) {
        [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", comment: @"Button to dismiss alert with no further action")
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction * _Nonnull action) {
                                                    [self didDismissAlert:alert];
                                                }]];
    }
    [_queue addObject:alert];
    [self presentNextInQueueIfNecessary];
}

- (void)showAlert:(NSString *)title message:(NSString *)message {
    [self showAlert:[UIAlertController alertControllerWithTitle:title
                                                        message:message
                                                 preferredStyle:UIAlertControllerStyleAlert]];
}

- (void)didDismissAlert:(UIAlertController *)alert {
    MSDCheck(alert == _queue.firstObject, @"This alert is not the currently-visible alert so it is not being dismissed");
    
    _isShowingAnAlert = NO;
    
    NSUInteger index = [_queue indexOfObject:alert];
    if (index != NSNotFound && index < _queue.count) {
        [_queue removeObjectAtIndex:index];
    }
    [self presentNextInQueueIfNecessary];
}

@end


