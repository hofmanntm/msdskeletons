//
//  MSDCollectionController.m
//  MSDTouchLibraries
//
//  Created by John Arnold on 2013-06-11.
//
//

#import "MSDCollectionController.h"
#import "MSDCollectionCellControllerDelegate.h"
#import "MSDTouchLibraryDefines.h"


static NSString * const MSDCollectionSectionSingleCellKey = @"SingleCell";
static NSString * const MSDCollectionSectionCellArrayKey = @"CellArray";
NSString * const MSDCollectionSectionContentKey = @"Content";
static NSString * const MSDCollectionSectionFeedKey = @"Feed";
NSString * const MSDCollectionSectionCellControllerKey = @"CellController";
static NSString * const MSDCollectionSectionHeaderViewKey = @"SectionHeaderView";

@interface MSDCollectionControllerFeedChange : NSObject

@property (nonatomic, strong) NSIndexPath *reloadIndexPath;
@property (nonatomic, strong) NSIndexPath *deleteIndexPath;
@property (nonatomic, strong) NSIndexPath *insertIndexPath;
@property (nonatomic, strong) NSIndexPath *moveFromIndexPath;
@property (nonatomic, strong) NSIndexPath *moveToIndexPath;

@end

@implementation MSDCollectionControllerFeedChange

- (NSString *)debugDescription {
    NSMutableArray *paths = [NSMutableArray new];
    void (^append)(NSString *, NSIndexPath *) = ^(NSString *what, NSIndexPath *indexPath) {
        [paths addObject:[NSString stringWithFormat:@"%@ = (%@, %@)", what, @(indexPath.section), @(indexPath.item)]];
    };
    if (self.reloadIndexPath) {
        append(@"reload", self.reloadIndexPath);
    }
    if (self.deleteIndexPath) {
        append(@"delete", self.deleteIndexPath);
    }
    if (self.insertIndexPath) {
        append(@"insert", self.insertIndexPath);
    }
    if (self.moveFromIndexPath) {
        append(@"from", self.moveFromIndexPath);
    }
    if (self.moveToIndexPath) {
        append(@"to", self.moveToIndexPath);
    }
    NSString *together = [paths componentsJoinedByString:@"; "];
    return [NSString stringWithFormat:@"<%@: %p %@>", self.class, self, together];
}

@end


/// UICollectionView complains loudly if you provide it a section header or footer view that wasn't dequeued from the collection view. But we allow passing a UIView instance as a section's header view, so dequeueing doesn't make sense. Instead we register and dequeue one of these containers and stick our view on it.
@interface MSDCollectionControllerStaticHeaderFooterContainer: UICollectionReusableView

+ (NSString *)reuseIdentifier;

@end

@implementation MSDCollectionControllerStaticHeaderFooterContainer

+ (NSString *)reuseIdentifier { return @"MSDCollectionControllerStaticHeaderFooterContainer"; }

@end


@interface MSDCollectionController()

@property (nonatomic, strong) NSMutableArray *mutableSections;

@property (nonatomic, strong, nullable) NSMutableArray *feedChanges;

- (id)contentForIndexPath:(NSIndexPath *)indexPath;

@end

@implementation MSDCollectionController

- (instancetype)init {
    if ((self = [super init])) {
        self.mutableSections = [[NSMutableArray alloc] init];
    }
    return self;
}

- (instancetype)initWithCollectionView:(UICollectionView*)collectionView {
    self = [self init];
    
    if (self) {
        self.collectionView = collectionView;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
    }
    
    return self;
}

- (NSArray *)sections {
    return [NSArray arrayWithArray:self.mutableSections];
}

- (void)setCollectionCellControllerDelegate:(id<MSDCollectionCellControllerDelegate>)theCollectionCellControllerDelegate {
    MSDDebugFail(@"Use setDelegate: instead of setCollectionCellControllerDelegate");
    
    // we can cast this because all MSDTableControllerDelegate methods are optional
    [self setDelegate:(id<MSDCollectionControllerDelegate>)theCollectionCellControllerDelegate];
}

- (void)setDelegate:(id<MSDCollectionControllerDelegate>)theDelegate {
    _delegate = theDelegate;
    
    for (NSDictionary *section in self.mutableSections) {
        id<MSDCollectionCellController> cellController = section[MSDCollectionSectionCellControllerKey];
        if (cellController) {
            cellController.delegate = self.delegate;
        }
    }
}

- (void)setCollectionView:(nullable UICollectionView *)theCollectionView {
    if (_collectionView != theCollectionView) {
        if (_collectionView.delegate == self) {
            _collectionView.delegate = nil;
        }
        
        if (_collectionView.dataSource == self) {
            _collectionView.dataSource = nil;
        }
        
        [_collectionView registerClass:nil
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:[MSDCollectionControllerStaticHeaderFooterContainer reuseIdentifier]];
        [_collectionView registerClass:nil
            forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                   withReuseIdentifier:[MSDCollectionControllerStaticHeaderFooterContainer reuseIdentifier]];
        
        _collectionView = theCollectionView;
        
        // Default to us, but allow others to get between if desired
        if (!_collectionView.delegate) {
            _collectionView.delegate = self;
        }
        
        if (!_collectionView.dataSource) {
            _collectionView.dataSource = self;
        }
        
        [_collectionView registerClass:[MSDCollectionControllerStaticHeaderFooterContainer class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                   withReuseIdentifier:[MSDCollectionControllerStaticHeaderFooterContainer reuseIdentifier]];
        [_collectionView registerClass:[MSDCollectionControllerStaticHeaderFooterContainer class]
            forSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                   withReuseIdentifier:[MSDCollectionControllerStaticHeaderFooterContainer reuseIdentifier]];
    }
}

#pragma mark - Sections

- (void)removeAllSections {
    // Note: If we ever allow removing a single section, we need to
    // ensure we don't clear the delegate of a cell controller that's
    // still in use with another section
    for (NSDictionary *section in self.mutableSections) {
        id<MSDCollectionCellController> cellController = section[MSDCollectionSectionCellControllerKey];
        if (cellController) {
            MSDCheck(cellController.delegate == self.delegate || cellController.delegate == nil, @"Cell controller's delegate modified after being added to a section.");
            cellController.delegate = nil;
        }
        
        id<MSDFeed> feed = section[MSDCollectionSectionFeedKey];
        [feed removeDelegate:self];
    }
    [self.mutableSections removeAllObjects];
}

- (void)addSectionWithContent:(NSArray *)content cellController:(id<MSDCollectionCellController>)cellController {
    [self addSectionWithContent:content cellController:cellController headerView:nil];
}

- (void)addSectionWithContent:(NSArray *)content
               cellController:(id<MSDCollectionCellController>)cellController
                   headerView:(nullable UIView *)headerView {
    
    MSDArgNotNil(content);
    MSDArgNotNil(cellController);
    
    for (id contentItem in content) {
        if (![cellController acceptsContent:contentItem]) {
            MSDDebugFail(@"Cell controller %@ doesn't accept some passed content (eg %@)", cellController, contentItem);
            break;
        }
    }
    
    cellController.delegate = self.delegate;
    
    [self.mutableSections addObject:@{MSDCollectionSectionContentKey: content,
MSDCollectionSectionCellControllerKey: cellController,
MSDCollectionSectionHeaderViewKey: MSDObjectOrNull(headerView)}];
}

- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDCollectionCellController>)cellController {
    [self addSectionWithFeed:feed cellController:cellController headerView:nil];
}

- (void)addSectionWithFeed:(id<MSDFeed>)feed cellController:(id<MSDCollectionCellController>)cellController headerView:(nullable UIView *)headerView {
    MSDArgNotNil(feed);
    MSDArgNotNil(cellController);
    MSDCheck(cellController.delegate == nil || cellController.delegate == self.delegate, @"Cell controller shouldn't have another object as a delegate when added to a section.");
    
    [feed addDelegate:self];
    
    cellController.delegate = self.delegate;
    
    [self.mutableSections addObject:@{MSDCollectionSectionFeedKey: feed,
MSDCollectionSectionCellControllerKey: cellController,
   MSDCollectionSectionHeaderViewKey: MSDObjectOrNull(headerView)}];
}

#pragma mark - Collection view data source

- (void)checkCollectionView:(UICollectionView *)theCollectionView {
    MSDCheck(self.collectionView, @"You need to assign the collectionView member of this controller before showing its collection view.");
    MSDCheck(self.collectionView == theCollectionView, @"Got a collection view callback from the wrong collection view");
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    [self checkCollectionView:collectionView];
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(collectionController:shouldDisplaySectionForFeed:)])
    {
        NSMutableArray *tmpSections = [NSMutableArray arrayWithCapacity:self.mutableSections.count];
        for (NSDictionary *section in self.mutableSections)
        {
            id<MSDFeed> feed = section[MSDCollectionSectionFeedKey];
            if (feed != nil)
            {
                if ([self.delegate collectionController:self shouldDisplaySectionForFeed:feed])
                {
                    [tmpSections addObject:section];
                }
            }
            else
            {
                [tmpSections addObject:section];
            }
        }
        self.mutableSections = tmpSections;
    }
    return [self.mutableSections count];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[section];
    NSArray *cellArray = sectionInfo[MSDCollectionSectionCellArrayKey];
    if (cellArray) {
        return [cellArray count];
    }
    NSArray *contentArray = sectionInfo[MSDCollectionSectionContentKey];
    if (contentArray) {
        return [contentArray count];
    }
    
    id<MSDFeed>feed = sectionInfo[MSDCollectionSectionFeedKey];
    if (feed) {
        return feed.count;
    }
    
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    UICollectionViewCell *cell = sectionInfo[MSDCollectionSectionSingleCellKey];
    if (cell) {
        return cell;
    }
    
    NSArray *cellArray = sectionInfo[MSDCollectionSectionCellArrayKey];
    if (cellArray) {
        return cellArray[indexPath.item];
    }
    
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        
        NSString *reuseIdentifier = [cellController reuseIdentifierForContent:content atIndexPath:indexPath];
        UICollectionViewCell *cell;
        
        cell = [cellController cellForContent:content inCollectionView:collectionView atIndexPath:indexPath];
        
        MSDCheck([cell.reuseIdentifier isEqual:reuseIdentifier], @"For content \"%@\" at indexPath (%ld - %ld), cell returned from cell controller (%@) had reuseIdentifier %@, which must be equal to the cell controller's reuseIdentifierForContent, which returned %@.", content, (long)indexPath.section, (long)indexPath.item, cellController, cell.reuseIdentifier, reuseIdentifier);
        [cellController configureCell:cell forContent:content];
        return cell;
    }

    MSDDebugFail(@"need a cell but couldn't figure out how to create one");
    
    return [UICollectionViewCell new];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    [self checkCollectionView:collectionView];
    
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    
    id key = MSDKeyForElementKind(kind);
    UIView *view = key ? sectionInfo[key] : nil;
    if (view && ![[NSNull null] isEqual:view]) {
        
        UICollectionReusableView *container = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:[MSDCollectionControllerStaticHeaderFooterContainer reuseIdentifier] forIndexPath:indexPath];
        view.frame = (CGRect){.size = container.bounds.size};
        view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        [container addSubview:view];
        
        return container;
    }
    
    id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
    if ([cellController respondsToSelector:@selector(viewForSupplementaryElementOfKind:inCollectionView:atIndexPath:)]) {
        UICollectionReusableView *view = [cellController viewForSupplementaryElementOfKind:kind inCollectionView:collectionView atIndexPath:indexPath];
        [cellController configureSupplementaryView:view ofKind:kind];
        return view;
    }

    MSDDebugFail(@"need a supplementary view but couldn't figure out how to create one");

    return [UICollectionReusableView new];
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self checkCollectionView:collectionView];
    
    id content = [self contentForIndexPath:indexPath];
    if (!content) {
        return YES;
    }
    
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
    MSDCheck(cellController, @"Section with content, but no cell controller found");
    
    if ([cellController respondsToSelector:@selector(shouldAllowSelectionForContent:cell:)]) {
        return [cellController shouldAllowSelectionForContent:content cell:[collectionView cellForItemAtIndexPath:indexPath]];
    }
    
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self checkCollectionView:collectionView];
    
    id content = [self contentForIndexPath:indexPath];
    if (!content) {
        return;
    }
    
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
    MSDCheck(cellController, @"Section with content, but no cell controller found");

    if ([cellController respondsToSelector:@selector(performSelectActionForContent:cell:)]) {
        [cellController performSelectActionForContent:content cell:[collectionView cellForItemAtIndexPath:indexPath]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self checkCollectionView:collectionView];
    
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        if ([cellController respondsToSelector:@selector(performDeselectActionForContent:cell:)]) {
            [cellController performDeselectActionForContent:content cell:[collectionView cellForItemAtIndexPath:indexPath]];
        }
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id content = [self contentForIndexPath:indexPath];

    if (content) {
        id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        if([cellController respondsToSelector:@selector(willDisplayCell:forContent:inCollectionView:)]) {
            [cellController willDisplayCell:cell forContent:content inCollectionView:collectionView];
        }
    }
}


- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.delegate respondsToSelector:@selector(collectionController:collectionView:didEndDisplayingCell:forItemAtIndexPath:)]) {
        [self.delegate collectionController:self collectionView:collectionView didEndDisplayingCell:cell forItemAtIndexPath:indexPath];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id content = [self contentForIndexPath:indexPath];
    
    if (content) {
        id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        if([cellController respondsToSelector:@selector(didHighlightCellForContent:atIndexPath:inCollectionView:)]) {
            [cellController didHighlightCellForContent:content atIndexPath:indexPath inCollectionView:collectionView];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id content = [self contentForIndexPath:indexPath];
    
    if (content) {
        id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        if([cellController respondsToSelector:@selector(didUnhighlightCellForContent:atIndexPath:inCollectionView:)]) {
            [cellController didUnhighlightCellForContent:content atIndexPath:indexPath inCollectionView:collectionView];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

static id _Nullable MSDKeyForElementKind(NSString *elementKind) {
    
    if ([elementKind isEqualToString:UICollectionElementKindSectionHeader]) {
        return MSDCollectionSectionHeaderViewKey;
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    
    id content = [self contentForIndexPath:indexPath];
    if (content) {
        id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
        MSDCheck(cellController, @"Section with content, but no cell controller found");
        
        return [cellController cellSizeForContent:content inCollectionView:collectionView layout:collectionViewLayout atIndexPath:indexPath];
    } else if ([collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionViewLayout;
        return layout.itemSize;
    }
    
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[section];
    
    id key = MSDKeyForElementKind(UICollectionElementKindSectionHeader);
    UICollectionReusableView *view = key ? sectionInfo[key] : nil;
    if (![[NSNull null] isEqual:view]) {
        return view.bounds.size;
    }
    
    id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
    if ([cellController respondsToSelector:@selector(referenceSectionHeaderSizeinCollectionView:layout:)]) {
        return [cellController referenceSectionHeaderSizeinCollectionView:collectionView layout:collectionViewLayout];
    } else if ([collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionViewLayout;
        return layout.headerReferenceSize;
    }
    
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    [self checkCollectionView:collectionView];
    NSDictionary *sectionInfo = self.mutableSections[section];
    
    id key = MSDKeyForElementKind(UICollectionElementKindSectionFooter);
    UICollectionReusableView *view = key ? sectionInfo[key] : nil;
    if (![[NSNull null] isEqual:view]) {
        return view.bounds.size;
    }
    
    id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
    if ([cellController respondsToSelector:@selector(referenceSectionFooterSizeinCollectionView:layout:)]) {
        return [cellController referenceSectionFooterSizeinCollectionView:collectionView layout:collectionViewLayout];
    } else if ([collectionViewLayout isKindOfClass:[UICollectionViewFlowLayout class]]) {
        UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)collectionViewLayout;
        return layout.footerReferenceSize;
    }
    
    return CGSizeZero;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
    [self checkCollectionView:collectionView];

    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    id<MSDCollectionCellController> cellController = sectionInfo[MSDCollectionSectionCellControllerKey];
    if ([cellController respondsToSelector:@selector(willDisplaySupplementaryView:forElementKind:atIndexPath:inCollectionView:)]) {
        [cellController willDisplaySupplementaryView:view forElementKind:elementKind atIndexPath:indexPath inCollectionView:collectionView];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidScroll:)]) {
        [self.scrollViewDelegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)]) {
        [self.scrollViewDelegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)]) {
        [self.scrollViewDelegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)willDecelerate {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]) {
        [self.scrollViewDelegate scrollViewDidEndDragging:scrollView willDecelerate:willDecelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillBeginDecelerating:)]) {
        [self.scrollViewDelegate scrollViewWillBeginDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)]) {
        [self.scrollViewDelegate scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)]) {
        [self.scrollViewDelegate scrollViewDidEndScrollingAnimation:scrollView];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewShouldScrollToTop:)]) {
        return [self.scrollViewDelegate scrollViewShouldScrollToTop:scrollView];
    }
    
    return YES;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidScrollToTop:)]) {
        [self.scrollViewDelegate scrollViewDidScrollToTop:scrollView];
    }
}

#pragma mark - Feed Support

- (NSUInteger)sectionIndexForFeed:(id<MSDFeed>)feed {
    __block NSUInteger result = NSNotFound;
    
    [self.mutableSections enumerateObjectsUsingBlock:^(NSDictionary *section, NSUInteger idx, BOOL *stop) {
        id<MSDFeed> sectionFeed = section[MSDCollectionSectionFeedKey];
        if (sectionFeed == feed) {
            result = idx;
        }
    }];
    
    MSDCheck(result != NSNotFound, @"Couldn't find feed %@ in list of feeds", feed);
    return result;
}

- (void)feedWillChangeContent:(id<MSDFeed>)feed {
    if ([self.delegate respondsToSelector:@selector(collectionController:willChangeCollectionContentForFeed:)]) {
        [self.delegate collectionController:self willChangeCollectionContentForFeed:feed];
    }

    // Since we don't do performBatchUpdates if the collection view does not have window, there is no need for the layout call either
    if (self.collectionView.window) {
        // RDAR: https://openradar.appspot.com/28167779 "UICollectionView performBatchUpdates can trigger a crash if the collection view is flagged for layout"
        // Seeing a hard to reproduce a crash in production so the radar above might be relevant, so we try to layout the collectionView before we update the underlying data
        [self.collectionView layoutIfNeeded];
    }

    self.feedChanges = [NSMutableArray new];
}

- (void)feedDidChangeContent:(id<MSDFeed>)feed {
    void (^completion)(BOOL) = ^(BOOL finished) {
        if ([self.delegate respondsToSelector:@selector(collectionController:didChangeCollectionContentForFeed:)]) {
            [self.delegate collectionController:self didChangeCollectionContentForFeed:feed];
        }
        
        self.feedChanges = nil;
    };
    
    if (self.collectionView.window) {
        [self.collectionView performBatchUpdates:^{
            for (MSDCollectionControllerFeedChange *change in self.feedChanges) {
                if (change.insertIndexPath) {
                    [self.collectionView insertItemsAtIndexPaths:@[change.insertIndexPath]];
                } else if (change.deleteIndexPath) {
                    [self.collectionView deleteItemsAtIndexPaths:@[change.deleteIndexPath]];
                } else if (change.reloadIndexPath) {
                    [self.collectionView reloadItemsAtIndexPaths:@[change.reloadIndexPath]];
                } else if (change.moveFromIndexPath && change.moveToIndexPath) {
                    [self.collectionView deleteItemsAtIndexPaths:@[change.moveFromIndexPath]];
                    [self.collectionView insertItemsAtIndexPaths:@[change.moveToIndexPath]];
                }
            }
        } completion:completion];
    } else {
        [self.collectionView reloadData];
        completion(YES);
    }
}

- (void)feed:(id<MSDFeed>)feed didUpdateItemAtIndex:(NSUInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self sectionIndexForFeed:feed]];
    MSDCollectionControllerFeedChange *change = [MSDCollectionControllerFeedChange new];
    change.reloadIndexPath = indexPath;
    [self.feedChanges addObject:change];
}

- (void)feed:(id<MSDFeed>)feed didDeleteItemAtIndex:(NSUInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self sectionIndexForFeed:feed]];
    MSDCollectionControllerFeedChange *change = [MSDCollectionControllerFeedChange new];
    change.deleteIndexPath = indexPath;
    [self.feedChanges addObject:change];
}

- (void)feed:(id<MSDFeed>)feed didAddItemAtIndex:(NSUInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:[self sectionIndexForFeed:feed]];
    MSDCollectionControllerFeedChange *change = [MSDCollectionControllerFeedChange new];
    change.insertIndexPath = indexPath;
    [self.feedChanges addObject:change];
}

- (void)feed:(id<MSDFeed>)feed didMoveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex {
    NSIndexPath *fromPath = [NSIndexPath indexPathForRow:fromIndex inSection:[self sectionIndexForFeed:feed]];
    NSIndexPath *toPath = [NSIndexPath indexPathForRow:toIndex inSection:[self sectionIndexForFeed:feed]];
    MSDCollectionControllerFeedChange *change = [MSDCollectionControllerFeedChange new];
    change.moveFromIndexPath = fromPath;
    change.moveToIndexPath = toPath;
    [self.feedChanges addObject:change];
}

#pragma mark - Utilities

- (nullable id)contentForIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *sectionInfo = self.mutableSections[indexPath.section];
    
    NSArray *contentArray = sectionInfo[MSDCollectionSectionContentKey];
    if (contentArray) {
        return contentArray[indexPath.item];
    }
    
    id<MSDFeed> feed = sectionInfo[MSDCollectionSectionFeedKey];
    if (feed) {
        return feed[indexPath.item];
    }
    return nil;
}

- (nullable NSIndexPath *)indexPathForView:(UIView *)view {
    while (view && ![view isKindOfClass:[UICollectionViewCell class]]) {
        view = view.superview;
    }
    
    if (!view) {
        return nil;
    }
    
    // Look through all static cells
    for (NSUInteger sectionIndex = 0; sectionIndex < [self.mutableSections count]; sectionIndex++) {
        NSDictionary *sectionInfo = self.mutableSections[sectionIndex];
        if (sectionInfo[MSDCollectionSectionSingleCellKey] == view) {
            return [NSIndexPath indexPathForRow:0 inSection:sectionIndex];
        } else {
            NSArray *cells = sectionInfo[MSDCollectionSectionCellArrayKey];
            if (cells) {
                NSUInteger arrayIndex = [cells indexOfObject:view];
                if (arrayIndex != NSNotFound) {
                    return [NSIndexPath indexPathForRow:arrayIndex inSection:sectionIndex];
                }
            }
        }
    }
    
    // If we didn't find it in static cells, check if it is a controller-based cell
    return [self.collectionView indexPathForCell:(UICollectionViewCell *)view];
}

- (nullable NSIndexPath *)indexPathForContent:(id)targetContent {
    for (NSUInteger section = 0; section < [self.mutableSections count]; section++) {
        NSDictionary *sectionInfo = self.mutableSections[section];
        NSArray *sectionContent = sectionInfo[MSDCollectionSectionContentKey];
        if (!sectionContent) {
            // try from the feed
            sectionContent = [sectionInfo[MSDCollectionSectionFeedKey] content];
        }
        if (sectionContent) {
            NSUInteger row = [sectionContent indexOfObject:targetContent];
            if (row != NSNotFound) {
                return [NSIndexPath indexPathForRow:row inSection:section];
            }
        }
    }
    return nil;
}


#pragma mark - Memory management

- (void)dealloc {
    [self removeAllSections];
    
    // also clears delegate and dataSource
    self.collectionView = nil;
}

@end
