//
//  MSDMultiCollectionCellController.m
//  MSDTouchLibraries
//
//  Created by Kris Luttmer on 16-07-13.
//  Copyright 2016 MindSea Development Inc. All rights reserved.
//

#import "MSDMultiCollectionCellController.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

/// A cell controller that accepts no content and does absolutely nothing.
@interface MSDFailureCollectionCellController : NSObject <MSDCollectionCellController>

@end



@implementation MSDMultiCollectionCellController

@synthesize delegate = _delegate;

- (instancetype)init {
    if ((self = [super init])) {
        _cellControllers = @[];
    }
    return self;
}

- (instancetype)initWithCellControllers:(NSArray<id<MSDCollectionCellController>> *)cellControllers {
    if ((self = [self init])) {
        _cellControllers = [cellControllers copy];

        for (id<MSDCollectionCellController> controller in _cellControllers) {
            controller.delegate = _delegate;
        }
    }
    return self;
}

static void UnsetDelegates(NSArray<id<MSDCollectionCellController>> *controllers) {
    for (id<MSDCollectionCellController> controller in controllers) {
        controller.delegate = nil;
    }
}

- (void)setCellControllers:(NSArray<id<MSDCollectionCellController>> *)newControllers {
    UnsetDelegates(_cellControllers);

    newControllers = [newControllers copy] ?: @[];
    _cellControllers = newControllers;
    
    for (id<MSDCollectionCellController> controller in _cellControllers) {
        controller.delegate = self.delegate;
    }
}

- (void)setDelegate:(nullable id<MSDCollectionCellControllerDelegate>)newDelegate {
    _delegate = newDelegate;
    for (id<MSDCollectionCellController> controller in _cellControllers) {
        controller.delegate = _delegate;
    }
}

- (id<MSDCollectionCellController>)controllerForContent:(id)content {
    for (id<MSDCollectionCellController> controller in self.cellControllers) {
        if ([controller acceptsContent:content]) {
            return controller;
        }
    }

    MSDDebugFail(@"No matching controller for content");
    return [MSDFailureCollectionCellController new];
}

- (BOOL)acceptsContent:(id)content {
    return [self controllerForContent:content] != nil;
}

- (NSString *)reuseIdentifierForContent:(id)content atIndexPath:(NSIndexPath *)indexPath {
    return [[self controllerForContent:content] reuseIdentifierForContent:content atIndexPath:indexPath];
}

- (void)configureCell:(UICollectionViewCell *)cell forContent:(id)content {
    return [[self controllerForContent:content] configureCell:cell forContent:content];
}

- (CGSize)cellSizeForContent:(id)content inCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout atIndexPath:(NSIndexPath *)indexPath {
    return [[self controllerForContent:content] cellSizeForContent:content inCollectionView:collectionView layout:collectionViewLayout atIndexPath:indexPath];
}

- (nullable UICollectionReusableView *)viewForSupplementaryElementOfKind:(NSString *)kind inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {

    for (id<MSDCollectionCellController> cellController in self.cellControllers) {
        if (![cellController respondsToSelector:@selector(viewForSupplementaryElementOfKind:inCollectionView:atIndexPath:)]) {
            continue;
        }

        UICollectionReusableView *view = [cellController viewForSupplementaryElementOfKind:kind inCollectionView:collectionView atIndexPath:indexPath];
        if (view) {
            return view;
        }
    }

    return nil;
}

- (void)willDisplayCell:(UICollectionViewCell *)cell forContent:(id)content inCollectionView:(nonnull UICollectionView *)collectionView {
    id<MSDCollectionCellController> controller = [self controllerForContent:content];
    if([controller respondsToSelector:@selector(willDisplayCell:forContent:inCollectionView:)]) {
        [controller willDisplayCell:cell forContent:content inCollectionView:collectionView];
    }
}

- (BOOL)shouldAllowSelectionForContent:(id)content cell:(nullable UICollectionViewCell *)cell {
    id<MSDCollectionCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(shouldAllowSelectionForContent:cell:)]) {
        return [controller shouldAllowSelectionForContent:content cell:cell];
    }
    return YES;
}

- (void)performSelectActionForContent:(id)content cell:(nullable UICollectionViewCell *)cell {
    id<MSDCollectionCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(performSelectActionForContent:cell:)]) {
        return [controller performSelectActionForContent:content cell:cell];
    }
}

- (void)performDeselectActionForContent:(id)content cell:(nullable UICollectionViewCell *)cell {
    id<MSDCollectionCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(performDeselectActionForContent:cell:)]) {
        [controller performDeselectActionForContent:content cell:cell];
    }
}

- (void)didHighlightCellForContent:(nonnull id)content atIndexPath:(nonnull NSIndexPath *)indexPath inCollectionView:(nonnull UICollectionView*)collectionView {
    id<MSDCollectionCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(didHighlightCellForContent:atIndexPath:inCollectionView:)]) {
        [controller didHighlightCellForContent:content atIndexPath:indexPath inCollectionView:collectionView];
    }
}

- (void)didUnhighlightCellForContent:(nonnull id)content atIndexPath:(nonnull NSIndexPath *)indexPath inCollectionView:(nonnull UICollectionView*)collectionView {
    id<MSDCollectionCellController> controller = [self controllerForContent:content];
    if ([controller respondsToSelector:@selector(didUnhighlightCellForContent:atIndexPath:inCollectionView:)]) {
        [controller didUnhighlightCellForContent:content atIndexPath:indexPath inCollectionView:collectionView];
    }
}

- (UICollectionViewCell *)cellForContent:(id)content inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    return [[self controllerForContent:content] cellForContent:content inCollectionView:collectionView atIndexPath:indexPath];
}

- (nullable NSString *)reuseIdentifierForSupplementaryElementOfKind:(NSString *)kind {
    for (id<MSDCollectionCellController> controller in self.cellControllers) {
        if (![controller respondsToSelector:@selector(reuseIdentifierForSupplementaryElementOfKind:)]) {
            continue;
        }

        NSString *reuseIdentifier = [controller reuseIdentifierForSupplementaryElementOfKind:kind];
        if (reuseIdentifier) {
            return reuseIdentifier;
        }
    }

    return nil;
}

- (void)configureSupplementaryView:(UICollectionReusableView *)view ofKind:(NSString *)kind {
    for (id<MSDCollectionCellController> controller in self.cellControllers) {
        if (![controller respondsToSelector:@selector(configureSupplementaryView:ofKind:)]) {
            continue;
        }

        [controller configureSupplementaryView:view ofKind:kind];
    }
}

- (CGSize)referenceSectionHeaderSizeinCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout {
    for (id<MSDCollectionCellController> controller in self.cellControllers) {
        if (![controller respondsToSelector:@selector(referenceSectionHeaderSizeinCollectionView:layout:)]) {
            continue;
        }

        CGSize size = [controller referenceSectionHeaderSizeinCollectionView:collectionView layout:collectionViewLayout];
        if (!CGSizeEqualToSize(size, CGSizeZero)) {
            return size;
        }
    }
    
    return CGSizeZero;
}

- (CGSize)referenceSectionFooterSizeinCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout {
    for (id<MSDCollectionCellController> controller in self.cellControllers) {
        if (![controller respondsToSelector:@selector(referenceSectionFooterSizeinCollectionView:layout:)]) {
            continue;
        }

        CGSize size = [controller referenceSectionFooterSizeinCollectionView:collectionView layout:collectionViewLayout];
        if (!CGSizeEqualToSize(size, CGSizeZero)) {
            return size;
        }
    }

    return CGSizeZero;
}

- (void)willDisplaySupplementaryView:(__kindof UICollectionReusableView *)view forElementKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath inCollectionView:(UICollectionView *)collectionView {
    for (id<MSDCollectionCellController> controller in self.cellControllers) {
        if (![controller respondsToSelector:@selector(willDisplaySupplementaryView:forElementKind:atIndexPath:inCollectionView:)]) {
            continue;
        }

        [controller willDisplaySupplementaryView:view forElementKind:kind atIndexPath:indexPath inCollectionView:collectionView];
    }
}

- (void)dealloc {
    UnsetDelegates(_cellControllers);
}

@end


@implementation MSDFailureCollectionCellController

- (BOOL)acceptsContent:(id)content {
    return NO;
}

- (NSString *)reuseIdentifierForContent:(id)content atIndexPath:(NSIndexPath *)indexPath {
    MSDDebugFail(@"please do not talk to this cell controller");
    return @"";
}

- (void)configureCell:(UICollectionViewCell *)cell forContent:(id)content {
    MSDDebugFail(@"please do not talk to this cell controller");
}

- (UICollectionViewCell *)cellForContent:(id)content inCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    MSDDebugFail(@"please do not talk to this cell controller");
    return [UICollectionViewCell new];
}

- (CGSize)cellSizeForContent:(id)content inCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout atIndexPath:(NSIndexPath *)indexPath {
    MSDDebugFail(@"please do not talk to this cell controller");
    return CGSizeZero;
}

- (nullable id<MSDCollectionCellControllerDelegate>)delegate {
    MSDDebugFail(@"please do not talk to this cell controller");
    return nil;
}

- (void)setDelegate:(nullable id<MSDCollectionCellControllerDelegate>)delegate {
    MSDDebugFail(@"please do not talk to this cell controller");
}

@end

NS_ASSUME_NONNULL_END
