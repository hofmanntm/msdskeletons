//
//  TemplatedTableCellController.swift
//  MSDTouchLibraries
//
//  Created by Tomas Hofmann on 2018-03-05.
//

import UIKit

/**
 A very simple Table Cell Controller that uses an instance of the cell for measuring
 */
public final class TemplatedTableCellController<CellType: UITableViewCell, ContentType> : NSObject, MSDTableCellController {

    public weak var delegate: MSDTableCellControllerDelegate?

    private let configureBlock: ((CellType, ContentType, ConfigurationReason) -> Void)
    private let reuseIdentifier: String
    private let templateCell: CellType
    private var actionBlock: ((CellType, ContentType) -> Void)?
    private var willDisplayBlock: ((CellType, ContentType) -> Void)?

    public typealias ActionBlock = (_ cell: CellType, _ content: ContentType) -> Void
    public typealias ConfigureBlock = (_ cell: CellType, _ content: ContentType, _ reason: ConfigurationReason) -> Void
    public typealias HeightBlock = (_ content: ContentType) -> CGFloat
    public typealias WillDisplayBlock = (_ cell: CellType, _ content: ContentType) -> Void

    /**
     Used in the configure block to notifiy the user whether the cell is configured in order to be displayed or for calculating size
     */
    public enum ConfigurationReason {

        /// The configuration is for sizing purposes and the results will not be shown to the user. Consider skipping network fetches for images and similar.
        case sizing

        /// The configuration is for showing in the table view. Make sure it's ready to show to the user.
        case showing
    }
    
    /**
     Creates a TemplatedTableCellController which assumes that the nib's Indentifier matches the name of the class
     The TemplatedTableCellController will register the nib with the table
     */
    public convenience init(for tableView: UITableView, configureBlock: @escaping ConfigureBlock) {
        self.init(cellNibName: String(describing: CellType.self), for: tableView, configureBlock: configureBlock, actionBlock: nil, willDisplayBlock: nil)
    }
    
    /**
     Creates a TemplatedTableCellController that uses nibs
     The TemplatedTableCellController will register the nib with the table
     */
    public convenience init(
        cellNibName: String,
        for tableView: UITableView,
        configureBlock: @escaping ConfigureBlock)
    {
        self.init(cellNibName: cellNibName, for: tableView, configureBlock: configureBlock, actionBlock: nil, willDisplayBlock: nil)
    }
    
    /**
     Creates a TemplatedTableCellController which assumes that the nib's Indentifier matches the name of the class
     The TemplatedTableCellController will register the nib with the table
     */
    public convenience init(
        for tableView: UITableView,
        configureBlock: @escaping ConfigureBlock,
        actionBlock: ActionBlock?,
        willDisplayBlock: WillDisplayBlock?)
    {
        self.init(cellNibName: String(describing: CellType.self), for: tableView, configureBlock: configureBlock, actionBlock: actionBlock, willDisplayBlock: willDisplayBlock)
    }
    
    /**
     Creates a TemplatedTableCellController that uses nibs
     The TemplatedTableCellController will register the nib with the table
     */
    public init(
        cellNibName: String,
        for tableView: UITableView,
        configureBlock: @escaping ConfigureBlock,
        actionBlock: ActionBlock?,
        willDisplayBlock: WillDisplayBlock?)
    {
        self.templateCell = UINib.init(nibName: cellNibName, bundle: nil).instantiate(withOwner: nil, options: [:]).first as! CellType
        self.reuseIdentifier = cellNibName
        self.configureBlock = configureBlock
        self.actionBlock = actionBlock
        self.willDisplayBlock = willDisplayBlock
        tableView.register(UINib(nibName: cellNibName, bundle: nil), forCellReuseIdentifier: cellNibName)
    }
    
    /**
     Creates a TemplatedTableCellController without a nib
     The TemplatedTableCellController will register the nib with the table
     */
    public convenience init(
        cellType: CellType.Type,
        for tableView: UITableView,
        configureBlock: @escaping ConfigureBlock)
    {
        self.init(cellType: cellType, for: tableView, configureBlock: configureBlock, actionBlock: nil, willDisplayBlock: nil)
    }
    
    /**
     Creates a TemplatedTableCellController without a nib
     The TemplatedTableCellController will register the cell class with the table
     */
    public init(
        cellType: CellType.Type,
        for tableView: UITableView,
        configureBlock: @escaping ConfigureBlock,
        actionBlock: ActionBlock?,
        willDisplayBlock: WillDisplayBlock?)
    {
        self.templateCell = cellType.init(style: .default, reuseIdentifier: nil)
        self.reuseIdentifier = String(describing: cellType)
        self.configureBlock = configureBlock
        self.actionBlock = actionBlock
        self.willDisplayBlock = willDisplayBlock
        tableView.register(cellType, forCellReuseIdentifier: self.reuseIdentifier)
    }
    
    // MARK: MSDTableCellController
    
    public func acceptsContent(_ content: Any) -> Bool {
        return content is ContentType
    }
    
    public func reuseIdentifier(forContent content: Any) -> String {
        return self.reuseIdentifier
    }
    
    public func configureCell(_ untypedCell: UITableViewCell, forContent untypedContent: Any) {
        guard let cell = untypedCell as? CellType, let content = untypedContent as? ContentType else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }
        
        configureBlock(cell, content, .showing)
    }

    public func shouldAllowHighlight(forContent content: Any, cell: UITableViewCell) -> Bool {
        return shouldAllowHighlightOrSelection(for: content, cell: cell)
    }

    public func shouldAllowSelection(forContent content: Any, cell: UITableViewCell) -> Bool {
        return shouldAllowHighlightOrSelection(for: content, cell: cell)
    }

    private func shouldAllowHighlightOrSelection(for content: Any, cell: UITableViewCell) -> Bool {
        if actionBlock != nil {
            return true
        } else if let delegate = delegate, delegate.responds(to: #selector(MSDTableCellControllerDelegate.tableCellController(_:performSelectActionForContent:))) {
            return true
        } else {
            return false
        }
    }
    
    public func performSelectAction(forContent content: Any, cell: UITableViewCell) {
        guard let content = content as? ContentType, let cell = cell as? CellType else {
            return
        }
        if let actionBlock = self.actionBlock {
            actionBlock(cell, content)
        } else {
            self.delegate?.tableCellController?(self, performSelectActionForContent: content)
        }
    }
    
    public func willDisplay(_ untypedCell: UITableViewCell, forContent untypedContent: Any, in tableView: UITableView) {
        guard let cell = untypedCell as? CellType, let content = untypedContent as? ContentType else {
            fatalInDebug(fatalInDebugMessage(for: untypedCell, content: untypedContent))
            return
        }
        
        self.willDisplayBlock?(cell, content)
    }
    
    public func cell(forContent content: Any, in tableView: UITableView) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier) as! CellType
    }
    
    public func cellHeight(forContent untypedContent: Any, in tableView: UITableView) -> CGFloat {
        guard let content = untypedContent as? ContentType else {
            fatalInDebug(fatalInDebugMessage(for: nil, content: untypedContent))
            return 0
        }
        self.configureBlock(self.templateCell, content, .sizing)
        
        let tableWidth = tableView.bounds.size.width
        
        // Setting a specific height avoids unsatisfiable Auto-Layout warnings
        let templateSize = CGSize(width: tableView.frame.width, height: self.templateCell.frame.height)
        self.templateCell.bounds = CGRect(origin: .zero, size: templateSize)
        
        self.templateCell.setNeedsLayout()
        self.templateCell.layoutIfNeeded()

        let size = self.templateCell.systemLayoutSizeFitting(CGSize(width: tableWidth, height: UIView.layoutFittingCompressedSize.height), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel)
        return size.height
    }
    
    private func fatalInDebugMessage(for cell: UITableViewCell?, content: Any) -> String {
        if let cell = cell {
            return "Wrong types sent to \(type(of: self)). Got cell of type \(type(of: cell)) while expecting \(CellType.self) and content of type \(type(of: content)) when expecting \(ContentType.self)"
        }
        return "Wrong types sent to \(type(of: self)). Got content of type \(type(of: content)) when expecting \(ContentType.self)"
    }
}
