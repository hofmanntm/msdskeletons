//
//  NSString+MSDURL.h
//
//  Created by Jesse Rusak on 10-09-16.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (MSDURL)

// This differs from stringByAddingPercentEscapesUsingEncoding: by
// the fact that it also encodes characters that are significant in URLS:
// :/?#[]@!$&'()*+,;=  (see RFC 3986)
// This can be used for sanitizing, eg, arguments or path components of a URL
- (NSString *)msd_stringByEscapingForURLComponent;

// This differs from stringByReplacingEscapesUsingEncoding: by
// also replacing + with ' '
- (NSString *)msd_stringByUnescapingFromURL;

@end

NS_ASSUME_NONNULL_END
