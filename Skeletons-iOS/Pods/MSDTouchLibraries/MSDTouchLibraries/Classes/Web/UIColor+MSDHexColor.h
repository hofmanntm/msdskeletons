//
//  UIColor+MSDHexColor.h
//  Nocturne
//
//  Created by Mike Burke on 11-09-13.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (MSDHexColor)

/**
    Parses a color from an HTML- or CSS-style hex string.
 
    @example

        * `#8000FF` ("grape" in the macOS color picker)
        * `#8000ff` (case doesn't matter)
        * `8000FF` (the `#` is optional)
        * `#8000FF7F` (an alpha component)
 
    @return The color if parsing succeeds; or `nil` if parsing fails.
 */
+ (nullable instancetype)msd_colorFromHexString:(NSString *)hexColor NS_SWIFT_NAME(init(msd_hexString:));

@end

NS_ASSUME_NONNULL_END
