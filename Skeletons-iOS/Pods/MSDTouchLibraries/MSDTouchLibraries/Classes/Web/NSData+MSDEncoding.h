//
//  NSData+MSDEncoding.h
//
// Derived from http://colloquy.info/project/browser/trunk/NSDataAdditions.h
// and http://stackoverflow.com/questions/1524604/md5-algorithm-in-objective-c
// Created by khammond on Mon Oct 29 2001.
// Formatted by Timothy Hatcher on Sun Jul 4 2004.
// Copyright (c) 2001 Kyle Hammond. All rights reserved.
// Original development by Dave Winer.
//
//  Created by Dale Zak on 10-04-13.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSData (MSDEncoding)

@property (readonly, nonatomic) NSString *msd_md5EncodedString;

@end

NS_ASSUME_NONNULL_END
