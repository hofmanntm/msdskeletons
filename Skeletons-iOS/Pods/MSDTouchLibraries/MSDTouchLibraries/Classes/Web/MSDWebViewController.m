//
//  MSDWebViewController.m
//
//  Created by Scott Ferguson on 10-08-05.
//  Copyright 2010 MindSea. All rights reserved.
//

#import "MSDWebViewController.h"
#import "MSDTouchLibraryDefines.h"

typedef BOOL(^HandlerBlock)(NSURL *url, UIWebViewNavigationType navigationType);

@interface MSDWebViewController () <UIAlertViewDelegate, UIActionSheetDelegate>

@property (nonatomic, copy, nullable) NSString *htmlString;
@property (nonatomic, strong, nullable) UIBarButtonItem *activityButton;
@property (nonatomic, strong, nullable) UIBarButtonItem *reloadButton;
@property (nonatomic, strong, nullable) NSMutableDictionary* schemeHandlerDict;
@property (nonatomic, strong, nullable) UIAlertView *showingAlert;
@property (nonatomic, strong, nullable) UIActionSheet *schemeActionSheet;
@property (nonatomic, strong, nullable) NSURL *externalURL;

@end

static const CGFloat NavigationButtonSpacing = 20.0f;

@implementation MSDWebViewController

- (void)setHandlerForScheme:(NSString*)scheme withBlock:(BOOL(^)(NSURL *url, UIWebViewNavigationType navigationType))handlerBlock {
    MSDArgNotNil(scheme);
    MSDArgNotNil(handlerBlock);
    HandlerBlock block = [handlerBlock copy];
    if (!self.schemeHandlerDict) {
        self.schemeHandlerDict = [[NSMutableDictionary alloc] init];
    }
    [self.schemeHandlerDict setValue:block forKey:[scheme lowercaseString]];
}

- (void)removeHandlerForScheme:(NSString *)scheme {
    [self.schemeHandlerDict removeObjectForKey:[scheme lowercaseString]];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    NSString *scheme = [url scheme];
    HandlerBlock block = self.schemeHandlerDict[[scheme lowercaseString]];
    
    if (block) {
        return block(url, navigationType);
    } else {
        if (![[url scheme] isEqualToString:@"http"] && ![[url scheme] isEqualToString:@"https"] && ![[url scheme] isEqualToString:@"file"]) {
        
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                
                self.externalURL = url;
                
                self.schemeActionSheet = [[UIActionSheet alloc] initWithTitle:[url absoluteString]
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"Cancel", @"Button label for cancelling opening browser")
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:NSLocalizedString(@"Open Link", @"Button label for opening a link in a browser"), nil];
                [self.schemeActionSheet showInView:self.view.window];
                return NO;
            }
        }
        
        return YES;
    }
}

#pragma mark -
#pragma mark View lifecycle

- (instancetype)initWithURL:(nullable NSURL *)theURL nibName:(nullable NSString *)nibName bundle:(nullable NSBundle *)bundle {
	if ((self = [super initWithNibName:nibName bundle:bundle])) {
        self.baseURL = theURL;
        self.title = NSLocalizedString(@"Loading…", @"Default title for webviews before they load their content");
	}
	return self;
}

- (instancetype)initWithURLRequest:(NSURLRequest *)theURLRequest nibName:(NSString *)nibName bundle:(NSBundle *)bundle {
    if ((self = [super initWithNibName:nibName bundle:bundle])) {
        self.baseURLRequest = theURLRequest;
        self.title = NSLocalizedString(@"Loading…", @"Default title for webviews before they load their content");
    }
    return self;
}

- (instancetype)initWithHTMLString:(nullable NSString *)string baseURL:(nullable NSURL *)theURL nibName:(nullable NSString *)nibName bundle:(nullable NSBundle *)bundle {
	if ((self = [super initWithNibName:nibName bundle:bundle])) {
        self.baseURL = theURL;
        self.htmlString = string;
        self.title = NSLocalizedString(@"Loading…", @"Default title for webviews before they load their content");
	}
	return self;    
}

- (NSArray *)toolbarItemsForWebState {
    NSMutableArray *items = [NSMutableArray array];
    
    if (!self.usesCustomActivityIndicator) {
        if (self.activityButton == nil) { 
            if (self.activityIndicator == nil) {
                
                if (self.activityIndicatorViewStyle) {
                    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:_activityIndicatorViewStyle];
                } else {
                    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                }
                [self.activityIndicator startAnimating];
                self.activityIndicator.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);  
            }
            self.activityButton = [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
        }
    }
    
    if (self.reloadButton == nil) {
        self.reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(reload)];
    }
    
    self.reloadButton.tintColor = self.tintColor;
    
    if (self.backButton) {
        [items addObject:self.backButton];
        self.backButton.enabled = [self.webView canGoBack];
        self.backButton.tintColor = self.tintColor;
    }
    
    UIBarButtonItem *spacing = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
    spacing.width = NavigationButtonSpacing;
    [items addObject:spacing];
    
    if (self.forwardButton) {
        [items addObject:self.forwardButton];
        self.forwardButton.enabled = [self.webView canGoForward];
        self.forwardButton.tintColor = self.tintColor;
    }
    
    if (self.middleBarButtonItems.count > 0) {
        [items addObjectsFromArray:self.middleBarButtonItems];
    } else {
        [items addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL]];
    }
    
    if (self.webView.loading) {
        if (self.usesCustomActivityIndicator) {
            [self.activityIndicator startAnimating];
        } else {
            [items addObject:self.activityButton];
        }
    } else {
        if (self.usesCustomActivityIndicator) {
            [self.activityIndicator stopAnimating];
        }
        
        [items addObject:self.reloadButton];
    }
    return items;
}

- (void)updateToolbarItems {
    NSString *title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    if (title) {
        self.title = title;
    }
    [self setToolbarItems:[self toolbarItemsForWebState]];
}

- (void)setBaseURL:(nullable NSURL *)theURL {
    _baseURL = theURL;
    if ([self isViewLoaded]) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.baseURL]];
        [self updateToolbarItems];
    }
}

- (void)setBaseURLRequest:(NSURLRequest *)theBaseURLRequest {
    _baseURLRequest = theBaseURLRequest;
    if ([self isViewLoaded]) {
        [self.webView loadRequest:_baseURLRequest];
        [self updateToolbarItems];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.htmlString) {
        [self.webView loadHTMLString:self.htmlString baseURL:self.baseURL];
    } else if (self.baseURL) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.baseURL]];
    } else if (self.baseURLRequest) {
        [self.webView loadRequest:self.baseURLRequest];
    }
    
    [self updateToolbarItems];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.shouldShowToolbarOnAppear) {
        [self.navigationController setToolbarHidden:NO animated:YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.shouldHideToolbarOnDisappear) {
        [self.navigationController setToolbarHidden:YES animated:YES];
    }
}

#pragma mark -
#pragma mark UIWebViewDelegate functions

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self updateToolbarItems];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self updateToolbarItems];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    BOOL showAlert = YES;
    
    if ([[error domain] isEqual:NSURLErrorDomain] && [error code] == NSURLErrorCancelled) {
        showAlert = NO;
    }
    
    // http://stackoverflow.com/questions/4299403/how-to-handle-app-urls-in-a-uiwebview
    // Avoid displaying errors when handling external URLs that the system can handle 
    if ([[error domain] isEqual:@"WebKitErrorDomain"] && [error code] == 102) {
        showAlert = NO;
    }
    
    // Avoid showing more than one alert at once; if it's failing, they only need to know once.
    if (self.showingAlert) {
        showAlert = NO;
    }
    
    if (showAlert) {
        NSString *title = NSLocalizedString(@"Load error", @"Title of web view error");
        NSString *message = [error localizedDescription];
        NSString *okButtonTitle = NSLocalizedString(@"OK", @"Button in web view error");
        NSString *goBackTitle = NSLocalizedString(@"Stop Loading", @"Button to stop if there are many errors");

        self.showingAlert = [[UIAlertView alloc] initWithTitle:title 
                                                        message:message 
                                                       delegate:self
                                              cancelButtonTitle:okButtonTitle
                                              otherButtonTitles:goBackTitle, nil];
        [self.showingAlert show];
    }
    
    [self updateToolbarItems];
}

- (void)reload {
    [self.webView reload];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [alertView cancelButtonIndex]) {
        [self.webView stopLoading]; // they clicked "Stop Loading"
    }
    self.showingAlert.delegate = nil;
    self.showingAlert = nil;
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if ([actionSheet isEqual:_schemeActionSheet]) {
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            [[UIApplication sharedApplication] openURL:_externalURL];
        }
        
        self.externalURL = nil;
        self.schemeActionSheet.delegate = nil;
        self.schemeActionSheet = nil;
    }
}

#pragma mark - Memory management

- (void)dealloc {
    self.baseURL = nil;
    self.webView.delegate = nil;
    self.showingAlert.delegate = nil;
    self.schemeActionSheet.delegate = nil;
    
}

@end
