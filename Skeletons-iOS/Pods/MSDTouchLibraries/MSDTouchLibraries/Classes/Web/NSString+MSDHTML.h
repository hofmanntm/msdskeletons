// This file was originally part of Google's GTM; license follows.
// If you use this, include the Apache License 2.0 in your settings bundle
// (iOSLibraries has a sample settings bundle with this included.)
// This was modified to match iOSLibraries logging and naming

//
//  GTMNSString+HTML.h
//  Dealing with NSStrings that contain HTML
//
//  Copyright 2006-2008 Google Inc.
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not
//  use this file except in compliance with the License.  You may obtain a copy
//  of the License at
// 
//  http://www.apache.org/licenses/LICENSE-2.0
// 
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
//  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
//  License for the specific language governing permissions and limitations under
//  the License.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Utilities for NSStrings containing HTML
@interface NSString (MSDNSStringHTMLAdditions)

/// Get a string where internal characters that need escaping for HTML are escaped 
//
///  For example, '&' become '&amp;'. This will only cover characters from table
///  A.2.2 of http://www.w3.org/TR/xhtml1/dtds.html#a_dtd_Special_characters
///  which is what you want for a unicode encoded webpage. If you have a ascii
///  or non-encoded webpage, please use stringByEscapingAsciiHTML which will
///  encode all characters.
///
/// For obvious reasons this call is only safe once.
//
//  Returns:
//    Autoreleased NSString
//
- (NSString *)msd_stringByEscapingForHTML;

/// Get a string where internal characters that need escaping for HTML are escaped 
//
///  For example, '&' become '&amp;'
///  All non-mapped characters (unicode that don't have a &keyword; mapping)
///  will be converted to the appropriate &#xxx; value. If your webpage is
///  unicode encoded (UTF16 or UTF8) use stringByEscapingHTML instead as it is
///  faster, and produces less bloated and more readable HTML (as long as you
///  are using a unicode compliant HTML reader).
///
/// For obvious reasons this call is only safe once.
//
//  Returns:
//    Autoreleased NSString
//
- (NSString *)msd_stringByEscapingForAsciiHTML;

/// Get a string where internal characters that are escaped for HTML are unescaped 
//
///  For example, '&amp;' becomes '&'
///  Handles &#32; and &#x32; cases as well
///
//  Returns:
//    Autoreleased NSString
//
- (NSString *)msd_stringByUnescapingFromHTML;

/// Get a string where HTML tags have been removed and un-encoded from HTML.
//  NOTE: This only works for simple HTML; complex escaping via CDATA, for example, is unsupported.
//        In addition, this is basically mind-reading in terms of translating various tags. (eg. <br>)
//
///  For example, '<a href="hi">B &amp; W </a> likes pie' becomes 'B & W  likes pie'
///
//  Returns:
//    Autoreleased NSString
//
- (NSString *)msd_stringByRemovingHTMLTags;

@end

NS_ASSUME_NONNULL_END
