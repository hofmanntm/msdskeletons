//
//  MSDWebViewController.h
//
//  Created by Scott Ferguson on 10-08-05.
//  Copyright 2010 MindSea. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MSDWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, strong, nullable) NSURL *baseURL;
@property (nonatomic, strong) NSURLRequest *baseURLRequest;
@property (nonatomic, strong, nullable) IBOutlet UIWebView *webView;
@property (nonatomic, strong, nullable) IBOutlet UIBarButtonItem *backButton;
@property (nonatomic, strong, nullable) IBOutlet UIBarButtonItem *forwardButton;
@property (nonatomic, strong, nullable) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, readwrite) UIActivityIndicatorViewStyle activityIndicatorViewStyle;
@property (nonatomic, copy, nullable) NSArray *middleBarButtonItems;
@property (nonatomic, strong, nullable) UIColor *tintColor;

@property (nonatomic, assign) BOOL shouldShowToolbarOnAppear;
@property (nonatomic, assign) BOOL shouldHideToolbarOnDisappear;
@property (nonatomic, assign) BOOL usesCustomActivityIndicator;

// if you pass nil for nibName, we default to the class name
// if you pass nil for the bundle, we use the main bundle
- (instancetype)initWithURL:(nullable NSURL *)theURL nibName:(nullable NSString *)nibName bundle:(nullable NSBundle *)bundle;
- (instancetype)initWithHTMLString:(nullable NSString *)string baseURL:(nullable NSURL *)url nibName:(nullable NSString *)nibName bundle:(nullable NSBundle *)bundle;
- (id)initWithURLRequest:(NSURLRequest *)theURL nibName:(NSString *)nibName bundle:(NSBundle *)bundle;

//Note: do not retain a reference to the webcontroller in the handler block.
//Return YES from the blockto let webview load URL as normal.
- (void)setHandlerForScheme:(NSString *)scheme withBlock:(BOOL(^)(NSURL *url, UIWebViewNavigationType navigationType))block;

- (void)removeHandlerForScheme:(NSString *)scheme;

@end

NS_ASSUME_NONNULL_END
