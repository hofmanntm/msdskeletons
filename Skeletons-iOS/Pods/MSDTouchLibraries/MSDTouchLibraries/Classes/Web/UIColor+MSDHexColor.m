//
//  UIColor+MSDHexColor.m
//  MSDTouchLibraries
//
//  Created by Mike Burke on 11-09-13.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "UIColor+MSDHexColor.h"

NS_ASSUME_NONNULL_BEGIN

@implementation UIColor (MSDHexColor)

+ (nullable UIColor *)msd_colorFromHexString:(NSString *)hexColor {
    NSScanner *scanner = [NSScanner scannerWithString:(hexColor ?: @"")];
    [scanner scanString:@"#" intoString:nil];
    
    NSUInteger location = scanner.scanLocation;
    unsigned int hexInteger;
    if ([scanner scanHexInt:&hexInteger]) {
        NSUInteger length = scanner.scanLocation - location;
        #define MASK(bits) (((hexInteger >> (bits)) & 0xff) / 255.f)
        if (length == 6) {
            return [UIColor colorWithRed:MASK(16)
                                   green:MASK(8)
                                    blue:MASK(0)
                                   alpha:1.f];
        }
        else if (length == 8) {
            return [UIColor colorWithRed:MASK(24)
                                   green:MASK(16)
                                    blue:MASK(8)
                                   alpha:MASK(0)];
        }
        #undef MASK
    }
    
    return nil;
}

@end

NS_ASSUME_NONNULL_END
