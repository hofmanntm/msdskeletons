//
//  NSData+MSDEncoding.m
//
//  Created by Dale Zak on 10-04-13.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "NSData+MSDEncoding.h"
#import <CommonCrypto/CommonDigest.h>

NS_ASSUME_NONNULL_BEGIN

@implementation NSData (MSDEncoding)

- (NSString *)msd_md5EncodedString {
    unsigned char result[16];
    CC_LONG length = (CC_LONG)self.length;
    CC_MD5(self.bytes, length, result);
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3], 
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]];
}

@end

NS_ASSUME_NONNULL_END
