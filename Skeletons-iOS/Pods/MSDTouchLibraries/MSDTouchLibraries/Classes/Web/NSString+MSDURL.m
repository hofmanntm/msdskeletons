//
//  NSString+MSDURL.m
//
//  Created by Jesse Rusak on 10-09-16.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "NSString+MSDURL.h"

NS_ASSUME_NONNULL_BEGIN

@implementation NSString(MSDURL)

- (NSString *)msd_stringByEscapingForURLComponent {
    CFStringRef specialCharacters = CFSTR(":/?#[]@!$&’()*+,;=");
    NSString *result = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 kCFAllocatorDefault,
                                                                                 (CFStringRef)self,
                                                                                 NULL,
                                                                                 specialCharacters,
                                                                                 kCFStringEncodingUTF8));
    return result;
}

- (NSString *)msd_stringByUnescapingFromURL {
    NSMutableString *resultString = [NSMutableString stringWithString:self];
    [resultString replaceOccurrencesOfString:@"+"
                                  withString:@" "
                                     options:NSLiteralSearch
                                       range:NSMakeRange(0, [resultString length])];
    return [resultString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];    
}

@end

NS_ASSUME_NONNULL_END
