//
//  StringByEscapingForJavaScript.swift
//  Galveston
//
//  Created by Michael Burke on 2016-07-22.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

import Foundation


extension String {

    public var escapingForJavaScript: String {
        
        // JSONSerialization requires an Array or Dictionary as the top-level object…
        // So we create an array containing our string, then fetch it by index when we use it in JS.
        let jsonData = try! JSONSerialization.data(withJSONObject: [self], options: [])
        
        let jsonString = String(data: jsonData, encoding: .utf8)!
            // JSON is not (anymore) a subset of JavaScript!
            .replacingOccurrences(of: "\u{2028}", with: "\\u2028")
            .replacingOccurrences(of: "\u{2029}", with: "\\u2029")
        
        return "\(jsonString)[0]"
    }
    
}
