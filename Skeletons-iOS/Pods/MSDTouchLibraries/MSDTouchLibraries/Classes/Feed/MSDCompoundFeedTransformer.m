//
//  MSDCompoundTransformer.m
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2017-03-20.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

#import "MSDCompoundFeedTransformer.h"

@interface MSDCompoundFeedTransformer () <MSDFeedTransformerDelegate>

@end

@implementation MSDCompoundFeedTransformer

- (instancetype)initWithTransformers:(NSArray<id<MSDFeedTransformer>> *)transformers {
    if ((self = [super init])) {
        _transformers = [transformers copy];
        
        for (id<MSDFeedTransformer> transformer in transformers) {
            if ([transformer respondsToSelector:@selector(setDelegate:)]) {
                transformer.delegate = self;
            }
        }
    }
    return self;
}

- (void)dealloc {
    for (id<MSDFeedTransformer> transformer in _transformers) {
        if ([transformer respondsToSelector:@selector(setDelegate:)]
            && transformer.delegate == self)
        {
            transformer.delegate = nil;
        }
    }
}

- (void)setTransformers:(NSArray<id<MSDFeedTransformer>> *)transformers {
    _transformers = [transformers copy];
    
    [self.delegate feedTransformerContentDidChange:self];
}

- (NSArray *)transformedContentForOriginalContent:(NSArray *)content {
    for (id<MSDFeedTransformer> transformer in self.transformers) {
        content = [transformer transformedContentForOriginalContent:content];
    }
    return content;
}

// MARK: MSDFeedTransformerDelegate

- (void)feedTransformerContentDidChange:(id<MSDFeedTransformer>)transformer {
    [self.delegate feedTransformerContentDidChange:self];
}

@end
