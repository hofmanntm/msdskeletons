//
//  MSDTransformedFeed.m
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-26.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import "MSDTransformedFeed.h"
#import "MSDBlockBinding.h"
#import "MSDFeedTransformer.h"
#import "MSDTouchLibraryDefines.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDTransformedFeed () 

@property (nonatomic, strong) id<MSDFeed> originalFeed;
@property (nullable, nonatomic) id<MSDFeedTransformer> transformer;
@property (copy, nullable, nonatomic) NSArray * (^transformerBlock)(NSArray *originalContent);

@property (nonatomic, strong) MSDBlockBinding *originalFeedBinding;

@property (nonatomic) BOOL canReload;
@property (nonatomic) BOOL canLoadMore;
@property (nonatomic, getter=isReloading) BOOL reloading;
@property (nonatomic, getter=isLoadingMore) BOOL loadingMore;

@property (nonatomic, copy) NSArray *transformedContent;
@property (nonatomic, copy) NSArray *originalContent;

@property (nonatomic, strong) NSMutableSet *updatedItemIndexes;

- (void)mutateToNewOriginalContent:(NSArray *)newOriginalContent newTransformedContent:(NSArray *)newTransformedContent;

@end


@implementation MSDTransformedFeed {
    NSUInteger _changesInProgress;
}

@synthesize canReload = _canReload, canLoadMore = _canLoadMore, reloading = _reloading, loadingMore = _loadingMore;

- (instancetype)initWithOriginalFeed:(id<MSDFeed>)theFeed
                         transformer:(id<MSDFeedTransformer>)theFeedTransformer {
    MSDArgNotNil(theFeed);
    MSDArgNotNil(theFeedTransformer);
    
    if ((self = [super init])) {
        _originalFeed = theFeed;
        _transformer = theFeedTransformer;
        
        if ([_transformer respondsToSelector:@selector(setDelegate:)]) {
            _transformer.delegate = self;
        }
        
        CommonInit(self);
    }
    return self;
}

- (instancetype)initWithOriginalFeed:(id<MSDFeed>)feed
                    transformerBlock:(NSArray * (^)(NSArray *originalContent))transformerBlock
{
    MSDArgNotNil(feed);
    MSDArgNotNil(transformerBlock);
    
    if ((self = [super init])) {
        _originalFeed = feed;
        _transformerBlock = transformerBlock;
        
        CommonInit(self);
    }
    
    return self;
}

static void CommonInit(MSDTransformedFeed *self) {
    self->_canReload = self->_originalFeed.canReload;
    self->_reloading = self->_originalFeed.isReloading;
    self->_canLoadMore = self->_originalFeed.canLoadMore;
    self->_loadingMore = self->_originalFeed.loadingMore;

    [self->_originalFeed addDelegate:self];

    __weak __typeof__(self) welf = self;
    void (^updateAction)(id, NSString *) = ^(id object, NSString *keyPath) {
        __typeof__(self) self = welf;
        [self setValue:[object valueForKeyPath:keyPath] forKeyPath:keyPath];
    };
    
    self->_originalFeedBinding = [MSDBlockBinding new];
    [self->_originalFeedBinding bindObject:self.originalFeed
                                  keyPaths:@[@"canReload", @"canLoadMore", @"reloading", @"loadingMore"]
                         toDidChangeAction:updateAction];

    self->_originalContent = [self->_originalFeed.content copy];
    self->_transformedContent = [self contentTransformedByBlockOrTransformer:self->_originalContent];
}

#pragma mark -
#pragma mark Passing along MSDFeed calls

- (void)reload {
    [self.originalFeed reload];
}

- (void)cancelReload {
    [self.originalFeed cancelReload];
}

- (void)loadMore {
    [self.originalFeed loadMore];
}

- (void)cancelLoadMore {
    [self.originalFeed cancelLoadMore];
}

- (NSArray *)content {
    return self.transformedContent;
}

#pragma mark - Mutation

- (void)beginContentChange {
    _changesInProgress++;
    if (_changesInProgress == 1) {
        self.updatedItemIndexes = [NSMutableSet set];
        
        for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedWillChangeContent:)]) {
            [delegate feedWillChangeContent:self];
        }    
    }
}

- (void)endContentChange {
    MSDCheck(_changesInProgress != 0, @"endContentChange not paired with beginContentChange; probably due to feedDidChangeContent: not being paired with feedWillChangeContent:");
    if (_changesInProgress > 0) {
        _changesInProgress--;
        
        if (_changesInProgress == 0) {
            NSArray *newOriginalContent = self.originalFeed.content;
            NSArray *newTransformedContent = [self contentTransformedByBlockOrTransformer:newOriginalContent];
            
            [self mutateToNewOriginalContent:newOriginalContent newTransformedContent:newTransformedContent];
            
            for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidChangeContent:)]) {
                [delegate feedDidChangeContent:self];
            }
        }
    }
}

- (void)updateTransformedContent {
    if (_changesInProgress) {
        // We'll update when the change is finished
        return;
    }
    
    [self beginContentChange];
    [self endContentChange];
}

- (void)mutateToNewOriginalContent:(NSArray *)newOriginalContent newTransformedContent:(NSArray *)newTransformedContent {
    NSIndexSet *indexesOfItemsAlreadyUpdated = [self dispatchContentChangeNotificationsForContentChanged:self.transformedContent
                                                                                               toContent:newTransformedContent];
    NSArray *itemsAlreadyUpdated = [newTransformedContent objectsAtIndexes:indexesOfItemsAlreadyUpdated];
    
    // Finally, any item not already inserted should be updated if it changed
    for (NSNumber *originalIndexNumber in self.updatedItemIndexes) {
        NSUInteger originalIndex = [originalIndexNumber unsignedIntegerValue];
        MSDCheck(originalIndex < [self.originalContent count], @"Got updated index that is too large.");
        
        if (originalIndex < [self.originalContent count]) {
            id item = self.originalContent[originalIndex];
            //the && newTransformedContent is important here as sometimes CoreData or other feeds will send update requests
            //for items that no longer are in the feed for one reason or another. Checking to be sure the object is still in the feed
            //prevents the crash where we try to update and delete the same index.
            if (![itemsAlreadyUpdated containsObject:item] && [newTransformedContent containsObject:item]) {

                
                NSUInteger transformedIndex = [self.transformedContent indexOfObject:item];
                if (transformedIndex != NSNotFound) {
                    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feed:didUpdateItemAtIndex:)]) {
                        [delegate feed:self didUpdateItemAtIndex:transformedIndex];
                    }            
                }
            }
        }
    }
    
    // We're done!
    self.originalContent = newOriginalContent;
    self.transformedContent = newTransformedContent;
}

- (NSArray *)contentTransformedByBlockOrTransformer:(NSArray *)originalContent {
    if (self.transformerBlock) {
        return self.transformerBlock(originalContent);
    } else {
        return [self.transformer transformedContentForOriginalContent:originalContent];
    }
}

#pragma mark -
#pragma mark Passing along MSDFeedDelegate calls

- (void)feedDidBeginReload:(id<MSDFeed>)feed {
    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidBeginReload:)]) {
        [delegate feedDidBeginReload:self];
    }
}

- (void)feedDidBeginLoadMore:(id<MSDFeed>)feed {
    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidBeginLoadMore:)]) {
        [delegate feedDidBeginLoadMore:self];
    }
}

- (void)feedDidFinishReload:(id<MSDFeed>)feed {
    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidFinishReload:)]) {
        [delegate feedDidFinishReload:self];
    }
}

- (void)feedDidFinishLoadMore:(id<MSDFeed>)feed {
    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidFinishLoadMore:)]) {
        [delegate feedDidFinishLoadMore:self];
    }    
}

- (void)feedDidFailReload:(id<MSDFeed>)feed withError:(NSError *)error {
    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidFailReload:withError:)]) {
        [delegate feedDidFailReload:self withError:error];
    }
}

- (void)feedDidFailLoadMore:(id<MSDFeed>)feed withError:(NSError *)error {
    for (id<MSDFeedDelegate> delegate in [self delegatesRespondingToSelector:@selector(feedDidFailLoadMore:withError:)]) {
        [delegate feedDidFailLoadMore:self withError:error];
    }    
}

- (void)feedWillChangeContent:(id<MSDFeed>)feed {
    [self beginContentChange];
}

- (void)feedDidChangeContent:(id<MSDFeed>)feed {
    [self endContentChange];
}

- (void)feed:(id <MSDFeed>)feed didUpdateItemAtIndex:(NSUInteger)index {
    MSDCheck(_changesInProgress != 0, @"didUpdateItemAtIndex should only be called between willChange/didChange blocks.");
    
    [self.updatedItemIndexes addObject:@(index)];
}

#pragma mark - MSDFeedTransformerDelegate
- (void)feedTransformerContentDidChange:(id<MSDFeedTransformer>)theTransformer {
    MSDCheck(theTransformer == self.transformer, @"Got callback from wrong transformer: %@", theTransformer);
    
    if (_changesInProgress) {
        return; // we'll update after the change is complete, anyway
    }
    
    [self updateTransformedContent];
}

#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
    if ([self.transformer respondsToSelector:@selector(setDelegate:)]) {
        if (self.transformer.delegate == self) {
            self.transformer.delegate = nil;
        }
    }
    
    [self.originalFeedBinding detach];
    
    [self.originalFeed removeDelegate:self];
}

@end

NS_ASSUME_NONNULL_END
