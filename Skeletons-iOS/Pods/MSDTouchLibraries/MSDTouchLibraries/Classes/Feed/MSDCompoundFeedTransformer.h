//
//  MSDCompoundTransformer.h
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2017-03-20.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

#import "MSDFeedTransformer.h"

NS_ASSUME_NONNULL_BEGIN

/**
 A transformer that applies several transformers in succession.
 
 If you don't need a feed for any intermediate step, an `MSDCompoundTransformer` only emits a change notification after running all transformers so it may have much less overhead than a chain of `MSDTransformedFeed`s.
 */
@interface MSDCompoundFeedTransformer: NSObject <MSDFeedTransformer>

/**
 The transformers in the chain. Each transformer's delegate is set to the compound transformer.
 
 Setting a new array of transformers causes a `-feedTransformerContentDidChange:` message to be sent to the delegate.
 */
@property (copy, nonatomic) NSArray <id<MSDFeedTransformer>> *transformers;

/**
 @param transformers An array of transformers. The original content is passed to the first transformer, whose output is then passed to the second transformer, and so on.
 */
- (instancetype)initWithTransformers:(NSArray <id<MSDFeedTransformer>> *)transformers NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;

/// The delegate will receive a `-feedTransformerContentDidChange:` message if any compounded transformer sends the same.
@property (assign, nullable, nonatomic) id<MSDFeedTransformerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
