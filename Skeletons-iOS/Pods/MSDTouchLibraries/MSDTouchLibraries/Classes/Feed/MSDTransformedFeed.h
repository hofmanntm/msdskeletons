//
//  MSDTransformedFeed.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-26.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDFeed.h"
#import "MSDFeedTransformer.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDTransformedFeed : MSDFeed <MSDFeedDelegate, MSDFeedTransformerDelegate>

/// Initializes a transformed feed with a transformer that can optionally inform the transformed feed of a need to update.
- (instancetype)initWithOriginalFeed:(id<MSDFeed>)feed
                         transformer:(id<MSDFeedTransformer>)transformer NS_DESIGNATED_INITIALIZER;

/// Initializes a transformed feed using a block to map from original content to transformed content.
- (instancetype)initWithOriginalFeed:(id<MSDFeed>)feed
                    transformerBlock:(NSArray * (^)(NSArray *originalContent))transformerBlock NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

// Call to immediately update our content; you normally don't need to call
// this; instead the MSDFeedContentTransformer can indicate when it needs to be
// updated.
- (void)updateTransformedContent;

@end

NS_ASSUME_NONNULL_END
