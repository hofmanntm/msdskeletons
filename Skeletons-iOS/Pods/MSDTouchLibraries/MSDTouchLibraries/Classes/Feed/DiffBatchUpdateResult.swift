//
//  DiffBatchUpdateResult.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2018-06-27.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import Foundation

/**
 A `DiffResult` suitable for use in a `UICollectionView` or `UITableView` batch update.

 In a `UICollectionView` or `UITableView` batch update, an element cannot simultaneously move and update. `DiffBatchUpdateResult` turns that operation into a deletion and insertion. It can also create `IndexPath`s for convenience.

 - Seealso: `DiffResult`
 - Seealso: `RandomAccessCollection.diff(_:)`
 */
public struct DiffBatchUpdateResult {

    /// Elements that do not exist in the "new" collection.
    public let deletions: Set<Int>

    /// Elements that do not exist in the "old" collection.
    public let insertions: Set<Int>

    /// Identical elements (i.e. elements with the same diff identifier and are equal to one another) that changed index between the "old" and "new" collections.
    public let moves: [DiffResult.Move]

    /// Elements that did not change index but are unequal (same diff identifier).
    public let updates: Set<Int>

    /**
     How to treat updates.

     `UICollectionView` can apparently crash sometimes if you call `reloadItems(at:)` during batch updates ([cite][reload-crashes]). Since it turns each reload into a deletion/insertion anyway, we can do that up front and avoid crashes.

     `UITableView` always knows how to reload a row, and deletion/insertion animations are quite different from the animation for a reload, so we prefer to use proper reloads when possible.

     [reload-crashes]: https://github.com/Instagram/IGListKit/issues/297#issuecomment-270463076
     */
    public enum UpdateTreatment {

        /// Turn each update into a deletion and an insertion. This is how `UICollectionView` works best.
        case asDeletionsAndInsertions

        /// Keep each update as-is. `UITableView` is happy to work with this.
        case asUpdates
    }

    /**
     Turns a `DiffResult` into something suitable for a `UICollectionView` or `UITableView` batch update.

     - Parameter result: The non-`UIKit`-compliant result.
     - Parameter treatingUpdate: Whether to turn each update into a deletion and insertion.
     */
    public init(result: DiffResult, treatingUpdates: UpdateTreatment) {
        var deletions = Set(result.deletions)
        var insertions = Set(result.insertions)
        var updates = Set(result.updates)

        var moves = result.moves
        let includesUpdate = moves.partition { updates.contains($0.from) }

        // A move and update becomes a deletion and insertion.
        for move in moves[includesUpdate...] {
            updates.remove(move.from)
            deletions.insert(move.from)
            insertions.insert(move.to)
        }

        for (id, oldIndex) in result.oldMap where updates.contains(oldIndex) {
            guard let newIndex = result.newMap[id] else { continue }

            switch treatingUpdates {
            case .asDeletionsAndInsertions:
                updates.remove(oldIndex)
                deletions.insert(oldIndex)
                insertions.insert(newIndex)

            case .asUpdates:
                // If any updates also changed index, that's a deletion and insertion.
                if newIndex != oldIndex {
                    updates.remove(oldIndex)
                    deletions.insert(oldIndex)
                    insertions.insert(newIndex)
                }
            }
        }

        self.deletions = deletions
        self.insertions = insertions
        self.moves = Array(moves[..<includesUpdate])
        self.updates = updates
    }

    /// The number of deletions, insertions, moves, and updates. May differ from the original `DiffResult`'s `changeCount`.
    public var changeCount: Int {
        return deletions.count + insertions.count + moves.count + updates.count
    }

    /// `true` when the result has any deletions, insertions, moves, or updates.
    public var hasChanges: Bool {
        return !deletions.isEmpty || !insertions.isEmpty || !moves.isEmpty || !updates.isEmpty
    }

    /**
     Returns an array of index paths suitable for passing to `UICollectionView.deleteItems(at:)` or `UITableView.deleteRows(at:with:)` during batch updates.
     */
    public func deletionIndexPaths(section: Int) -> [IndexPath] {
        return deletions.map { [section, $0] }
    }

    /**
     Returns an array of index paths suitable for passing to `UICollectionView.insertItems(at:)` or `UITableView.insertRows(at:with:)` during batch updates.
     */
    public func insertionIndexPaths(section: Int) -> [IndexPath] {
        return insertions.map { [section, $0] }
    }

    /**
     Returns index paths suitable for passing to `UICollectionView.moveItem(at:to:)` or `UITableView.moveRow(at:to:)` during batch updates.
     */
    public func moveIndexPaths(section: Int) -> [Move] {
        return moves.map { Move(from: [section, $0.from], to: [section, $0.to]) }
    }

    /**
     Returns an array of index paths suitable for passing to `UITableView.reloadRows(at:with:)`.

     - Note: For `UICollectionView` you may be able to avoid some crashes by turning each update into a deletion and an insertion. See `UpdateTreatment` for more details.
     */
    public func updateIndexPaths(section: Int) -> [IndexPath] {
        return updates.map { [section, $0] }
    }

    /// An identical element whose index changed between the "old" and "new" collections.
    public struct Move {

        /// The element's position in the "old" collection.
        public let from: IndexPath

        /// The element's position in the "new" collection.
        public let to: IndexPath
    }
}


extension DiffResult {
    /**
     Make a result suitable for UICollectionView and UITableView batch updates.

     - Parameter treatingUpdates: Whether updates should be included in the result (`UICollectionView` doesn't have the concept of updates).
     */
    public func makeBatchUpdateResult(treatingUpdates: DiffBatchUpdateResult.UpdateTreatment) -> DiffBatchUpdateResult {
        return DiffBatchUpdateResult(result: self, treatingUpdates: treatingUpdates)
    }
}
