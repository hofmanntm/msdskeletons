//
//  Diff.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2018-06-25.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

/**
 A type that has identity and equality when diffing two collections.

 Identity is determined by `diffIdentifier`. Two instances with the same `diffIdentifier` are thought to represent the same underlying model.

 Two instances may have the same `diffIdentifier` yet not be equal to one another. This is how we determine e.g. an update vs. deletion/insertion. In a way it's like `Hashable`: two instances can have the same `hashValue` yet not be equal. In fact, it's often similar enough that we provide a default implementation of `DiffIdentifiable` for any `Hashable` type; just declare your conformance to `DiffIdentifiable` and you're all set.

 This protocol is used by `RandomAccessCollection.diff(_:)`.
 */
public protocol DiffIdentifiable: Equatable {

    /**
     A string that uniquely identifies the instance in a collection. Must not change during a call to `RandomAccessCollection.diff(_:)` or the behaviour is undefined.

     If the conforming type already has the concept of a unique identifier (e.g. from an API or a database), that's a good candidate to return for a `diffIdentifier`.

     If you're worried about possible collisions (e.g. your collection has instances of two types each with an integer unique ID), you might add a unique prefix to the returned `diffIdentifier` (e.g. `"CoolThing-\(id)"` and `"OtherThing-\(id)"`).

     For example, `return String(arc4random())` is an implementation with undefined behaviour, because `RandomAccessCollection.diff(_:)` may obtain `diffIdentifier` multiple times and get different values for the same instance.
     */
    var diffIdentifier: String { get }
}

/// If a type is already `Hashable`, there's a good chance that its `hashValue` is suitable as a diff identifier. In that case, simply declare its conformance to `DiffIdentifiable` and this default implementation will handle the rest.
public extension DiffIdentifiable where Self: Hashable {
    var diffIdentifier: String {
        return "\(hashValue)"
    }
}

/*
 This diffing approach comes from [Isolating Differences Between Files][Heckel] ([alternate link][Heckel2]), by Paul Heckel. The application to collection and table views comes via [IGListKit][]. Some of the terminology below comes from the paper.

 [Heckel]: http://dl.acm.org/citation.cfm?id=359467&dl=ACM&coll=DL&CFID=529464736&CFTOKEN=43088172
 [Heckel2]: http://documents.scribd.com/docs/10ro9oowpo1h81pgh1as.pdf
 [IGListKit]: https://github.com/Instagram/IGListKit
 */

/// For simplicity, we've restricted diff identifiers to strings.
private typealias DiffIdentifier = String

/**
 The symbol table has an entry for each unique `diffIdentifier` in the two collections being diffed.

 (The paper refers to unique lines in the two input files. Same idea, just think of lines as diff identifiers and files as collections.)
 */
private typealias SymbolTable = [DiffIdentifier: Entry]

/// An entry in the symbol table.
private class Entry {

    /// The number of instances in the "old" collection that have the diff identifier.
    var oldCounter: Counter = .zero

    /// The number of occurrences of the diff identifier in the "new" collection.
    var newCounter: Counter = .zero

    /// Offsets from `startIndex` in the "old" collection where the diff identifier can be found.
    var oldOffsets: [Int] = []

    /// When an instance with the diff identifier exists in both the "old" and "new" collections, this tracks whether those instances are equal to one another.
    var isUpdated = false

    /// We don't actually care exactly how many instances there are in each collection, we just need to know if there's none, one, or more than one.
    enum Counter {
        case zero, one, many

        mutating func increment() {
            switch self {
            case .zero:
                self = .one
            case .one:
                self = .many
            case .many:
                break
            }
        }
    }
}

/// Metadata kept for each element in the two collections being diffed.
private enum Record {
    case entry(Entry)
    case otherOffset(Int)
}

extension RandomAccessCollection where Element: DiffIdentifiable {

    /**
     Efficiently determines the difference between two collections, expressed as the offsets from `startIndex` that were deleted, inserted, moved, and updated.

     This diffing approach comes from [Isolating Differences Between Files][Heckel] ([alternate link][Heckel2]), by Paul Heckel. The application to collection and table views comes via [IGListKit][]. Some of the terminology below comes from the paper.

     - Parameter newCollection: A collection to compare to `self`.

     - Complexity: O(n) where n is the number of elements in the collections (assumes dictionary lookups amortize to O(1)).

     - Seealso: `DiffBatchUpdateResult`

     [Heckel]: http://dl.acm.org/citation.cfm?id=359467&dl=ACM&coll=DL&CFID=529464736&CFTOKEN=43088172
     [Heckel2]: http://documents.scribd.com/docs/10ro9oowpo1h81pgh1as.pdf
     [IGListKit]: https://github.com/Instagram/IGListKit
     */
    public func diff<OtherCollection>(_ newCollection: OtherCollection) -> DiffResult where OtherCollection: RandomAccessCollection, OtherCollection.Element == Element {

        var symbolTable = SymbolTable()

        // Pass 1 from the paper: prepare the metadata for everything in the "new" collection.
        var newRecords: [Record] = []
        for item in newCollection {
            let id = item.diffIdentifier
            let entry = symbolTable[id] ?? Entry()
            entry.newCounter.increment()
            symbolTable[id] = entry
            newRecords.append(.entry(entry))
        }

        // Pass 2 from the paper: prepare the metadata for everything in the "old" collection (i.e. `self`).
        var oldRecords: [Record] = []
        for (offset, item) in self.enumerated() {
            let id = item.diffIdentifier
            let entry = symbolTable[id] ?? Entry()
            entry.oldCounter.increment()
            entry.oldOffsets.append(offset)
            symbolTable[id] = entry

            oldRecords.append(.entry(entry))
        }

        // Pass 3 from the paper: find the elements that appear in both collections and update their symbol table entries to point to their location in the other collection.
        for i in newRecords.indices {
            if case .entry(let entry) = newRecords[i], !entry.oldOffsets.isEmpty {
                let oldOffset = entry.oldOffsets.removeFirst()
                let new = newCollection[newCollection.index(newCollection.startIndex, offsetBy: i)]
                let old = self[self.index(self.startIndex, offsetBy: oldOffset)]
                entry.isUpdated = new != old

                if entry.oldCounter != .zero, entry.newCounter != .zero {
                    newRecords[i] = .otherOffset(oldOffset)
                    oldRecords[oldOffset] = .otherOffset(i)
                }
            }
        }

        // Now we can figure out what instances have been deleted: everything in the "old" collection whose record doesn't include an offset into the "new" collection.
        var deletions: [Int] = []
        var deleteOffsets: [Int] = []
        var oldMap: [String: Int] = [:]
        var runningDeleteOffset = 0
        for (i, record) in oldRecords.enumerated() {

            // We keep track of how many previous elements were deleted so that we can detect moves later on.
            deleteOffsets.append(runningDeleteOffset)

            if case .entry = record {
                deletions.append(i)
                runningDeleteOffset += 1
            }

            // `oldMap` is for the benefit of `DiffBatchUpdateResult`.
            let id = self[index(startIndex, offsetBy: i)].diffIdentifier
            oldMap[id] = i
        }

        // Inserts, moves, and updates are determined by looking at the records of everything in the "new" collection.
        var insertions: [Int] = []
        var insertOffsets: [Int] = []
        var moves: [DiffResult.Move] = []
        var newMap: [String: Int] = [:]
        var runningInsertOffset = 0
        var updates: [Int] = []
        for (i, record) in newRecords.enumerated() {

            // Keep track of how many previous elements were inserted so that we can detect moves shortly.
            insertOffsets.append(runningInsertOffset)

            let id = newCollection[newCollection.index(newCollection.startIndex, offsetBy: i)].diffIdentifier

            switch record {
            case .entry:
                insertions.append(i)
                runningInsertOffset += 1

            case .otherOffset(let oldOffset):

                // At this level, the same instance can be both moved and updated. UICollectionView and UITableView don't work that way, but fixing that is the job of `DiffBatchUpdateResult` so we don't worry about it here.
                if symbolTable[id]!.isUpdated {
                    updates.append(oldOffset)
                }

                let insertOffset = insertOffsets[i]
                let deleteOffset = deleteOffsets[oldOffset]
                if (oldOffset - deleteOffset + insertOffset) != i {
                    moves.append(.init(from: oldOffset, to: i))
                }
            }

            // `newMap` is for the benefit of `DiffBatchUpdateResult`.
            newMap[id] = i
        }

        return DiffResult(deletions: deletions, insertions: insertions, moves: moves, updates: updates, oldMap: oldMap, newMap: newMap)
    }
}

/**
 The difference between the two input collections.

 - Warning: A `DiffResult` should not be used for a `UICollectionView` or `UITableView` batch update. See `DiffBatchUpdateResult` for the necessary preparation.

 - Seealso: `RandomAccessCollection.diff(_:)`.
 */
public struct DiffResult {

    /// Offsets from the "old" collection's `startIndex` of elements that do not exist in the "new" collection.
    public let deletions: [Int]

    /// Offsets from the "new" collection's `startIndex` of elements that do not exist in the "old" collection.
    public let insertions: [Int]

    /**
     Offsets from the "old" and "new" collections' respective `startIndex` of an element that exists in both collections but has changed position.

     - Note: The same element can be present in both `moves` and `updates`.
     */
    public let moves: [Move]

    /**
     Offsets from the "new" collection's `startIndex` of an element that exists in both collections but was not equal.

     - Note: The same element can be present in both `moves` and `updates`.
     */
    public let updates: [Int]

    /// A map of `diffIdentifier`s to offsets from the "old" collection's `startIndex`, included for the benefit of `DiffBatchUpdateResult`.
    public let oldMap: [String: Int]

    /// A map of `diffIdentifier`s to offsets from the "new" collection's `startIndex`, included for the benefit of `DiffBatchUpdateResult`.
    public let newMap: [String: Int]

    /// A change in offset for an element between the "old" collection and the "new" collection.
    public struct Move {

        /// The offset from the "old" collection's `startIndex`.
        public let from: Int

        /// The offset from the `new` collection's `startIndex`.
        public let to: Int

        public init(from: Int, to: Int) {
            self.from = from
            self.to = to
        }
    }

    /// The number of deletions, insertions, moves, and updates.
    public var changeCount: Int {
        return deletions.count + insertions.count + moves.count + updates.count
    }

    /// `true` when the result has any deletions, insertions, moves, or updates.
    public var hasChanges: Bool {
        return !deletions.isEmpty || !insertions.isEmpty || !moves.isEmpty || !updates.isEmpty
    }
}
