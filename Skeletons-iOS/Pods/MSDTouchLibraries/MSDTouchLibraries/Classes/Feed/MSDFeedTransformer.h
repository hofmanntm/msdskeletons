//
//  MSDFeedTransformer.h
//  MSDTouchLibraries
//
//  Created by Jesse Rusak on 11-02-26.
//  Copyright 2011 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MSDFeedTransformerDelegate;

/*!
 * Implement this protocol to modify some feed content.
 * If you include the delegate property, you can ask it to reload you
 * if some external factor causes you to change your content.
 */
@protocol MSDFeedTransformer<NSObject>

- (NSArray *)transformedContentForOriginalContent:(NSArray *)content;

@optional

@property (nonatomic, assign, nullable) id<MSDFeedTransformerDelegate> delegate;

@end


@protocol MSDFeedTransformerDelegate<NSObject>

- (void)feedTransformerContentDidChange:(id<MSDFeedTransformer>)transformer;

@end

NS_ASSUME_NONNULL_END
