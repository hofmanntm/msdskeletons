//
//  ArrayFeed.swift
//  MSDTouchLibraries
//
//  Created by Joel Glanfield on 2016-07-12.
//  Copyright © 2016 MindSea. All rights reserved.
//

/// A very boring `MSDFeed` that keeps an array and dispatches change notifications when you set a new array. Can't reload, can't load more.
open class ArrayFeed<Element>: MSDFeed {
    private let equalBlock: ((Any, Any) -> Bool)?
    
    public var underlyingContent: [Element] {
        willSet {
            for delegate in delegates(respondingToSelector: #selector(MSDFeedDelegate.feedWillChangeContent)) {
                delegate.feedWillChangeContent!(self)
            }
        }

        didSet {
            dispatchContentChangeNotifications(forContentChanged: oldValue, toContent: underlyingContent, isEqualBlock: equalBlock)
            
            for delegate in delegates(respondingToSelector: #selector(MSDFeedDelegate.feedDidChangeContent)) {
                delegate.feedDidChangeContent!(self)
            }
        }
    }
    
    public init(content: [Element], isEquivalent block: ((Element, Element) -> Bool)? = nil) {
        underlyingContent = content
        if let block = block {
            equalBlock = { block($0 as! Element, $1 as! Element) }
        } else {
            equalBlock = nil
        }
    }

    public convenience override init() {
        self.init(content: [])
    }
    
    // MARK: MSDFeed
    
    override open var content: [Any] {
        return underlyingContent
    }
}
