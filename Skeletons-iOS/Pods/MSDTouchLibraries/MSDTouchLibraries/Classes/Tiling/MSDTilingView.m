//
//  MSDTilingView.m
//
//  Created by Jesse Rusak on 10-11-24.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDTilingView.h"
#import "MSDTilingViewLayout.h"
#import "MSDReuseQueue.h"
#import "MSDTouchLibraryDefines.h"


@interface MSDTilingView()

// layout
- (void)updateGridSize;
- (void)tileCellsIncludingAlreadyVisible:(BOOL)tileAlreadyVisible;
- (NSIndexSet *)indexesInUseIncludingOffscreen:(BOOL)includingOffscreen;

- (CGRect)frameForTilingViewCell:(UIView *)cellView
                inContainingRect:(CGRect)gridRect;

- (CGRect)frameForTilingViewCell:(UIView *)cellView
                         atIndex:(NSUInteger)cellIndex;

- (void)handleMemoryWarning:(NSNotification *)notification;

- (void)dispatchCellDidDisappear:(UIView *)view;
- (nullable UIView *)tilingViewCellForItemIndex:(NSNumber *)index reuseIdentifierOrNil:(NSString ** __nullable)reuseIdentifierOrNil;

@property (nonatomic, strong) NSMutableDictionary *reuseQueues;
@property (nonatomic, strong) NSIndexSet *visibleIndexes;
@property (nonatomic, strong, nullable) UIView *extractedCell;

@end

static const NSTimeInterval ChangeAnimationDuration = 0.4;

@implementation MSDTilingView {
    CGRect _oldBounds;
    BOOL _animateChanges;
    BOOL _postponeTiling;
}

@synthesize delegate = _delegate;

#pragma mark - Creation and Configuration

- (void)commonInit {
    self.reuseQueues = [NSMutableDictionary dictionary];
    self.preloadItems = YES;
    self.visibleIndexes = [NSIndexSet indexSet];
    self.autoresizesSubviews = NO; // Most of the time, you don't want this, since we position subviews manually.
    self.placeCellsOnTop = NO;
    
    _oldBounds = self.bounds;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMemoryWarning:)
                                                 name:UIApplicationDidReceiveMemoryWarningNotification
                                               object:nil];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self commonInit];
    }
    return self;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self commonInit];
    }
    return self;
}

- (void)setLayout:(id<MSDTilingViewLayout>)newLayout animated:(BOOL)animated {
    if (newLayout != _layout) {
        _layout = newLayout;
        [self setNeedsLayout];
        if (animated) {
            _animateChanges = YES;
            [self layoutIfNeeded];
            _animateChanges = NO;
        }
    }
}


- (void)setCellContentMode:(MSDTilingViewContentMode)newContentMode {
    if (newContentMode != _cellContentMode) {
        _cellContentMode = newContentMode;
        [self setNeedsLayout];
    }
}

- (void)setDelegate:(id<MSDTilingViewDelegate>)theDelegate {
    [super setDelegate:theDelegate];
    _delegate = theDelegate;
}

#pragma mark - Data Access

- (nullable UIView *)dequeueReusableTilingViewCellWithReuseIdentifier:(NSString *)reuseIdentifier {
    MSDReuseQueue *queue = (self.reuseQueues)[reuseIdentifier];
    if (!queue) {
        return nil;
    } else {
        return [queue dequeueReusableObject];
    }
}

- (nullable UIView *)dequeueReusableTilingViewCell {
    return [self dequeueReusableTilingViewCellWithReuseIdentifier:@""];
}

- (NSUInteger)itemCount {
    return [self.delegate numberOfItemsInTilingView:self];
}

#pragma mark - Photo Layout

- (void)layoutSubviews {
    [super layoutSubviews];

    if (!CGSizeEqualToSize(_oldBounds.size, self.bounds.size)) {
        _oldBounds = self.bounds;
        
        CGPoint currentOffset = self.contentOffset;
        CGSize currentSize = self.contentSize;
        
        [self updateGridSize];
        
        if (currentSize.width && currentSize.height) {
            CGSize newSize = self.contentSize;
            CGPoint newOffset = CGPointMake(currentOffset.x / currentSize.width * newSize.width,
                                            currentOffset.y / currentSize.height * newSize.height);
            
            // Adjust content offset so we're never scrolled farther than possible
            CGFloat maxOffsetX = MAX(0, newSize.width - self.bounds.size.width);
            if (newOffset.x > maxOffsetX) {
                newOffset.x = maxOffsetX;
            }
            
            CGFloat maxOffsetY = MAX(0, newSize.height - self.bounds.size.height);
            if (newOffset.y > maxOffsetY) {
                newOffset.y = maxOffsetY;
            }
            
            if (self.pagingEnabled) {
                // ensure that we're on a paging boundary. A straightforward scale won't always do that (because the
                // content offset is sometimes changed elsewhere by uiscrollview), but if we see what's visible and always
                // take the last one, we should get the right result
                CGRect newRect = (CGRect){newOffset, self.bounds.size};
                NSUInteger photoIndex = [self.layout indexBestMatchingRect:newRect
                                                              viewportSize:self.bounds.size];
                if (photoIndex != NSNotFound) {
                    // TODO: this calculation makes serious assuptions about self.layout
                    newOffset = CGPointMake(photoIndex*self.bounds.size.width, newOffset.y);
                }
            }
            
            // this causes it to stop scrolling during resize, which prevents animation glitches
            [self setContentOffset:newOffset animated:NO];
        }
    } else {
        [self updateGridSize];
    }
    
    // This is set to YES when we want to allow our view to adjust our content size, etc,
    // without actually tiling any views.
    if (!_postponeTiling) {
        // Get everything ready for display
        [self tileCellsIncludingAlreadyVisible:YES];
        
        // Inform any newly visible cell that they're going to be shown now
        NSIndexSet *newVisibleCellIndexes = [self indexesInUseIncludingOffscreen:NO];
        NSIndexSet *oldVisibleCellIndexes = self.visibleIndexes;
        
        // we do this immediately in case the callbacks below cause us to re-layout
        self.visibleIndexes = newVisibleCellIndexes;
        
        if (![newVisibleCellIndexes isEqualToIndexSet:oldVisibleCellIndexes]
            && [self.delegate respondsToSelector:@selector(tilingView:cellWillAppear:)]) {
            [newVisibleCellIndexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                if (![oldVisibleCellIndexes containsIndex:idx]) {
                    UIView *cell = [self tilingViewCellForItemIndex:@(idx) reuseIdentifierOrNil:nil];
                    // The new visible cell indices can include cells that don't actually exist (eg it
                    // is outside the set of our actual indices).
                    // In that case, cell will be nil, so we don't bother sending a cellWillAppear message.
                    if (cell) {
                        [self.delegate tilingView:self cellWillAppear:cell];
                    }
                }
            }];
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(tilingViewDidLayoutSubviews:)]) {
        [self.delegate tilingViewDidLayoutSubviews:self];
    }
}

- (void)setFrame:(CGRect)newFrame {
    // By default, UIScrollView will adjust our content offset to fit it within
    // our contentSize when the frame changes. This is inconvinient, as we adjust it
    // ourselves in layoutSubviews, but we need to use the old value to determine what
    // we should be adjusting to; so, preserve the old value here.
    
    // If we don't do this, rotation screws up when we rotate near the end
    // of our content, because the view adjusts our content offset and we
    // lose our place.
    CGPoint oldContentOffset = self.contentOffset;
    [super setFrame:newFrame];
    self.contentOffset = oldContentOffset;
}


- (void)updateGridSize {
    if (_animateChanges) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:ChangeAnimationDuration];
    }
    self.contentSize = [self.layout sizeForItemCount:[self itemCount]
                                        viewportSize:self.bounds.size];
    if (_animateChanges) {
        [UIView commitAnimations];
    }
}

- (void)tileCellsIncludingAlreadyVisible:(BOOL)tileAlreadyVisible {
    NSIndexSet *indexes = [self indexesInUseIncludingOffscreen:YES];
    MSDCheck(indexes, @"Missing indexes");
    
    for (MSDReuseQueue *queue in [self.reuseQueues objectEnumerator]) {
        for (NSNumber *itemKey in queue.keysInUse) {
            UIView *visibleView = [queue objectInUseForKey:itemKey];
            if (![indexes containsIndex:[itemKey integerValue]]
                && visibleView != self.extractedCell) {
                if (_animateChanges) {
                    [queue removeObjectForKey:itemKey];
                    
                    [UIView animateWithDuration:ChangeAnimationDuration
                                          delay:0.0f
                                        options:UIViewAnimationOptionAllowUserInteraction
                                     animations:^(void) {
                                         visibleView.alpha = 0.0f;
                                     } completion:^(BOOL finished) {
                                         [queue makeObjectReusable:visibleView];
                                         [visibleView removeFromSuperview];
                                         
                                         [self dispatchCellDidDisappear:visibleView];
                                     }];
                } else {
                    [queue makeObjectReusable:visibleView];
                    [visibleView removeFromSuperview];
                    [self dispatchCellDidDisappear:visibleView];
                }
            }
        }
    }
    
    for (NSUInteger itemIndex = [indexes firstIndex];
         itemIndex != NSNotFound && itemIndex < [self itemCount];
         itemIndex = [indexes indexGreaterThanIndex:itemIndex]) {
        
        BOOL isNewCell = NO;
        
        NSNumber *itemKey = @(itemIndex);
        
        NSString *reuseIdentifier = nil;
        UIView *cell = [self tilingViewCellForItemIndex:itemKey reuseIdentifierOrNil:&reuseIdentifier];
        
        if (cell) {
            if (tileAlreadyVisible && cell != self.extractedCell) {
                if (_animateChanges) {
                    [UIView beginAnimations:nil context:NULL];
                    [UIView setAnimationDuration:ChangeAnimationDuration];
                    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                }
                // We avoid re-setting the same frame, as subviews that are scrollviews
                // don't like it if their frame is re-set every frame (they stop bouncing, etc)
                CGRect newFrame = [self frameForTilingViewCell:cell atIndex:itemIndex];
                if (!CGRectEqualToRect(newFrame, cell.frame)) {
                    cell.frame = newFrame;
                }
                if (_animateChanges) {
                    [UIView commitAnimations];
                }
            }
        } else {
            if ([self.delegate respondsToSelector:@selector(tilingView:reuseIdentifierForCellAtIndex:)]) {
                reuseIdentifier = [self.delegate tilingView:self reuseIdentifierForCellAtIndex:itemIndex];
                MSDCheck(reuseIdentifier, @"reuseIdentifier from tilingView:tilingViewCellForIndex:reuseIdentifier: must not be nil");
            } else {
                reuseIdentifier = @"";
            }
            cell = [self.delegate tilingView:self tilingViewCellForIndex:itemIndex];
            MSDCheck(cell, @"Delegate returned a nil cell for index %@", @(itemIndex));
            isNewCell = YES;
            cell.frame = [self frameForTilingViewCell:cell atIndex:itemIndex];
        }
        
        if (cell != self.extractedCell) {
            if (isNewCell && _animateChanges) {
                cell.alpha = 0.0f;
                [UIView beginAnimations:nil context:NULL];
                [UIView setAnimationDuration:ChangeAnimationDuration];
                cell.alpha = 1.0f;
                [UIView commitAnimations];
            } else {
                cell.alpha = 1.0f;
            }
            if ([cell superview] != self) {
                if (self.placeCellsOnTop) {
                    [self addSubview:cell];
                }
                else {
                    [self insertSubview:cell atIndex:0];
                }
                
            }
            MSDReuseQueue *queue = (self.reuseQueues)[reuseIdentifier];
            if (!queue) {
                queue = [[MSDReuseQueue alloc] init];
                (self.reuseQueues)[reuseIdentifier] = queue;
            }
            [queue useObject:cell forKey:itemKey];
        }
    }
}

- (NSIndexSet *)indexesInUseIncludingOffscreen:(BOOL)includingOffscreen {
    CGRect gridRect = self.bounds;
    
    // We expand the grid rect here so that (a) when rotating, the items don't disappear when
    // the height shrinks to landscape and (b) so we preload some items
    // Postfetching is also useful for having backwards scrolling work nicely
    if (includingOffscreen && self.preloadItems) {
        gridRect.size.width += 2 * self.bounds.size.width;
        gridRect.origin.x -= self.bounds.size.width;
        
        gridRect.size.height += 2 * self.bounds.size.height;
        gridRect.origin.y -= self.bounds.size.height;
    }
    
    if (!self.layout) {
        return [NSIndexSet indexSet];
    }
    
    return [self.layout indexesForItemsInRect:gridRect viewportSize:self.bounds.size];
}

// Gets the containing-rect-relative frame for content of the given size
- (CGRect)frameForContentOfSize:(CGSize)contentSize inContainingRectOfSize:(CGSize)gridSize {
    CGFloat horizontalSpace = gridSize.width - contentSize.width;
    CGFloat verticalSpace = gridSize.height - contentSize.height;
    
    // This result is within the origin of the grid rect
    CGRect result = CGRectNull;
    
    switch (self.cellContentMode) {
        case MSDTilingViewContentModeScaleToFill:
            result.size = gridSize;
            result.origin = CGPointZero;
            break;
            
        case MSDTilingViewContentModeAspectFit:
        {
            CGFloat contentScale;
            
            // If we're wider than the grid rect, in aspect, then we'll touch in x
            if (contentSize.width / contentSize.height > gridSize.width / gridSize.height) {
                contentScale = gridSize.width / contentSize.width;
            } else {
                contentScale = gridSize.height / contentSize.height;
            }
            result.size = CGSizeMake(contentScale * contentSize.width, contentScale * contentSize.height);
            result.origin = CGPointMake((gridSize.width - result.size.width) / 2,
                                        (gridSize.height - result.size.height) / 2);
        }
            break;
            
        case MSDTilingViewContentModeCenter:
            result.size = contentSize;
            result.origin = CGPointMake(horizontalSpace / 2, verticalSpace / 2);
            break;
            
        case MSDTilingViewContentModeTop:
            result.size = contentSize;
            result.origin = CGPointMake(horizontalSpace / 2, 0);
            break;
            
        case MSDTilingViewContentModeBottom:
            result.size = contentSize;
            result.origin = CGPointMake(horizontalSpace / 2, verticalSpace);
            break;
            
        case MSDTilingViewContentModeLeft:
            result.size = contentSize;
            result.origin = CGPointMake(0, verticalSpace / 2);
            break;
            
        case MSDTilingViewContentModeRight:
            result.size = contentSize;
            result.origin = CGPointMake(horizontalSpace, verticalSpace / 2);
            break;
            
        case MSDTilingViewContentModeTopLeft:
            result.size = contentSize;
            result.origin = CGPointMake(0, 0);
            break;
            
        case MSDTilingViewContentModeTopRight:
            result.size = contentSize;
            result.origin = CGPointMake(horizontalSpace, 0);
            break;
            
        case MSDTilingViewContentModeBottomLeft:
            result.size = contentSize;
            result.origin = CGPointMake(0, verticalSpace);
            break;
            
        case MSDTilingViewContentModeBottomRight:
            result.size = contentSize;
            result.origin = CGPointMake(horizontalSpace, verticalSpace);
            break;
    }
    return result;
}

- (CGRect)frameForTilingViewCell:(UIView *)cell
                inContainingRect:(CGRect)gridRect {
    
    CGSize contentSize = [cell sizeThatFits:gridRect.size];
    
    CGRect result = [self frameForContentOfSize:contentSize inContainingRectOfSize:gridRect.size];
    
    //CGRectIntegral would in some cases increase the size of the rect.
    result.origin.x = roundf(result.origin.x + gridRect.origin.x);
    result.origin.y = roundf(result.origin.y + gridRect.origin.y);
    result.size.width = roundf(result.size.width);
    result.size.height = roundf(result.size.height);
    return result;
}

- (CGRect)frameForTilingViewCell:(UIView *)cell atIndex:(NSUInteger)itemIndex {
    return [self frameForTilingViewCell:cell
                       inContainingRect:[self.layout rectForItemAtIndex:itemIndex
                                                           viewportSize:self.bounds.size]];
}

#pragma mark - Data Alteration

- (void)reloadData {
    for (MSDReuseQueue *queue in [self.reuseQueues objectEnumerator]) {
        for (UIView *view in queue.objectsInUse) {
            if (view != self.extractedCell) {
                [self dispatchCellDidDisappear:view];
                [view removeFromSuperview];
                [queue makeObjectReusable:view];
            }
        }
    }
    self.visibleIndexes = [NSIndexSet indexSet];
    [self setNeedsLayout];
}

- (void)deleteItemAtIndex:(NSUInteger)index animated:(BOOL)animated {

    NSMutableIndexSet *newVisibleIndexes = [self.visibleIndexes mutableCopy];
    
    for (MSDReuseQueue *queue in [self.reuseQueues objectEnumerator]) {
        
        NSMutableDictionary *movingViews = [NSMutableDictionary dictionary];

        for (NSNumber *itemKey in queue.keysInUse) {
            if (index == [itemKey intValue]) {
                
                UIView *view = [queue objectInUseForKey:itemKey];
                [self dispatchCellDidDisappear:view];
                [view removeFromSuperview];
                [newVisibleIndexes removeIndex:[itemKey intValue]];
                [queue makeObjectReusable:view];
                
            } else if (index < [itemKey intValue]) {
                
                UIView *view = [queue objectInUseForKey:itemKey];
                [queue removeObjectForKey:itemKey];
                [newVisibleIndexes removeIndex:[itemKey intValue]];
                
                NSUInteger newKeyIndex = [itemKey intValue] - 1;
                movingViews[[NSNumber numberWithUnsignedInteger:newKeyIndex]] = view;
            }
        }

        for (NSNumber *key in movingViews) {
            [queue useObject:movingViews[key] forKey:key];
            [newVisibleIndexes addIndex:[key intValue]];
        }
    }
    self.visibleIndexes = newVisibleIndexes;
    [self setNeedsLayout];
    
    if (animated) {
        _animateChanges = YES;
        [self layoutIfNeeded];
        _animateChanges = NO;
    }
}

#pragma mark - Delegate management

- (void)dispatchCellDidDisappear:(UIView *)view {
    if ([self.delegate respondsToSelector:@selector(tilingView:cellDidDisappear:)]) {
        [self.delegate tilingView:self cellDidDisappear:view];
    }
}

#pragma mark - Animation/Modification

- (NSUInteger)itemIndexAtPoint:(CGPoint)point {
    NSIndexSet *indexSet = [self.layout indexesForItemsInRect:(CGRect){point, CGSizeZero}
                                                 viewportSize:self.bounds.size];
    return [indexSet firstIndex];
}

- (void)scrollItemIndexToVisible:(NSUInteger)itemIndex animated:(BOOL)animated {
    // Start by laying outselves out so that we're the right size and have the
    // correct content size for the rest of this method. We postpone actually laying
    // out any views to avoid displaying the first item before scrolling to the
    // Nth item.
    //
    // TODO: Instead, set a target index and read that in layoutSubviews
    
    _postponeTiling = YES;
    [self layoutIfNeeded];
    _postponeTiling = NO;
    
    [self setNeedsLayout];
    
    CGRect visibleRect = [self.layout rectForItemAtIndex:itemIndex
                                            viewportSize:self.bounds.size];
    
    // Bring this rect to the middle
    CGPoint desiredCenter = CGPointMake(visibleRect.origin.x + visibleRect.size.width/2,
                                        visibleRect.origin.y + visibleRect.size.height/2);
    
    CGPoint desiredContentOffset = CGPointMake(desiredCenter.x - self.bounds.size.width / 2,
                                               desiredCenter.y - self.bounds.size.height / 2);
    if (self.pagingEnabled) {
        desiredContentOffset.x = roundf(desiredContentOffset.x / self.bounds.size.width) * self.bounds.size.width;
        desiredContentOffset.y = roundf(desiredContentOffset.y / self.bounds.size.height) * self.bounds.size.height;
    } else {
        desiredContentOffset.x = roundf(desiredContentOffset.x);
        desiredContentOffset.y = roundf(desiredContentOffset.y);
    }
    
    // Clamp the desired content offset so we stay in the scrollable area
    desiredContentOffset.x = MAX(0, MIN(self.contentSize.width - self.bounds.size.width, desiredContentOffset.x));
    desiredContentOffset.y = MAX(0, MIN(self.contentSize.height - self.bounds.size.height, desiredContentOffset.y));
    
    [self setContentOffset:desiredContentOffset animated:animated];
}

- (UIView *)extractTilingViewCellForItemIndex:(NSUInteger)itemIndex {
    MSDCheck(!self.extractedCell, @"Can't extract a view while you still have one an extracted view.");
    UIView *cell = [self tilingViewCellForItemIndex:@(itemIndex) reuseIdentifierOrNil:nil];
    self.extractedCell = cell;
    return cell;
}

- (void)finishExtract {
    MSDCheck(self.extractedCell, @"No extracted cell");
    self.extractedCell = nil;
    [self setNeedsLayout];
}

- (NSArray *)tilingViewCellsInUse {
    NSMutableArray *objectsInUse = [NSMutableArray array];
    for(MSDReuseQueue *queue in [self.reuseQueues objectEnumerator]) {
        [objectsInUse addObjectsFromArray:[queue objectsInUse]];
    }
    return objectsInUse;
}

- (nullable UIView *)tilingViewCellForItemIndex:(NSNumber *)index reuseIdentifierOrNil:(NSString ** __nullable)reuseIdentifierOrNil {
    UIView *cell = nil;
    for (NSString *reuseId in [self.reuseQueues keyEnumerator]) {
        cell = [(self.reuseQueues)[reuseId] objectInUseForKey:index];
        if (cell) {
            if (reuseIdentifierOrNil) {
                *reuseIdentifierOrNil = reuseId;
            }
            return cell;
        }
    }
    return nil;
}

- (UIView *)tilingViewCellForItemIndex:(NSUInteger)itemIndex {
    return [self tilingViewCellForItemIndex:@(itemIndex) reuseIdentifierOrNil:nil];
}

- (NSString *)reuseIdentifierForItemIndex:(NSUInteger)itemIndex {
    NSString *reuseIdentifier;
    
    [self tilingViewCellForItemIndex:@(itemIndex) reuseIdentifierOrNil:&reuseIdentifier];
    return reuseIdentifier;
}

- (NSUInteger)indexForTilingViewCell:(UIView *)cell {
    NSNumber *itemKey = nil;
    
    for(MSDReuseQueue *queue in [self.reuseQueues objectEnumerator]) {
        itemKey = [queue keyUsedForObject:cell];
        if (itemKey) {
            return [itemKey unsignedIntValue];
        }
    }
    
    return NSNotFound;
}

- (CGRect)frameForTilingViewCell:(UIView *)cell {
    NSUInteger itemIndex = [self indexForTilingViewCell:cell];
    if (itemIndex == NSNotFound) {
        return CGRectNull;
    }
    return [self frameForTilingViewCell:cell atIndex:itemIndex];
}

#pragma mark -
#pragma mark Memory Management

- (void)handleMemoryWarning:(NSNotification *)notification {
    for(NSString *reuseIdentifier in [self.reuseQueues keyEnumerator]) {
        while ([self dequeueReusableTilingViewCellWithReuseIdentifier:reuseIdentifier]);
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

@end

