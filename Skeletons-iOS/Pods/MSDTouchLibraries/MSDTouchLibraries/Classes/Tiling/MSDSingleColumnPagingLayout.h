//
//  MSDSingleColumnPagingLayout.h
//  MSDCatalog
//
//  Created by Bill Wilson on 12-02-29.
//  Copyright (c) 2012 MindSea Development Inc. All rights reserved.
//

#import "MSDTilingViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDSingleColumnPagingLayout : MSDTilingViewLayout

- (instancetype)initWithTopMargin:(CGFloat)topMargin
                     bottomMargin:(CGFloat)bottomMargin NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
