//
//  MSDVerticalGridLayout.m
//
//  Created by Jesse Rusak on 10-10-20.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDVerticalGridLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDVerticalGridLayout()

@property (nonatomic, assign) UIEdgeInsets internalPadding;
@property (nonatomic, assign) CGFloat goalWidth;
@property (nonatomic, assign) CGSize itemSize;

@end


@implementation MSDVerticalGridLayout

- (instancetype)initWithItemSize:(CGSize)size minimumPadding:(UIEdgeInsets)padding {
    if (self = [super init]) {
        self.itemSize = size;
        self.internalPadding = padding;
        
    }
    return self;
}

- (instancetype)initWithPadding:(UIEdgeInsets)thePadding approximateColumnWidth:(CGFloat)theGoalWidth {
    if ((self = [super init])) {
        self.internalPadding = thePadding;
        self.goalWidth = theGoalWidth;
    }
    return self;
}

- (NSUInteger)columnCountForViewportSize:(CGSize)viewportSize {
    if (self.itemSize.width) {
        return viewportSize.width / (self.itemSize.width + self.internalPadding.left + self.internalPadding.right);
    }
    return roundf(viewportSize.width / self.goalWidth);
}

- (CGFloat)columnWidthForViewportSize:(CGSize)viewportSize {
    return floorf(viewportSize.width / [self columnCountForViewportSize:viewportSize]);
}

- (CGFloat)rowHeightForViewportSize:(CGSize)viewportSize {
    if (self.itemSize.height) {
        return self.itemSize.height + self.internalPadding.top + self.internalPadding.bottom;
    }
    return [self columnWidthForViewportSize:viewportSize];
}

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize {
    return CGSizeMake(viewportSize.width,
                      ([self rowHeightForViewportSize:viewportSize] 
                       * ceilf((float)itemCount / [self columnCountForViewportSize:viewportSize])));
}

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize {    
    CGFloat xOffset = rect.origin.x;
    CGFloat yOffset = rect.origin.y;
    
    CGFloat columnOrigin = xOffset / [self columnWidthForViewportSize:viewportSize];
    CGFloat rowOrigin = yOffset / [self rowHeightForViewportSize:viewportSize];
    
    CGFloat columnSpan = rect.size.width / [self columnWidthForViewportSize:viewportSize];
    CGFloat rowSpan = rect.size.height / [self rowHeightForViewportSize:viewportSize];
    
    NSInteger firstIndex = floorf(rowOrigin) * [self columnCountForViewportSize:viewportSize] + floorf(columnOrigin);
    firstIndex = MAX(0, firstIndex);

    NSInteger lastIndex = ceilf(rowOrigin + rowSpan) * [self columnCountForViewportSize:viewportSize] + ceilf(columnOrigin + columnSpan);
    lastIndex = MAX(0, lastIndex);

    return [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(firstIndex, lastIndex - firstIndex)];
}

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize {
    NSUInteger column = index % [self columnCountForViewportSize:viewportSize];
    NSUInteger row = index / [self columnCountForViewportSize:viewportSize];
    
    CGRect fullRect = CGRectMake(column * [self columnWidthForViewportSize:viewportSize],
                                 row * [self rowHeightForViewportSize:viewportSize],
                                 [self columnWidthForViewportSize:viewportSize],
                                 [self rowHeightForViewportSize:viewportSize]);
    
    if (self.itemSize.width) {
        // Fixed size items: we already have the padding factored into the column/row sizes,
        // so we just center in the already-existing space
        fullRect.origin.x += (fullRect.size.width - self.itemSize.width) / 2;
        fullRect.size.width = self.itemSize.width;

        fullRect.origin.y += (fullRect.size.height - self.itemSize.height) / 2;
        fullRect.size.height = self.itemSize.height;
    } else {
        // Non-fixed size items; we need to apply the padding
        // For backwards compatibility, we don't include padding on outer edges
        // if the user didn't specify an exact item size
        UIEdgeInsets insetsToApply = self.internalPadding;

        if (column == 0) {
            insetsToApply.left = 0;
        }
        if (column == [self columnCountForViewportSize:viewportSize] - 1) {
            insetsToApply.right = 0;
        }
        if (row == 0) {
            insetsToApply.top = 0;
        }
        fullRect = CGRectIntegral(UIEdgeInsetsInsetRect(fullRect, insetsToApply));
    }        

    return fullRect;
}

@end

NS_ASSUME_NONNULL_END
