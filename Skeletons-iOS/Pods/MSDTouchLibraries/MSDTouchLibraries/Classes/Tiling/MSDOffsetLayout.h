//
//  MSDOffsetLayout.h
//
//  Created by Jesse Rusak on 10-12-17.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDTilingViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDOffsetLayout : MSDTilingViewLayout

- (instancetype)initWithChildLayout:(MSDTilingViewLayout *)layout offset:(CGPoint)offset NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
