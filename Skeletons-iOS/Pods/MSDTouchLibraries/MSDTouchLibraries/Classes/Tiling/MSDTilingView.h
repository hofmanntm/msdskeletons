//
//  MSDTilingView.h
//
//  Created by Jesse Rusak on 10-11-24.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MSDTilingViewLayout;
@class MSDTilingView;

NS_ASSUME_NONNULL_BEGIN

@protocol MSDTilingViewDelegate<UIScrollViewDelegate>
@required

- (NSUInteger)numberOfItemsInTilingView:(MSDTilingView *)tilingView;
- (UIView *)tilingView:(MSDTilingView *)tilingView 
tilingViewCellForIndex:(NSUInteger)index;

@optional

- (NSString *)tilingView:(MSDTilingView *)tilingView reuseIdentifierForCellAtIndex:(NSUInteger)index;

- (void)tilingView:(MSDTilingView *)tilingView cellWillAppear:(UIView *)cell;
- (void)tilingView:(MSDTilingView *)tilingView cellDidDisappear:(UIView *)cell;
- (void)tilingViewDidLayoutSubviews:(MSDTilingView *)tilingView;

@end

typedef NS_ENUM(NSInteger, MSDTilingViewContentMode) {
    MSDTilingViewContentModeScaleToFill,
    MSDTilingViewContentModeAspectFit,  // There's no aspect fill because there's no outer view to cut off the extra

    MSDTilingViewContentModeCenter,

    MSDTilingViewContentModeTop,
    MSDTilingViewContentModeBottom,
    MSDTilingViewContentModeLeft,
    MSDTilingViewContentModeRight,
    
    MSDTilingViewContentModeTopLeft,
    MSDTilingViewContentModeTopRight,
    MSDTilingViewContentModeBottomLeft,
    MSDTilingViewContentModeBottomRight,
};

@interface MSDTilingView : UIScrollView

// Delegate
@property (nonatomic, weak) IBOutlet id<MSDTilingViewDelegate> delegate;

// Layout
@property (nonatomic, strong, nullable) id<MSDTilingViewLayout> layout;
- (void)setLayout:(id<MSDTilingViewLayout>)layout animated:(BOOL)animated;

/*!
 * Set to describe how to size & position the cell views inside their
 * layout rects. Defaults to MSDTilingViewContentModeScaleToFill.
 *
 * The layout code first asks the view what size it would
 * like to be with sizeThatFits:, and then positions within the 
 * layout rect according to the mode set.
 */
@property (nonatomic, assign) MSDTilingViewContentMode cellContentMode;

/*!
 * When YES, the tiling view preloads some items on both sides of the 
 * current visible area. Defaults to YES.
 */
@property (nonatomic, assign) BOOL preloadItems;

/*!
 * When NO, the tiling view will add cells at the lowest level in the view hierarchy. 
 * When YES, the tiling view adds cells to the top of the subview hiearchy. This is useful to set if you
 * have another view in the scroll view that you would like to scroll in the background.
 * Defaults to NO.
 */
@property (nonatomic, assign) BOOL placeCellsOnTop;

// Data Modification
- (void)reloadData;

// Cell Reuse
- (nullable UIView *)dequeueReusableTilingViewCellWithReuseIdentifier:(NSString *)reuseIdentifier;
- (nullable UIView *)dequeueReusableTilingViewCell;

// Animation/Modification
- (CGRect)frameForTilingViewCell:(UIView *)cell;
- (NSUInteger)indexForTilingViewCell:(UIView *)cell;

- (void)scrollItemIndexToVisible:(NSUInteger)index animated:(BOOL)animated;

- (NSUInteger)itemIndexAtPoint:(CGPoint)point;
- (UIView *)extractTilingViewCellForItemIndex:(NSUInteger)itemIndex;
- (void)finishExtract;
- (UIView *)tilingViewCellForItemIndex:(NSUInteger)itemIndex;
- (NSString *)reuseIdentifierForItemIndex:(NSUInteger)itemIndex;
- (NSArray *)tilingViewCellsInUse;
- (void)deleteItemAtIndex:(NSUInteger)index animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
