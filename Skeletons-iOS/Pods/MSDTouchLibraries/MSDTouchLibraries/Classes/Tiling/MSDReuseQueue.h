//
//  MSDReuseQueue.h
//
//  Created by Jesse Rusak on 10-10-20.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MSDReuseQueue : NSObject

- (void)makeObjectReusable:(id)object;
- (void)useObject:(id)object forKey:(id)key;
- (void)removeObjectForKey:(id)key;

- (id)objectInUseForKey:(id)key;
- (id)keyUsedForObject:(id)object;
- (nullable id)dequeueReusableObject;

@property (weak, nonatomic, readonly) NSArray *keysInUse;
@property (weak, nonatomic, readonly) NSArray *objectsInUse;
@property (weak, nonatomic, readonly) NSArray *reusableObjects;

@end

NS_ASSUME_NONNULL_END
