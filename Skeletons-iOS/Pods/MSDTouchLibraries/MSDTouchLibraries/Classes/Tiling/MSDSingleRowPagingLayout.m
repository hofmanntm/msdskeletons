//
//  MSDSingleRowPagingLayout.m
//
//  Created by Jesse Rusak on 10-11-29.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDSingleRowPagingLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDSingleRowPagingLayout()

@property (nonatomic, assign) CGFloat leftMargin;
@property (nonatomic, assign) CGFloat rightMargin;

@end



@implementation MSDSingleRowPagingLayout

- (instancetype)initWithLeftMargin:(CGFloat)theLeftMargin
                       rightMargin:(CGFloat)theRightMargin {
    if ((self = [super init])) {
        self.leftMargin = theLeftMargin;
        self.rightMargin = theRightMargin;
    }   
    return self;
}

- (CGFloat)itemWidthForViewportSize:(CGSize)viewportSize {
    return viewportSize.width - self.leftMargin - self.rightMargin;
}

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize {
    NSUInteger firstIndex = MAX(0, rect.origin.x / viewportSize.width);
    NSUInteger lastIndex = ceil(MAX(0, (rect.origin.x + rect.size.width) / viewportSize.width));
    
    return [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(firstIndex, lastIndex-firstIndex)];
}

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize {
    CGFloat itemWidth = [self itemWidthForViewportSize:viewportSize];
    CGFloat x = viewportSize.width * index + self.leftMargin;
    
    return CGRectMake(x, 
                      0,
                      itemWidth,
                      viewportSize.height);
}

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize {
    return CGSizeMake(viewportSize.width * itemCount,
                      viewportSize.height);
}

@end

NS_ASSUME_NONNULL_END
