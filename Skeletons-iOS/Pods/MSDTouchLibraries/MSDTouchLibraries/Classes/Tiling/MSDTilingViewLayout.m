//
//  MSDTilingViewLayout.m
//
//  Created by Jesse Rusak on 10-11-29.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDTilingViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@implementation MSDTilingViewLayout

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize {
    [NSException raise:NSInternalInconsistencyException format:@"Should be implemented in subclass"];
    return nil;
}

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize {
    [NSException raise:NSInternalInconsistencyException format:@"Should be implemented in subclass"];
    return CGRectNull;
}

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize {
    [NSException raise:NSInternalInconsistencyException format:@"Should be implemented in subclass"];   
    return CGSizeZero;
}

- (NSUInteger)indexBestMatchingRect:(CGRect)queryRect viewportSize:(CGSize)viewportSize {
    // We find all items in this rect, then find the item whose rect's center is the closest
    
    NSIndexSet *indexes = [self indexesForItemsInRect:queryRect viewportSize:viewportSize];
    CGPoint queryCenter = CGPointMake(queryRect.origin.x + queryRect.size.width / 2,
                                      queryRect.origin.y + queryRect.size.height / 2);
    
    CGFloat bestDistanceSquared = CGFLOAT_MAX;
    NSUInteger bestIndex = NSNotFound;
    for (NSUInteger index = [indexes firstIndex];
         index != NSNotFound;
         index = [indexes indexGreaterThanIndex:index]) {
        CGRect itemRect = [self rectForItemAtIndex:index viewportSize:viewportSize];
        CGPoint center = CGPointMake(itemRect.origin.x + itemRect.size.width / 2,
                                     itemRect.origin.y + itemRect.size.height / 2);
        
        CGFloat dx = center.x - queryCenter.x;
        CGFloat dy = center.y - queryCenter.y;
        CGFloat distanceSquared = dx * dx + dy * dy;
        
        if (distanceSquared < bestDistanceSquared) {
            bestDistanceSquared = distanceSquared;
            bestIndex = index;
        }
    }
    return bestIndex;
}

@end

NS_ASSUME_NONNULL_END
