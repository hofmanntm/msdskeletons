//
//  MSDHorizontalFlexibleLayout.h
//
//  Created by Jesse Rusak on 10-10-24.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDTilingViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDHorizontalFlexibleLayout : MSDTilingViewLayout

// The padding passed goes /between/ the items and does not take up any of itemSize.
// (That is, changing padding doesn't change the size of the resulting item rects.)
// The padding is relative to the item size, so a padding width 0.5 means "half item width"
// - avoidsPartialItems means that it will adjust horizontal padding to ensure
// only a whole number of images fits in a viewport
// - matchViewportAspect means that it will resize the grid if the viewport
// resizes (otherwise, it leaves them the same size and shows more/less of them.)

- (instancetype)initWithPadding:(UIEdgeInsets)padding
                    forRowCount:(NSUInteger)rowCount
             avoidsPartialItems:(BOOL)avoidsPartialItems;

- (instancetype)initWithPadding:(UIEdgeInsets)padding
                 forItemsOfSize:(CGSize)itemSize
                 inViewportSize:(CGSize)viewportSize
            matchViewportAspect:(BOOL)matchViewportAspect
             avoidsPartialItems:(BOOL)avoidsPartialItems NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NSUInteger rowCount;

@end

NS_ASSUME_NONNULL_END
