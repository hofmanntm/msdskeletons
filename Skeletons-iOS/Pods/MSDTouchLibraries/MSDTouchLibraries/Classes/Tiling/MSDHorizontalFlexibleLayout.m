//
//  MSDHorizontalFlexibleLayout.m
//
//  Created by Jesse Rusak on 10-10-24.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDHorizontalFlexibleLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDHorizontalFlexibleLayout()

// These are scaled such that the viewport is 1.0 in height
@property (nonatomic, assign) CGSize scaledItemSize;
@property (nonatomic, assign) UIEdgeInsets baseScaledPadding;

@property (nonatomic, assign) BOOL matchViewportAspect;
@property (nonatomic, assign) BOOL avoidsPartialItems;

- (CGFloat)scaledColumnWidthForViewportSize:(CGSize)viewportSize;
- (CGFloat)scaledRowHeight;

- (NSUInteger)rowCount;

- (CGFloat)widthScaleForViewportSize:(CGSize)viewportSize;
- (CGFloat)heightScaleForViewportSize:(CGSize)viewportSize;

- (UIEdgeInsets)scaledPaddingForViewportSize:(CGSize)viewportSize;

@end


@implementation MSDHorizontalFlexibleLayout

- (instancetype)initWithPadding:(UIEdgeInsets)padding
                 forItemsOfSize:(CGSize)itemSize
                 inViewportSize:(CGSize)viewportSize
            matchViewportAspect:(BOOL)theMatchViewportAspect
             avoidsPartialItems:(BOOL)theAvoidsPartialItems {
    if ((self = [super init])) {
        self.matchViewportAspect = theMatchViewportAspect;
        
        _scaledItemSize = CGSizeMake(itemSize.width / [self widthScaleForViewportSize:viewportSize],
                                     itemSize.height / [self heightScaleForViewportSize:viewportSize]);
        
        _baseScaledPadding = UIEdgeInsetsMake(padding.top * _scaledItemSize.height,
                                              padding.left * _scaledItemSize.width,
                                              padding.bottom * _scaledItemSize.height,
                                              padding.right * _scaledItemSize.width);
        
        // set vertical padding to be larger to make total row size + bottom margin = 1.0
        CGFloat totalHeight = [self scaledRowHeight] * [self rowCount];
        CGFloat extraSpace = 1.0 - totalHeight;
        _baseScaledPadding.top += (extraSpace / ([self rowCount] + 1)); // +1 to spread space to bottom margin, too
        
        _avoidsPartialItems = theAvoidsPartialItems;
    }
    return self;
}

- (instancetype)initWithPadding:(UIEdgeInsets)padding
                    forRowCount:(NSUInteger)rowCount
             avoidsPartialItems:(BOOL)theAvoidsPartialItems {
    
    // Since Total height = 1
    // and Total height = rowCount * (itemSize + (itemSize * padding))
    // -> total height = rowCount * itemSize + (1 + padding)
    // -> 1 = rowCount * itemSize * (1 + padding)
    // -> itemSize = 1 / (rowCount * (1 + padding))
    CGFloat itemHeight = 1.0/(rowCount * (1 + padding.top + padding.bottom));
    
    return [self initWithPadding:padding
                  forItemsOfSize:CGSizeMake(itemHeight, itemHeight)
                  inViewportSize:CGSizeMake(1.0, 1.0)
             matchViewportAspect:NO
             avoidsPartialItems:theAvoidsPartialItems];
}

- (CGFloat)widthScaleForViewportSize:(CGSize)viewportSize {
    if (self.matchViewportAspect) {
        return viewportSize.width;
    } else {
        return viewportSize.height;
    }

}

- (CGFloat)heightScaleForViewportSize:(CGSize)viewportSize {
    return viewportSize.height;
}

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize {
    
    CGFloat widthScale = [self widthScaleForViewportSize:viewportSize];
    CGFloat heightScale = [self heightScaleForViewportSize:viewportSize];
    
    CGRect scaledRect = CGRectMake(rect.origin.x / widthScale,
                                   rect.origin.y / heightScale,
                                   rect.size.width / widthScale,
                                   rect.size.height / heightScale);
    
    if (self.avoidsPartialItems) {
        // if we're avoiding partial images, we bring in our w & h by half a grid
        // spacing so we don't slip into the next row/column by a small amount
        scaledRect.size.width -= [self scaledColumnWidthForViewportSize:viewportSize]/2;
        scaledRect.size.height -= [self scaledRowHeight]/2;
    }
    
    NSInteger firstColumn = floorf(scaledRect.origin.x / [self scaledColumnWidthForViewportSize:viewportSize]);
    NSInteger firstRow = floorf(scaledRect.origin.y / [self scaledRowHeight]);
    
    NSInteger lastColumn = floorf((scaledRect.origin.x + scaledRect.size.width) / [self scaledColumnWidthForViewportSize:viewportSize]);
    NSInteger lastRow = floorf((scaledRect.origin.y + scaledRect.size.height) / [self scaledRowHeight]);
    
    NSInteger firstIndex = firstRow + firstColumn * [self rowCount];
    firstIndex = MAX(0, firstIndex);
    
    NSInteger lastIndex = lastRow + lastColumn * [self rowCount];
    lastIndex = MAX(0, lastIndex);
    return [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(firstIndex, lastIndex - firstIndex + 1)]; // + 1 because we include both first and last
}

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize {

    CGFloat widthScale = [self widthScaleForViewportSize:viewportSize];
    CGFloat heightScale = [self heightScaleForViewportSize:viewportSize];

    NSUInteger columnIndex = index / [self rowCount];
    NSUInteger rowIndex = index % [self rowCount];
    
    CGFloat x = columnIndex * [self scaledColumnWidthForViewportSize:viewportSize] + [self scaledPaddingForViewportSize:viewportSize].left;
    CGFloat y = rowIndex * [self scaledRowHeight] + [self scaledPaddingForViewportSize:viewportSize].top;
    
    return CGRectMake(x * widthScale,
                      y * heightScale,
                      self.scaledItemSize.width * widthScale,
                      self.scaledItemSize.height * heightScale);
}


- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize {
    CGFloat widthScale = [self widthScaleForViewportSize:viewportSize];
    CGFloat heightScale = [self heightScaleForViewportSize:viewportSize];

    NSUInteger numColumns = ceilf((float)itemCount / [self rowCount]);
    
    return CGSizeMake(numColumns * [self scaledColumnWidthForViewportSize:viewportSize] * widthScale,
                      [self rowCount] * [self scaledRowHeight] * heightScale);
}

- (NSUInteger)rowCount {
    NSUInteger rowCount = floorf(1.0f / [self scaledRowHeight]);
    if (rowCount == 0) {
        rowCount = 1;
    }
    return rowCount;
}

- (UIEdgeInsets)scaledPaddingForViewportSize:(CGSize)viewportSize {
    UIEdgeInsets padding = self.baseScaledPadding;
    if (self.avoidsPartialItems) {
        CGFloat totalHorizontalSpace = viewportSize.width;
        CGFloat widthScale = [self widthScaleForViewportSize:viewportSize]; 
        CGFloat itemWidth = widthScale * [self scaledItemSize].width;
        CGFloat spacePerImage = itemWidth + widthScale * (self.baseScaledPadding.left + self.baseScaledPadding.right);
        NSInteger imagesAfterFirst = (totalHorizontalSpace - itemWidth) / spacePerImage;
        if (imagesAfterFirst > 0) {
            CGFloat extraSpace = totalHorizontalSpace - (itemWidth + imagesAfterFirst * spacePerImage);
            padding.right += (extraSpace / imagesAfterFirst) / widthScale;
        }
    }
    return padding;
}

- (CGFloat)scaledVerticalPadding {
    return self.baseScaledPadding.top + self.baseScaledPadding.bottom;
}

- (CGFloat)scaledHorizontalPaddingForViewportSize:(CGSize)viewportSize {
    UIEdgeInsets padding = [self scaledPaddingForViewportSize:viewportSize];
    return padding.left + padding.right;
}

- (CGFloat)scaledColumnWidthForViewportSize:(CGSize)viewportSize {
    return self.scaledItemSize.width + [self scaledHorizontalPaddingForViewportSize:viewportSize];
}

- (CGFloat)scaledRowHeight {
    return self.scaledItemSize.height + [self scaledVerticalPadding];
}

@end

NS_ASSUME_NONNULL_END
