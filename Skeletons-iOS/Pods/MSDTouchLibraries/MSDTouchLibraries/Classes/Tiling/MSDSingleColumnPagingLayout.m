//
//  MSDSingleColumnPagingLayout.m
//  MSDCatalog
//
//  Created by Bill Wilson on 12-02-29.
//  Copyright (c) 2012 MindSea Development Inc. All rights reserved.
//

#import "MSDSingleColumnPagingLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDSingleColumnPagingLayout()

@property (nonatomic, assign) CGFloat topMargin;
@property (nonatomic, assign) CGFloat bottomMargin;

@end


@implementation MSDSingleColumnPagingLayout

- (instancetype)initWithTopMargin:(CGFloat)theTopMargin
                     bottomMargin:(CGFloat)theBottomMargin {
    if (self = [super init]) {
        self.topMargin = theTopMargin;
        self.bottomMargin = theBottomMargin;
    }
    return self;
}

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize {
    NSUInteger firstIndex = MAX(0, rect.origin.y / viewportSize.height);
    NSUInteger lastIndex = ceil(MAX(0, (rect.origin.y + rect.size.height) / viewportSize.height));
    
    return [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(firstIndex, lastIndex-firstIndex)];

}

- (CGFloat)itemHeightForViewportSize:(CGSize)viewportSize {
    return viewportSize.height - self.topMargin - self.bottomMargin;
}

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize {

    CGFloat itemHeight = [self itemHeightForViewportSize:viewportSize];
    CGFloat y = viewportSize.height * index + self.topMargin;
    
    return CGRectMake(0, 
                      y,
                      viewportSize.width,
                      itemHeight);
}

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize {

    return CGSizeMake(viewportSize.width,
                      viewportSize.height * itemCount);
}

@end

NS_ASSUME_NONNULL_END
