//
//  MSDSingleRowPagingLayout.h
//
//  Created by Jesse Rusak on 10-11-29.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSDTilingViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDSingleRowPagingLayout : MSDTilingViewLayout

- (instancetype)initWithLeftMargin:(CGFloat)leftMargin
                       rightMargin:(CGFloat)rightMargin NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
