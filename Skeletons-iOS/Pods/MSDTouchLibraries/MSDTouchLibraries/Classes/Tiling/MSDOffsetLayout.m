//
//  MSDOffsetLayout.m
//
//  Created by Jesse Rusak on 10-12-17.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDOffsetLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDOffsetLayout()

@property (nonatomic, strong) MSDTilingViewLayout *childLayout;
@property (nonatomic, assign) CGPoint offset;

@end


@implementation MSDOffsetLayout

- (instancetype)initWithChildLayout:(MSDTilingViewLayout *)theLayout offset:(CGPoint)theOffset {
    if ((self = [super init])) {
        self.childLayout = theLayout;
        self.offset = theOffset;
    }
    return self;
}

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize {
    rect.origin.x -= self.offset.x;
    rect.origin.y -= self.offset.y;
    return [self.childLayout indexesForItemsInRect:rect viewportSize:viewportSize];
}

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize {
    CGRect itemRect = [self.childLayout rectForItemAtIndex:index viewportSize:viewportSize];
    itemRect.origin.x += self.offset.x;
    itemRect.origin.y += self.offset.y;
    
    return itemRect;
}

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize {
    CGSize size = [self.childLayout sizeForItemCount:itemCount viewportSize:viewportSize];
    size.width += self.offset.x;
    size.height += self.offset.y;
    return size;
}


@end

NS_ASSUME_NONNULL_END
