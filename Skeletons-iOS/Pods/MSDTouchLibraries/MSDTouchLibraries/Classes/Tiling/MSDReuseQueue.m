//
//  MSDReuseQueue.m
//
//  Created by Jesse Rusak on 10-10-20.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import "MSDReuseQueue.h"


@interface MSDReuseQueue()

@property (nonatomic, strong) NSMutableArray *mutableReusableObjects;

@end


@implementation MSDReuseQueue {
    CFMutableDictionaryRef _keysToObjects;
    CFMutableDictionaryRef _objectsToKeys;
}

- (instancetype)init {
    if ((self = [super init])) {
        self.mutableReusableObjects = [NSMutableArray array];

        // We avoid copying keys or objects, because they could be anything
        // We maintain mappings in both directions because allKeysForValue: is slow
        _keysToObjects = CFDictionaryCreateMutable(NULL,
                                                   0,
                                                   &kCFTypeDictionaryKeyCallBacks,
                                                   &kCFTypeDictionaryValueCallBacks);
        
        _objectsToKeys = CFDictionaryCreateMutable(NULL,
                                                   0,
                                                   &kCFTypeDictionaryKeyCallBacks,
                                                   &kCFTypeDictionaryValueCallBacks);
    }
    return self;
}

- (void)removeObjectForKey:(id)key {
    id object = (id)CFDictionaryGetValue(_keysToObjects, (__bridge const void *)(key));
    CFDictionaryRemoveValue(_keysToObjects, (__bridge const void *)(key));
    if (object) {
        CFDictionaryRemoveValue(_objectsToKeys, (__bridge const void *)(object));
    }
}

- (void)makeObjectReusable:(id)object {
    [self.mutableReusableObjects addObject:object];
    
    id key = (id)CFDictionaryGetValue(_objectsToKeys, (__bridge const void *)(object));
    if (key) {
        CFDictionaryRemoveValue(_keysToObjects, (__bridge const void *)(key));
    }
    CFDictionaryRemoveValue(_objectsToKeys, (__bridge const void *)(object));
}

- (void)useObject:(id)object forKey:(id)key {
    CFDictionarySetValue(_keysToObjects, (__bridge const void *)(key), (__bridge const void *)(object));
    CFDictionarySetValue(_objectsToKeys, (__bridge const void *)(object), (__bridge const void *)(key));
    
    [self.mutableReusableObjects removeObjectIdenticalTo:object];
}

- (id)objectInUseForKey:(id)key {
    if (!key) {
        return nil;
    }
    return (id)CFDictionaryGetValue(_keysToObjects, (__bridge const void *)(key));
}

- (id)keyUsedForObject:(id)object {
    if (!object) {
        return nil;
    }
    return (id)CFDictionaryGetValue(_objectsToKeys, (__bridge const void *)(object));
}

- (nullable id)dequeueReusableObject {
    id object = nil;
    if ([self.mutableReusableObjects count] > 0) {
        object = [self.mutableReusableObjects lastObject];
        [self.mutableReusableObjects removeLastObject];
    }
    return object;
}

- (NSArray *)keysInUse {
    return [(__bridge NSDictionary *)_keysToObjects allKeys];
}

- (NSArray *)objectsInUse {
    return [(__bridge NSDictionary *)_keysToObjects allValues];
}

- (NSArray *)reusableObjects {
    return [self.mutableReusableObjects copy];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p; objectsInUse = %@; reusableObjects = %@>", [self class], self, (__bridge id)_keysToObjects, self.mutableReusableObjects];
}

- (void)dealloc {
    CFRelease(_keysToObjects);
    CFRelease(_objectsToKeys);
    
}

@end
