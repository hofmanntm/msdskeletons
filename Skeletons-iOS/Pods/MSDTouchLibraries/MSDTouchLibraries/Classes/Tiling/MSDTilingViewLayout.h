//
//  MSDTilingViewLayout.h
//
//  Created by Jesse Rusak on 10-11-29.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MSDTilingViewLayout <NSObject>

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize;

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize;

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize;

- (NSUInteger)indexBestMatchingRect:(CGRect)queryRect
                       viewportSize:(CGSize)viewportSize;

@end


@interface MSDTilingViewLayout : NSObject<MSDTilingViewLayout>

// Primitive methods all layouts must implement

- (NSIndexSet *)indexesForItemsInRect:(CGRect)rect 
                         viewportSize:(CGSize)viewportSize;

- (CGRect)rectForItemAtIndex:(NSUInteger)index 
                viewportSize:(CGSize)viewportSize;

- (CGSize)sizeForItemCount:(NSUInteger)itemCount 
              viewportSize:(CGSize)viewportSize;

// Helper methods
// Returns the index of an item that's closest to the passed rect
- (NSUInteger)indexBestMatchingRect:(CGRect)queryRect
                       viewportSize:(CGSize)viewportSize;

@end

NS_ASSUME_NONNULL_END
