//
//  MSDVerticalGridLayout.h
//
//  Created by Jesse Rusak on 10-10-20.
//  Copyright 2010 MindSea Development Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSDTilingViewLayout.h"

NS_ASSUME_NONNULL_BEGIN

@interface MSDVerticalGridLayout : MSDTilingViewLayout

// Creates a grid which has square cells that fit in the viewport in width
// using as close to the goal width as possible.
// Padding here is in /points/
- (instancetype)initWithPadding:(UIEdgeInsets)padding approximateColumnWidth:(CGFloat)goalWidth NS_DESIGNATED_INITIALIZER;


// Creates a grid with cells of the size requested, with as many columns as possible,
// spreading out any additional padding as needed.
- (instancetype)initWithItemSize:(CGSize)size minimumPadding:(UIEdgeInsets)padding NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
