//
//  InsistentZPositionLayer.swift
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2018-02-07.
//  Copyright © 2018 MindSea Development Inc. All rights reserved.
//

import QuartzCore

/**
 A layer that ignores calls to set `zPosition`, instead using another property `insistentZPosition`.

 This is useful for `MKAnnotationView` if you want to prevent the map from setting `zPosition` and wrecking your day. Suggested usage:

 class MyAnnotationView: MKAnnotationView {
 override class var layerClass: AnyClass {
 return InsistentZPositionLayer.self
 }

 var insistentLayer: InsistentZPositionLayer { return layer as! InsistentZPositionLayer }
 }

 // elsewhere…
 if let view = annotationView as? MyAnnotationView {
 view.insistentLayer.insistentZPosition = view.isSelected ? 1 : -1
 }

 Shout out to https://stackoverflow.com/a/47726329/1063051
 */
open class InsistentZPositionLayer: CALayer {

    /// Setter does nothing. To change `zPosition`, please set `insistentZPosition` instead.
    override public final var zPosition: CGFloat {
        get { return super.zPosition }
        set { /* nop */ }
    }

    /// Sets the layer's `zPosition`.
    var insistentZPosition: CGFloat {
        get { return zPosition }
        set { super.zPosition = newValue }
    }
}
