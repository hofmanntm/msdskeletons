//
//  MSDFeed+Swift.swift
//  MSDTouchLibraries
//
//  Created by Joel Glanfield on 2016-06-15.
//  Copyright © 2016 MindSea Development Inc. All rights reserved.
//

// delegatesRespondingToSelector() returns an NSFastEnumeration, whose contents aren't easily cast in Swift.
extension MSDFeed {
    public func delegates(respondingToSelector selector: Selector) -> FeedDelegateSequence {
        return FeedDelegateSequence(feed: self, selector: selector)
    }
}

/// A sequence of feed delegates responding to a particular selector.
public struct FeedDelegateSequence: Sequence {
    private let feed: MSDFeed
    private let selector: Selector
    
    fileprivate init(feed: MSDFeed, selector: Selector) {
        self.feed = feed
        self.selector = selector
    }
    
    public func makeIterator() -> AnyIterator<MSDFeedDelegate> {
        // This warning sucks, but will go away once we're all building with iOS 11 SDK.
        var fastGenerator = NSFastEnumerationIterator(feed.delegatesResponding(to: selector))
        return AnyIterator {
            return fastGenerator.next() as! MSDFeedDelegate?
        }
    }
}

