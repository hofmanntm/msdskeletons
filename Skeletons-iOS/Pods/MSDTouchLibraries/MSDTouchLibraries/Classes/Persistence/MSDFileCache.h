//
//  MSDFileCache.h
//  MSDTouchLibraries
//

#import <Foundation/Foundation.h>
#import "MSDCompatibility.h"
@class MSDFileCache;

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MSDFileCacheStorageType) {
    MSDFileCacheStorageTypeTemporary, // stored in tmp (eagerly deleted by iOS)
    MSDFileCacheStorageTypeCache,     // stored in Caches (deleted in low-disk-space conditions by iOS)
    MSDFileCacheStorageTypeOffline,   // stored in Library, marked do-not-backup (not deleted by iOS)
} NS_SWIFT_NAME(FileCacheStorageType);


extern NSString * const MSDFileCacheErrorDomain;
typedef NS_ERROR_ENUM(MSDFileCacheErrorDomain, MSDFileCacheError) {
    MSDFileCacheErrorNoBundle,         // asked to perform an operation requiring a bundle, but none was passed
    MSDFileCacheErrorNoSubdirectory,   // asked to perform an operation requiring a subdirectory, but none was passed
    MSDFileCacheErrorNoRemoteURL,      // can't fetch from remote URL because there's no delegate or the delegate returned nil
    MSDFileCacheErrorMultipleErrors,   // there were multiple errors (check the console log)
    MSDFileCacheErrorNoResource,       // asked to perform an operation requiring a bundle resource, but it wasn't found
    MSDFileCacheErrorFailedValidation, // file failed delegate validation after download
    MSDFileCacheErrorNoData,           // no data returned in response
    MSDFileCacheErrorAborted,          // request aborted
    MSDFileCacheErrorTruncated,        // response truncated (expected x bytes, got y < x instead)
    MSDFileCacheErrorHTTPErrorCode,    // response had an HTTP code >= 400
};


NS_SWIFT_NAME(FileCacheDelegate)
@protocol MSDFileCacheDelegate <NSObject>

@optional

/**
 * Return the remote URL for the given local filename.
 *
 * Defaults to nil, meaning there is no remote URL.
 * This is called from an arbitrary thread, and the result should
 * not change over time.
 */
- (nullable NSURL *)fileCache:(MSDFileCache *)cache remoteURLForFileNamed:(NSString *)filename;

/**
 * Return request parameters used to load the given file name.
 * This is used with the URL from fileCache:remoteURLForFileNamed:
 *
 */
- (NSDictionary *)fileCache:(MSDFileCache *)cache remoteRequestParametersForFileNamed:(NSString *)filename;

/**
 * Return request method used to load the given file name.
 * This is used with the URL from fileCache:remoteURLForFileNamed:
 *
 */
- (NSString *)fileCache:(MSDFileCache *)cache remoteRequestMethodForFileNamed:(NSString *)filename;

/**
 * Allows the delegate to perform some post-processing before the cache returns its data.
 * Return nil and fill in error if there is a problem.
 * Note that this might be called multiple times for a filename, since the cache will discard
 * and retry with local data if remote data causes an error.
 *
 * This will be called from an arbitrary thread, and its result might be cached, so this should
 * be idempotent and the returned value immutable.
 * 
 * Defaults to using the file's contents as NSData if unimplemented.
 */
- (nullable id)fileCache:(MSDFileCache *)cache contentsForFileNamed:(NSString *)filename fromPath:(NSString *)path error:(NSError **)error;

/**
 * Return YES if you would like the MSDFileCache to keep these contents in-memory. 
 * These objects will be dropped in response to memory pressure, etc.
 * 
 * This will be called from an arbitrary thread.
 *
 * Defaults to YES if unimplemented.
 */
- (BOOL)fileCache:(MSDFileCache *)cache shouldCacheContents:(id)contents forFileNamed:(NSString *)filename;

/**
 * Return YES if MSDFileCache should replace the contents of filename with the contents of
 * the file at path.  This is called to validate new data after downloading it.
 * 
 * This will be called from an arbitrary thread.
 *
 * If unimplemented, fileCache:contentsForFileNamed:data:error: will be called instead with
 * the new data (and the contents may be cached as per
 * fileCache:shouldCacheContents:forFileNamed:); if that method is also unimplemented, this 
 * method simply defaults to YES.
 */
- (BOOL)fileCache:(MSDFileCache *)cache shouldReplaceFileNamed:(NSString *)filename withContentsAtPath:(NSString *)path;

@end


/**
 * A cache of files, optionally backed by the resources in the bundle,
 * stored on the file system and potentially updated from remote URLs.
 * It is safe to call methods of this class from any thread.
 *
 * Why not just use the HTTP cache?
 *  - It doesn't default to bundle resources.
 *  - It won't return results immediately for cached results (often a small delay).
 *    This is awkward for images in scrolling lists, for example.
 *  - It doesn't provide access to the files on the filesystem.
 *    This is most useful for cached HTML files with associated resources.
 *  - It doesn't guarantee offline availability. 
 */
NS_SWIFT_NAME(FileCache)
@interface MSDFileCache : NSObject

/**
 * Returns a cache for files from your main bundle.
 * The name provides a unique identifier for this cache.
 * Defaults to MSDFileCacheStorageTypeCache.
 */
- (instancetype)initWithName:(NSString *)name;

/**
 * Returns a cache for files from the given sub-directory in this bundle.
 * The name provides a unique identifier for this cache.
 */
- (instancetype)initWithName:(NSString *)name
                      bundle:(nullable NSBundle *)bundle
                subdirectory:(nullable NSString *)directory
                 storageType:(MSDFileCacheStorageType)type NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

/**
 * The delegate; controls the behaviour of this file cache.
 * Methods on this delegate are not guaranteed to be called on any particular thread.
 */
@property (weak, nullable) id<MSDFileCacheDelegate> delegate;

#pragma mark - Immediate Methods

/**
 * Returns the contents for the given file, using the local version or bundle version.
 * This does not wait for async operations to complete.
 */
- (nullable id)contentsOfFileNamed:(NSString *)name error:(NSError **)error;

/**
 * Returns the in-cache path for this file.
 * This does not wait for async operations to complete.
 */
- (NSString *)pathForFileNamed:(NSString *)name;
- (NSURL *)URLForFileNamed:(NSString *)name;

/**
 * Removes the in-cache version for this file immediately.
 * (If this cache is bundle-backed, future calls to contentsOfFileNamed: will
 * return the bundle version.)
 */
- (BOOL)removeFileNamed:(NSString *)name error:(NSError **)error;

#pragma mark - Asynchronous Methods

/**
 * Returns the data for the given file asynchronously, 
 * calling your completion block on a private queue
 * when it is available. Will fetch from the remote URL first if possible.
 * Note that this can return data and an error (if we used a local version).
 */
- (void)loadContentsOfFileNamed:(NSString *)name
              completionHandler:(void(^ __nullable)(id __nullable contents, NSError * __nullable error))completionHandler;

/**
 * Updates the contents from the remote URL.
 */
- (void)forceUpdateOfFileNamed:(NSString *)name
             completionHandler:(void(^ __nullable)(NSHTTPURLResponse* response, NSError *error))completionHandler;

/**
 * This lets you add a resource into the cache if not already present.
 * Useful for associated files. The completion blocks runs on a private queue.
 */
- (void)addDefaultFileNamed:(NSString *)name
          overwriteExisting:(BOOL)overwrite
                       eTag:(nullable NSString *)eTagOrNil
                    eTagURL:(nullable NSURL *)eTagURLOrNil
          completionHandler:(void(^)(NSError * __nullable error))completionHandler;

/**
 * Same as 
 * -[addDefaultFileNamed:name
 *     overwriteExisting:overwrite 
 *                  eTag:nil
 *               eTagURL:nil
 *     completionHandler:completionHandler];
 */
- (void)addDefaultFileNamed:(NSString *)name
          overwriteExisting:(BOOL)overwrite
          completionHandler:(void(^)(NSError * __nullable error))completionHandler;

/**
 * Populates cache with all resources from the sub-directory
 * it was created with. The completion block runs on a private queue.
 */
- (void)addAllDefaultFilesOverwritingExisting:(BOOL)overwrite
                            completionHandler:(void(^)(NSError * __nullable error))completionHandler;

- (void)clearInMemoryCache;

@end

NS_ASSUME_NONNULL_END
