/*
     File: MSDKeychainItem.h
 Abstract: 
 Objective-C wrapper for accessing a single keychain item.
 
  Version: 1.2
 
 Originally from Apple Sample code. Modifications by MindSea Development.
 
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2010 Apple Inc. All Rights Reserved.
 
*/

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/*
    The MSDKeychainItem class is an abstraction layer for the iPhone Keychain communication. It is merely a 
    simple wrapper to provide a distinct barrier between all the idiosyncracies involved with the Keychain
    CF/NS container objects.
*/
NS_SWIFT_NAME(KeychainItem)
@interface MSDKeychainItem : NSObject

@property (nonatomic, strong) NSMutableDictionary *keychainItemData;
@property (nonatomic, strong) NSMutableDictionary *genericPasswordQuery;

/*! 
 * Designated initializer; fetches data from the keychain.
 * 
 * @param serviceIdentifier A string unique within this app for this piece of data.
 * (Stored as the kSecAttrService of this keychain item.)
 * @param accessGroup The shared access group if this item should be accessible from
 * multiple apps, otherwise this can be nil.
 */
- (instancetype)initWithIdentifier:(NSString *)serviceIdentifier accessGroup:(nullable NSString *)accessGroup NS_DESIGNATED_INITIALIZER;

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (void)setObject:(nullable id)inObject forKey:(id)key;
- (nullable id)objectForKey:(id)key;

// Some useful keys; they just access objectForKey: and setObject:forKey:
@property (nonatomic, copy, nullable) NSString *comment; // kSecAttrComment
@property (nonatomic, copy, nullable) NSString *description; //kSecAttrDescription
@property (nonatomic, copy, nullable) NSString *label; // kSecAttrLabel
@property (nonatomic, copy, nullable) NSString *account; // kSecAttrAccount

@property (nonatomic, copy, nullable) id encryptedObject; // kSecValueData, NSKeyedArchiver'd

// Initializes and resets the default generic keychain item data.
- (void)resetKeychainItem;

@end

NS_ASSUME_NONNULL_END
