//
//  MSDFileCache.m
//  MSDTouchLibraries
//

#include <sys/xattr.h>

#import "MSDFileCache.h"
#import "MSDTouchLibraryDefines.h"
#import "NSData+MSDEncoding.h"
#import "NSURL+MSDParameters.h"

NSString * const MSDFileCacheErrorDomain = @"com.mindsea.msdtouchlibraries.filecache.error";

NSString * const MSDFileCacheUserDefaultsKey = @"com.mindsea.msdtouchlibraries.filecache";
static NSString * const MSDFileCacheUserDefaultsKeyEtags = @"etags";

static NSString * const MSDFileCacheEtagDictKeyURL = @"url";
static NSString * const MSDFileCacheEtagDictKeyEtag = @"etag";

NSString * const MSDFileCacheErrorUserInfoHTTPCodeKey = @"responseStatusCode";

NSString * const MSDFileCacheSerialQueueKeyName = @"MSDFileCacheSerialQueueKeyName";

// just like dispatch_sync, but doesn't deadlock if you're already on that queue
// this can still deadlock if you are not careful, ie. dispatch_safe to queueA then from there dispatch_safe to queueB and then from there dispatch_safe back to queueA. This case is generally avoided in MSDFileCache
static void filecache_dispatch_sync_safe(dispatch_queue_t queue, NSString * filecacheQueueValue, dispatch_block_t block) {
    
    if(dispatch_get_specific((__bridge const void * _Nonnull)(MSDFileCacheSerialQueueKeyName)) == (__bridge void * _Nullable)(filecacheQueueValue)) {
        block();
    } else {
        dispatch_sync(queue, block);
    }
}


NS_ASSUME_NONNULL_BEGIN

@interface MSDFileCacheRequest : NSObject
// callbacks to call when an update completes 
@property (nonatomic, strong) NSMutableArray *callbacks;
@property (nonatomic, assign) BOOL cancelled;
@end

@implementation MSDFileCacheRequest

- (instancetype)init {
    if ((self = [super init])) {
        self.callbacks = [NSMutableArray array];
    }
    return self;
}


@end

/////
/* */
// //

@interface MSDFileCache()

@property (copy) NSString *localRoot;

@property (strong, nullable) NSBundle *bundle;
@property (copy, nullable) NSString *bundleDirectory;

@property (copy) NSString *userDefaultsKey;

// A dictionary for download requests currently in progress; 
// keys are filenames, objects are MSDFileCacheRequest instances.
// This should only be accessed inside @synchronized(self).
@property (strong, nonatomic) NSMutableDictionary *activeRequests;

@property (strong, nonatomic) NSURLSession * urlSession;


@property (nonatomic, strong) dispatch_queue_t serialQueue;
@property (nonatomic, strong) NSString * serialQueueName;

@property (nonatomic, strong) NSMutableDictionary *contentsCache;
@property (nonatomic, assign) BOOL shouldSetBackupProperty;

@end

@implementation MSDFileCache

- (instancetype)init {
    return [self initWithName:[[NSProcessInfo processInfo] globallyUniqueString]
                       bundle:nil
                 subdirectory:nil
                  storageType:MSDFileCacheStorageTypeTemporary];
}

- (instancetype)initWithName:(NSString *)name {
    return [self initWithName:name bundle:[NSBundle mainBundle] subdirectory:nil storageType:MSDFileCacheStorageTypeCache];
}

- (instancetype)initWithName:(NSString *)name
                      bundle:(nullable NSBundle *)bundle
                subdirectory:(nullable NSString *)directory
                 storageType:(MSDFileCacheStorageType)type {
    
    NSParameterAssert(name != nil);
    
    if (self = [super init]) {
        
        self.urlSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration]];
        
        self.serialQueueName = [NSString stringWithFormat:@"MSDFileCache-serial-%@", name];
        self.serialQueue = dispatch_queue_create([self.serialQueueName UTF8String], DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(self.serialQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0));
        dispatch_queue_set_specific(self.serialQueue, (__bridge const void * _Nonnull)(MSDFileCacheSerialQueueKeyName), (__bridge void * _Nullable)(self.serialQueueName), nil);
        
        self.activeRequests = [NSMutableDictionary dictionary];
        _contentsCache = [[NSMutableDictionary alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didReceiveMemoryWarning:)
                                                     name:UIApplicationDidReceiveMemoryWarningNotification
                                                   object:nil];
        
        self.bundle = bundle;
        self.bundleDirectory = directory;
        
        NSString *parentDirectory;
        NSString *oldParentDirectory = nil;
        
        switch (type) {
            case MSDFileCacheStorageTypeTemporary:
                parentDirectory = NSTemporaryDirectory();
                self.shouldSetBackupProperty = NO;
                break;
                
            case MSDFileCacheStorageTypeOffline:
                [self getOfflineCacheParentDirectory:&parentDirectory oldParentDirectory:&oldParentDirectory];
                self.shouldSetBackupProperty = YES;
                break;
            default:
                MSDDebugFail(@"Unknown storage type %d", (int)type);
                // fall through
            case MSDFileCacheStorageTypeCache:
                parentDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
                self.shouldSetBackupProperty = NO;
                break;
        }
        
        NSCharacterSet *oddCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
        if ([name rangeOfCharacterFromSet:oddCharacters].location != NSNotFound
            || [name length] > 30) {
            // got special characters or a long string; fallback to MD5
            const char *utf8String = [name UTF8String];
            NSData *nameData = [NSData dataWithBytesNoCopy:(void *)utf8String length:strlen(utf8String)];
            name = [nameData msd_md5EncodedString];
        }

        self.userDefaultsKey = [MSDFileCacheUserDefaultsKey stringByAppendingFormat:@".%@", name];

        self.localRoot = [[parentDirectory stringByAppendingPathComponent:@"MSDFileCache"] stringByAppendingPathComponent:name];
        NSString *oldLocalRoot = [[oldParentDirectory stringByAppendingPathComponent:@"MSDFileCache"] stringByAppendingPathComponent:name];
        
        __weak MSDFileCache *weakSelf = self;
        dispatch_async(_serialQueue, ^{
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            // if we have an old location, move it
            if (oldLocalRoot && [fileManager fileExistsAtPath:oldLocalRoot]) {
                NSError *error;
                // create parent dir, then move old contents over
                MSDCheck([fileManager createDirectoryAtPath:[self.localRoot stringByDeletingLastPathComponent]
                                withIntermediateDirectories:YES
                                                 attributes:nil
                                                      error:&error], @"Couldn't create cache directory at %@: %@", self.localRoot, error);
                MSDCheck([fileManager moveItemAtPath:oldLocalRoot toPath:self.localRoot error:&error], @"Couldn't migrate old files to new directory: %@", error);
            } else if (![fileManager fileExistsAtPath:self.localRoot]){
                // no old location, but also no new directory; create it
                NSError *error;
                MSDCheck([fileManager createDirectoryAtPath:self.localRoot
                                withIntermediateDirectories:YES
                                                 attributes:nil
                                                      error:&error], @"Couldn't create cache directory at %@: %@", self.localRoot, error);
                
            }
            
            MSDFileCache *strongSelf = weakSelf;
            if (strongSelf.shouldSetBackupProperty) {
                [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:self.localRoot]];
            }
        });
        
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (nullable id)contentsOfFileNamed:(NSString *)name error:(NSError **)error {
    __block id result;
    __block NSError *blockError;
    filecache_dispatch_sync_safe(_serialQueue, self.serialQueueName, ^{
        result = [self onSerialContentsOfFileNamed:name error:&blockError];
    });
    if (error && blockError) {
        *error = blockError;
    }
    return result;
}

- (NSString *)bundlePathForFileNamed:(NSString *)name {
    NSString *extension = [name pathExtension];
    NSString *basename = [name stringByDeletingPathExtension];
    
    NSString *bundlePath = [self.bundle pathForResource:basename ofType:extension inDirectory:self.bundleDirectory];
    return bundlePath;
}

- (NSString *)pathForFileNamed:(NSString *)name {
    NSParameterAssert(name != nil);
    return [self.localRoot stringByAppendingPathComponent:name];
}

- (NSURL *)URLForFileNamed:(NSString *)name {
    return [NSURL fileURLWithPath:[self pathForFileNamed:name]];
}

- (BOOL)removeFileNamed:(NSString *)name error:(NSError **)error {
    NSParameterAssert(name != nil);
    
    name = [name copy];
    __block BOOL success;
    __block NSError *blockError;
    
    __weak MSDFileCache *weakSelf = self;
    filecache_dispatch_sync_safe(_serialQueue, self.serialQueueName, ^{
        MSDFileCache *strongSelf = weakSelf;
        
        @synchronized(self) {
            MSDFileCacheRequest *fcrequest = [self.activeRequests objectForKey:name];
            fcrequest.cancelled = YES;
        }
        
        NSString *path = [self pathForFileNamed:name];
        NSFileManager *manager = [[NSFileManager alloc] init];

        success = [manager removeItemAtPath:path error:&blockError];
        [self removeEtagForFileNamed:name];
        
        [strongSelf->_contentsCache removeObjectForKey:name];
    });

    if (error && blockError) {
        *error = blockError;
    }
    
    return success;
}

#pragma mark - Asynchronous Methods

- (void)loadContentsOfFileNamed:(NSString *)name completionHandler:(void(^ __nullable)(id __nullable contents, NSError * __nullable error))completionHandler {

    void(^wrapperCompletionBlock)(NSHTTPURLResponse* response, NSError * __nullable) = nil;
    
    if (completionHandler) {
        wrapperCompletionBlock = ^(NSHTTPURLResponse* __nullable response, NSError * __nullable error) {
            NSError *contentsError;
            NSData *contents = [self contentsOfFileNamed:name error:&contentsError];
            // we pass the original error unless we have an unexpected problem reading the file contents
            if (!contents && !error) {
                error = contentsError;
            }
            completionHandler(contents, error);
        };
    }
    
    [self forceUpdateOfFileNamed:name completionHandler:wrapperCompletionBlock];
}

- (NSString *)userDefaultsKeyEtagsDict {
    return [self.userDefaultsKey stringByAppendingFormat:@".%@", MSDFileCacheUserDefaultsKeyEtags];
}

- (void)updateEtagForFileNamed:(NSString *)name URL:(nullable NSURL *)requestURLOrNil etag:(nullable NSString *)etagHeaderOrNil {
    // update last known etag
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary *etagsDict = [[userDefaults dictionaryForKey:[self userDefaultsKeyEtagsDict]] mutableCopy] ?: [NSMutableDictionary dictionary];
    
    if (etagHeaderOrNil && requestURLOrNil) {
        [etagsDict setObject:[NSDictionary dictionaryWithObjectsAndKeys:
                              etagHeaderOrNil, MSDFileCacheEtagDictKeyEtag,
                              [requestURLOrNil absoluteString], MSDFileCacheEtagDictKeyURL,
                              nil]
                      forKey:name];
        
    } else {
        [etagsDict removeObjectForKey:name];
    }
    [userDefaults setObject:etagsDict
                     forKey:[self userDefaultsKeyEtagsDict]];
    [userDefaults synchronize];
}

- (void)removeEtagForFileNamed:(NSString *)name {
    [self updateEtagForFileNamed:name URL:nil etag:nil];
}


- (void)forceUpdateOfFileNamed:(NSString *)name completionHandler:(void (^ __nullable)(NSHTTPURLResponse* __nullable, NSError * __nullable))completionHandler {
    NSParameterAssert(name != nil);
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    MSDCheck(![self.delegate respondsToSelector:@selector(fileCache:contentsForFileNamed:data:error:)], @"MSDFileCache delegate implements fileCache:contentsForFileNamed:data:error:, which should be updated to use fileCache:contentsForFileNamed:fromPath:error: instead.");
#pragma clang diagnostic pop
    
    // get immutable version for thread safety, consistency
    name = [name copy];
    completionHandler = [completionHandler copy];

    __weak MSDFileCache *weakSelf = self;
    dispatch_async(self.serialQueue, ^{
        MSDFileCache *strongSelf = weakSelf;
        BOOL startOperation = NO;
        
        @synchronized(strongSelf) {
            MSDFileCacheRequest *fcrequest = [self.activeRequests objectForKey:name];
            if (!fcrequest) {
                fcrequest = [[MSDFileCacheRequest alloc] init];
                [self.activeRequests setObject:fcrequest forKey:name];
                
                startOperation = YES;
            }
            
            if (completionHandler) {
                [fcrequest.callbacks addObject:completionHandler];
            }
        }
        
        
        
        if (startOperation) {
            NSURL * requestURL = nil;
            NSDictionary * requestParameters = nil;
            
            if ([strongSelf.delegate respondsToSelector:@selector(fileCache:remoteURLForFileNamed:)]) {
                requestURL = [strongSelf->_delegate fileCache:self remoteURLForFileNamed:name];
            }
            
            if ([strongSelf.delegate respondsToSelector:@selector(fileCache:remoteRequestParametersForFileNamed:)]) {
                requestParameters = [strongSelf.delegate fileCache:self remoteRequestParametersForFileNamed:name];
                if (requestParameters && requestURL) {
                    requestURL = [requestURL msd_URLByAddingParameters:requestParameters];
                }
            }
            
            NSString *requestMethod = @"GET";
            
            if ([strongSelf->_delegate respondsToSelector:@selector(fileCache:remoteRequestMethodForFileNamed:)]) {
                requestMethod = [strongSelf->_delegate fileCache:self remoteRequestMethodForFileNamed:name];
            }

            if (!requestURL) {
                // no URL; just fire callbacks
                dispatch_async(strongSelf->_serialQueue, ^{
                    [self onSerialFinishRequestForFileNamed:name response:nil error:[NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorNoRemoteURL userInfo:nil]];
                });
                
            } else {
                
                NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:requestURL];
                request.HTTPMethod = requestMethod;
                request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
                
                // Use ETag if present
                NSDictionary *etagsDict = ([[NSUserDefaults standardUserDefaults] dictionaryForKey:[self userDefaultsKeyEtagsDict]] ?: [NSDictionary dictionary]);
                NSDictionary *fileEtagDict = [etagsDict objectForKey:name];
                NSString *urlString = [fileEtagDict objectForKey:MSDFileCacheEtagDictKeyURL];
                NSString *etag = [fileEtagDict objectForKey:MSDFileCacheEtagDictKeyEtag];
                
                NSFileManager *etagFileManager = [[NSFileManager alloc] init];
                if ([urlString isEqual:[requestURL absoluteString]] && etag && [etagFileManager fileExistsAtPath:[self pathForFileNamed:name]]) {
                    NSMutableDictionary * headerFields = [request.allHTTPHeaderFields mutableCopy];
                    if (!headerFields) {
                        headerFields = [NSMutableDictionary dictionary];
                    }
                    [headerFields setObject:etag forKey:@"If-None-Match"];
                    request.allHTTPHeaderFields = headerFields;
                } else {
                    [self removeEtagForFileNamed:name];
                }
                
                NSString *temporaryDownloadPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSProcessInfo processInfo] globallyUniqueString]];
                
                NSURLSessionDownloadTask * downloadTask = [self.urlSession downloadTaskWithRequest:request completionHandler:^(NSURL * _Nullable location, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    
                    NSHTTPURLResponse * httpResponse = nil;
                    if([response isKindOfClass:[NSHTTPURLResponse class]]) {
                        httpResponse = (NSHTTPURLResponse*)response;
                    }
                    
                    if(error) {
                        dispatch_async(strongSelf.serialQueue, ^{
                            [self onSerialFinishRequestForFileNamed:name response:httpResponse error:error];
                        });
                        return;
                    }
                    
                    NSString *etagHeader = nil;
                    if([response isKindOfClass:[NSHTTPURLResponse class]]) {
                        NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse*)response;
                        etagHeader = [httpResponse.allHeaderFields objectForKey:@"ETag"];
                    
                        if(httpResponse.statusCode == 304) {
                            dispatch_async(strongSelf.serialQueue, ^{
                                [self onSerialFinishRequestForFileNamed:name response:httpResponse error:error];
                            });
                            return;
                        }
                    }
                    
                    //the last thing we need to do is move the temp file from the download task to another temp file
                    //if we don't do that then the file created by the download task could be rmeoved by the system
                    NSError * copyError = nil;
                    bool didMove = [[NSFileManager defaultManager] moveItemAtPath:location.path toPath:temporaryDownloadPath error:&copyError];
                    if(!didMove || copyError) {
                        if(!copyError) {
                            copyError = [NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorFailedValidation userInfo:nil];
                        }
                        dispatch_async(strongSelf.serialQueue, ^{
                            [self onSerialFinishRequestForFileNamed:name response:httpResponse error:copyError];
                        });
                        return;
                    }
                    
                    dispatch_async(strongSelf.serialQueue, ^{
                        BOOL requestCancelled;
                        @synchronized(self) {
                            MSDFileCacheRequest *fcrequest = [self.activeRequests objectForKey:name];
                            requestCancelled = fcrequest.cancelled;
                        }
                        if (requestCancelled) {
                            NSError *error = [NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorAborted userInfo:nil];
                            [self onSerialFinishRequestForFileNamed:name response:httpResponse error:error];
                            return;
                        }
                        
                        NSFileManager *manager = [[NSFileManager alloc] init];
                        NSError *error = nil;
                        NSString *path = [self pathForFileNamed:name];
                        id contents = nil;
                        
                        BOOL shouldReplaceContents = YES;
                        BOOL tempFileExists = [manager fileExistsAtPath:temporaryDownloadPath];
                        
                        // Validate new contents
                        if (!tempFileExists) {
                            
                            shouldReplaceContents = NO;
                            error = [NSError errorWithDomain:MSDFileCacheErrorDomain
                                                        code:MSDFileCacheErrorNoData
                                                    userInfo:nil];
                            
                        } else if ([self.delegate respondsToSelector:@selector(fileCache:shouldReplaceFileNamed:withContentsAtPath:)]) {
                            
                            shouldReplaceContents = [self.delegate fileCache:self
                                                      shouldReplaceFileNamed:name
                                                          withContentsAtPath:temporaryDownloadPath];
                            
                            if (!shouldReplaceContents) {
                                error = [NSError errorWithDomain:MSDFileCacheErrorDomain
                                                            code:MSDFileCacheErrorFailedValidation
                                                        userInfo:nil];
                            }
                            
                        } else if ([self.delegate respondsToSelector:@selector(fileCache:contentsForFileNamed:fromPath:error:)]) {
                            contents = [self.delegate fileCache:self
                                           contentsForFileNamed:name
                                                       fromPath:temporaryDownloadPath
                                                          error:&error];
                            if (!contents) {
                                shouldReplaceContents = NO;
                            }
                        }
                        
                        if (!shouldReplaceContents) {
                            MSDWarningLog(@"File named %@ at %@ failed validation: %@", name, temporaryDownloadPath, error);
                            if (tempFileExists) {
                                NSError *fileError;
                                if (![manager removeItemAtPath:temporaryDownloadPath error:&fileError]) {
                                    MSDDebugLog(@"Couldn't delete file at %@: %@", temporaryDownloadPath, fileError);
                                }
                            }
                            [self onSerialFinishRequestForFileNamed:name response:httpResponse error:error];
                            return;
                        }
                        
                        // We have new data; clear out old in-memory cache
                        [strongSelf->_contentsCache removeObjectForKey:name];
                        
                        if (contents) {
                            [self cacheContentsIfAppropriate:contents forFileNamed:name];
                        }
                        
                        // Delete old version first (the move won't work if it's already there).
                        NSError *deleteError = nil;
                        BOOL didDelete = [manager removeItemAtPath:path error:&deleteError];
                        
                        BOOL didMove = [manager moveItemAtPath:temporaryDownloadPath toPath:path error:&error];
                        if (!didMove) {
                            if (!didDelete) {
                                // We didn't move, and it looks like it was because we couldn't delete the old file, so
                                // log the can't-delete error for debugging purposes.
                                MSDWarningLog(@"Couldn't move file to %@: %@", path, error);
                                MSDWarningLog(@"Possible cause: couldn't delete that file: %@", deleteError);
                            }
                            [self onSerialFinishRequestForFileNamed:name response:httpResponse error:error];
                        } else {
                            [self updateEtagForFileNamed:name URL:requestURL etag:etagHeader];
                            
                            [self addSkipBackupAttributeToItemAtURL:[self URLForFileNamed:name]];
                            [self onSerialFinishRequestForFileNamed:name response:httpResponse error:nil];
                        }
                    });

                    
                    
                }];
                
                [downloadTask resume];
            }
        }
    });
}

- (void)addDefaultFileNamed:(NSString *)name
          overwriteExisting:(BOOL)overwrite
                       eTag:(nullable NSString *)eTagOrNil
                    eTagURL:(nullable NSURL *)eTagURLOrNil
          completionHandler:(void(^)(NSError * __nullable error))completionHandler {
    
    if (!self.bundle) {
        completionHandler([NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorNoBundle userInfo:nil]);
        return;
    }
    
    NSString *bundlePath = [self bundlePathForFileNamed:name];
    if (!bundlePath) {
        completionHandler([NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorNoResource userInfo:nil]);
        return;
    }
    
    // get immutable version for thread safety, consistency
    name = [name copy];

    dispatch_async(_serialQueue, ^{
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        NSString *path = [self pathForFileNamed:name];
        
        if (overwrite) {
            // we ignore errors here, since it is likely just not yet present
            [fileManager removeItemAtPath:path error:nil];
            [self removeEtagForFileNamed:name];

            @synchronized(self) {
                MSDFileCacheRequest *fcrequest = [self.activeRequests objectForKey:name];
                fcrequest.cancelled = YES;
            }
        }
        
        if (![fileManager fileExistsAtPath:path]) {
            NSError *error;
            BOOL copied = [fileManager copyItemAtPath:bundlePath
                                               toPath:path
                                                error:&error];
            if (copied) {
                [self addSkipBackupAttributeToItemAtURL:[self URLForFileNamed:name]];
                
                if (eTagOrNil && eTagURLOrNil) {
                    [self updateEtagForFileNamed:name URL:eTagURLOrNil etag:eTagOrNil];
                }
                
                completionHandler(nil);
            } else {
                completionHandler(error);
            }
        } else {
            completionHandler(nil);
        }
    });
}

- (void)addDefaultFileNamed:(NSString *)name
          overwriteExisting:(BOOL)overwrite
          completionHandler:(void(^)(NSError * __nullable error))completionHandler {
    [self addDefaultFileNamed:name overwriteExisting:overwrite eTag:nil eTagURL:nil completionHandler:completionHandler];
}

- (void)addAllDefaultFilesOverwritingExisting:(BOOL)overwrite completionHandler:(void (^)(NSError * __nullable))completionHandler {
    dispatch_async(_serialQueue, ^{
        if (!self.bundle) {
            completionHandler([NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorNoBundle userInfo:nil]);
            return;
        }
        
        if (!self.bundleDirectory) {
            completionHandler([NSError errorWithDomain:MSDFileCacheErrorDomain code:MSDFileCacheErrorNoSubdirectory userInfo:nil]);
            return;
        }
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSString *parentPath = [[self.bundle resourcePath] stringByAppendingPathComponent:self.bundleDirectory];

        NSError *error;
        NSArray *contents = [fileManager contentsOfDirectoryAtPath:parentPath error:&error];
        if (!contents) {
            completionHandler(error);
        } else {
            NSMutableArray *errors = [NSMutableArray array];
            for (NSString *name in contents) {
                NSString *fullPath = [parentPath stringByAppendingPathComponent:name];
                BOOL isDirectory;
                MSDCheck([fileManager fileExistsAtPath:fullPath isDirectory:&isDirectory], @"Couldn't find out if file is directory: %@", fullPath);
                
                if (isDirectory) {
                    MSDWarningLog(@"Ignoring file %@, since we don't put directories in our cache", fullPath);
                } else {
                    NSString *destinationPath = [self pathForFileNamed:name];
                    if (overwrite) {
                        // we ignore errors here, since it is likely just not yet present
                        [fileManager removeItemAtPath:destinationPath error:nil];
                    }

                    if (![fileManager fileExistsAtPath:destinationPath]) {
                        BOOL success = [fileManager copyItemAtPath:fullPath
                                                            toPath:destinationPath
                                                             error:&error];
                        if (success) {
                            [self addSkipBackupAttributeToItemAtURL:[self URLForFileNamed:name]];
                        } else {
                            [errors addObject:error];
                        }
                    }
                }
            }
            if ([errors count]) {
                if ([errors count] == 1) {
                    error = [errors objectAtIndex:0];
                } else {
                    MSDErrorLog(@"Errors copying default files: %@", errors);
                    error = [NSError errorWithDomain:MSDFileCacheErrorDomain
                                                code:MSDFileCacheErrorMultipleErrors
                                            userInfo:nil];
                }
            } else {
                error = nil;
            }
            completionHandler(error);
        }
    });
}


#pragma mark - Helper Methods

- (void)getOfflineCacheParentDirectory:(NSString **)currentOut
                    oldParentDirectory:(NSString **)oldOut {
    
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
    NSString *applicationSupportDirectory = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) lastObject];
    
    int majorVersion = 0, minorVersion = 0, revisionVersion = 0;
    [self getOSMajorVersion:&majorVersion minorVersion:&minorVersion revision:&revisionVersion];
    
    if (majorVersion < 5 || (majorVersion == 5 && minorVersion == 0 && revisionVersion == 0)) {
        // on <= iOS 5.0.0, we need to store in caches to comply with Apple's data storage rules
        *currentOut = cachesDirectory;
        *oldOut = nil;
    } else {
        // on iOS 5.0.1+, we store in application support, but migrate from old possibility
        *currentOut = applicationSupportDirectory;
        *oldOut = cachesDirectory;
    }
}

- (void)getOSMajorVersion:(int *)major minorVersion:(int *)minor revision:(int *)revision {
    NSString *version = [UIDevice currentDevice].systemVersion;
    
    NSScanner *scanner = [NSScanner scannerWithString:version];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"."]];
    
    [scanner scanInt:major];
    [scanner scanInt:minor];
    [scanner scanInt:revision];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    if (!self.shouldSetBackupProperty) {
        return YES;
    }
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error:&error];
    MSDCheck(success, @"Couldn't set backup attribute: %@", error);
    return success;
}

- (void)didReceiveMemoryWarning:(NSNotification *)notification {
    [self clearInMemoryCache];
}

- (void)clearInMemoryCache {
    __weak MSDFileCache *weakSelf = self;
    dispatch_async(_serialQueue, ^{
        MSDFileCache *strongSelf = weakSelf;
        [strongSelf->_contentsCache removeAllObjects];
    });
}

- (void)cacheContentsIfAppropriate:(id)contents forFileNamed:(NSString *)name {
    BOOL shouldCache = YES;
    if ([self.delegate respondsToSelector:@selector(fileCache:shouldCacheContents:forFileNamed:)]) {
        shouldCache = [self.delegate fileCache:self shouldCacheContents:contents forFileNamed:name];
    }
    
    if (shouldCache) {
        [_contentsCache setObject:contents forKey:name];
    }
}

- (nullable id)onSerialContentsOfFileNamed:(NSString *)name error:(NSError **)error {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
    MSDCheck(![self.delegate respondsToSelector:@selector(fileCache:contentsForFileNamed:data:error:)], @"MSDFileCache delegate implements fileCache:contentsForFileNamed:data:error:, which should be updated to use fileCache:contentsForFileNamed:fromPath:error: instead.");
#pragma clang diagnostic pop
    
    NSString *filePath = [self pathForFileNamed:name];
    
    id cachedContents = [_contentsCache objectForKey:name];
    if (cachedContents) {
        return cachedContents;
    }
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    id contents = nil;
    if ([fileManager fileExistsAtPath:filePath]) {
        if ([self.delegate respondsToSelector:@selector(fileCache:contentsForFileNamed:fromPath:error:)]) {
            contents = [self.delegate fileCache:self contentsForFileNamed:name fromPath:filePath error:error];
        } else {
            contents = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:error];
        }
    }
    
    NSString *bundlePath = [self bundlePathForFileNamed:name];
    if (!contents && bundlePath) {
        if ([fileManager fileExistsAtPath:bundlePath]) {
            // there was a file in the bundle, and the cached file failed to load; overwrite
            NSError *fileError;
            if (![fileManager removeItemAtPath:filePath error:&fileError]) {
                MSDDebugLog(@"Couldn't delete corrupt file: %@", fileError);
            }
            
            if (![fileManager copyItemAtPath:bundlePath toPath:filePath error:&fileError]) {
                MSDErrorLog(@"Couldn't copy fresh file from bundle: %@", fileError);
            }
            
            if ([fileManager fileExistsAtPath:filePath]) {
                if ([self.delegate respondsToSelector:@selector(fileCache:contentsForFileNamed:fromPath:error:)]) {
                    contents = [self.delegate fileCache:self contentsForFileNamed:name fromPath:filePath error:error];
                } else {
                    contents = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:error];
                }
            }
        }
    }
    
    if (contents) {
        [self cacheContentsIfAppropriate:contents forFileNamed:name];
    }
    
    return contents;
}

- (void)onSerialFinishRequestForFileNamed:(NSString *)name response:(nullable NSHTTPURLResponse*)response error:(nullable NSError *)error {
    MSDFileCacheRequest *fcrequest;
    
    @synchronized(self) {
        fcrequest = [self.activeRequests objectForKey:name];
        [self.activeRequests removeObjectForKey:name];
    }
    
    for (void(^callback)(NSHTTPURLResponse*, NSError *) in fcrequest.callbacks) {
        callback(response, error);
    }
}

@end

NS_ASSUME_NONNULL_END
