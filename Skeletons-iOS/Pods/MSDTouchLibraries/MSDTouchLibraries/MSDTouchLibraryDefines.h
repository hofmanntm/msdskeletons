//
//  MSDTouchLibraryDefines.h
//  MSDTouchLibraries
//
//  Created by Ash Furrow on 2014-03-06.
//  Copyright (c) 2014 MindSea Development Inc. All rights reserved.
//


/*
 * This defines the following macros, which all take a format string and args.
 * MSDDebugLog: Logs in debug like NSLog, but silent in release
 * MSDDebugFail: Always raises an exception in debug, logs in release
 * MSDCheck: In debug, just like an assert. In release, checks the condition and logs if false.
 * MSDArgNotNil: A convinience macro which checks for a nil arg and asserts in debug while logging in release.
 *
 * From http://www.cimgf.com/2010/05/02/my-current-prefix-pch-file/
 */

#import "MSDCompatibility.h"

#ifdef DEBUG
#  define MSDDebugFail(...) [[NSAssertionHandler currentHandler] handleFailureInFunction:[NSString stringWithCString:__PRETTY_FUNCTION__ encoding:NSUTF8StringEncoding] file:[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lineNumber:__LINE__ description:__VA_ARGS__]
#else
#  ifndef NS_BLOCK_ASSERTIONS
#    define NS_BLOCK_ASSERTIONS
#  endif
#  define MSDDebugFail(...) MSDCErrorLog(@"MSDCheck", @"%s %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:__VA_ARGS__])
#endif

#define MSDCheck(condition, ...) do { if (!(condition)) { MSDDebugFail(__VA_ARGS__); }} while(0)
#define MSDArgNotNil(arg) do { if(!(arg)) { MSDDebugFail(@"Argument " #arg @" must not be nil"); } } while(0)

// These must be included after NS_BLOCK_ASSERTIONS is defined
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MSDLogger.h"

// Returns the object, or NSNull if the object is nil
NS_INLINE id MSDObjectOrNull(id object)  {
    return object == nil ? [NSNull null] : object;
}

// Returns the object, if it is a kind of the passed class name. Otherwise returns nil.
#define MSDCastOrNil(theObject, theClassName) ((theClassName *)({ id __value = (theObject); [__value isKindOfClass:[theClassName class]] ? __value : nil; }))


// Defines a + method in the given class which returns a single
// instance of this class, created with - (instancetype)init.
#define MSD_SINGLETON_METHOD(className, methodName) \
+ (className *)methodName { \
    static className *methodName; \
    static dispatch_once_t onceToken; \
    dispatch_once(&onceToken, ^{ \
        methodName = [self new]; \
    }); \
    return methodName; \
}
