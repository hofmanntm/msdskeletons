/**
 * A collection of the currently-known DOM elements that are eligible
 * for native views.
 *
 * This collection maps names to an object containing the DOM elements
 * and their bounding rect.  In other words, this collection is
 * structured exactly like the return value from
 * msdFindNativeViewHolders().
 *
 * Call msdUpdateNativeViewHolders() to scan the document and update
 * this collection.
 */
var msdNativeViewHolderElements = {};

// adapted from http://underscorejs.org/docs/underscore.html
function msdDebounce(func, wait) {
    var args, context, result, timeout, timestamp;
    
    var later = function() {
        var last = Date.now - timestamp;
        if (last < wait && last >= 0) {
            timeout = setTimeout(later, wait - last);
        }
        else {
            timeout = null;
            result = func.apply(context, args);
            
            if (!timeout) {
                context = null;
                args = null;
            }
        }
    };
    
    return function() {
        context = this;
        args = arguments;
        timestamp = Date.now;
        var callNow = !timeout;
        if (!timeout) {
            timeout = setTimeout(later, wait);
        }
        if (callNow) {
            result = func.apply(context, args);
            context = null;
            args = null;
        }
        
        return result;
    };
}

/**
 * Find native view holders.
 *
 * This searches the entire DOM for tags with the attribute
 * data-msd-native-view-holder-name.
 *
 * eg. <div data-msd-native-view-holder-name="ad spot 1"></div>
 *
 * @returns An object mapping names to the element object and bounding
 *          rect.
 *          eg. {"ad spot 1": {"element": DOMElement(),
 *                             "boundingRect": DOMRect()}}
 */
function msdFindNativeViewHolders() {
    var domElements = document.querySelectorAll("[data-msd-native-view-holder-name]");
    var nativeViewHolderElements = {};

    for(var i = 0; i < domElements.length; i++) {
        var element = domElements[i];
        var dataset = element.dataset;

        var boundingRect = element.getBoundingClientRect();
        nativeViewHolderElements[element.dataset.msdNativeViewHolderName] = {"element": element,
            "boundingRect": boundingRect};
    }

    return nativeViewHolderElements;
}


/**
 * Send all known native view holders to the native app through a
 * bridge.
 *
 * No arguments or return value.
 */
var msdUpdateNativeViewHolders = msdDebounce(function() {
    msdNativeViewHolderElements = msdFindNativeViewHolders();

    var namesWithRects = {};
    for (var viewHolderName in msdNativeViewHolderElements) {
        var boundingRect = msdNativeViewHolderElements[viewHolderName].boundingRect;
        
        namesWithRects[viewHolderName] = {'x': boundingRect.left + window.scrollX,
                                          'y': boundingRect.top + window.scrollY,
                                          'width': boundingRect.width,
                                          'height': boundingRect.height};
    }

    window.webkit.messageHandlers.updateNativeViewHolders.postMessage(namesWithRects);
}, 50);

/**
 * Impose a particular width and height onto the native view holder.
 * This is used to update the element to match a custom w/h size set by
 * the UIView.
 */
function msdSetNativeViewHolderWidth(name, width) {
    var element = msdNativeViewHolderElements[name].element;
    element.style.width = width;
}
function msdSetNativeViewHolderHeight(name, height) {
    var element = msdNativeViewHolderElements[name].element;
    element.style.height = height;
}

var msdDOMDidLoad = false;
document.addEventListener("DOMContentLoaded", function(event) {

    if (msdDOMDidLoad) {
        return;
    }
    msdDOMDidLoad = true;

    msdUpdateNativeViewHolders();
    
    var observer = new MutationObserver(function (mutations) {
        msdUpdateNativeViewHolders();
    });
    observer.observe(document.body, {
        attributes: true,
        characterData: true,
        childList: true,
        subtree: true
    });
});


window.addEventListener("resize", function (event) {
    msdUpdateNativeViewHolders();
});
