//
//  MSDCompatibility.h
//  MSDTouchLibraries
//
//  Created by Nolan Waite on 2017-07-26.
//  Copyright © 2017 MindSea Development Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 This file contains bits to ensure MSDTL compiles across multiple versions of the SDK. Namely, everything in here should be wrapped in an appropriate feature check.

 This file will pollute the namespace of anywhere that imports it. Please be considerate.

 MSD-specific helpers should go in MSDTouchLibraryDefines.h (which is only imported in .m files and so does not pollute).
 */


// Macro promised by SE-0112 that never appeared (as of Xcode 8.3.3).
// From https://gist.github.com/bdash/bf29e26c429b78cc155f1a2e1d851f8b thank you!
#ifndef NS_ERROR_ENUM
    #if __has_attribute(ns_error_domain)
        #define NS_ERROR_ENUM(domain, name) \
            _Pragma("clang diagnostic push") \
            _Pragma("clang diagnostic ignored \"-Wignored-attributes\"") \
            NS_ENUM(NSInteger, __attribute__((ns_error_domain(domain))) name) \
            _Pragma("clang diagnostic pop")
    #else
        #define NS_ERROR_ENUM(type, name, domain) NS_ENUM(type, name)
    #endif
#endif

// So we can use this before it was defined by Apple's headers
#ifndef NS_RETURNS_RETAINED
    #define NS_RETURNS_RETAINED
#endif
