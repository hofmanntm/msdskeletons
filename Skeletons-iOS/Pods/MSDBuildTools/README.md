# Overview

iOS build tools shared between MindSea projects.

With cocoapods, you can add `pod 'MSDBuildTools'` to your `Podfile`. You'll then need to reference these build scripts within the `Pods` folder.

# Relationship to Xcode

MSDBT version 2 is targeted to Xcode 8.2 and later. If your project is not yet updated to this point, you should update it so it works with MSDBT 2.x, however the legacy scripts remain available. 

# TODO

 * Watch targets and extensions are not yet supported.

# Before Using In Your Project

Make sure your project's `.gitignore` contains `build.noindex`, as the `msdbuild` scripts place all build outputs in that folder.

# Contributing to MSDBuildTools

Make sure you clone the repo! Don't make the changes directly in your project's `Pods` folder or they'll be overwritten next time someone does a `pod install`.

Make your changes. Test them out in your project. If you're mucking with a command-line tool, try running it from the MSDBT repo but pointing it at some guinea pig project. (If your tool can't support this use case, consider adding it!) If this is part of an Xcode "Run Script" build phase, it's probably pointing at the project's pods instead of your clone of the MSDBT repo. You might temporarily change the build phase to point at your local repo.

Once you're satisfied with your work, you should update the change log in the MSDBT repo (`CHANGELOG.md`). The idea behind the change log is so you can glance down a list of changes and hopefully answer the question "what's going to break when I update this two-year-old project to the latest MSDBT?". Hot tip: you can add a new section titled `## Unreleased` to the change log, and it'll get replaced with the version number when you get to a future step.

All set? Commit your changes to MSDBT. Your commit message might be pretty similar to your change log entry, that's ok.

Once you've committed some changes to MSDBT you'll want to let everyone use the new goodies. First, push your changes to MSDBT. Then, run the `tag-and-push` script in the MSDBT repo root. This script will increment the version number, update the podspec, update the change log, and push those changes. Please try to stick somewhat to semantic versioning: if there's no changes to public API or changes are purely additive, bump the minor version; otherwise bump the major version.

Nearly there, I promise! Now you need to update our CocoaPods specs repository. This is how CocoaPods figures out how to install libraries. We have our own private specs repo called `mindseaspecs`. You'll need to clone that repo. In that repo, look for the folder `Specs/MSDBuildTools`. Add a new folder for your new version. Copy `MSDBuildTools.podspec` from the MSDBT repo into the new folder. Then commit and push your change to `mindseaspecs`.

Finally it's time to get the updated MSDBT version into your project. Run `pod update MSDBuildTools`. You should see CocoaPods update MSDBT to the version you just pushed.

Congrats, you're done! It gets easier once you're familiar with all the steps.

# iOS Build utilities

These live in `bin`, and are useful in projects.

## build

Builds an Xcode project. Examines Xcode build settings to determine what to build, how to build it, and what should happen before and after.

Typical usage looks like:

    cd project # where the Xcode project is
    build config # where config is the name of a build configuration

`build` uses Xcode build settings to figure out what to do and how to do it. See its documentation for details.

For some help, run `build -h`.

`build` is part of the `msdbuild` Python package, so if you want to change something you should probably look there.


## distribute

Take an `.xcarchive` and send it to Crashlytics Beta and/or iTunes Connect. It uses the same build settings and logic as `build`. This was made to be a post-integration trigger for Xcode bots, so we can make the `.xcarchvie` using the bot and then distribute it from there.

In general you should prefer to use `build`.

Run `distribute --help` for more info.


## find-profile

Find a provisioning profile matching the specified criteria.

For some help, run `find-profile -h`.

Used primarily by `build-utils.sh`.


## insert-msd-info-plist-keys

Adds build date and git branch/commit to an app or extension's Info.plist. Add this to a Run Script build phase in your app and extension targets, and drag that Run Script build phase below "Copy Bundle Resources", and you're all set.

This obviates `generate-infoplist-h` and probably Info.plist preprocessing too.

**WARNING** this approach has caused problems in Xcode 8.0 and Xcode 9.0 (but maybe not Xcode 8.3?). When you're afflicted, you'll have the first install to a device succeed, then on subsequent installs Xcode will give a vague "app install failed" error. You probably want to stay with `generate-infoplist-h` for now.


## msdtheme

Generates a `Theme.swift` file using a plist of colors, fonts, numbers, and strings.

Please see documentation at the top of the [bin/msdtheme]() file.

## pprofiles

Manages provisioning profiles in an Xcode project. It will make or update provisioning profiles based on the Xcode project's build settings `CODE_SIGN_IDENTITY` and `PROVISIONING_PROFILE_SPECIFIER`.

For some help, run `pprofiles -h`.

`pprofiles` is part of the `msdbuild` Python package, so if you want to change something you should probably look there.


# Legacy

If you are using version 2.x, you probably don't need these, but they are here if you need them.

## build-utils.sh

Contains functions used for building Xcode projects.

It's easiest to look at existing projects to see how to use it.  Find the `bin/{build,deploy,archive}*` scripts in the project root.

Some commentary:

    # Optional.  Path to the .xcworkspace file used to build the
    # project, if the project uses workspace.
    WORKSPACE_PATH="Sable.xcworkspace"
    
    # Name of the project.
    PROJECT_NAME="Sable"

    #
    # The above should be set before calling load_base_config
    #

    # Optional.  Space-delimited names of schemes to build for this
    # project.
    # If omitted, uses a scheme with the name of the project.
    SCHEME_NAME_LIST="target1 target2 target3"

    # Optional - Xcode 5+ only.  Provisioning profile settings.
    # Uses find-profile to find a profile matching the supplied
    # criteria.
    # Any of these arguments can be omitted.

    # Name of the provisioning profile to use.
    # Matches on initial substring, so you can use eg.
    # "Mindsea Ad-Hoc profile" to match
    # "Mindsea Ad-Hoc profile Jan 4 2014"
    PROVISIONING_PROFILE_NAME="iOS Team Provisioning Profile: *"

    # Team ID for profile selection.
    PROVISIONING_PROFILE_TEAMID="5UT64Z4SLQ"
    
    # App ID for profile selection.
    PROVISIONING_PROFILE_APPID="CM8YQXXQWJ"

    # Needed to make usable .xcarchives and .ipas for WatchKit. Hidden
    # behind a setting in case iTunes Connect dislikes the
    # WatchKitSupport folder in apps that don't use WatchKit.
    # WATCHKIT_ARCHIVE=yes

    # xcconfig settings to use when building this project.
    XCCONFIG='...'

### Configuration: .msdbuild

`build-utils.sh` looks for a `~/.msdbuild` script to control some aspects of how it works. It then looks for a `.msdbuild` script in the build directory.

You can create either or both of those files and include some of the following:

    # A path where the build scripts will store the .xcarchive products
    # from building projects.
    ARCHIVE_PATH=~/code/archives
    
    # Or, if you want to get real fancy, put the archives right into Xcode.
    # ARCHIVE_PATH=~/Library/Developer/Xcode/Archives/`date +"%Y-%m-%d"`

## tag-and-push

Usage:

    tag-and-push <tag-name>
    
For projects using AGV (Apple Generic Versioning), this script will:

1. Increment the project version
2. Commit the new version to git
3. Tag the new commit in git as `build-(number)`
4. Tag the new commit with an annotated tag named with the supplied `tag-name` (for use by `git describe`)
5. Push

Best practice is to tag-and-push before making any Testflight build, so the build number can be easily used to identify the build.  It cleans up version labels too, so they don't get git hashes on them like _1.2-g1325623_.

Typically added to a project with a script like:

    #!/usr/bin/env bash
    # <proj>/bin/tag-and-push

    projectDir=<DIR_WITH_XCPROJECT>
    source External/MSDTouchLibraries/MSDTouchLibraries/bin/tag-and-push

Example usage:

    cd ~/projects/myproject
    bin/tag-and-push 1.2rc1
