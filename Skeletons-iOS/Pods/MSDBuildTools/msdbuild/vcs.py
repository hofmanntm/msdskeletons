from .subbyproc import check_call, check_output




def tag_and_push(version, keepBuildNumber, log):
    """Tags and pushes a new build number to git
    
        1. check if the repo is clean, if not throw exception
        2. check if there are upstream changes on your branch, if so throw an exception and quit
        3. get the remote build number from git (default to project build number)
        4. increment build number and push changes to project and push build number tag
    """

    if keepBuildNumber:
        log.info("Keeping build number")
        get_tag_name_args = ['git', 'describe']
        tag = check_output(get_tag_name_args).strip() 
        if 'build-' in tag:
            try:
                build_number = int(tag.replace('build-', ''))
                log.info("Making build " + tag)
            except ValueError:
                raise AttributeError('In order to keep a build number you must be on a buid tag.')
        return

    if isHeadDetached():
        raise AttributeError('You cannot make a new build from a detached HEAD. If you are on a build tag and want to create a build with the same number you might want to try \'--keepBuildNumber\' as a command line option. Otherwise, you can branch and push the branch to your remote and try again.')

    check_for_uncommited_changes_args = [ 'git', 'status', '--porcelain']
    
    repo_status = check_output(check_for_uncommited_changes_args)
    if repo_status:
        raise AttributeError('git repo status is not clean. Commit changes and try again')

    get_branch_name_args = ['git', 'rev-parse', '--abbrev-ref', 'HEAD']
    branch_name = check_output(get_branch_name_args).strip()    

    if doesBranchExistOnRemote(branch_name) == False:
        raise AttributeError('Your current branch does not exist remotely on origin. Push your branch to origin and try again')

    fetch_remote_refs_args = ['git', 'fetch', 'origin']
    check_output(fetch_remote_refs_args)

    get_upstream_repo_changes_args = ['git', 'log', 'HEAD..origin/' + branch_name, '--oneline']
    git_repo_changes = check_output(get_upstream_repo_changes_args)
    if git_repo_changes:
        raise AttributeError('Your current branch has upstream changes. Pull changes and try again')

    if version:
        xcode_update_version_number = ['agvtool', 'new-marketing-version', version]
        check_output(xcode_update_version_number)

    current_build_number = get_build_number()

    new_build_number = current_build_number + 1
    new_build_tag = 'build-' + str(new_build_number)

    log.debug("Creating build tag '" + new_build_tag + "'")

    xcode_update_build_number = ['agvtool', 'new-version', '-all', str(new_build_number)]
    check_output(xcode_update_build_number)

    git_add_changes_args = ['git', 'add', '-A']
    check_output(git_add_changes_args)

    git_commit_message = 'Increment CFBundleVersion to ' + str(new_build_number)

    git_commit_changes_args = ['git', 'commit', '-m', git_commit_message]
    check_output(git_commit_changes_args)

    git_make_version_tag_args = ['git', 'tag', '-a', new_build_tag, '-m', git_commit_message]
    check_output(git_make_version_tag_args)

    git_push_build_tag_args = ['git', 'push', 'origin', new_build_tag]
    check_output(git_push_build_tag_args)

    git_push_changes_args = ['git', 'push']
    check_output(git_push_changes_args)

def get_build_number():

    get_remote_build_numbers_args = ['git', 'ls-remote', '--tags'] 
    remote_build_numbers_git_output = check_output(get_remote_build_numbers_args, should_log=False).split('\n')
    
    build_number = -1

    for line in remote_build_numbers_git_output:
        split_line = line.split('refs/tags/build-')
        if len(split_line) >= 2:
            try:
                number = int(split_line[1].split('-')[0])
                if number > build_number:
                    build_number = number
            except ValueError:
                continue

    if build_number >= 0:
        return build_number

    #we need to check the plist for this, lets use agvtool
    get_local_build_version_args = ['agvtool', 'what-version', '-terse']
    local_build_number_agv_output = check_output(get_local_build_version_args)
    try:
        build_number = int(local_build_number_agv_output)
    except ValueError:
        pass
    
    return build_number

def isHeadDetached():
    get_current_branch = ['git', 'branch'] 
    branches = check_output(get_current_branch).split('\n')
    for branch in branches:
        if '*' in branch[:1]:
            #this is our branch
            if 'HEAD detached at ' in branch:
                return True
    return False


def doesBranchExistOnRemote(branch_name):
    git_list_remote_branches = ['git', 'branch', '-r']
    remote_branches = check_output(git_list_remote_branches).split('\n')
    remote_branches = map(str.strip, remote_branches)
    current_branch = 'origin/' + branch_name
    return current_branch in remote_branches

