# -*- coding: utf-8 -*-

import errno
import os


def makedirs(path):
    """Create all directories in path that do not already exist."""
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise
