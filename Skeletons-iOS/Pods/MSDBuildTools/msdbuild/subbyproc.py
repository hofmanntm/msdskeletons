# -*- coding: utf-8 -*-

import logging as _logging
from Queue import Queue
import subprocess
from threading import Thread


log = _logging.getLogger(__name__)
log.setLevel(_logging.DEBUG)
log.addHandler(_logging.NullHandler())


def _ugh_threads(args, line_processor):
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    q = Queue()
    
    def _read_from_file(f, name, q):
        while True:
            line = f.readline()
            q.put((name, line))
            if not line:
                break
    
        f.close()
    
    Thread(target=_read_from_file, args=(p.stdout, 'stdout', q)).start()
    Thread(target=_read_from_file, args=(p.stderr, 'stderr', q)).start()
    
    closed = set()
    while closed != {'stdout', 'stderr'}:
        who, line = q.get()
        if line:
            line_processor(who, line)
        else:
            closed.add(who)
    
    p.wait()
    
    if p.returncode != 0:
        raise subprocess.CalledProcessError(p.returncode, args)


def check_call(args):
    """Run args, a command and some arguments, while logging its stdout/stderr.
    
    Waits for command to complete.
    
    Raises:
        subprocess.CalledProcessError -- If the return code is not zero.
    """
    
    def each_line(who, line):
        log.info("%s", line.rstrip())
    
    _ugh_threads(args, each_line)


def check_output(args, should_log=True):
    """Run args, a command and some arguments, while optionally logging its stdout/stderr.
    
    Waits for command to complete.
    
    Returns the command's stdout as a string.
    """
    
    lines = []
    
    def each_line(who, line):
        if should_log:
            log.info("%s", line.rstrip())
        
        if who == 'stdout':
            lines.append(line)
    
    _ugh_threads(args, each_line)
    
    return b"".join(lines)
