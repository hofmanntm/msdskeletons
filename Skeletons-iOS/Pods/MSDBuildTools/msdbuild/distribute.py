#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .subbyproc import check_call
from .util import makedirs
from .xcode import ProjectInfo, WorkspaceInfo, find_build_configuration, find_schemes, find_project_path, find_workspace_path, get_provisioning_profile_info_from_archive, rename_xcarchive, write_export_options
import argparse
import logging as _logging
import os
import sys
import textwrap


log = _logging.getLogger(__name__)
log.setLevel(_logging.DEBUG)
log.addHandler(_logging.NullHandler())




def submit_to_crashlytics(workspace, scheme, build_dir, build_settings, archive_path):
    """Exports an .ipa from an .xcarchive and uploads the .ipa to Crashlytics Beta."""
    
    submit_path = build_settings.get('CRASHLYTICS_BETA_SUBMIT_PATH')
    if not submit_path:
        log.info("Skipping Crashlytics submission. Set CRASHLYTICS_BETA_SUBMIT_PATH to submit to Crashlytics Beta.")
        return

    log.debug("Attempting to submit to Crashlytics")
    
    api_key = build_settings.get('CRASHLYTICS_API_KEY', "").strip()
    build_secret = build_settings.get('CRASHLYTICS_BUILD_SECRET', "").strip()
    
    if not all((api_key, build_secret)):
        msg = "CRASHLYTICS_BETA_SUBMIT_PATH is set but missing CRASHLYTICS_API_KEY or CRASHLYTICS_BUILD_SECRET, which are needed to submit to Crashlytics Beta. Either clear CRASHLYTICS_BETA_SUBMIT_PATH or provide both CRASHLYTICS_API_KEY and CRASHLYTICS_BUILD_SECRET"
        log.error(msg)
        raise Exception(msg)
    
    groups = build_settings.get('CRASHLYTICS_BETA_GROUP_ALIASES', "").strip()
    notify = build_settings.get('CRASHLYTICS_BETA_NOTIFICATIONS', "").strip().lower() == "yes"
    preferred_export_method = build_settings.get('CRASHLYTICS_EXPORT_METHOD', "ad-hoc").strip()
    
    export_options_path = os.path.join(build_dir, 'exportOptions-Crashlytics.plist')
    write_export_options(export_options_path,
                         provisioning_profiles=get_provisioning_profile_info_from_archive(archive_path),
                         method=preferred_export_method)
    
    ipa_name = "{}-Crashlytics".format(scheme)
    rename_xcarchive(archive_path, ipa_name)
    
    ipa_path = os.path.join(build_dir, "{}.ipa".format(ipa_name))
    
    export_args = [
        'xcrun', 'xcodebuild',
        '-exportArchive',
        '-archivePath', archive_path,
        '-exportPath', os.path.dirname(ipa_path),
        '-exportOptionsPlist', export_options_path,
    ]
    
    log.debug("Calling {}".format(export_args))
    check_call(export_args)
    
    submit_args = [
        submit_path, api_key, build_secret,
        '-ipaPath', ipa_path,
    ]
    if groups:
        submit_args += ['-groupAliases', groups]
    if notify:
        submit_args += ['-notifications', 'YES']
    
    log.debug("Calling {}".format(submit_args))
    check_call(submit_args)
    
    
    # upload dSYMs if specified
    upload_dsyms_path = build_settings.get('CRASHLYTICS_DSYMS_UPLOAD_PATH')
    if not upload_dsyms_path:
        log.info("Skipping uploading dSYMs to Crashlytics. Set CRASHLYTICS_DYMS_UPLOAD_PATH to upload dSYMs.")
        return
    
    log.debug("Attempting to uploads dSYMs to Crashlytics")
    
    dsyms_folder = "{}/dSYMS".format(archive_path)
        
    if not os.path.isdir(dsyms_folder):
        log.info("Skipping uploading dSYMs to Crashlytics. Enable dSYMs in Build Options.")
        return
    
    upload_dsyms_args = [
        upload_dsyms_path,
        '-a', api_key,
        '-p', 'ios',
        dsyms_folder
    ]
    
    log.debug("Calling {}".format(upload_dsyms_args))
    check_call(upload_dsyms_args)


def main(raw_args):
    log_format = "[%(asctime)s](%(levelname).1s) %(message)s"
    _logging.basicConfig(format=log_format, level=_logging.DEBUG)

    parser = argparse.ArgumentParser(description="Distribute an Xcode archive.", epilog=textwrap.dedent("""\
        The simplest invocation looks something like:
        
            distribute beta --archive build/CoolApp.xcarchive
        
        
        That will:
        
            * Use the workspace found in the current directory.
            * Use the scheme with the same name as the workspace.
            * Use a build configuration called "Beta" (or "beta", it's case-insensitive).
            * Export a .ipa for sending to Crashlytics Beta and/or iTunes Connect.
        
        If any conventions fail (e.g. there's no scheme with the same name as the workspace), you'll have to specify that part manually:
        
            distribute beta "Cool App" --archive build/CoolApp.xcarchive
            distribute --workspace CoolApp.xcworkspace beta "Cool App" --archive build/CoolApp.xcarchive
        
        This build script acts on certain build settings:
        
            CRASHLYTICS_BETA_SUBMIT_PATH -- Path to the Crashlytics `submit` tool. If set, a build is uploaded to Crashlytics Beta.
            CRASHLYTICS_DSYMS_UPLOAD_PATH -- Path to Fabric's `upload-symbols` tool. If set, the dSYMs for a build are uploaded to Crashlytics Beta.            
            CRASHLYTICS_API_KEY -- Needed to submit to Crashlytics Beta.
            CRASHLYTICS_BUILD_SECRET -- Needed to submit to Crashlytics Beta.
            CRASHLYTICS_BETA_GROUP_ALIASES -- Comma-separated list of groups to grant access to the beta.
            CRASHLYTICS_BETA_NOTIFICATIONS -- Set to "YES" to notify everyone that there's a new beta.
            CRASHLYTICS_EXPORT_METHOD -- Exported archives for Crashlytics get resigned with ad-hoc by default. If set, this value will be used instead.
            
            TESTFLIGHT_USERNAME -- Username (probably an email address) for iTunes Connect. If set, a build is uploaded to TestFlight.
        """), formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('--archive', required=True,
                        help="Path to the Xcode archive")
    parser.add_argument('--workspace',
                        help="Path to the Xcode workspace (defaults to the only workspace in the current directory)")
    parser.add_argument('configuration',
                        help="The build configuration to use (case-insensitive)")
    parser.add_argument('scheme', nargs='?', default="",
                        help="The scheme to build (defaults to the scheme with the same name as the workspace)")

    args = parser.parse_args(raw_args)
    
    plan_lines = ["Plan:"]
    def log_plan():
        for line in plan_lines:
            log.info(line)
    
    
    workspace_path = find_workspace_path(args.workspace or os.getcwd())
    workspace = WorkspaceInfo.load_path(workspace_path)
    
    plan_lines.append("  Workspace:      {}".format(workspace.path))
    
    project_path = find_project_path(workspace)
    project = ProjectInfo.load_path(project_path)
    
    if not args.configuration:
        parser.print_usage()
        
        log_plan()
        
        log.info("Please choose a configuration:")
        for configuration in sorted(project.configurations):
            log.info("  {}".format(configuration))
        
        return 1
    
    configuration = find_build_configuration(project, args.configuration)
    
    plan_lines.append("  Configuration:  {}".format(configuration))
    
    schemes = find_schemes(workspace, project, [args.scheme])
    
    if not schemes:
        parser.print_usage()
        
        log_plan()
        
        log.info("Could not decide which scheme to run. Did you want one of:")
        for scheme in sorted(project.schemes):
            log.info("  {}".format(scheme))
        
        log.info("Or one of these:")
        for scheme in sorted(set(workspace.schemes) - set(project.schemes)):
            log.info("  {}".format(scheme))
        
        return 1
    
    scheme = schemes[0]
    
    plan_lines.append("  Scheme:        {}".format(scheme))
    
    build_dir = os.path.join(os.path.split(project.path)[0], "build")
    makedirs(build_dir)

    log_path = os.path.join(build_dir, "distribute.log")
    file_handler = _logging.FileHandler(log_path, mode='w')
    file_handler.setFormatter(_logging.Formatter(log_format))
    _logging.getLogger().addHandler(file_handler)
    
    log_plan()
    
    build_settings = workspace.build_settings(scheme, configuration)
    submit_to_crashlytics(workspace, scheme, build_dir, build_settings, args.archive)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]) or 0)
