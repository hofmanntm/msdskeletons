# -*- coding: utf-8 -*-

from .subbyproc import check_call
from .xcode import find_build_configuration, ProjectInfo, ProvisioningProfileInfo
import argparse
import collections
import glob
import json
import logging as _logging
import os
import re
import shutil
import sys
import tempfile

log = _logging.getLogger(__name__)
log.setLevel(_logging.DEBUG)
log.addHandler(_logging.NullHandler())


class WrongNumberOfXcodeProjectsError(Exception):
    pass


def find_project_path(path):
    """Returns the path to an Xcode project in a directory."""
    parts = list(os.path.split(path))
    if not parts[-1]:
        parts.pop()
    if parts[-1].endswith('.xcodeproj'):
        return path
    
    paths = glob.glob(os.path.join(path, "*.xcodeproj"))
    count = len(paths)
    
    if count != 1:
        raise WrongNumberOfXcodeProjectsError("expected one project but found {}".format(count))
    
    return paths[0]


def load_profiles(project_info, configurations=None, targets=None):
    """Returns a list of ProvisioningProfileInfo for all combinations of configuration and targets. The list contains no duplicates.
    
    Arguments:
        configuration -- The name of a build configuration (defaults to all build configurations).
        targets -- A list of target names (defaults to all targets).
    """
    if configurations is None:
        configurations = project_info.configurations
    else:
        configurations = [find_build_configuration(project_info, c) for c in configurations]
    
    if targets is None:
        targets = project_info.targets
    
    log.debug("Checking for profiles:")
    log.debug("  in configuration{}: {}".format(
        "" if len(configurations) == 1 else "s",
        ", ".join(configurations)))
    log.debug("  in target{}: {}".format(
        "" if len(targets) == 1 else "s",
        ", ".join(targets)))
    
    profiles = {}
    for config in configurations:
        for target in targets:
            settings = project_info.build_settings(config, target)
            try:
                info = ProvisioningProfileInfo(settings)
            except ValueError:
                continue
            
            key = (info.team, info.identity, info.name, info.wildcard or info.bundle_id)
            if key not in profiles:
                log.debug("Found profile: {} ({})".format(info.name, info.team))
                profiles[key] = info
    
    return sorted(profiles.itervalues(), key=lambda p: p.name)


def update_profile(profile_info):
    """Downloads the latest version of a provisioning profile from the Apple Developer portal."""
    if profile_info.development and profile_info.wildcard:
        app_identifier = "*"
    else:
        app_identifier = profile_info.bundle_id
    
    args = [
        'fastlane', 'sigh',
        '--username', "iosdev@mindsea.com",
        '--team_id', profile_info.team,
        '--app_identifier', app_identifier,
        '--provisioning_name', profile_info.name,
        '--ignore_profiles_with_different_name',
        '--platform', "ios",
    ]
    
    if profile_info.development:
        args += ["--development", "--force"]
    elif profile_info.ad_hoc:
        args += ["--adhoc", "--force"]
    else:
        # default is app store and there's no corresponding flag because sigh is a wonderful tool
        pass

    # sigh unconditionally dumps out a .mobileprovision file somewhere, which is not helpful. So we give it a temp folder to play in, which we'll delete later.
    temp_folder = tempfile.mkdtemp()
    args += ['--output_path', temp_folder]
    
    print(args)
    check_call(args)
    
    shutil.rmtree(temp_folder, ignore_errors=True)


def run_print(profiles):
    by_team = collections.defaultdict(list)
    for profile in profiles:
        by_team[profile.team].append(profile.name)
    
    for team, profiles in by_team.iteritems():
        print("Development Team {}:".format(team))
        
        for profile in sorted(set(profiles)):
            print("    {}".format(profile))


def run_json(profiles):
    dicts = [p.__dict__ for p in profiles]
    print(json.dumps(dicts, indent=4, separators=(',', ': '), sort_keys=True))


def run_update(profiles):
    count = len(profiles)
    log.debug("Will update {} profile{}".format(count, "" if count == 1 else "s"))
    
    os.environ["FASTLANE_SKIP_UPDATE_CHECK"] = ""
    
    for profile in profiles:
        log.debug("Updating profile {}".format(profile.name))
        update_profile(profile)


def main(raw_args):
    log_format = "[%(asctime)s](%(levelname).1s) %(message)s"
    _logging.basicConfig(format=log_format, level=_logging.DEBUG)
    
    parser = argparse.ArgumentParser(description="Manage provisioning profiles for an Xcode project.")
    subs = parser.add_subparsers(title="commands")

    def add_common_args(parser):
        group = parser.add_argument_group(title="Xcode project parameters")
        group.add_argument('--project',
                           help="The Xcode project to search for provisioning profile names (default is the only project in the current directory)")
        group.add_argument('--configuration', nargs='*',
                           help="The build configuration to search for a provisioning profile name (default is all configurations)")
        group.add_argument('--target', nargs='*',
                           help="The Xcode targets to search for provisioning profile names (default is all targets)")
    
    just_print = subs.add_parser('print',
                                 help="Print all the provisioning profiles from the Xcode project")
    just_print.set_defaults(func=run_print)
    add_common_args(just_print)
    
    just_json = subs.add_parser('json',
                                help="Output all the provisioning profiles from the Xcode project as JSON")
    just_json.set_defaults(func=run_json)
    add_common_args(just_json)
    
    update = subs.add_parser('update',
                             help="Install up-to-date provisioning profiles from the Apple Developer portal")
    update.set_defaults(func=run_update)
    add_common_args(update)
    
    args = parser.parse_args(raw_args)
    
    project_path = find_project_path(args.project or os.getcwd())
    
    log.debug("Loading {}".format(os.path.basename(os.path.normpath(project_path))))
    project_info = ProjectInfo.load_path(project_path)
    
    log.debug("Finding profiles")
    profiles = load_profiles(project_info, args.configuration, args.target)
    
    if not profiles:
        eprint("Couldn't find any provisioning profiles")
        return 1
    
    args.func(profiles)


if __name__ == '__main__':
    main(sys.argv[1:])
