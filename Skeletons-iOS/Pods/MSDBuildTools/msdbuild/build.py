# -*- coding: utf-8 -*-

from .distribute import submit_to_crashlytics
from .subbyproc import check_call, check_output
from .util import makedirs
from .vcs import tag_and_push
from .xcode import ProjectInfo, WorkspaceInfo, find_build_configuration, find_schemes, find_project_path, find_workspace_path, get_provisioning_profile_info_from_archive, rename_xcarchive, write_export_options
import argparse
import glob
import logging as _logging
import os
import plistlib
import re
import shutil
import subprocess
import sys
import textwrap
import time


log = _logging.getLogger(__name__)
log.setLevel(_logging.DEBUG)
log.addHandler(_logging.NullHandler())


RESEMBLES_BUILD_SETTING = re.compile(r"[A-Z]\S*=")

BUILD_DIRECTORY_NAME = "build.noindex"


def find_altool_path():
    """Returns the path to altool, the command-line interface to Application Loader.

    The returned path takes the active developer directory into account.
    """

    developer_dir = check_output(['xcode-select', '-p'])
    return os.path.abspath(os.path.join(
        developer_dir, '..',
        'Applications',
        'Application Loader.app',
        'Contents', 'Frameworks',
        'ITunesSoftwareService.framework', 'Versions', 'Current',
        'Support', 'altool'
    ))


def keychain_item_name(itc_username):
    """Returns the name of the Keychain item for storing an iTunes Connect password."""
    return 'msdbuild {} (iTunes Connect)'.format(itc_username)


def write_export_options(path, provisioning_profiles, method):
    """Writes a plist file to path suitable for exporting an xcarchive using method."""

    plist = {
        "compileBitcode": False,
        "uploadBitcode": False,
        "method": method,
        "provisioningProfiles": provisioning_profiles
    }
    if method == "development":
        plist["signingCertificate"] = "iPhone Developer"
    plistlib.writePlist(plist, path)


def rename_xcarchive(archive_path, name):
    """Modifies the .xcarchive's name in its Info.plist.

    This is useful because exported .ipa files take their basename from the .xcarchive's name.
    """

    info_plist_path = os.path.join(archive_path, "Info.plist")
    plist = plistlib.readPlist(info_plist_path)
    plist["Name"] = name
    plistlib.writePlist(plist, info_plist_path)

def get_provisioning_profile_info_from_archive(archive_path):
    info_plist_path = os.path.join(archive_path, "Info.plist")
    plist = plistlib.readPlist(info_plist_path)

    bundle_id = plist["ApplicationProperties"]["CFBundleIdentifier"]
    application_path = os.path.join(os.path.join(archive_path, "Products"), plist["ApplicationProperties"]["ApplicationPath"])
    profile_path = os.path.join(application_path, "embedded.mobileprovision")

    read_profile_args = ['security', 'cms', '-D', '-i', profile_path.encode('utf-8')]
    profile_plist_string = check_output(read_profile_args, should_log=False)
    profile_plist = plistlib.readPlistFromString(profile_plist_string)
    profile_uuid = profile_plist["UUID"]

    return { bundle_id : profile_uuid }


def ensure_itunes_connect_password(build_settings):
    """Verifies that there's a Keychain item for the iTunes Connect username specified in build_settings.

    If there is no such item, the user is prompted to add one.

    Returns the iTunes Connect username from the build settings, or None if no such username is specified.
    """
    itc_username = build_settings.get('ITUNES_CONNECT_USERNAME', "").strip()
    if not itc_username:
        return

    try:
        # altool looks up "generic" (not "internet") passwords by service. It seems to take the account (the username you pass in) into consideration, so it's ok to have multiple passwords with the same service.
        check_call([
            'security', 'find-generic-password',
            '-s', 'msdbuild (iTunes Connect)',
            '-l', keychain_item_name(itc_username),
            '-a', itc_username,
        ])
        log.info("Found iTC username and password successfully")
        return itc_username

    except subprocess.CalledProcessError as e:
        if e.returncode != 44:
            log.error("Unexpected error: {}".format(e.returncode))
            return

    # Migrate old keychain items. altool doesn't look for "internet" passwords.
    try:
        old_password = check_output([
            'security', 'find-internet-password',
            '-s', 'msdbuildtools.developers@mindsea.com',
            '-a', itc_username,
            '-w',
        ], should_log=False) # Skip log so we don't leak password.

        log.info("Migrating iTC username and password from previous location in keychain…")
        check_call([
            'security', 'add-generic-password',
            '-s', 'msdbuild (iTunes Connect)',
            '-l', keychain_item_name(itc_username),
            '-a', itc_username,
            '-w', old_password,
        ])
        log.info("Successfully migrated iTC username and password.")

    except subprocess.CalledProcessError:
        # Migration can fail because there's nothing to migrate; that's ok!
        log.info("It seems you would like to send your build to TestFlight, but the password for {} is missing from the keychain.".format(itc_username))
        log.info("Press Control-C to cancel the build.")
        log.info("iTunes Connect password for {} (check 1Password):".format(itc_username))
        check_call([
            'security', 'add-generic-password',
            '-s', 'msdbuild (iTunes Connect)',
            '-l', keychain_item_name(itc_username),
            '-a', itc_username,
            '-w',
        ])
        log.info("Added iTC password to keychain")

    return itc_username


def submit_to_crashlytics(workspace, scheme, build_dir, build_settings, archive_path):
    """Exports an .ipa from an .xcarchive and uploads the .ipa to Crashlytics Beta."""

    submit_path = build_settings.get('CRASHLYTICS_BETA_SUBMIT_PATH')

    # Sometimes paths are quoted in Xcode build settings, which generally isn't a bad idea but we need to strip them if present.
    if submit_path:
        if submit_path.startswith('"'):
            submit_path = submit_path[1:]
        if submit_path.endswith('"'):
            submit_path = submit_path[:-1]

    if not submit_path:
        log.info("Skipping Crashlytics submission. Set CRASHLYTICS_BETA_SUBMIT_PATH to submit to Crashlytics Beta.")
        return

    log.debug("Attempting to submit to Crashlytics")

    api_key = build_settings.get('CRASHLYTICS_API_KEY', "").strip()
    build_secret = build_settings.get('CRASHLYTICS_BUILD_SECRET', "").strip()

    if not all((api_key, build_secret)):
        msg = "CRASHLYTICS_BETA_SUBMIT_PATH is set but missing CRASHLYTICS_API_KEY or CRASHLYTICS_BUILD_SECRET, which are needed to submit to Crashlytics Beta. Either clear CRASHLYTICS_BETA_SUBMIT_PATH or provide both CRASHLYTICS_API_KEY and CRASHLYTICS_BUILD_SECRET"
        log.error(msg)
        raise Exception(msg)

    groups = build_settings.get('CRASHLYTICS_BETA_GROUP_ALIASES', "").strip()
    notify = build_settings.get('CRASHLYTICS_BETA_NOTIFICATIONS', "").strip().lower() == "yes"
    preferred_export_method = build_settings.get('CRASHLYTICS_EXPORT_METHOD', "ad-hoc").strip()

    export_options_path = os.path.join(build_dir, 'exportOptions-Crashlytics.plist')
    write_export_options(export_options_path, provisioning_profiles=get_provisioning_profile_info_from_archive(archive_path) ,method=preferred_export_method)

    ipa_name = "{}-Crashlytics".format(scheme)
    rename_xcarchive(archive_path, ipa_name)

    ipa_path = os.path.join(build_dir, "{}.ipa".format(ipa_name))

    export_args = [
        'xcrun', 'xcodebuild',
        '-exportArchive',
        '-archivePath', archive_path,
        '-exportPath', os.path.dirname(ipa_path),
        '-exportOptionsPlist', export_options_path,
    ]

    log.debug("Calling {}".format(export_args))
    check_call(export_args)

    submit_args = [
        submit_path, api_key, build_secret,
        '-ipaPath', ipa_path,
    ]
    if groups:
        submit_args += ['-groupAliases', groups]
    if notify:
        submit_args += ['-notifications', 'YES']

    log.debug("Calling {}".format(submit_args))
    check_call(submit_args)


    # upload dSYMs if specified
    upload_dsyms_path = build_settings.get('CRASHLYTICS_DSYMS_UPLOAD_PATH')
    if not upload_dsyms_path:
        log.info("Skipping uploading dSYMs to Crashlytics. Set CRASHLYTICS_DYMS_UPLOAD_PATH to upload dSYMs.")
        return

    log.debug("Attempting to uploads dSYMs to Crashlytics")

    dsyms_folder = "{}/dSYMS".format(archive_path)

    if not os.path.isdir(dsyms_folder):
        log.info("Skipping uploading dSYMs to Crashlytics. Enable dSYMs in Build Options.")
        return

    upload_dsyms_args = [
        upload_dsyms_path,
        '-a', api_key,
        '-p', 'ios',
        dsyms_folder
    ]

    log.debug("Calling {}".format(upload_dsyms_args))
    check_call(upload_dsyms_args)


def submit_to_testflight(workspace, scheme, build_dir, build_settings, archive_path, itc_username):
    """Exports an .ipa from an .xcarchive and uploads the .ipa to iTunes Connect."""

    if not itc_username:
        log.warning("Skipping TestFlight submission. Set ITUNES_CONNECT_USERNAME build setting to submit to TestFlight.")
        return

    log.debug("Attempting to submit to TestFlight")

    export_options_path = os.path.join(build_dir, 'exportOptions-TestFlight.plist')
    write_export_options(export_options_path, provisioning_profiles=get_provisioning_profile_info_from_archive(archive_path), method='app-store')

    ipa_name = "{}-TestFlight".format(scheme)
    rename_xcarchive(archive_path, ipa_name)

    ipa_path = os.path.join(build_dir, "{}.ipa".format(ipa_name))

    export_args = [
        'xcrun', 'xcodebuild',
        '-exportArchive',
        '-archivePath', archive_path,
        '-exportPath', os.path.dirname(ipa_path),
        '-exportOptionsPlist', export_options_path,
    ]

    log.debug("Calling {}".format(export_args))
    check_call(export_args)

    altool_args = [
        find_altool_path(),
        '--upload-app',
        '-f', ipa_path,
        '-u', itc_username,
        '-p', '@keychain:msdbuild (iTunes Connect)',
    ]

    log.debug("Calling {}".format(altool_args))
    check_call(altool_args)


def build(workspace, project, scheme, configuration, extra_build_settings, build_dir, is_dry_run):
    """Makes an Xcode archive and does things with it.

    Arguments:
        workspace -- An xcode.WorkspaceInfo instance.
        scheme -- A scheme in workspace.
        configuration -- The build configuration to use.
        extra_build_settings -- A (possibly empty) list of build settings to pass to xcodebuild.
        is_dry_run -- If this flag is set, avoid tag and push and attempted upload to Crashlytics and Testflight
        version -- If present, sets CFBundleShortVersionString to this value
    """
    build_settings = workspace.build_settings(scheme, configuration, extra_build_settings)

    if not is_dry_run:
        # let's get the iTC password up front so the user can go have a coffee during the build process
        itc_username = ensure_itunes_connect_password(build_settings)

    scheme_build_dir = os.path.join(build_dir, scheme)

    makedirs(scheme_build_dir)

    log.info("Building {} using {} configuration".format(workspace.name, configuration))

    archive_name = "{}.xcarchive".format(scheme)
    archive_path = os.path.join(scheme_build_dir, archive_name)
    log.debug("Creating archive at {}".format(archive_path))

    shutil.rmtree(archive_path, ignore_errors=True)

    archive_args = [
        'xcrun', 'xcodebuild', 'clean',
        '-workspace', workspace.path,
        '-scheme', scheme,
        '-configuration', configuration,
        '-destination', 'generic/platform=iOS',
        '-archivePath', archive_path,
        'archive',
    ] + extra_build_settings

    log.debug("Calling {}".format(archive_args))
    check_call(archive_args)

    build_settings = workspace.build_settings(scheme, configuration, extra_build_settings)

    if not is_dry_run:
        submit_to_crashlytics(workspace, scheme, scheme_build_dir, build_settings, archive_path)
        submit_to_testflight(workspace, scheme, scheme_build_dir, build_settings, archive_path, itc_username)
    else:
        # rename the xcarchive to something that Xcode won't open automatically
        dirty_archive_path = os.path.join(scheme_build_dir, "{}_ARCHIVE_DIRTY".format(scheme))
        shutil.rmtree(dirty_archive_path, ignore_errors=True)
        shutil.move(archive_path, dirty_archive_path)


def main(raw_args):
    log_format = "[%(asctime)s](%(levelname).1s) %(message)s"
    _logging.basicConfig(format=log_format, level=_logging.DEBUG)

    parser = argparse.ArgumentParser(description="Build an Xcode project.", epilog=textwrap.dedent("""\
        The simplest invocation looks something like:

            build beta


        That will:

            * Use the workspace found in the current directory.
            * Use the scheme with the same name as the workspace.
            * Use a build configuration called "Beta" (or "beta", it's case-insensitive).
            * Update the build number of the project then commit & tag the change

        If any conventions fail (e.g. there's no scheme with the same name as the workspace), you'll have to specify that part manually:

            build beta --scheme "Cool App"
            build beta --workspace CoolApp.xcworkspace --scheme "Cool App"

        You can give multiple schemes, handy if your project has multiple targets:

            build beta --scheme "Financial Post" "National Post"

        This build script acts on certain Xcode build settings:

            CRASHLYTICS_BETA_SUBMIT_PATH -- Path to the Crashlytics `submit` tool. If set, a build is uploaded to Crashlytics Beta.
            CRASHLYTICS_DSYMS_UPLOAD_PATH -- Path to Fabric's `upload-symbols` tool. If set, the dSYMs for a build are uploaded to Crashlytics Beta.
            CRASHLYTICS_API_KEY -- Needed to submit to Crashlytics Beta.
            CRASHLYTICS_BUILD_SECRET -- Needed to submit to Crashlytics Beta.
            CRASHLYTICS_BETA_GROUP_ALIASES -- Comma-separated list of groups to grant access to the beta.
            CRASHLYTICS_BETA_NOTIFICATIONS -- Set to "YES" to notify everyone that there's a new beta.
            CRASHLYTICS_EXPORT_METHOD -- Exported archives for Crashlytics get resigned with ad-hoc by default. If set, this value will be used instead.

            ITUNES_CONNECT_USERNAME -- Username (probably an email address) for iTunes Connect. If set, a build is uploaded to TestFlight.
            You will be prompted for your iTC password, and it (along with the username) will be stored in the keychain.

        """), formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('configuration', nargs='?',
                        help="The build configuration to use (case-insensitive)")
    parser.add_argument('--workspace',
                        help="Path to the Xcode workspace (defaults to the only workspace in the current directory)")
    parser.add_argument('--schemes', nargs='+', metavar="SCHEME", default=[],
                        help="The schemes to build (defaults to the scheme with the same name as the workspace)")
    parser.add_argument('--gccflags', nargs='*', metavar="DEBUG=1", default=[],
                        help="One or more GCC_PREPROCESSOR_DEFINITIONS")
    parser.add_argument('--swiftflags', nargs='*', metavar="DEBUG", default=[],
                        help="One or more SWIFT_ACTIVE_COMPILATION_CONDITIONS")
    parser.add_argument('build_settings', nargs='*', metavar="BUILD_SETTING=value", default=[],
                        help="Anything resembling a build setting is passed to xcodebuild verbatim")
    parser.add_argument('--keepBuildNumber', action='store_true',
                        help="Don't increment the build number. This only works if you are on a build tag")
    parser.add_argument('--dry_run', action='store_true',
                        help="Don't tag or attempt to upload to Crashlytics or Testflight")
    parser.add_argument('--version',
                        help="Sets CFBundleShortVersionString to this value if present")

    args = parser.parse_args(raw_args)

    plan_lines = ["Plan:"]
    def log_plan():
        if args.dry_run:
            log.info("========================= DRY RUN =========================")
            log.info("    - Skipping tag")
            log.info("    - Skipping uploads")
            log.info("    - Archive will be saved as _ARCHIVE_DIRTY folder")
            log.info("===========================================================")
            time.sleep(2) # give the user time to think about what they've done

        for line in plan_lines:
            log.info(line)


    workspace_path = find_workspace_path(args.workspace or os.getcwd())
    workspace = WorkspaceInfo.load_path(workspace_path)

    plan_lines.append("  Workspace:      {}".format(workspace.path))

    project_path = find_project_path(workspace)
    project = ProjectInfo.load_path(project_path)

    if not args.configuration:
        parser.print_usage()

        log_plan()

        log.info("Please choose a configuration:")
        for configuration in sorted(project.configurations):
            log.info("  {}".format(configuration))

        return 1

    configuration = find_build_configuration(project, args.configuration)

    plan_lines.append("  Configuration:  {}".format(configuration))


    schemes = find_schemes(workspace, project, args.schemes)

    if not schemes:
        parser.print_usage()

        log_plan()

        log.info("Could not decide which scheme to run. Did you want any of:")
        for scheme in sorted(project.schemes):
            log.info("  {}".format(scheme))

        log.info("Or any of these:")
        for scheme in sorted(set(workspace.schemes) - set(project.schemes)):
            log.info("  {}".format(scheme))

        return 1

    plan_lines.append("  Schemes:        {}".format(", ".join(schemes)))

    build_settings = [b for b in args.build_settings
                      if re.match(RESEMBLES_BUILD_SETTING, b)]

    if args.gccflags:
        build_settings.append("GCC_PREPROCESSOR_DEFINITIONS=${{inherited}} {}".format(" ".join(args.gccflags)))

    if args.swiftflags:
        build_settings.append("SWIFT_ACTIVE_COMPILATION_CONDITIONS=${{inherited}} {}".format(" ".join(args.swiftflags)))

    if build_settings:
        plan_lines.append("  Build settings: {}".format(", ".join(build_settings)))


    build_dir = os.path.join(os.path.split(project.path)[0], BUILD_DIRECTORY_NAME)
    makedirs(build_dir)

    log_path = os.path.join(build_dir, "build.log")
    file_handler = _logging.FileHandler(log_path, mode='w')
    file_handler.setFormatter(_logging.Formatter(log_format))
    _logging.getLogger().addHandler(file_handler)

    # TODO: release notes?
    log_plan()
    if not args.dry_run:
        tag_and_push(args.version, args.keepBuildNumber, log)

    for scheme in schemes:
        # TODO: run pprofiles? don't forget about watch/extension targets profiles
        build(workspace, project, scheme, configuration, build_settings, build_dir, args.dry_run)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]) or 0)
