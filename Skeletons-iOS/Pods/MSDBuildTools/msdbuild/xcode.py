# -*- coding: utf-8 -*-

from .subbyproc import check_call, check_output
import glob
import json
import os
import plistlib
import re
import subprocess
from xml.etree import ElementTree


class _BuildSettingsLoader(object):
    """Loads build settings from an Xcode workspace or project.
    
    Basically just prepares and calls an xcodebuild command.
    """
    
    BUILD_SETTING_LINE = re.compile(r"^\s+(\S+)\s+=\s+(.*)$", flags=re.MULTILINE)
    
    def __init__(self, flag, path, configuration, scheme_target_flag, scheme_or_target=None, extra_build_settings=None):
        self.flag = flag
        self.path = path
        self.configuration = configuration
        self.scheme_target_flag = scheme_target_flag
        self.scheme_or_target = scheme_or_target
        self.extra_build_settings = extra_build_settings
    
    @classmethod
    def from_workspace(cls, workspace_path, scheme, configuration, extra_build_settings=None):
        return cls("-workspace", workspace_path, configuration, "-scheme", scheme, extra_build_settings)
    
    @classmethod
    def from_project(cls, project_path, configuration, target=None, extra_build_settings=None):
        return cls("-project", project_path, configuration, "-target", target, extra_build_settings)
    
    @property
    def build_settings(self):
        args = ['xcrun', 'xcodebuild', self.flag, self.path, '-configuration', self.configuration]
        
        if self.scheme_or_target is not None:
            args += [self.scheme_target_flag, self.scheme_or_target]
        
        args.append('-showBuildSettings')
        
        if self.extra_build_settings is not None:
            args.extend(self.extra_build_settings)
        
        output = subprocess.check_output(args)
        
        parsed = dict(m.group(1, 2) for m in re.finditer(_BuildSettingsLoader.BUILD_SETTING_LINE, output))
        
        return parsed


class ProjectInfo(object):
    """The targets, schemes, build configurations, and build settings in an Xcode project."""
    
    def __init__(self, path, info):
        """Initializes project info from a JSON dictionary returned by `xcodebuild -list`."""
        self.path = path
        self._info = info
        self._build_settings = {}
    
    @classmethod
    def load_path(cls, path):
        """Initializes project info from a path to an Xcode project."""
        output = subprocess.check_output(['xcrun', 'xcodebuild', '-project', path, '-list', '-json'])
        info = json.loads(output)
        return cls(path, info)
    
    def build_settings(self, configuration, target=None, extra_build_settings=None):
        """Returns a dictionary of build settings for the Xcode project.
        
        Build settings are loaded once and cached per `(configuration, target, extra_build_settings)` tuple.
        
        Arguments:
            configuration -- The build configuration to use for build settings.
            target -- The target to use. If unspecified, `xcodebuild` decides what to return.
            extra_build_settings -- A list of build settings to specify on the xcodebuild command line. Each item should resemble `BUILD_SETTING=value`.
        """
        key = (configuration, target, tuple(extra_build_settings) if extra_build_settings else None)
        
        if key not in self._build_settings:
            loader = _BuildSettingsLoader.from_project(self.path,
                                                       configuration=configuration,
                                                       target=target,
                                                       extra_build_settings=extra_build_settings)
            self._build_settings[key] = loader.build_settings
            
        return self._build_settings[key]
    
    @property
    def configurations(self):
        """The names of all build configurations in the project."""
        return self._info["project"]["configurations"]
    
    @property
    def name(self):
        """The name of the Xcode project. Presumably the basename of the .xcodeproj."""
        return self._info["project"]["name"]
    
    @property
    def schemes(self):
        """A list of names of all schemes in the Xcode project."""
        return self._info["project"]["schemes"]
    
    @property
    def targets(self):
        """The names of all targets in the Xcode project."""
        return self._info["project"]["targets"]


class ProvisioningProfileInfo(object):
    IDENTITY_SEEMINGLY_DEVELOPER = re.compile(r"(?:iOS|iPhone)\s+Developer")
    PROFILE_NAME_SEEMINGLY_AD_HOC = re.compile(r"Ad[\s\-]*Hoc", flags=re.IGNORECASE)
    PROFILE_NAME_SEEMINGLY_WILDCARD = re.compile(r"wildcard|\*", flags=re.IGNORECASE)
    
    def __init__(self, build_settings):
        """Pulls provisioning profile info out of a dictionary of build settings.
        
        Raises:
            ValueError -- When a piece of information is missing from the build settings.
        """
        try:
            self.team = build_settings['DEVELOPMENT_TEAM']
            self.identity = build_settings['CODE_SIGN_IDENTITY']
            self.name = build_settings['PROVISIONING_PROFILE_SPECIFIER']
            self.bundle_id = build_settings['PRODUCT_BUNDLE_IDENTIFIER']
        except KeyError as e:
            raise ValueError(e.message)
    
    def __eq__(self, other):
        return other and (self.team == other.team and
                          self.identity == other.identity and
                          self.name == other.name and
                          self.bundle_id == other.bundle_id)
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __hash__(self):
        return hash((self.team, self.identity, self.name, self.bundle_id))
    
    @property
    def ad_hoc(self):
        """Returns True if, basedo n the name and identity, the profile is probably for ad hoc distribution."""
        return not self.development and re.search(ProvisioningProfileInfo.PROFILE_NAME_SEEMINGLY_AD_HOC, self.name)
    
    @property
    def app_store(self):
        """Returns True if, based on the name and identity, the profile is probably for App Store distribution."""
        return not any(self.ad_hoc, self.development)
    
    @property
    def development(self):
        """Returns True if, based on the code sign identity, the profile is probably for development."""
        return bool(re.search(ProvisioningProfileInfo.IDENTITY_SEEMINGLY_DEVELOPER, self.identity))
    
    @property
    def wildcard(self):
        """Returns True if, based on the name, the profile is probably a wildcard profile."""
        return bool(re.search(ProvisioningProfileInfo.PROFILE_NAME_SEEMINGLY_WILDCARD, self.name))


class WorkspaceInfo(object):
    """The schemes and projects in an Xcode workspace."""
    
    def __init__(self, path, info, xml):
        """Initializes workspace info.
        
        Arguments:
            path -- Location of the Xcode workspace (the .xcworkspace)
            info -- A JSON dictionary returned by `xcodebuild -list`
            xml -- The XML file within the workspace.
        """
        self.path = path
        self._info = info
        self._xml = xml
        self._build_settings = {}
    
    @classmethod
    def load_path(cls, path):
        """Initializes workspace info from a path to an Xcode workspace."""
        output = subprocess.check_output(['xcrun', 'xcodebuild', '-workspace', path, '-list', '-json'])
        info = json.loads(output)
        
        data_path = os.path.join(path, "contents.xcworkspacedata")
        with open(data_path) as data_file:
            data = data_file.read()
        
        return cls(path, info, data)
    
    def build_settings(self, scheme, configuration, extra_build_settings=None):
        """Returns a dictionary of build settings for the Xcode project.
        
        Build settings are loaded once and cached per `(configuration, target, extra_build_settings)` tuple.
        
        Arguments:
            scheme -- The scheme to use.
            configuration -- The build configuration to use for build settings.
            extra_build_settings -- A list of build settings to specify on the xcodebuild command line. Each item should resemble `BUILD_SETTING=value`.
        """
        key = (scheme, configuration, tuple(extra_build_settings) if extra_build_settings else None)
        
        if key not in self._build_settings:
            loader = _BuildSettingsLoader.from_workspace(self.path, scheme, configuration, extra_build_settings)
            self._build_settings[key] = loader.build_settings
            
        return self._build_settings[key]
    
    @property
    def name(self):
        """The name of the Xcode workspace. Presumably the basename of the .xcworkspace."""
        return self._info["workspace"]["name"]
    
    @property
    def projects(self):
        """A list of absolute paths to the Xcode projects in the workspace."""
        return _WorkspaceLocationResolver(self.path, self._xml).file_references
    
    @property
    def schemes(self):
        """The names of all schemes in the workspace."""
        return self._info["workspace"]["schemes"]


class _WorkspaceLocationResolver(object):
    """Figures out paths to projects in an Xcode workspace.
    
    Locations are prefixed with one of the following:
    
        absolute: -- Path seems to be relative to the directory containing the workspace? Not sure why this is distinct from container:.
        container: -- Path is relative to the directory containing the workspace.
        developer: -- Path is relative to the developer directory.
        group: -- Path is relative to its containing group's path (or the workspace path if there's no containing group).
    """
    
    def __init__(self, workspace_path, xml):
        self.workspace_path = workspace_path
        self.root = ElementTree.fromstring(xml)
        self.parent_map = {child: parent for parent in self.root.iter() for child in parent}
    
    @property
    def container_path(self):
        return os.path.abspath(os.path.join(self.workspace_path, '..'))
    
    @property
    def developer_path(self):
        return subprocess.check_output(['xcode-select', '-p']).strip()
    
    @property
    def file_references(self):
        """Returns a list of paths to everything referenced in the workspace."""
        
        def resolve(node):
            if node.tag == 'Workspace':
                return self.container_path
            
            (kind, path) = node.get('location').split(':', 1)
            if kind in ("absolute", "container"):
                return os.path.abspath(os.path.join(self.container_path, path))
            elif kind == "developer":
                return os.path.abspath(os.path.join(self.developer_path, path))
            elif kind == "group":
                return os.path.abspath(os.path.join(resolve(self.parent_map[node]), path))
            else:
                raise Exception("unknown location type {}".format(kind))
            
        return [resolve(node) for node in self.root.findall(".//FileRef[@location]")]


def find_build_configuration(project, user_input):
    """Returns the name of a build configuration in project that closely resembles user_input.
    
    Raises:
        ValueError -- When no build configurations resemble user_input.
    """
    tidied_input = user_input.strip()
    for config in project.configurations:
        if config.lower() == tidied_input.lower():
            return config
    
    raise ValueError("couldn't find configuration {} in list of known configurations {}".format(user_input, project.configurations))


def find_project_path(workspace):
    """Returns the path to what is probably the main project in the workspace.
    
    Raises:
        ValueError -- When there are no projects in the workspace.
        ValueError -- When the only projects in the workspace are hilariously unlikely to be the main project (e.g. Pods).
    """
    all_project_paths = workspace.projects
    if not all_project_paths:
        raise ValueError("workspace {} has no projects".format(workspace.path))
    
    def obviously_wrong(project_path):
        (name, ext) = os.path.splitext(os.path.basename(project_path))
        return name.lower() == "pods" or ext != ".xcodeproj"
    
    candidates = [p for p in all_project_paths if not obviously_wrong(p)]
    
    if not candidates:
        raise ValueError("workspace {} has no projects that seem like the main project")
    
    for project_path in candidates:
        (name, _) = os.path.splitext(os.path.basename(project_path))
        if name == workspace.name:
            return project_path
    
    return candidates[0]


def find_schemes(workspace, project, user_input_schemes):
    """Returns a list of schemes that are probably what the user wants to run.
    
    Arguments:
        user_input_schemes -- A (possibly empty) list of schemes that the user wants to build.
    
    Raises:
        ValueError -- When not all of the user_input_schemes appear in the workspace.
    """
    if user_input_schemes:
        schemes = [s for s in workspace.schemes if s in user_input_schemes]
    else:
        schemes = [s for s in workspace.schemes
                   if s.lower() == workspace.name.lower()]
    
    if not schemes and len(project.schemes) == 1:
        schemes = project.schemes
    
    remainder = [s for s in user_input_schemes if s not in schemes]
    if remainder and user_input_schemes:
        raise ValueError("could not find schemes {}".format(remainder))
    
    return schemes


def find_workspace_path(path):
    """Returns a path to the only Xcode workspace at path.
    
    Raises:
        IOError -- When there is no workspace at path, or when there are multiple workspaces at path.
    """
    curr = path
    while os.path.dirname(curr) != curr:
        (curr, tail) = os.path.split(curr)
        if tail.endswith(".xcworkspace"):
            return os.path.join(curr, tail)
    
    workspaces = glob.glob(os.path.join(path, "*.xcworkspace"))
    if len(workspaces) == 1:
        return workspaces[0]
    elif len(workspaces) > 1:
        raise IOError("too many workspaces in {}, please pick one".format(path))
    else:
        raise IOError("couldn't find a workspace in {}".format(path))


def get_provisioning_profile_info_from_archive(archive_path):
    info_plist_path = os.path.join(archive_path, "Info.plist")
    plist = plistlib.readPlist(info_plist_path)

    bundle_id = plist["ApplicationProperties"]["CFBundleIdentifier"]
    application_path = os.path.join(os.path.join(archive_path, "Products"), plist["ApplicationProperties"]["ApplicationPath"])
    profile_path = os.path.join(application_path, "embedded.mobileprovision")

    read_profile_args = ['security', 'cms', '-D', '-i', profile_path.encode('utf-8')]
    profile_plist_string = check_output(read_profile_args, should_log=False)
    profile_plist = plistlib.readPlistFromString(profile_plist_string)
    profile_uuid = profile_plist["UUID"]
    
    return { bundle_id : profile_uuid }


def rename_xcarchive(archive_path, name):
    """Modifies the .xcarchive's name in its Info.plist.
    
    This is useful because exported .ipa files take their basename from the .xcarchive's name.
    """

    info_plist_path = os.path.join(archive_path, "Info.plist")
    plist = plistlib.readPlist(info_plist_path)
    plist["Name"] = name
    plistlib.writePlist(plist, info_plist_path)


def write_export_options(path, provisioning_profiles, method):
    """Writes a plist file to path suitable for exporting an xcarchive using method."""
    
    plist = {
        "compileBitcode": False,
        "uploadBitcode": False,
        "method": method,
        "provisioningProfiles": provisioning_profiles
    }
    plistlib.writePlist(plist, path)
