# msdbuild

A Python library for building Xcode projects.

Actually a Python package with various useful bits for build scripts. There are a couple shims in the [../bin][] folder. More detailed documentation should probably go in either [../README.md][] or the relevant `.py` file.
