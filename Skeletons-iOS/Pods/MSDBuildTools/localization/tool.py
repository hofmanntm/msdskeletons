from optparse import OptionParser, Option, OptionValueError
from stringsdatabase import StringsDatabase
from os.path import basename

def strings_file_arg(arg):
    return pieces

parser = OptionParser(usage='usage: %prog [options] [strings files...]')
parser.add_option('-t', '--tsv-file')
parser.add_option('-f', '--fresh-file', action='store_true')
parser.add_option('-u', '--assume-unused', action='store_true')
parser.add_option('-d', '--default-language')
parser.add_option('-r', '--read-language')
parser.add_option('-w', '--write-language')
parser.add_option('-b', '--fill-blanks', action='store_true', help='Copy translated strings from others with identical source strings when missing.')
parser.add_option('-n', '--need-translation', metavar="FILE", help='Output a new TSV that has only strings with missing translations.')
args, strings_files = parser.parse_args()

if not args.default_language:
    raise Exception('Please specify a --default-language')

db = StringsDatabase(args.default_language)
tsv_needs_update = False

if args.fresh_file:
    tsv_needs_update = True
else:
    db.read_tsv_file(args.tsv_file)

if args.assume_unused:
    db.mark_all_unused()
    tsv_needs_update = True

if args.read_language:
    for path in strings_files:
        table_name = basename(path)
        db.read_strings_file(path, table_name, args.read_language, True)
        tsv_needs_update = True
    
if args.fill_blanks:
    db.fill_blanks_with_identical_strings()
    tsv_needs_update = True

if args.write_language:
    for path in strings_files:
        table_name = basename(path)
        db.write_strings_file(path, table_name, args.write_language, True)
    
if args.need_translation:
    db.write_tsv_file(args.need_translation, omit_translated=True)
    
if tsv_needs_update:
    db.write_tsv_file(args.tsv_file)
