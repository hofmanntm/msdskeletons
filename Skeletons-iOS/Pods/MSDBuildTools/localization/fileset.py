from tempfile import mkdtemp
import subprocess
from os import chmod, makedirs, rename
import os
from os.path import basename, dirname, exists, join, split
from stat import *
from shutil import rmtree
import codecs
import plistlib
import logging

def _subprocess_lines(command_args):
    return subprocess.Popen(command_args, stdout=subprocess.PIPE).communicate()[0].split()

def _localized_path(path, default_language, language):
    to_replace = default_language + '.lproj'
    replacement = language + '.lproj'

    pieces = path.split(os.sep)
    pieces = [replacement if piece == to_replace else piece for piece in pieces]
    return os.sep.join(pieces)

class FileSet(object):
    def __init__(self):
        self.mark_missing = True

    def import_strings(self, database, language=None):
        pass
    
    def export_strings(self, database):
        pass


class PlainFileSet(FileSet):
    '''For other flat files you'd like handled. Override read_strings and write_strings.'''

    def __init__(self, filenames, encoding='utf-8'):
        '''Pass an encoding to have the original/target files read as unicode strings with that encoding,
        or pass None to have them read as byte strings.   self._filenames = filenames'''
        super(PlainFileSet, self).__init__()
        self._encoding = encoding
        self._filenames = filenames

    def read_strings(self, original_file):
        '''Return a dictionary mapping from localized string keys to their unlocalized values.
        
        The unlocalized value can either be a str object (treated as the value), or a dict with 'value' and 'comment' keys.
        '''
        return {}

    def write_strings(self, strings, original, target):
        '''Given a dictionary mapping keys to localized values and the original file (a file object),
        should write the localized values into the target file object.'''
        pass
    
    def import_strings(self, database, language=None):
        if language is None:
            language = database.default_language

        for original_filename in self._filenames:
            if _localized_path(original_filename, database.default_language, '_') == original_filename:
                raise Exception('File name %s must be in a localized directory.' % original_filename)

            path = _localized_path(original_filename, database.default_language, language)

            if self._encoding:
                f = codecs.open(path, 'r', encoding=self._encoding)
            else:
                f = open(path, 'r')

            strings = self.read_strings(f)

            for key, info in strings.items():
                if isinstance(info, dict):
                    value = info['value']
                    comment = info.get('comment', None)
                else:
                    value = info

                table = basename(original_filename)
                database.add_string(table, key, value, comment=comment, language=language)
    
    def export_strings(self, database):
        for filename in self._filenames:
            table = basename(filename)
            for lang in database.languages:
                if lang == database.default_language:
                    continue
                target_path = _localized_path(filename, database.default_language, lang)
                strings = {}
                for string_info in database.strings_in_table(table):
                    localized = string_info.get(lang, None)
                    if localized:
                        strings[string_info['key']] = localized

                if exists(target_path):
                    chmod(target_path, 0664)
                
                if self._encoding:
                    target = codecs.open(target_path, 'w', encoding=self._encoding)
                else:
                    target = open(target_path, 'w')

                if self._encoding:
                    original = codecs.open(filename, 'r', encoding=self._encoding)
                else:
                    original = open(filename, 'r')


                self.write_strings(strings, original, target)
                target.close()

                chmod(target_path, 0444)

    def tables(self):
        return [basename(filename) for filename in self._filenames]



class PlistFileSet(PlainFileSet):
    def __init__(self, filenames, key_paths, comments={}):
        '''key_paths should be an sequence of key paths to localized in the plist.
        If an array is in your keypath, you should use '*' to have it iterate through all elements.
        So, the key_path "foo.bar.*.baz" if foo.bar resolves to an array, will localize
        the "baz" key of all the entries of that array.
        
        The comments argument should be a mapping from key paths to comments.
        '''
        super(PlistFileSet, self).__init__(filenames, encoding=None)
        self._key_paths = key_paths
        self._comments = comments

    def read_strings(self, original):
        strings = {}
        plist = plistlib.readPlist(original)
        for path in self._key_paths:
            pieces = path.split('.')
            
            try:
                self._collect_strings(plist, pieces, 0, strings, self._comments.get(path, None))
            except Exception:
                logging.exception("Trying to get strings for path %s", path)

        return strings

    def _collect_strings(self, current_object, pieces, piece_index, strings, comment):
        if len(pieces) > piece_index:
            this_piece = pieces[piece_index]
            if this_piece == '*':
                for index, value in enumerate(current_object):
                    pieces[piece_index] = str(index) # we populate this to get unique keys
                    self._collect_strings(current_object[index], pieces, piece_index + 1, strings, comment)
            else:
                next_object = current_object.get(this_piece, None)
                if next_object is not None:
                    self._collect_strings(next_object, pieces, piece_index + 1, strings, comment)
        else:
            # no pieces left
            strings['.'.join(pieces)] = {'value': current_object, 'comment': comment}

    def _insert_strings(self, current_object, pieces, piece_index, strings):
        if len(pieces) == piece_index + 1:
            # last piece
            this_piece = pieces[piece_index]
            if this_piece == '*':
                for index in range(len(current_object)):
                    pieces[piece_index] = str(index)
                    key = '.'.join(pieces)
                    default = current_object[index]
                    if self.mark_missing:
                        default = '***' + default + '***'
                    localized = strings.get(key, default)
                    current_object[index] = localized
            else:
                key = '.'.join(pieces)
                default = current_object[this_piece]
                if self.mark_missing:
                    default = '***' + default + '***'
                localized = strings.get(key, default)
                current_object[this_piece] = localized
        else:
            this_piece = pieces[piece_index]
            if this_piece == '*':
                for index, value in enumerate(current_object):
                    pieces[piece_index] = str(index)
                    self._insert_strings(current_object[index], pieces, piece_index + 1, strings)
            else:
                next_object = current_object.get(this_piece, None)
                if next_object is not None:
                    self._insert_strings(next_object, pieces, piece_index + 1, strings)
    
    def write_strings(self, strings, original, target):
        plist = plistlib.readPlist(original)

        for path in self._key_paths:
            pieces = path.split('.')
            self._insert_strings(plist, pieces, 0, strings)

        plistlib.writePlist(plist, target)

class SourceFileSet(FileSet):

    def __init__(self, roots, strings_dir):
        self._roots = roots
        self._strings_dir = strings_dir


    def _strings_path_for_language(self, language, name):
        return join(self._strings_dir, language + ".lproj/", name)

    def import_strings(self, database, language=None):
        if language is None:
            self._temp_dir = mkdtemp()
            m_files = _subprocess_lines(['find'] + self._roots + ['-name', '*.m', '-o', '-name', '*.mm'])
            subprocess.check_call(['genstrings', '-o', self._temp_dir] + m_files)

            self._temp_strings_files = _subprocess_lines(['find', self._temp_dir, '-name', '*.strings'])

            for path in self._temp_strings_files:
                table_name = basename(path)
                database.read_strings_file(path, table_name, database.default_language, True)
        else:
            # asked to import a particular language
            for temp_path in self._temp_strings_files:
                name = split(temp_path)[1]
                strings_path = self._strings_path_for_language(language, name)
                database.read_strings_file(strings_path, name, language, False)
    
    def export_strings(self, database):
        for language in database.languages:
            for temp_path in self._temp_strings_files:
                name = split(temp_path)[1]
                
                for lang in database.languages:
                    final_path = self._strings_path_for_language(lang, name)
                    if exists(final_path):
                        chmod(final_path, 0664)
                    if not exists(dirname(final_path)):
                        makedirs(dirname(final_path))
                    database.write_strings_file(final_path, name, lang, True, mark_missing=self.mark_missing)
                    chmod(final_path, 0444)
        
        rmtree(self._temp_dir)



class NIBFileSet(FileSet):

    def __init__(self, roots):
        self._roots = roots

    def _table_name_for_temp_path(self, path):
        return basename(path).replace('__' + self._import_language + '.lproj__', '__')

    def import_strings(self, database, language=None):
        self._import_language = language or database.default_language

        unlocalized_xib_files = _subprocess_lines(['find'] + self._roots + ['-name', '*.xib','-not', '-path', '*.lproj*'])
        for unlocalized in unlocalized_xib_files:
            new_dir = dirname(unlocalized) + "/" + self._import_language + ".lproj"
            if not exists(new_dir):
                makedirs(new_dir)
            rename(unlocalized, new_dir + '/' + basename(unlocalized))

        self._temp_dir = mkdtemp()
        self._xib_files = _subprocess_lines(['find'] + self._roots + ['-path', '*/'+self._import_language+'.lproj/*.xib'])
        for xib_file in self._xib_files:
            escaped_xib = xib_file.replace("/", "__") + ".strings"
            subprocess.check_call(['ibtool', '--export-strings-file', self._temp_dir + "/" + escaped_xib, xib_file])

        self._xib_strings_files = _subprocess_lines(['find', self._temp_dir, '-name', '*.xib.strings'])
        for strings_file in self._xib_strings_files:
            table = self._table_name_for_temp_path(strings_file)
            database.read_strings_file(strings_file, table, self._import_language, True)

    def export_strings(self, database):
        target_languages = [lang for lang in database.languages if lang != database.default_language]

        for lang in target_languages:
            for xib_strings_file in self._xib_strings_files:
                database.write_strings_file(xib_strings_file, self._table_name_for_temp_path(xib_strings_file), lang, True, mark_missing=self.mark_missing)
                base_xib_path = basename(xib_strings_file).replace("__", "/").replace(".strings", "")
                local_xib_path = base_xib_path.replace(os.sep + database.default_language + '.lproj' + os.sep, os.sep + lang + '.lproj' + os.sep)
                if not exists(dirname(local_xib_path)):
                    makedirs(dirname(local_xib_path))
                if exists(local_xib_path):
                    chmod(local_xib_path, 0664)
                _subprocess_lines(['ibtool', '--import-strings-file', xib_strings_file, '--write', local_xib_path, base_xib_path])
                chmod(local_xib_path, 0444)
        
        rmtree(self._temp_dir)
