import re
import csv
from unicodecsv import *


comment_regex = re.compile(r'\s*/\*(.*?)\*/', re.DOTALL) # match[1] is the comment
string_regex = re.compile(r'\s*"((?:[^"\\]|\\.)*?)"\s*=\s*"((?:[^"\\]|\\.)*?)"\s*;', re.DOTALL) # match[1] is the string contents, with backslash-escapes intact
end_regex = re.compile(r'\s*$', re.DOTALL) # match[1] is the string contents, with backslash-escapes intact

non_language_keys = ['comment', 'table', 'key', 'in_use']


def _uniform_newlines(value):
    return value.replace('\r\n', '\0').replace('\r', '\0').replace('\n', '\0').replace('\0', '\r\n')

class StringsDatabase:
    def __init__(self, default_language):
        self.strings = {}
        self.languages = []
        self.default_language = default_language
    
    def add_language(self, language):
        if language not in self.languages:
            self.languages.append(language)
    
    def remove_strings_matching(self, filter):
        for unique_key in self.strings.keys():
            string_info = self.strings[unique_key]
            default_value = string_info.get(self.default_language, None)
            if default_value is not None and filter(default_value):
                del self.strings[unique_key]
    
    def add_string(self, table, key, value, comment='', in_use=True, language=None):
        if language is None:
            language = self.default_language
        self.add_string_info({'table': table,
                              'key': key,
                              language: value,
                              'comment': comment,
                              'in_use': in_use})

    def add_string_info(self, new_string_info):
        unique_key = (new_string_info['table'], new_string_info['key'])
        if unique_key in self.strings:
            string_info = self.strings[unique_key]
            # check to see if we need to make an old string entry
            # we do this if the base language string has changed
            new_value = new_string_info.get(self.default_language, None)
            old_value = string_info.get(self.default_language, None)
            if old_value and new_value and _uniform_newlines(new_value) != _uniform_newlines(old_value):
                # we have a conflict; store old string elsewhere and use new info
                string_info['key'] = string_info['key'] + '_OLD'
                string_info['in_use'] = False
                self.add_string_info(string_info)
                self.strings[unique_key] = new_string_info
            else:
                # copy over everything, since nothing conflicts.
                # merge in_use
                for key in new_string_info:
                    if key == 'in_use':
                        string_info[key] = string_info['in_use'] or new_string_info['in_use']
                    else:
                        string_info[key] = new_string_info[key]
        else:
            self.strings[unique_key] = new_string_info
        
    
    
    def read_strings_file(self, path, table, language, mark_in_use):
        try:
            contents = unicode(open(path, 'r').read(), encoding='utf-16')
        except:
            contents = unicode(open(path, 'r').read(), encoding='utf-8')
    
        last_comment = None

        while contents:
            comment_match = re.match(comment_regex, contents)
            string_match = re.match(string_regex, contents)    

            if comment_match:
                last_comment = comment_match.group(1)
                contents = contents[comment_match.end():]
            elif string_match:
                key = string_match.group(1)
                value = string_match.group(2)
                contents = contents[string_match.end():]
                
                string_info = {'table': table, 'key': key, 'comment':last_comment}
                if language:
                    string_info[language] = value
                string_info['in_use'] = mark_in_use
                self.add_string_info(string_info)
                
                last_comment = None
            elif re.match(end_regex, contents):
                break
            else:
                raise Exception("Couldn't parse file at %r", contents)
        if language not in self.languages:
            self.languages.append(language)
    
    def strings_in_table(self, target_table):
        all_keys = self.strings.keys()
        for string_table, key in all_keys:
            string_info = self.strings[(string_table, key)]
            if target_table == string_table:
                yield string_info
    
    def tables(self):
        tables = set([table for (table, key) in self.strings.keys()])
        return list(tables)

    def write_strings_file(self, path, target_table, language, in_use_only, mark_missing=True):
        
        out = open(path, 'w')
        out.write(codecs.BOM_UTF16_LE)
        out.close()
        
        out = codecs.open(path, 'a', encoding='utf-16-le')
        all_keys = self.strings.keys()
        all_keys.sort()
        for string_table, key in all_keys:
            string_info = self.strings[(string_table, key)]
            if target_table == string_table:
                if string_info['in_use'] or not in_use_only:
                    value = None
                    if language in string_info:
                        value = string_info[language]
                    if not value:
                        value = string_info[self.default_language] 
                        if mark_missing:
                            value = "***" + value + "***"
                    out.write(unicode("/*%s*/\n\"%s\" = \"%s\";\n\n" % (string_info['comment'], key, value)))
        out.close()
    
    
    def read_tsv_file(self, filename):
        in_file = UnicodeReader(open(filename, 'r'), dialect=csv.excel_tab, encoding='utf-16')
        rows = [row for row in in_file]

        columns = rows[0]
        for col in columns:
            if col not in non_language_keys and col not in self.languages:
                self.languages.append(col)

        rows = rows[1:]
        for row in rows:
            string_info = {}
            i = 0
            for column in columns:
                string_info[column] = row[i]
                i = i + 1
            
            # convert to bool
            if 'in_use' in string_info:
                string_info['in_use'] = (string_info['in_use'].lower() == 'true') # Excel saves them as TRUE and FALSE
            else:
                string_info['in_use'] = False
                 
            self.add_string_info(string_info)
             
    def write_tsv_file(self, filename, omit_translated=False):
        out_file = codecs.open(filename, 'wb')
        out = UnicodeWriter(out_file, dialect=csv.excel_tab, encoding='utf-16')
        columns = self.languages + non_language_keys
        out.writerow(columns)
        
        all_keys = self.strings.keys()
        all_keys.sort()
        for table, key in all_keys:
            string = self.strings[(table, key)]
            
            translated = True
            for language in self.languages:
                if language not in string or string[language] == '':
                    translated = False
            
            if translated and omit_translated:
                continue
            
            row = []
            for column in columns:
                if column in string:
                    value = string[column]
                    if value is True: # Excel saves them as TRUE and FALSE; mimic this to avoid merge issues
                        value = "TRUE"
                    elif value is False:
                        value = "FALSE"
                    elif type(value) == str or type(value) == unicode:
                        value = _uniform_newlines(value)
                    row.append(unicode(value))
                else:
                    row.append('')
            out.writerow(row)
            
    def fill_blanks_with_identical_strings(self):
        source_to_languages = {}
        # populate source_to_languages with original_string -> {language: translation, language2: translation} 
        for string_info in self.strings.values():
            source = string_info.get(self.default_language, None)
            translations = {}
            if source in source_to_languages:
                translations = source_to_languages[source]
            for language in self.languages:
                if language in string_info:
                    translated = string_info[language]
                    if not translated == '':
                        translations[language] = translated
            source_to_languages[source] = translations
        
        for string_info in self.strings.values():
            source = string_info.get(self.default_language, None)
            translations = source_to_languages[source]
            for language in self.languages:
                current = None
                if language in string_info:
                    current = string_info[language]
                if not current or current == '':
                    if language in translations and (not translations[language] == ''):
                        string_info[language] = translations[language]
                
        
    def mark_all_unused(self):
        for string_info in self.strings.values():
            string_info['in_use'] = False
 
