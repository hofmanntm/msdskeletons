Pod::Spec.new do |s|
  s.name         = "MSDBuildTools"
  s.version      = "2.25"
  s.summary      = "Shared build tools between MindSea projects for iOS."
  s.homepage     = "https://bitbucket.org/mindsea/msdbuildtools"
  s.license      = { :type => 'COMMERCIAL', :text => 'COMMERCIAL' } 
  s.author       = { "Mike Burke" => "mike@mindsea.com" }
  s.source       = { :git => "git@bitbucket.org:mindsea/msdbuildtools.git", :tag => "#{s.version}" }

  s.preserve_paths = 'bin', 'localization', 'msdbuild'
end
