MSDSkeletons
============

They're coming out of the closet!
This project contains a bunch of examples of how to do stuff we've accomplished before, but generally are customized just enough to make it a bit of pain to add it to MSDTouchLibraries.
The samples are meant to be copied as they are and modified in their new location.

### Sample project
The sample project's root is 1 massive list view where each item takes you to another skeleton.

### Adding new skeletons

Ideally, each skeleton sits in it's own appropriately named folder. The folder should have a storyboard for easy navigation, which shows the item in use,
